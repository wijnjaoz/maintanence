rem @echo OFF
cls
set TC_PWF_infodba=-u=infodba -p=infodba -g=dba
TITLE Import Site Preferences

call :setup_kit_dir
call :setup_customization_override

:processing
	echo.
	echo ****************************
	set INPUT_DIR=%KIT_DIR%\Site_Preferences
	if not exist %INPUT_DIR% (
		echo Files not found ...
		goto:eof
	)

	for /F "tokens=* eol=;" %%A in ( %KIT_DIR%\_SitePreferenceList.TXT) do  (
        echo Importing '%%A' Preferences...
        preferences_manager %TC_PWF_infodba% -mode=import -scope=SITE -action=OVERRIDE -file="%INPUT_DIR%\%%A.xml"
		echo.
		echo.		
	)

	echo.
	echo ****************************
	echo.
goto:eof


:: --- Sub-routines used ------------------------------------------------------
:setup_kit_dir
	:: Storing current directory in variable KIT_DIR
	set KIT_DIR=%~dp0
	for %%g in ("%KIT_DIR%") do set KIT_DIR=%%~fsg
	cd /d %KIT_DIR%

	echo.
	echo Running from current directory:
	echo.
	echo   %KIT_DIR%

goto:eof

:setup_customization_override
	:: For Site Prefs NO Override ...
	set TC_PREFERENCES_OVERLAY_FILE=	
	set TC_PREFERENCES_OVERLAY_FILE=
goto:eof
