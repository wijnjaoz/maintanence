rem @echo OFF
cls
set TC_PWF_infodba=-u=infodba -p=infodba -g=dba
TITLE Export Site Preferences

call :setup_kit_dir
call :setup_customization_override

:processing
	set OUTPUT_DIR=%KIT_DIR%Site_Preferences
	if not exist %OUTPUT_DIR% md %OUTPUT_DIR%
	
	echo.
	echo ****************************
	for /F "tokens=* eol=;" %%A in ( %KIT_DIR%\_SitePreferenceList.TXT) do  (
		echo Exporting '%%A' Preferences...
        preferences_manager %TC_PWF_infodba% -mode=export -scope=SITE -out_file="%OUTPUT_DIR%\%%A.xml" -file="%%A.txt"
		echo.
		echo.
	)

	echo ****************************
	echo.
goto:eof


:: --- Sub-routines used ------------------------------------------------------
:setup_kit_dir
	:: Storing current directory in variable KIT_DIR
	set KIT_DIR=%~dp0
	for %%g in ("%KIT_DIR%") do set KIT_DIR=%%~fsg
	cd /d %KIT_DIR%

	echo.
	echo Running from current directory:
	echo.
	echo   %KIT_DIR%

goto:eof

:setup_customization_override
	:: For Site Prefs NO Override ...
	set TC_PREFERENCES_OVERLAY_FILE=	
	set TC_PREFERENCES_OVERLAY_FILE=
goto:eof
