@echo off

rem ********************************************************************
REM Replace CHANGE_ME with correct values as shown in Example
REM Set TC_ROOT to directory where TeamCenter is installed.
REM Set TC_DATA to directory where TeamCenter tcdata is installed.
REM Example set TC_ROOT=E:\PLM\Teamcenter10
REM Example set TC_DATA=F:\PLM\plmshare\tcdev_data
rem ********************************************************************
set TC_ROOT=CHANGE_ME
set TC_DATA=CHANGE_ME

rem ********************************************************************
rem set user - password - group
rem ********************************************************************
SET TRANSLATE_DIR=%~dp0
call "%TRANSLATE_DIR%\setuserpwd.bat"

@set PARAM1=%1

if x%PARAM1%== x goto help
if %1== -help goto help

call %TC_DATA%\tc_profilevars.bat

set INPUTFILE=%~1
set OUTPUTDIR=%~2
set ITEMID=%~3
set REVID=%~4

"%TC_ROOT%\bin\plmxml_export.exe" -u=%TC_USER% -p=%TC_PWD% -g=%TC_GRP% -xml_file=%OUTPUTDIR%\%ITEMID%.xml -transfermode=ConfiguredDataFilesExportDefault -item=%ITEMID% -rev=%REVID%

set EXITVALUE=%ERRORLEVEL%

rem copy TPD files
copy %OUTPUTDIR%\%ITEMID%\*.zip %OUTPUTDIR%
copy %OUTPUTDIR%\%ITEMID%\*.stp %OUTPUTDIR%
copy %OUTPUTDIR%\%ITEMID%\*.pdf %OUTPUTDIR%
rem for testing purpose
copy %OUTPUTDIR%\%ITEMID%\*.docx %OUTPUTDIR%
copy %OUTPUTDIR%\%ITEMID%\*.prt  %OUTPUTDIR%

rem Deleting plmxml_export directory
rem rmdir /s /q "%OUTPUTDIR%\%ITEMID%"

endlocal
@echo on

EXIT %EXITVALUE%

:help
echo     Example
echo     d:\bnlexport.bat D:\PLM\DP101\Stage\DC\Ua800e11ac5673d400160d\ D:\PLM\DP101\Stage\DC\Ua800e11ac5673d400160d\result 000530 A 
echo     see translator BnlReport for arguments details
endlocal
EXIT %EXITVALUE%
