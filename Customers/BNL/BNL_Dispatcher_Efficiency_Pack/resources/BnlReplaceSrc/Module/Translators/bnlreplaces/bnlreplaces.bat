rem @echo off
echo %*
rem Copyright by Siemens PLM Software 2013
rem
rem Start script for the report example
rem
rem J. Mansvelders   Created                       05-06-2014
rem
rem
rem ********************************************************************

setlocal

rem ********************************************************************
rem INPUT PARAMETERS
rem ********************************************************************
set OUTPUTDIR=%~1
set ITEMID=%~2
set REVID=%~3
set TRANSFERMODE=%~4

rem ********************************************************************
rem TRANSLATOR DIRECTORY
rem ********************************************************************
SET TRANSLATE_DIR=%~dp0

rem ********************************************************************
rem Perform report, nut now just a pdf file is copied to output directory
rem ********************************************************************
copy %TRANSLATE_DIR%\example.pdf %OUTPUTDIR%\Document.docx

set EXITVALUE=%ERRORLEVEL%

endlocal

@echo on

EXIT %EXITVALUE%