@echo off
rem Copyright by Siemens PLM Software 2014
rem
rem Start script for the MSO translation
rem
rem J. Mansvelders   Created                       01-04-2014
rem J. Mansvelders   Added syncfile                30-03-2016
rem
rem
rem ********************************************************************

setlocal

rem ********************************************************************
rem INPUT PARAMETERS
rem ********************************************************************
set INPUTFILE=%~1
set OUTPUTDIR=%~2
set PRINTFILE=%~3
set SYNCFILE=%~4

rem ********************************************************************
rem TRANSLATOR DIRECTORY
rem ********************************************************************
SET TRANSLATE_DIR=%~dp0

rem ********************************************************************
rem CHECK FILE EXTENSION
rem ********************************************************************
SET FILE_EXT=%~x1
IF "%FILE_EXT%" == ".doc"  GOTO :SETCOMMAND
IF "%FILE_EXT%" == ".docx" GOTO :SETCOMMAND
IF "%FILE_EXT%" == ".docm" GOTO :SETCOMMAND

IF "%FILE_EXT%" == ".xls"  GOTO :SETCOMMAND
IF "%FILE_EXT%" == ".xlsx" GOTO :SETCOMMAND
IF "%FILE_EXT%" == ".xlsm" GOTO :SETCOMMAND

IF "%FILE_EXT%" == ".ppt"  GOTO :SETCOMMAND
IF "%FILE_EXT%" == ".pptx" GOTO :SETCOMMAND
IF "%FILE_EXT%" == ".pptm" GOTO :SETCOMMAND

echo %INPUTFILE% is not valid.
set EXITVALUE=1
goto :end


rem ********************************************************************
rem RUN mso PRINT
rem ********************************************************************
rem                    BNL_ms_print (C) 2013 Siemens PLM Software
rem
rem                    Usage:
rem                    -logfile=<logfile location and name>
rem                    -filename=<name and location of document to print>
rem                    General optional:
rem                    -queue=<printer queue name> (if omitted default printer is used)
rem                    -printfile=<location and name of file to output> (if omitted paper out is assumed)
rem                    -pdf|-nopdf
rem                    -macro|-nomacro
rem                    -page=<page to generate an png file from (1 is first page)>
rem                    -syncfile=<path\sync.xml>
rem                    Word optional:
rem                    -create_bookmarks_using_headings (only applicable when using SAVEASPDF)
rem                    Excel optional:
rem                    -allsheets
rem                    PowerPoint optional:
rem                    -hidden
rem                    -layout=<print layout>

:SETCOMMAND
rem set MSO_COMMAND=%TRANSLATE_DIR%\BNL_MS_print.exe "-logfile=%CD%\office.log" "-filename=%INPUTFILE%" "-queue=SAVEASPDF" "-printfile=%OUTPUTDIR%\%PRINTFILE%" "-pdf" "-nomacro" "-syncfile=%SYNCFILE%" 
set MSO_COMMAND=%TRANSLATE_DIR%\BNL_MS_print.exe "-logfile=%CD%\office.log" "-filename=%INPUTFILE%" "-queue=SAVEASPDF" "-printfile=%OUTPUTDIR%\%PRINTFILE%" "-pdf" "-nomacro" "-syncfile=%SYNCFILE%"
goto :command

:command
echo Running [%MSO_COMMAND%]
%MSO_COMMAND%
rem Remove result pdf file. Otherwise pdf file will be renamed to Offcie extension.
del /F /Q %OUTPUTDIR%\%PRINTFILE%
rem copy updated Office document to reult directory.
copy %INPUTFILE% %OUTPUTDIR%

set EXITVALUE=%ERRORLEVEL%

:end

endlocal

@echo on

EXIT %EXITVALUE%