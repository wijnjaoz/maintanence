rem @echo off
echo %*
rem Copyright by UGS Benelux 2007
rem
rem Script for generating stamped files
rem
rem JWV/UGS   First setup                                          02-mar-2007
rem Hartong/Contiweb    Script geschikt gemaakt voor PDF2Tiff      02-Aug-2011
rem
rem **************************************************************************

setlocal

rem ********************************************************************
rem Setting variables
rem ********************************************************************
SET FN_IN=%1
SET FN_OUT=%2\%~n1.tif
rem ********************************************************************
rem Ghostscript bin directory
rem ********************************************************************
rem Set GS_HOME To the Directory where Ghost is installed
rem Example set GS_HOME=C:\Program Files\gs\gs9.10
set GS_HOME=C:\Program Files\gs\gs9.10

rem ********************************************************************
rem Convert PDF file to Tiff using GhostScript
rem ********************************************************************
rem C:\Soft\SPLM\gs\bin\gswin32c -sDEVICE=tiffg4 -dBATCH -r150 -dSAFER -dNOPAUSE %2 -sOutputFile=%FN_OUT% %FN_IN%
"%GS_HOME%\bin\gswin64c.exe" -sPAPERSIZE=a3 -sDEVICE=tiffg4 -dBATCH -r200 -dSAFER -dNOPAUSE -sOutputFile=%FN_OUT% %FN_IN%
endlocal
