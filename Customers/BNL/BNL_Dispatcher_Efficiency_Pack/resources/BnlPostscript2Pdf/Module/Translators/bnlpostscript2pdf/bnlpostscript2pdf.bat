rem @echo off
rem Copyright by Siemens PLM Software 2013-2016
rem
rem Start script for the BnlSe2Pdf
rem
rem D. Tjon          Created              	   16-01-2016
rem
rem ********************************************************************

setlocal

rem ********************************************************************
rem Ghostscript bin directory
rem ********************************************************************
rem Replace CHANGE_ME with correct values as shown in Example
rem Set GS_HOME To the Directory where Ghost is installed
rem Example set GS_HOME=C:\Program Files\gs\gs9.10
set GS_HOME=C:\Program Files\gs\gs9.10
cd /D "%GS_HOME%\bin"

rem ********************************************************************
rem INPUT PARAMETERS
rem ********************************************************************
set INPUTFILE=%~1
set OUTPUTDIR=%~2\%~n1.pdf

:CreatePDF
set PDF_COMMAND="%GS_HOME%\lib\ps2pdf.bat" %INPUTFILE% %OUTPUTDIR%

echo Running [%PDF_COMMAND%]
%PDF_COMMAND%
echo.

goto :EOF

:End
endlocal

@echo on
EXIT %EXITVALUE%
