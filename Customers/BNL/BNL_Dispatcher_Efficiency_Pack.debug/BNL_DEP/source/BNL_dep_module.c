/**
 (C) copyright by Siemens PLM Software

 Developed for IMAN
 Versions     OS                          platform
 -----------------------------------------------------------------------------------------------------------
 5.x          Microsoft Windows NT        Intel


 Description    :   DEP module

 Created for    :   Siemens PLM Software

 Comments       :   Every new procedure or function starts with BNL_

 Filename       :   $Id: BNL_AH_dataset_copy.c 20 2016-04-15 13:14:28Z tjonkonj $

 Version info   :   The number consist of the iman release and then the release of the handler
                    <handlerrelease>.<pointrelease>


 History of changes
 reason /description                                      Vers  By      date
 ----------------------------------------------------------------------------------------------------------
 Creation                                                 0.1   JM      06-09-2011
 Updated for TC10.1                                       0.2   JM      29-08-2013
 Added BNL_dep_check_for_request                          0.3   JM      24-09-2013
 Also remove DispatcherRequest from reference if TERMINAL 0.4   DTJ     20-may-2016
*/

#include <stdlib.h>

#ifdef  TC10_1
#include <tc/iman.h>
#include <tccore/item.h>
#include <bom/bom.h>
#include <tccore/aom_prop.h>
#else
#include <iman.h>
#include <item.h>
#include <bom.h>
#include <ps.h>
#include <lov.h>
#include <aom_prop.h>
#include <form.h>
#include <user_server_exits.h>
#include <grmtype.h>
#endif

#include <BNL_register_bnllibs.h>
#include <BNL_tb_module.h>
#include <BNL_em_module.h>
#include <BNL_fm_module.h>
#include <BNL_xml_module.h>
#include <BNL_strings_module.h>
#include <BNL_errors.h>
#include <BNL_csp_module.h>

#include <BNL_dep_module.h>

int BNL_dep_perform_signoff(char *pszLevel, char *pszPuid, CR_signoff_decision_t descision, char *pszComment);
int BNL_dep_get_task_level(tag_t tRequest, char **pszTaskPuid, char **pszLevel);
int BNL_dep_signoff_check(tag_t tRootTask, tag_t tRequestObject, char *pszTaskPuid, char *pszLevel, tag_t *ptRequest, logical *plRequestFound);

static char BNL_module[] = "BNL DEP MOD 0.4";

FILE *bnllog;

#define BNL_DEP_STATE           "currentState"
#define BNL_REMOVE_DP_REFS_PREF "BNL_REMOVE_DP_REFS" // pref to remove the DispatcherRequest as reference of the workflow

int BNL_dep_initialise(FILE *pLog)
{
  int iRetCode        = 0;

  bnllog = pLog;

  return iRetCode;
}


int BNL_dep_exit_module()
{
  int iRetCode        = 0;

  return iRetCode;
}


/**
int BNL_dep_save_request_pm
(
  METHOD_message_t *pmMessage,
  va_list args
)

Description:
  Post action on the save of DispactherRequest

Returns:
  ITK_ok or !ITK_ok

History of changes
Date        Author           Description
----------- ---------------- ----------------------------------------------------------------------------------------
23-may-2016 D. Tjon Kon Joe  add functionality for preference BNL_REMOVE_DP_REFS_PREF
*/
int BNL_dep_save_request_pm
(
  METHOD_message_t *pmMessage,
  va_list args
)
{
  int iRetCode      = 0;
  int i             = 0;

  tag_t tProp      = NULL_TAG;
  tag_t tObject    = NULL_TAG;
  tag_t tRequest   = NULL_TAG;
  tag_t tRootTask  = NULL_TAG;

  char *pszState    = NULL;
  char *pszLevel    = NULL;
  char *pszTaskPuid = NULL;
  char *pszRemoveDpRef   = NULL;

  logical lRequestFound = false;

  EPM_state_t state;


  tProp = va_arg(args, tag_t);
  pszState = va_arg(args, char*);

  if (strcmp(pszState, "COMPLETE") != 0 && strcmp(pszState, "TERMINAL") != 0)
  {
    // Nothing to do
    goto function_exit;
  }

  iRetCode = PROP_ask_owning_object(tProp, &tObject);
  if (BNL_em_error_handler("PROP_ask_owning_object", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

  if ((iRetCode = BNL_dep_get_task_level(tObject, &pszTaskPuid, &pszLevel)) != 0) goto function_exit;

  if (pszTaskPuid != NULL && pszLevel != NULL)
  {
    iRetCode = POM_string_to_tag(pszTaskPuid, &tRootTask);
    if (BNL_em_error_handler("POM_string_to_tag", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

    if ((iRetCode = BNL_dep_signoff_check(tRootTask, tObject, pszTaskPuid, pszLevel, &tRequest, &lRequestFound)) != 0) goto function_exit;

    if (strcmp(pszState, "COMPLETE") == 0)
    {
      fprintf(bnllog, "%s: State has set to COMPLETE, checking for signoff.\n", BNL_module);

      if (tRequest != NULL_TAG)
      {
        iRetCode = EPM_ask_state(tRootTask, &state);
        if (BNL_em_error_handler("EPM_ask_state", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

        if (state != EPM_completed)
        {
          fprintf(bnllog, "%s: Removing current Dispatcher Request as reference attachment.\n", BNL_module);

          iRetCode = EPM_remove_attachments(tRootTask, 1, &tRequest);
          if (BNL_em_error_handler("EPM_remove_attachments", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
        }
        else
        {
          fprintf(bnllog, "%s: Skipping the removal of current Dispatcher Request as reference attachment, since task has already completed.\n", BNL_module);
        }
      }
      if (tRequest != NULL_TAG && !lRequestFound)
      {
        if ((iRetCode = BNL_dep_perform_signoff(pszLevel, pszTaskPuid, CR_approve_decision, "Dispatcher Request(s) finished successfully")) != 0) goto function_exit;
      }
    }
    else if (strcmp(pszState, "TERMINAL") == 0)
    {
      fprintf(bnllog, "%s: State has set to TERMINAL.\n", BNL_module);

      if ((iRetCode = BNL_dep_perform_signoff(pszLevel, pszTaskPuid, CR_reject_decision, "Dispatcher Request(s) finished with error(s)")) != 0) goto function_exit;

      if (tRequest != NULL_TAG)
      {
        BNL_tb_pref_ask_char_value(BNL_REMOVE_DP_REFS_PREF, IMAN_preference_site, &pszRemoveDpRef);
        if (pszRemoveDpRef != NULL && _stricmp(pszRemoveDpRef, "true") == 0)
        {
          fprintf(bnllog, "%s: [%s]=%s\n", BNL_module, BNL_REMOVE_DP_REFS_PREF, pszRemoveDpRef);

          iRetCode = EPM_ask_state(tRootTask, &state);
          if (BNL_em_error_handler("EPM_ask_state", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

          if (state != EPM_completed)
          {
            fprintf(bnllog, "%s: Removing terminal Dispatcher Request as reference attachment.\n", BNL_module);

            iRetCode = EPM_remove_attachments(tRootTask, 1, &tRequest);
            if (BNL_em_error_handler("EPM_remove_attachments", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
          }
          else
          {
            fprintf(bnllog, "%s: Skipping the removal of the terminal Dispatcher Request as reference attachment, since task has already completed.\n", BNL_module);
          }
        }
      }
    }
    fprintf(bnllog, "\n");
  }

function_exit:

  if (pszLevel != NULL) free(pszLevel);
  if (pszTaskPuid != NULL) free(pszTaskPuid);
  if (pszRemoveDpRef != NULL) MEM_free(pszRemoveDpRef);

  return iRetCode;
}


int BNL_dep_perform_signoff(char *pszLevel, char *pszPuid, CR_signoff_decision_t descision, char *pszComment)
{
  int iRetCode          = 0;
  int iReviewers        = 0;
  int iTasks            = 0;
  int iSignoffTasks     = 0;

  tag_t tTask           = NULL_TAG;
  //tag_t tParentTask     = NULL_TAG;
  tag_t tJob            = NULL_TAG;

  tag_t *ptTasks        = NULL;
  tag_t *ptSignoffTasks = NULL;
  tag_t *ptReviewers    = NULL;

  EPM_state_t state;

  BNL_em_set_log(bnllog);

  if (pszLevel != NULL && strlen(pszLevel) > 0)
  {
    fprintf(bnllog, "%s: Performing signoff on task [%s] with comments [%s].\n", BNL_module, pszLevel, pszComment);

    // Try to find the task
    iRetCode = POM_string_to_tag(pszPuid, &tTask);
    if (BNL_em_error_handler("POM_string_to_tag", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

    iRetCode = AOM_refresh(tTask, false);
    if (BNL_em_error_handler("AOM_refresh (task)", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

    iRetCode = EPM_ask_job(tTask, &tJob);
    if (BNL_em_error_handler("EPM_ask_job", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

    iRetCode = CR_ask_reviewers(tJob, pszLevel, &iReviewers, &ptReviewers);
    if (BNL_em_error_handler("CR_ask_reviewers", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

    if (iReviewers != 1)
    {
      fprintf(bnllog, "%s: Warning, found [%d] reviewers. Only one allowed.\n", BNL_module, iReviewers);
      goto function_exit;
    }

    if ((iRetCode = BNL_tb_retrieve_all_subtasks(tTask, "EPMReviewTask", pszLevel, &iTasks, &ptTasks)) != 0) goto function_exit;

    if (iTasks != 1)
    {
      fprintf(bnllog, "%s: Warning, found [%d] review tasks with name [%s]. Only one allowed.\n", BNL_module, iTasks, pszLevel);
      goto function_exit;
    }

    if ((iRetCode = BNL_tb_retrieve_all_subtasks(ptTasks[0], "EPMPerformSignoffTask", NULL, &iSignoffTasks, &ptSignoffTasks)) != 0) goto function_exit;

    if (iSignoffTasks != 1)
    {
      fprintf(bnllog, "%s: Warning, found [%d] subtasks of type [EPMPerformSignoffTask]. Only one allowed.\n", BNL_module, iSignoffTasks);
      goto function_exit;
    }

    iRetCode = AOM_refresh(ptSignoffTasks[0], true);
    if (BNL_em_error_handler("AOM_refresh (1)", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

    // Check state
    iRetCode = EPM_ask_state(ptSignoffTasks[0], &state);
    if (BNL_em_error_handler("EPM_ask_state", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

    if (state != EPM_started)
    {
      fprintf(bnllog, "%s: Warning, task state is not \'Started\'. Performing of signoff will be skipped.\n", BNL_module);
    }
    else
    {
      iRetCode = EPM_set_decision(ptSignoffTasks[0], ptReviewers[0], descision, pszComment, true);
      if (BNL_em_error_handler("EPM_set_decision", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

      iRetCode = AOM_save(ptSignoffTasks[0]);
      if (BNL_em_error_handler("AOM_save", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;
    }

    iRetCode = AOM_refresh(ptSignoffTasks[0], false);
    if (BNL_em_error_handler("AOM_refresh (2)", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;
  } // End of if (pszLevel != NULL)
  else
  {
    fprintf(bnllog, "%d: No need to perform signoff.\n", BNL_module);
  }

function_exit:

  if (iReviewers > 0) MEM_free(ptReviewers);
  if (iTasks > 0) MEM_free(ptTasks);
  if (iSignoffTasks > 0) MEM_free(ptSignoffTasks);

  return iRetCode;
}


int BNL_dep_get_task_level(tag_t tRequest, char **pszTaskPuid, char **pszLevel)
{
  int iRetCode      = 0;
  int iKeys         = 0;
  int iValues       = 0;
  int i             = 0;

  char **pszKeys    = NULL;
  char **pszValues  = NULL;


  iRetCode = AOM_ask_value_strings(tRequest, "argumentData", &iValues, &pszValues);
  if (BNL_em_error_handler("AOM_ask_value_strings (data)", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  iRetCode = AOM_ask_value_strings(tRequest, "argumentKeys", &iKeys, &pszKeys);
  if (BNL_em_error_handler("AOM_ask_value_strings (keys)", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  for (i = 0; i < iKeys; i++)
  {
    if (strcmp(pszKeys[i], "task") == 0)
    {
      BNL_tb_copy_string(pszValues[i], pszTaskPuid);
    }
    else if (strcmp(pszKeys[i], "level") == 0)
    {
      BNL_tb_copy_string(pszValues[i], pszLevel);
    }
  }

function_exit:

  if (iKeys > 0) MEM_free(pszKeys);
  if (iValues > 0) MEM_free(pszValues);

  return iRetCode;
}


// Check if DispatcherRequest is attached to the job
int BNL_dep_signoff_check(tag_t tRootTask, tag_t tRequestObject, char *pszTaskPuid, char *pszLevel, tag_t *ptRequest, logical *plRequestFound)
{
  int iRetCode                        = 0;
  int iReferenceCount                 = 0;
  int i                               = 0;

  tag_t *ptReferences                 = NULL;

  tag_t tType                         = NULL_TAG;

  char szType[TCTYPE_name_size_c + 1] = "";

  char *pszRequestTask                = NULL;
  char *pszRequestLevel               = NULL;


  *ptRequest = NULL_TAG;
  *plRequestFound = false;


  iRetCode = AOM_refresh(tRootTask, false);
  if (BNL_em_error_handler("AOM_refresh (roottask 1)", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

  iRetCode = EPM_ask_attachments(tRootTask, EPM_reference_attachment, &iReferenceCount, &ptReferences);
  if (BNL_em_error_handler("EPM_ask_attachments", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  for (i = 0; i < iReferenceCount; i++)
  {
    iRetCode = TCTYPE_ask_object_type(ptReferences[i], &tType);
    if (BNL_em_error_handler("TCTYPE_ask_object_type", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    iRetCode = TCTYPE_ask_name(tType, szType);
    if (BNL_em_error_handler("TCTYPE_ask_name", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    if (strcmp(szType, "DispatcherRequest") == 0)
    {
      if (ptReferences[i] == tRequestObject)
      {
        fprintf(bnllog, "%s: Dispatcher Request has been attached as reference attachment.\n", BNL_module);
        *ptRequest = tRequestObject;
      } // End of if (ptReferences[i] == tRequestObject)
      else
      {
        // Check if DispatcherRequest points to same signoff task
        if ((iRetCode = BNL_dep_get_task_level(ptReferences[i], &pszRequestTask, &pszRequestLevel)) != 0) goto function_exit;

        //fprintf(bnllog, "%s: Task [%s] level [%s].\n", BNL_module, pszRequestTask, pszRequestLevel);

        if (pszRequestTask != NULL && strcmp(pszRequestTask, pszTaskPuid) == 0 && pszRequestLevel != NULL && strcmp(pszRequestLevel, pszLevel) == 0)
        {
          fprintf(bnllog, "%s: Another Dispatcher Request has been attached as reference attachment. Signoff will be skipped.\n", BNL_module);
          *plRequestFound = true;
        }

        if (pszRequestTask != NULL) free(pszRequestTask);
        pszRequestTask = NULL;
        if (pszRequestLevel != NULL) free(pszRequestLevel);
        pszRequestLevel = NULL;
      } // End of else if (ptReferences[i] == tRequestObject)
    } // End of if (strcmp(szType, "DispatcherRequest") == 0)
    if ((*ptRequest) != NULL_TAG && (*plRequestFound) == true) break;
  } // End of for (i = 0; i < iReferenceCount; i++)


function_exit:

  if (iReferenceCount > 0) MEM_free(ptReferences);
  if (pszRequestTask != NULL) free(pszRequestTask);
  if (pszRequestLevel != NULL) free(pszRequestLevel);

  return iRetCode;
}


 /* Check if request is already available (in case of a DEMOTE)
    Request is duplicate when:
      Request is available as reference attachment
      State = TERMINAL
      Arguments task and level are indentical
      Provider & Service are identical
      Primary Objects and Secondary Objects are indentical
*/
int BNL_dep_check_for_request
(
  tag_t tRootTask,
  char *pszProviderName,
  char *pszServiceName,
  char *pszTaskPuid,
  char *pszLevel,
  tag_t tPrimary,
  tag_t tSecondary,
  tag_t *ptRequest
)
{
  int iRetCode                        = 0;
  int iRefCount                       = 0;
  int i                               = 0;
  int iPrim                           = 0;
  int iSec                            = 0;

  tag_t tType                         = NULL_TAG;

  tag_t *pRefsObjs                    = NULL;
  tag_t *ptPrimObjs                   = NULL;
  tag_t *ptSecObjs                    = NULL;

  char szType[TCTYPE_name_size_c + 1] = "";

  char *pszRequestTask                = NULL;
  char *pszRequestLevel               = NULL;
  char *pszState                      = NULL;
  char *pszProvider                   = NULL;
  char *pszService                    = NULL;


  iRetCode = EPM_ask_attachments(tRootTask, EPM_reference_attachment, &iRefCount, &pRefsObjs);
  if (BNL_em_error_handler("EPM_ask_attachments", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  for (i = 0; i < iRefCount; i++)
  {
    iRetCode = AOM_refresh(pRefsObjs[i], false);
    if (BNL_em_error_handler("AOM_refresh)", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

    iRetCode = TCTYPE_ask_object_type(pRefsObjs[i], &tType);
    if (BNL_em_error_handler("TCTYPE_ask_object_type", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    iRetCode = TCTYPE_ask_name(tType, szType);
    if (BNL_em_error_handler("TCTYPE_ask_name", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    if (strcmp(szType, "DispatcherRequest") == 0)
    {
      // Check the state, provider and service
      iRetCode = AOM_ask_value_string(pRefsObjs[i], "currentState", &pszState);
      if (BNL_em_error_handler("AOM_ask_value_string (state)", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

      iRetCode = AOM_ask_value_string(pRefsObjs[i], "providerName", &pszProvider);
      if (BNL_em_error_handler("AOM_ask_value_string (provider)", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

      iRetCode = AOM_ask_value_string(pRefsObjs[i], "serviceName", &pszService);
      if (BNL_em_error_handler("AOM_ask_value_string (service)", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

      fprintf(bnllog, "%s: Found DispatcherRequest: state [%s] provider [%s] service [%s].\n", BNL_module, pszState, pszProvider, pszService);

      if (strcmp(pszState, "TERMINAL") == 0
        && strcmp(pszProvider, pszProviderName) == 0
        && strcmp(pszService, pszServiceName) == 0)
      {
        // Check the level and the task
        if ((iRetCode = BNL_dep_get_task_level(pRefsObjs[i], &pszRequestTask, &pszRequestLevel)) != 0) goto function_exit;

        fprintf(bnllog, "%s: Task [%s] Level [%s].\n", BNL_module, pszRequestTask, pszRequestLevel);

        if (pszRequestTask != NULL && strcmp(pszRequestTask, pszTaskPuid) == 0
          && pszRequestLevel != NULL && strcmp(pszRequestLevel, pszLevel) == 0)
        {
          // Check the primary and secondary object (at the most one primary and one secondary object)
          iRetCode = AOM_ask_value_tags(pRefsObjs[i], "primaryObjects", &iPrim, &ptPrimObjs);
          if (BNL_em_error_handler("AOM_ask_value_tags (primary)", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

          iRetCode = AOM_ask_value_tags(pRefsObjs[i], "secondaryObjects", &iSec, &ptSecObjs);
          if (BNL_em_error_handler("AOM_ask_value_tags (secondary)", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

          if ((iPrim == 1 && tPrimary == ptPrimObjs[0])
            && ((iSec == 0 && tSecondary == NULL_TAG) || (iSec == 1 && tSecondary == ptSecObjs[0])))
          {
            fprintf(bnllog, "%s: Found indentical DispatcherRequest with state [%s].\n", BNL_module, pszState);
            // We have a match
            *ptRequest = pRefsObjs[i];
            break;
          } // End of checking primary and secondary objects
        } // End of checking of task and level
      } // End of checking state provider and translator

      if (pszState) MEM_free(pszState);
      pszState = NULL;
      if (pszProvider) MEM_free(pszProvider);
      pszProvider = NULL;
      if (pszService) MEM_free(pszService);
      pszService = NULL;
      if (pszRequestTask) free(pszRequestTask);
      pszRequestTask = NULL;
      if (pszRequestLevel) free(pszRequestLevel);
      pszRequestLevel = NULL;
    }
  } // End of for (i = 0; i < iRefCount; i++)

function_exit:

  if (iRefCount > 0) MEM_free(pRefsObjs);
  if (pszState) MEM_free(pszState);
  if (pszProvider) MEM_free(pszProvider);
  if (pszService) MEM_free(pszService);
  if (pszRequestTask) free(pszRequestTask);
  if (pszRequestLevel) free(pszRequestLevel);

  return iRetCode;
}
