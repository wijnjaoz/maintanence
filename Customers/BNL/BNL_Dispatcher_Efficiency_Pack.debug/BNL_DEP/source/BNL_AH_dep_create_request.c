/**
 (C) copyright by Siemens PLM Software

 Developed for IMAN
 Versions     OS                          platform
 -----------------------------------------------------------------------------------------------------------
 5.x          Microsoft Windows NT        Intel


 Description    : This action handler creates a Dispatcher request.

 Created for    : Siemens PLM Sofwtare

 Comments       : Every new procedure or function starts with BNL_
                  This Actionhandler uses BNL_tools.

 Filename       : $Id: bnl_register_sonaca.c 144 2015-04-01 13:33:33Z tjonkonj $

 Version info   ; The number consist of the iman release and then the release of the handler
                  <imanrelease>.<handlerrelease>.<pointrelease>

 History of changes
 Reason / Description                                       Version By      Date
 ----------------------------------------------------------------------------------------------------------
 Creation                                                   0.1     JM      05-09-2011
 Added Extra Arguments functionality                        0.2     JM      24-09-2012
 Fixed issue with getting the class                         0.3     JM      19-10-2012
 Replace env in the configured ini file                     0.4     JM      03-04-2013
 Updated for TC10.1                                         0.5     JM      29-08-2013
 Added BNL_dep_check_for_request                            0.6     JM      24-09-2013
 Added possibillity to retrieve real property value         0.7     JM      15-11-2013
 Added possibillity to process all kinds of workspace
  objects                                                   0.8     JM      10-09-2014
 Updated get_prop fucnction(puid)                           0.9     JM      22-09-2015
 Updated for TC11                                           1.0     DTJ     14-10-2015
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef  TC10_1
#include <tc/iman.h>
#include <sa/tcfile.h>
#include <ae/ae.h>
#include <tccore\imantype.h>
#include <fclasses/tc_date.h>
#include <tccore/aom_prop.h>
#else
#include <iman.h>
#include <imanfile.h>
#include <epm.h>
#include <tc_date.h>
#include <aom_prop.h>
#endif
#include <dispatcher\dispatcher_itk.h>

#include <BNL_tb_module.h>
#include <BNL_fm_module.h>
#include <BNL_em_module.h>
#include <BNL_ini_module.h>
#include <BNL_errors.h>

#include <BNL_dep_module.h>
#include <BNL_AH_dep_create_request.h>

extern FILE *ugslog;
extern int giErrorOffset;

#define DEP_INI_SEC_PROVIDERS         "Providers"
#define DEP_INI_SEC_TRANSLATORS       "Translators"
#define DEP_INI_SEC_EXTRA_ARGS        "Extra Arguments"



/* Global declarations */
int BNL_AH_dep_create_request(EPM_action_message_t msg);
static int process_bomview(t_DEP *psDEPdata, tag_t tBomview);
static int process_namedref(t_DEP *psDEPdata, char *pszNamedRef);
static int process_object(t_DEP *psDEPdata, tag_t tObject);
int BNL_lookup_item_revision(tag_t tDataset, t_DEP *psDEPdata, tag_t *ptRev);
int BNL_create_request(tag_t tPrimary, tag_t tSecondary, char *pszType, t_DEP *psDEPdata);

static char AH_Version[] = "AH-dep-create-request 1.1a";


/*--------------------------------------------------------------------------------------*/

/*start of code*/

/**
int BNL_Register_AH_dep_create_request

    Description:

        Registers the callback action handler

    Returns     :

        ITK_ok

    Parameters  :

        None
*/
int BNL_Register_AH_dep_create_request(void)
{
  int iRetCode         = ITK_ok;

  iRetCode = EPM_register_action_handler("AH-dep-create-request", "", BNL_AH_dep_create_request);
  if (BNL_em_error_handler("EPM_register_action_handler", AH_Version, EMH_severity_user_error, iRetCode , false))
      return iRetCode;
  fprintf(ugslog, "%s\n", AH_Version);

  return iRetCode;
}

/**
int BNL_AH_dep_create_request()

    Description :

        This action handler creates Dispatcher Requests.

    Returns     :

        ITK_ok or !ITK_ok

    Parameters  :

        None
*/
int BNL_AH_dep_create_request(EPM_action_message_t msg)
{
  int iRetCode                     = ITK_ok;

  tag_t tJob                       = NULL_TAG;

  tag_t *ptTargets                 = NULL;

  char szSelectArgument[SA_name_size_c+1];

  char *pszArgument	               = NULL;
  char *pszTemp                    = NULL;
  char *pszIniFile                 = NULL;
  char *pszDateTime                = NULL;

  logical lNoInfo                  = false;
  logical lFlag                    = false;

  t_DEP sDEPdata;

  /* Preset variables */
  sDEPdata.pszIni = NULL;
  sDEPdata.iDStypesCount = 0;
  sDEPdata.pszDStypes = NULL;
  sDEPdata.iNamedRefCount= 0;
  sDEPdata.pszNamedRefs = NULL;
  sDEPdata.pszTranslator = NULL;
  sDEPdata.pszProvider = NULL;
  sDEPdata.iPriority = 0;
  sDEPdata.iProcessOneLine = 0;
  sDEPdata.lRecursive = false;
  sDEPdata.pszListName = NULL;
  sDEPdata.lUseBomView = false;
  sDEPdata.pszBomview = NULL;
  sDEPdata.pszRevisionRule = NULL;
  sDEPdata.iBomLevel = 0;
  sDEPdata.lIgnoreUnconfigBom = false;
  sDEPdata.iExcludeNote = 0;
  sDEPdata.pszNote = NULL;
  sDEPdata.pszNoteType = NULL;
  sDEPdata.tDataset = NULL_TAG;
  sDEPdata.tItem = NULL_TAG;
  sDEPdata.tItemRev = NULL_TAG;
  sDEPdata.tNamedRef = NULL_TAG;
  sDEPdata.lIsOccOfBomView = false;
  sDEPdata.lTranslationRequestCreated = false;
  sDEPdata.iNumTargets = 0;
  sDEPdata.ptTargets = 0;
  sDEPdata.iObjectTypes = 0;
  sDEPdata.pszObjectsTypes = NULL;
  sDEPdata.pszLevel = NULL;
  sDEPdata.pszTaskPuid = NULL;
  sDEPdata.tRootTask = NULL_TAG;
  sDEPdata.iExtraArgsCount = 0;
  sDEPdata.ppsExtraArgs = NULL;

  /*-----------------------------------------------------------------------*/
  BNL_tb_get_date_time_stamp(&pszDateTime, NULL);
  fprintf(ugslog, "\n%s: Starting %s\n", pszDateTime, AH_Version);

  /* Get arguments */
  // inifile
  strcpy(szSelectArgument, "inifile");
  iRetCode = BNL_tb_get_argument_optional
  (
    msg.arguments,
    msg.task,
    szSelectArgument,
    true,
    &sDEPdata.pszIni
  );
  if (BNL_em_error_handler("BNL_tb_get_argument_optional", AH_Version, EMH_severity_error, iRetCode, true)) goto handler_exit;

  BNL_fm_replace_env_for_path(sDEPdata.pszIni, &pszIniFile);
  if (pszIniFile != NULL)
  {
    MEM_free(sDEPdata.pszIni);
    sDEPdata.pszIni = MEM_string_copy(pszIniFile);
    free(pszIniFile);
  };
  fprintf(ugslog, "%s: The configured inifile is [%s].\n", AH_Version, sDEPdata.pszIni);

  // dataset_types
  strcpy(szSelectArgument, "dataset_types");
  iRetCode = BNL_tb_get_arguments_optional
  (
    msg.arguments,
    msg.task,
    szSelectArgument,
    false,
    &sDEPdata.iDStypesCount,
    &sDEPdata.pszDStypes
  );
  if (BNL_em_error_handler("BNL_tb_get_arguments_optional", AH_Version, EMH_severity_error, iRetCode, true)) goto handler_exit;

  // named_references
  strcpy(szSelectArgument, "named_references");
  iRetCode = BNL_tb_get_arguments_optional
  (
    msg.arguments,
    msg.task,
    szSelectArgument,
    false,
    &sDEPdata.iNamedRefCount,
    &sDEPdata.pszNamedRefs
  );
  if (BNL_em_error_handler("BNL_tb_get_arguments_optional", AH_Version, EMH_severity_error, iRetCode, true)) goto handler_exit;

  // object_types
  strcpy(szSelectArgument, "object_types");
  iRetCode = BNL_tb_get_arguments_optional
  (
    msg.arguments,
    msg.task,
    szSelectArgument,
    true,
    &sDEPdata.iObjectTypes,
    &sDEPdata.pszObjectsTypes
  );
  if (BNL_em_error_handler("BNL_tb_get_arguments_optional", AH_Version, EMH_severity_error, iRetCode, true)) goto handler_exit;

  // Provider and translator need to be supplied when inifile name has not been supplied
  if (sDEPdata.pszIni == NULL)
  {
    fprintf(ugslog, "%s: Since the inifile argument has not been provided, the arguments translator and provider are obliged.\n", AH_Version);
    lFlag = false;
  }
  else
  {
    lFlag = true;
  }

  // translator
  strcpy(szSelectArgument, "translator");
  iRetCode = BNL_tb_get_argument_optional
  (
    msg.arguments,
    msg.task,
    szSelectArgument,
    lFlag,
    &sDEPdata.pszTranslator
  );
  if (BNL_em_error_handler("BNL_tb_get_argument_optional", AH_Version, EMH_severity_error, iRetCode, true)) goto handler_exit;

  // provider
  strcpy(szSelectArgument, "provider");
  iRetCode = BNL_tb_get_argument_optional
  (
    msg.arguments,
    msg.task,
    szSelectArgument,
    lFlag,
    &sDEPdata.pszProvider
  );
  if (BNL_em_error_handler("BNL_tb_get_argument_optional", AH_Version, EMH_severity_error, iRetCode, true)) goto handler_exit;

  // priority
  strcpy(szSelectArgument, "priority");
  iRetCode = BNL_tb_get_argument_optional
  (
    msg.arguments,
    msg.task,
    szSelectArgument,
    true,
    &pszArgument
  );
  if (BNL_em_error_handler("BNL_tb_get_argument_optional", AH_Version, EMH_severity_error, iRetCode, true)) goto handler_exit;
  if (pszArgument != NULL)
  {
    sDEPdata.iPriority = atoi(pszArgument);
    MEM_free(pszArgument);
  }
  if (sDEPdata.iPriority <= 0 || sDEPdata.iPriority > 3)
  {
    sDEPdata.iPriority = 3;
  }

  // oneline_process
  strcpy(szSelectArgument, "oneline_process");
  iRetCode = BNL_tb_get_argument_optional
  (
    msg.arguments,
    msg.task,
    szSelectArgument,
    true,
    &pszArgument
  );
  if (BNL_em_error_handler("BNL_tb_get_argument_optional", AH_Version, EMH_severity_error, iRetCode, true)) goto handler_exit;
  if (pszArgument != NULL)
  {
    if (_stricmp(pszArgument, "true") == 0)
      sDEPdata.iProcessOneLine = 1;
    else
      sDEPdata.iProcessOneLine = 0;
    MEM_free(pszArgument);
  }

  // recursive
  /* If -recursive=false is given, only the targets are processed, targets will not be scanned for childs */
  strcpy(szSelectArgument, "recursive");
  iRetCode = BNL_tb_get_argument_optional
  (
    msg.arguments,
    msg.task,
    szSelectArgument,
    true,
    &pszArgument
  );
  if (BNL_em_error_handler("BNL_tb_get_argument_optional", AH_Version, EMH_severity_error, iRetCode, true)) goto handler_exit;
fprintf(ugslog, "1. psDEPdata.lRecursive [%s].\n", sDEPdata.lRecursive?"true":"false");
  if (pszArgument != NULL)
  {
    if (_stricmp(pszArgument, "false") == 0)
      sDEPdata.lRecursive = false;
    else
      sDEPdata.lRecursive = true;
    MEM_free(pszArgument);
  }
  else
    sDEPdata.lRecursive = true;
fprintf(ugslog, "2. psDEPdata.lRecursive [%s].\n", sDEPdata.lRecursive?"true":"false");

  // listname
  strcpy(szSelectArgument, "listname");
  iRetCode = BNL_tb_get_argument_optional
  (
    msg.arguments,
    msg.task,
    szSelectArgument,
    true,
    &sDEPdata.pszListName
  );
  if (BNL_em_error_handler("BNL_tb_get_argument_optional", AH_Version, EMH_severity_error, iRetCode, true)) goto handler_exit;

  // no_info
  strcpy(szSelectArgument, "no_info");
  iRetCode = BNL_tb_get_argument_optional
  (
    msg.arguments,
    msg.task,
    szSelectArgument,
    true,
    &pszArgument
  );
  if (BNL_em_error_handler("BNL_tb_get_argument_optional", AH_Version, EMH_severity_error, iRetCode, true)) goto handler_exit;
  if (pszArgument != NULL)
  {
    if (_stricmp(pszArgument, "true") == 0)
      lNoInfo = true;
    MEM_free(pszArgument);
  }

  // level
  strcpy(szSelectArgument, "level");
  iRetCode = BNL_tb_get_argument_optional
  (
    msg.arguments,
    msg.task,
    szSelectArgument,
    true,
    &sDEPdata.pszLevel
  );
  if (BNL_em_error_handler("BNL_Get_Argument_Optional",AH_Version,EMH_severity_error,iRetCode, true)) goto handler_exit;

  // bomviewtype
  strcpy(szSelectArgument, "bomviewtype");
  iRetCode = BNL_tb_get_argument_optional
  (
    msg.arguments,
    msg.task,
    szSelectArgument,
    true,
    &sDEPdata.pszBomview
  );
  if (BNL_em_error_handler("BNL_tb_get_argument_optional", AH_Version, EMH_severity_error, iRetCode, true)) goto handler_exit;
  if (sDEPdata.pszBomview != NULL)
  {
    sDEPdata.lUseBomView = true;
  }

  /* if bomviewtype was given, revrule and leveldepth are required arguments */
  if (sDEPdata.lUseBomView)
  {
    // revrule
    strcpy(szSelectArgument, "revrule");
    iRetCode = BNL_tb_get_argument_optional
    (
      msg.arguments,
      msg.task,
      szSelectArgument,
      false,
      &sDEPdata.pszRevisionRule
    );
    if (BNL_em_error_handler("BNL_tb_get_argument_optional", AH_Version, EMH_severity_error, iRetCode, true)) goto handler_exit;

    // leveldepth
    strcpy(szSelectArgument, "leveldepth");
    iRetCode = BNL_tb_get_argument_optional
    (
      msg.arguments,
      msg.task,
      szSelectArgument,
      false,
      &pszArgument
    );
    if (BNL_em_error_handler("BNL_tb_get_argument_optional", AH_Version, EMH_severity_error, iRetCode, true)) goto handler_exit;
    if (pszArgument != NULL)
    {
      sDEPdata.iBomLevel = atoi(pszArgument);
      MEM_free(pszArgument);
    }

    // ignore_unconfigured
    strcpy(szSelectArgument, "ignore_unconfigured");
    iRetCode = BNL_tb_get_argument_optional
    (
      msg.arguments,
      msg.task,
      szSelectArgument,
      false,
      &pszArgument
    );
    if (BNL_em_error_handler("BNL_tb_get_argument_optional", AH_Version, EMH_severity_error, iRetCode, true)) goto handler_exit;
    if (pszArgument != NULL)
    {
      if (_stricmp(pszArgument, "true") == 0)
      {
        sDEPdata.lIgnoreUnconfigBom = true;
      }
      MEM_free(pszArgument);
    }

    // excludednote
    strcpy (szSelectArgument,"excludednote");
    iRetCode = BNL_tb_get_argument_optional
    (
        msg.arguments,
        msg.task,
        szSelectArgument,
        true,
        &pszArgument
    );
    if (BNL_em_error_handler("BNL_tb_get_arguments_optional", AH_Version, EMH_severity_error, iRetCode, true)) goto handler_exit;
    if (pszArgument != NULL)
    {
      sDEPdata.iExcludeNote = 1;

		  pszTemp = strchr(pszArgument, ':');
		  if (pszTemp == NULL)
			{
        fprintf(ugslog, "%s: Error the excludednote parameter value does not have the right format like notetype:notevalue.\n", AH_Version);
				goto handler_exit;
			}

      *pszTemp = '\0';
      BNL_tb_copy_string(pszArgument, &sDEPdata.pszNoteType);

			pszTemp++;
			BNL_tb_copy_string(pszTemp, &sDEPdata.pszNote);

      MEM_free(pszArgument);
    }

  } /* End of if (sDEPdata.lUseBomView) */


  /* End of geting the arguments */

  /* Initialize FOLDER module */
  iRetCode = FL_init_module();
  if (BNL_em_error_handler("FL_init_module", AH_Version, EMH_severity_error, iRetCode, true)) goto handler_exit;

  /* Initialize AE module */
  iRetCode = AE_init_module();
  if (BNL_em_error_handler("AE_init_module", AH_Version, EMH_severity_error, iRetCode, true)) goto handler_exit;

  /* Get the job */
  iRetCode = EPM_ask_job(msg.task, &tJob);
  if (BNL_em_error_handler("EPM_ask_job", AH_Version, EMH_severity_error, iRetCode, true)) goto handler_exit;

fprintf(ugslog, "3. psDEPdata.lRecursive [%s].\n", sDEPdata.lRecursive?"true":"false");
  /* Create the dispatcher requests */
  if ((iRetCode = create_translation_requests(tJob, &sDEPdata)) != 0) goto handler_exit;

  /* Check if a Dispatcher Request have been created */
  if (!sDEPdata.lTranslationRequestCreated)
  {
    if (sDEPdata.pszLevel > NULL)
    {
      fprintf(ugslog, "%s: No Dispatcher Request has been created, setting the task result to false.\n", AH_Version);

      iRetCode = EPM_set_condition_task_result(msg.task, 0);
      BNL_em_error_handler("EPM_set_condition_task_result", AH_Version, EMH_severity_error, iRetCode, true);
    }

    if (lNoInfo)
    {
		  fprintf(ugslog, "%s: Warning, no Dispatcher Request has been created.\n", AH_Version);
    }
		else
		{
      BNL_em_error_handler_s1("AH-dep-create-request", AH_Version, EMH_severity_information, BNL_display_confirmation + giErrorOffset, true, "No Dispatcher Request has been created.");
		}
  }
  else
  {
    if (sDEPdata.pszLevel > NULL)
    {
      fprintf(ugslog, "%s: Plot Request has been created, setting the task result to true.\n", AH_Version);

      iRetCode = EPM_set_condition_task_result(msg.task, 1);
      BNL_em_error_handler("EPM_set_condition_task_result", AH_Version, EMH_severity_error, iRetCode, true);
    }
  }

   /**
    This label will be called on errors or if decided to exit the handler in anyway.
    Beneath this label the memory will be freed in a save way and logfile messages will
    be logical closed.
   */
handler_exit:

  // This that the custom error message will be displayed properly
  EMH_ask_last_error(&iRetCode);

  fprintf(ugslog,"%s: Start to free memory of main process\n", AH_Version);

  if (sDEPdata.pszIni != NULL) MEM_free(sDEPdata.pszIni);
  if (sDEPdata.iDStypesCount > 0) BNL_mm_free_list((void *)sDEPdata.pszDStypes, sDEPdata.iDStypesCount);
  if (sDEPdata.iNamedRefCount > 0) BNL_mm_free_list((void *)sDEPdata.pszNamedRefs, sDEPdata.iNamedRefCount);
  if (sDEPdata.iObjectTypes > 0) BNL_mm_free_list((void *)sDEPdata.pszObjectsTypes, sDEPdata.iObjectTypes);
  if (sDEPdata.pszTranslator != NULL) MEM_free(sDEPdata.pszTranslator);
  if (sDEPdata.pszProvider != NULL) MEM_free(sDEPdata.pszProvider);
  if (sDEPdata.pszListName != NULL) MEM_free(sDEPdata.pszListName);
  if (sDEPdata.pszBomview != NULL) MEM_free(sDEPdata.pszBomview);
  if (sDEPdata.pszRevisionRule != NULL) MEM_free(sDEPdata.pszRevisionRule);
  if (sDEPdata.pszNote != NULL) MEM_free(sDEPdata.pszNote);
  if (sDEPdata.pszNoteType != NULL) MEM_free(sDEPdata.pszNoteType);
  if (sDEPdata.pszLevel != NULL) MEM_free(sDEPdata.pszLevel);
  if (sDEPdata.pszTaskPuid != NULL) MEM_free(sDEPdata.pszTaskPuid);
  BNL_ini_free_ini_array(sDEPdata.iExtraArgsCount, sDEPdata.ppsExtraArgs);
  if (pszDateTime != NULL) free(pszDateTime);

  AE_exit_module();
  FL_exit_module();

  fprintf(ugslog, "%s: End\n",AH_Version);
  fprintf(ugslog,"\n");

  return iRetCode;
} /* End of BNL_AH_dep_create_request */

/**
static int create_translation_requests
(
  tag_t tTask,         <I>     tag of the task
  t_DEP *psDEPdata     <IO>    general data of the selected object and arguments of the handler
)

  Description:

    Creates the Dispatcher Requests

  Returns:

    0 if OK

*/
int create_translation_requests(tag_t tJob, t_DEP *psDEPdata)
{
  int iRetCode        = 0;
  int iTargetCount    = 0;
  int i               = 0;
  int tRootTask		    = 0;

  tag_t *ptTargets    = NULL;


  iRetCode = EPM_ask_root_task(tJob, &tRootTask);
  if (BNL_em_error_handler("EPM_ask_root_task",AH_Version,EMH_severity_error,iRetCode, true)) return iRetCode;

  psDEPdata->tRootTask = tRootTask;

  iRetCode = POM_tag_to_string(tRootTask, &psDEPdata->pszTaskPuid);
  if (BNL_em_error_handler("POM_tag_to_string", AH_Version, EMH_severity_error, iRetCode, true)) return iRetCode;

  /* Loop through targets */
  if (psDEPdata->pszListName != NULL)
  {
    /* Get the targets from a special dataset added by the add handlers */
    iRetCode = BNL_tb_get_tags_from_targetref_ds(psDEPdata->pszListName, tJob, NULL_TAG, &iTargetCount, &ptTargets);
    if (BNL_em_error_handler("BNL_tb_get_tags_from_targetref_ds", AH_Version, EMH_severity_error, iRetCode, true)) return iRetCode;
  }
  else
  {
    iRetCode = EPM_ask_attachments(tRootTask, EPM_target_attachment, &iTargetCount, &ptTargets);
    if (BNL_em_error_handler("EPM_ask_attachments",AH_Version,EMH_severity_error,iRetCode, true)) return iRetCode;
  }
fprintf(ugslog, "create_translation_requests: iTargetCount [%d].\n", iTargetCount);
fprintf(ugslog, "psDEPdata->lRecursive [%s].\n", psDEPdata->lRecursive?"true":"false");

  psDEPdata->iNumTargets = iTargetCount;
  psDEPdata->ptTargets = ptTargets;
  /* Loop through the targets and process them */
  for (i = 0; i < iTargetCount; i++)
  {
    psDEPdata->tCurrentTarget = NULL_TAG;
    psDEPdata->tItem = NULL_TAG;
    psDEPdata->tItemRev = NULL_TAG;
    psDEPdata->tDataset = NULL_TAG;
    psDEPdata->tNamedRef = NULL_TAG;

    iRetCode = process_target(psDEPdata, ptTargets[i]);

    if (psDEPdata->lTranslationRequestCreated && psDEPdata->iProcessOneLine == 1)
    {
      break;
    }
  }

  BNL_mm_free((void *)ptTargets);

  return iRetCode;
}

/**
static int process_target
(
    t_DEP *psDEPdata,     <IO>    structure with required information
    tag_t tObject   <I>     target to process
)

  Description:

    Accepts Folders, Items, ItemRevisions and Datasets as target object and
    will call itself recursively till Dataset level. For each Named reference with the
    required name process_namedref is called.

  Returns:

    ITK_ok if OK

*/
int process_target(t_DEP *psDEPdata, tag_t tObject)
{
  int iRetCode        = ITK_ok;
  int iRefCount       = 0;
  int b, i, j, k, t   = 0;
  int iBomCount       = 0;
	int iBvrCount			  = 0;

  char szClassName[IMANTYPE_name_size_c+1];
  char szTargetClass[IMANTYPE_name_size_c+1];
  char szBomType[WSO_name_size_c + 1];
  char szType[WSO_name_size_c + 1];

  tag_t *ptBvrs			  = NULL;
  tag_t *ptRefs       = NULL;
  tag_t *ptBomViews   = NULL;

  tag_t tDstype       = NULL_TAG;
  tag_t tBv           = NULL_TAG;
  tag_t tBvr          = NULL_TAG;

  ITEM_attached_object_t *attached_objects    = NULL;

  szType[0]='\0';

  psDEPdata->tCurrentTarget =  tObject;

  //iRetCode = BNL_tb_determine_object_class(tObject, szTargetClass);
  //if (BNL_em_error_handler("BNL_tb_determine_object_class", AH_Version, EMH_severity_error, iRetCode, true)) return iRetCode;
  iRetCode = BNL_tb_determine_super_class(tObject, szTargetClass);
  if (BNL_em_error_handler("BNL_tb_determine_super_class", AH_Version, EMH_severity_error, iRetCode, true)) return iRetCode;

  fprintf(ugslog, "%s: Processing target class [%s].\n", AH_Version, szTargetClass);

  if (strcmp(szTargetClass, "Folder") == 0)
  {
    if (psDEPdata->lRecursive)
    {
      /* Loop through folder elements */
      iRetCode = FL_ask_references(tObject, FL_fsc_as_ordered, &iRefCount, &ptRefs);
      if (BNL_em_error_handler("FL_ask_references", AH_Version, EMH_severity_error, iRetCode, true)) return iRetCode;
      for (i = 0; i < iRefCount; i++)
      {
        psDEPdata->tCurrentTarget = NULL_TAG;
        psDEPdata->tItem = NULL_TAG;
        psDEPdata->tItemRev = NULL_TAG;
        psDEPdata->tDataset = NULL_TAG;
        psDEPdata->tNamedRef = NULL_TAG;
        process_target(psDEPdata, ptRefs[i]);
      }
      BNL_mm_free((void*)ptRefs);
    }
  }
  else if (strcmp(szTargetClass, "Item") == 0)
  {
    psDEPdata->tItem = tObject;
    if (psDEPdata->lUseBomView)
    {
      iRetCode = ITEM_list_bom_views(psDEPdata->tItem, &iBomCount, &ptBomViews);
      if (BNL_em_error_handler("ITEM_list_bom_views", AH_Version, EMH_severity_error, iRetCode, true)) return iRetCode;

      /* check for the bomview type*/
      for (b = 0; b < iBomCount; b++)
      {
        iRetCode = process_target(psDEPdata, ptBomViews[b]);
        if (iRetCode == -1) return -1;
      }
      BNL_mm_free((void*)ptBomViews);
    }
    else if (psDEPdata->lRecursive)
    {
      /* Loop through ItemsRevisions */
      iRetCode = ITEM_list_all_revs(tObject, &iRefCount, &ptRefs);
      if (BNL_em_error_handler("ITEM_list_all_revs", AH_Version, EMH_severity_error, iRetCode, true)) return iRetCode;
      for (i = 0; i < iRefCount; i++)
      {
        process_target(psDEPdata, ptRefs[i]);
      }
      BNL_mm_free((void*)ptRefs);
    }
  }
  else if (strcmp(szTargetClass, "ItemRevision") == 0)
  {
    /*

      The handler should be able to recognize that if a bomview is found at the target and its
		  -viewtype is given then it will go into the bomview. If no bomviews found then handle just the the datasets
		  below the target.

      Because this function is recursive called watch out for some states.

      If itemrevision has bomview and is not occ of bom (in case of recursive call) and the viewtype is
		  right then handle the bomview.

      In other cases do not handle bomview.

    */
    /* Check for the types */
fprintf(ugslog, "%s: Processing target class [ItemRevision].\n", AH_Version);
fprintf(ugslog, "psDEPdata->lUseBomView [%s].\n", psDEPdata->lUseBomView?"true":"false");
fprintf(ugslog, "psDEPdata->lIsOccOfBomView [%s].\n", psDEPdata->lIsOccOfBomView?"true":"false");
    if (psDEPdata->lUseBomView && !psDEPdata->lIsOccOfBomView)
    {
fprintf(ugslog, "if (psDEPdata->lUseBomView && !psDEPdata->lIsOccOfBomView).\n");
		  /* Get the revisions of the bomview */
			iRetCode = ITEM_rev_list_all_bom_view_revs(tObject, &iBvrCount, &ptBvrs);
			if (BNL_em_error_handler("ITEM_rev_list_all_bom_view_revs", AH_Version, EMH_severity_error, iRetCode, true)) return iRetCode;

		  for (b = 0; b < iBvrCount; b++)
			{
			  /* Can be only one bomview at the revision of the same type */
				iRetCode = BNL_tb_determine_object_type(ptBvrs[b], szBomType);
        if (BNL_em_error_handler("BNL_tb_determine_object_type", AH_Version, EMH_severity_error, iRetCode, true)) return iRetCode;

        if (strcmp(psDEPdata->pszBomview, szBomType) == 0)
				{
				  fprintf(ugslog, "[%s]\n", szBomType);
          tBvr = ptBvrs[b];
    		}
			} /* End of for (b = 0; b < iBvrCount; b++) */
      BNL_mm_free((void*)ptBvrs);
    } /* End of if (if (psDEPdata->lUseBomView && psDEPdata->lIsOccOfBomView) */

		/* This determines if there is bvr to handle */
fprintf(ugslog, "psDEPdata->lUseBomView [%s].\n", psDEPdata->lUseBomView?"true":"false");
fprintf(ugslog, "psDEPdata->lIsOccOfBomView [%s].\n", psDEPdata->lIsOccOfBomView?"true":"false");
fprintf(ugslog, "tBvr [%s].\n", tBvr!=NULL_TAG?"tag":"empty");
fprintf(ugslog, "psDEPdata->lRecursive [%s].\n", psDEPdata->lRecursive?"true":"false");
    if (psDEPdata->lUseBomView && !psDEPdata->lIsOccOfBomView && tBvr!=NULL_TAG)
		{
fprintf(ugslog, "if (psDEPdata->lUseBomView && !psDEPdata->lIsOccOfBomView && tBvr!=NULL_TAG).\n");
      /* We need the item because BOM_set_window_top_line needs the bomview not a bomview revision! */
      iRetCode = ITEM_ask_item_of_rev(tObject, &psDEPdata->tItem);
      if (BNL_em_error_handler("ITEM_ask_item_of_rev", AH_Version, EMH_severity_error, iRetCode, true)) return iRetCode;

      iRetCode = process_target(psDEPdata, tBvr);
      if (iRetCode == -1) return -1;
			tBvr = NULL_TAG;
		}
    else if (psDEPdata->lRecursive || psDEPdata->lIsOccOfBomView)
    {
      int iFlag     = 0;

fprintf(ugslog, "else if (psDEPdata->lRecursive || psDEPdata->lIsOccOfBomView).\n");
      psDEPdata->tItemRev = tObject;

      iRetCode = WSOM_ask_object_type(tObject,szType);
      if (BNL_em_error_handler("WSOM_ask_object_type", AH_Version, EMH_severity_error, iRetCode, true)) return iRetCode;

      fprintf(ugslog, "%s: Object is of type [%s].\n", AH_Version, szType);

      for (t = 0;t < psDEPdata->iObjectTypes; t++)
      {
        if (strcmp(psDEPdata->pszObjectsTypes[t], "-") == 0 || strcmp(szType, psDEPdata->pszObjectsTypes[t]) == 0)
        {
          fprintf(ugslog, "%s: Object type matches...\n", AH_Version, szType);
          process_object(psDEPdata, tObject);
          iFlag = 1;
        }
      }

      szType[0]='\0';

      if (iFlag == 0)
      {
        /* Loop through datasets */
        iRetCode = ITEM_list_all_rev_attachments(tObject, &iRefCount, &attached_objects);
        if (BNL_em_error_handler("ITEM_list_all_rev_attachments", AH_Version, EMH_severity_error, iRetCode, true)) return iRetCode;

			  fprintf(ugslog, "%s: Found [%d] revision attachments.\n", AH_Version, iRefCount);

        for (i = 0; i < iRefCount; i ++)
        {
          /* For each attachment */
          iRetCode = BNL_tb_determine_object_class(attached_objects[i].attachment, szClassName);
          if (BNL_em_error_handler("BNL_tb_determine_object_class", AH_Version, EMH_severity_error, iRetCode, true)) return iRetCode;

				  fprintf(ugslog,"%s: [%d] Attachment class [%s].\n",AH_Version, i, szClassName);

          if (strcmp(szClassName, "Dataset") == 0)
          {
            process_target(psDEPdata, attached_objects[i].attachment);
          }
        }/* End of loop for each attachment */
        BNL_mm_free((void*)attached_objects);
      }
    }
  }
  else if (strcmp(szTargetClass, "PSBOMView") == 0)
  {
    if (psDEPdata->lUseBomView)
    {
      if (psDEPdata->tItem == NULL_TAG)
      {
        iRetCode = PS_ask_item_of_bom_view( tObject, &psDEPdata->tItem);
        if (BNL_em_error_handler("PS_ask_item_of_bom_view", AH_Version, EMH_severity_error, iRetCode, true)) return iRetCode;
      }
      iRetCode = BNL_tb_determine_object_type(tObject, szBomType);
      if (BNL_em_error_handler("BNL_tb_determine_object_type", AH_Version, EMH_severity_error, iRetCode, true)) return iRetCode;

      if (strcmp(psDEPdata->pszBomview, szBomType) == 0)
      {
				fprintf(ugslog, "%s :Processing bomview of type [%s].\n", AH_Version, szBomType);
        if (process_bomview(psDEPdata, tObject) == -1) return -1;

      }
    }
  }
  else if (strcmp(szTargetClass, "PSBOMViewRevision") == 0)
  {
    if (psDEPdata->lUseBomView)
    {
      if (psDEPdata->tItem == NULL_TAG)
      {
        iRetCode = PS_ask_bom_view_of_bvr(tObject, &tBv);
        if (BNL_em_error_handler("PS_ask_item_of_bom_view", AH_Version, EMH_severity_error, iRetCode, true)) return iRetCode;

        iRetCode = PS_ask_item_of_bom_view(tBv, &psDEPdata->tItem);
        if (BNL_em_error_handler("PS_ask_item_of_bom_view", AH_Version ,EMH_severity_error, iRetCode, true)) return iRetCode;
      }
      iRetCode = BNL_tb_determine_object_type(tObject, szBomType);
      if (BNL_em_error_handler("BNL_tb_determine_object_type", AH_Version, EMH_severity_error, iRetCode, true)) return iRetCode;

      if (strcmp(psDEPdata->pszBomview, szBomType) == 0)
      {
			  fprintf(ugslog, "%s :Processing bomview revision of type [%s].\n", AH_Version, szBomType);
        if (process_bomview(psDEPdata, tObject)==-1) return -1;
      }
    }
  }
  else if (strcmp(szTargetClass, "Dataset") == 0)
  {
    tag_t tCurDStype      = NULL_TAG;
    char szDsName[WSO_name_size_c+1];

    /* Loop through named refs elements */
    psDEPdata->tDataset = tObject;

    iRetCode = WSOM_ask_object_type(tObject, szType);
    if (BNL_em_error_handler("WSOM_ask_object_type",AH_Version,EMH_severity_error, iRetCode, true)) return iRetCode;

    for (t = 0;t < psDEPdata->iObjectTypes; t++)
    {
      if (strcmp(psDEPdata->pszObjectsTypes[t], "-") == 0 || strcmp(szType, psDEPdata->pszObjectsTypes[t]) == 0)
      {
        tag_t tItemRevision   = NULL_TAG;

        if ((iRetCode = BNL_lookup_item_revision(psDEPdata->tDataset, psDEPdata, &tItemRevision)) != 0) return iRetCode;
        if (tItemRevision != NULL_TAG)
        {
          process_object(psDEPdata, tItemRevision);
        } // End of if (tItemRevision != NULL_TAG)
      } // End of if (strcmp(psDEPdata->pszObjectsTypes[t], "-") == 0 || strcmp(szType, psDEPdata->pszObjectsTypes[t]) == 0)
    } // End of for (t = 0;t < psDEPdata->iObjectTypes; t++)
    szType[0] = '\0';

    /* Get the dataset type of the found dataset */
    iRetCode = AE_ask_dataset_datasettype(psDEPdata->tDataset, &tCurDStype);
    if (BNL_em_error_handler("AE_ask_dataset_datasettype", AH_Version, EMH_severity_error, iRetCode, true)) return iRetCode;

    /* Find the requested named references in this dataset */
    for (i = 0; i < psDEPdata->iDStypesCount; i++)
    {
      /* Check first if datasettype is known to avoid errors */
      iRetCode=AE_find_datasettype(psDEPdata->pszDStypes[i], &tDstype);
      if (BNL_em_error_handler("AE_find_datasettype", AH_Version, EMH_severity_error, iRetCode, true)) return iRetCode;

      if (tDstype != NULL_TAG)
      {
        /* Is this dataset type requested? */
        if (tDstype == tCurDStype)
        {
          /* check for named refs in this dataset of this datasettype */
          for (k = 0; k < psDEPdata->iNamedRefCount; k++)
          {
            iRetCode = AE_ask_all_dataset_named_refs(psDEPdata->tDataset, psDEPdata->pszNamedRefs[k], &iRefCount, &ptRefs);
            if (BNL_em_error_handler("AE_ask_all_dataset_named_refs", AH_Version, EMH_severity_error, iRetCode, true)) return iRetCode;

            iRetCode = WSOM_ask_name(tObject, szDsName);
            if (BNL_em_error_handler("WSOM_ask_name", AH_Version, EMH_severity_error, iRetCode, true)) return iRetCode;
            fprintf(ugslog,"%s: Found %d named refs [%s] in dataset [%s] of type [%s].\n",
              AH_Version, iRefCount, psDEPdata->pszNamedRefs[k], szDsName, psDEPdata->pszDStypes[i]);

            for (j = 0; j < iRefCount; j++)
            {
              psDEPdata->tNamedRef = ptRefs[j];
              process_namedref(psDEPdata, psDEPdata->pszNamedRefs[k]);
            }

            iRefCount = 0;
            BNL_mm_free((void*)ptRefs);
          }
          break;
        }
      }
    }
  }
  else
  {
    logical lTypeMatches      = false;

    // Check the type
    iRetCode = WSOM_ask_object_type(tObject,szType);
    if (BNL_em_error_handler("WSOM_ask_object_type", AH_Version, EMH_severity_error, iRetCode, true)) return iRetCode;

    fprintf(ugslog, "%s: Object is of type [%s].\n", AH_Version, szType);

    for (t = 0;t < psDEPdata->iObjectTypes; t++)
    if (strcmp(psDEPdata->pszObjectsTypes[t], "-") == 0 || strcmp(szType, psDEPdata->pszObjectsTypes[t]) == 0)
    {
      fprintf(ugslog, "%s: Object type matches...\n", AH_Version, szType);
      lTypeMatches = true;
    }
    if (lTypeMatches)
      process_object(psDEPdata, tObject);
    else
      fprintf( ugslog, "%s: WARNING: unsupported object [%d], rejected.\n", AH_Version, szTargetClass);
  }

  return iRetCode;
}


int BNL_get_non_note_excluded_revs
(
  logical lIgnoreUnconfigBom,
  char *pszNote,
  char *pszValue,
  tag_t tBomLine,
  int iLevel,
  int iMaxLevel,
  int *iRevCount,
  tag_t **tItemRevs
)
{
  tag_t *tBomLines            = NULL;
  tag_t tItemRev              = NULL_TAG;

  int iRetCode                = ITK_ok;
  int iAttrRevId              = 0;

  int iBomLineCount           = 0;
  int l                       = 0;
  int i                       = 0;

  int iSuppressed             = 0;
  int iCount                  = 0;
  int iFound                  = 0;
  int iNoteAttr               = 0;


  tag_t tNoteType             = NULL_TAG;
  tag_t tItem                 = NULL_TAG;
  tag_t tChild                = NULL_TAG;

  tag_t *ptOccurences         = NULL;

  char *pszOccValue           = NULL;

  tag_t *ptRevs = *tItemRevs;

  iCount = *iRevCount;


  if (iLevel>0)
  {
    iRetCode = BOM_line_look_up_attribute(pszNote, &iNoteAttr);
    if (BNL_em_error_handler("BOM_line_look_up_attribute", AH_Version, EMH_severity_error, iRetCode, true)) return false;

    iRetCode = BOM_line_ask_attribute_string(tBomLine, iNoteAttr, &pszOccValue);
    if (BNL_em_error_handler("BOM_line_ask_attribute_string", AH_Version, EMH_severity_error, iRetCode, true)) return false;

    iRetCode = BOM_line_look_up_attribute(bomAttr_lineItemRevTag,&iAttrRevId);
    if (BNL_em_error_handler("BOM_line_look_up_attribute",AH_Version,EMH_severity_error, iRetCode, true)) return false;

    iRetCode = BOM_line_ask_attribute_tag(tBomLine, iAttrRevId, &tItemRev);
    if (BNL_em_error_handler("BOM_line_ask_attribute_tag", AH_Version, EMH_severity_error, iRetCode, true)) return false;

    if(tItemRev != NULL_TAG)
    {
      /* Check if it is supressed */
      if (strcmp(pszOccValue, pszValue) == 0) iSuppressed = 1;

      if (!iSuppressed)
      {
        /* Check for doubles, did it here but for some reason
           it will not pick up the counter
        */
          iCount++;
          ptRevs = (tag_t *)MEM_realloc(ptRevs, iCount * sizeof(tag_t));
          ptRevs[iCount-1] = tItemRev;
      }
    }
    else
    {
      /* This happens when the revision rule is set and there are occurence wich not
         which is not a allowed occurence the (???) occurence.
         EN quik and dirty implementation 21-05-2001 for Philips EMT
      */

      fprintf(ugslog, "%s: Unconfigured occurence found.\n",AH_Version);
      *tItemRevs = ptRevs;
      *iRevCount = iCount;

      return -1;
    }
  }
  else
  {
    *tItemRevs = NULL;
    *iRevCount = 0;
  }

  if (( iLevel < iMaxLevel || iMaxLevel == -1) && iSuppressed == 0)
  {
    iRetCode = BOM_line_ask_child_lines(tBomLine, &iBomLineCount, &tBomLines);
    if (BNL_em_error_handler("BOM_line_ask_child_lines", AH_Version, EMH_severity_error, iRetCode, true)) return false;

    fprintf(stderr, "%s: Found at bomline at level [%d] [%d] bomlines lines.\n  ",AH_Version, (iLevel+1), iBomLineCount);

    for (l = 0; l < iBomLineCount; l++)
    {
      iRetCode = BNL_get_non_note_excluded_revs(lIgnoreUnconfigBom, pszNote, pszValue, tBomLines[l],(iLevel+1), iMaxLevel, &iCount, &ptRevs);
      if (iRetCode == -1)
      {
        if (lIgnoreUnconfigBom)
        {
          /*in case of unconfigured revs*/
          fprintf(ugslog, "%s: Skipped unconfigured occurence.\n", AH_Version);
          iRetCode = 0;
        }
        else
        {
            return -1;
        }
      }
      else
      {
        /* Normal error handling */
        if (BNL_em_error_handler("BNL_get_non_note_excluded_revs", AH_Version, EMH_severity_error, iRetCode, true)) return false;
      }
      fprintf(stderr, "%s: Found at bomline [%d] of level [%d] [%d] child lines.\n ", AH_Version, l, (iLevel+1), *iRevCount);
    }
    if (iBomLineCount >0 ) BNL_mm_free((void*)tBomLines);
  }

  *tItemRevs = ptRevs;
  *iRevCount = iCount;

  return iRetCode;
} /* End of BNL_get_non_note_excluded_revs */



static int process_bomview(t_DEP *psDEPdata, tag_t tBomview);
/**
static int process_bomview
(
  t_DEP *psDEPdata,     <IO>    structure with required information to produce CSV data
  tag_t tBomview        <I>     tag of bomview or bomview revision
)

  Description:

    Open a bomview window for the given revision rule and
    process all item revisions in this bomview window.

  Returns:

    ITK_ok if OK

*/
static int process_bomview(t_DEP *psDEPdata, tag_t tObject)
{
  int iRetCode        = ITK_ok;
  int iItemRevCount   = 0;
  int i               = 0;
	int iOccs 		    	= 0;
	int o			        	= 0;
	int d               = 0;
	int t				        = 0;
	int	iRevs		      	= 0;

	tag_t *ptBvrRevs		= NULL;

  tag_t *ptItemRevs   = NULL;
	tag_t *ptOccs			  = NULL;

  tag_t tBomwindow    = NULL_TAG;
	tag_t tBomViewRev		= NULL_TAG;
  tag_t tTopLine      = NULL_TAG;
	tag_t	tBomView		  = NULL_TAG;

  logical lPrecise		= false;

	char szClass[WSO_name_size_c + 1];

  /* Create bom window */
  iRetCode = BOM_create_window(&tBomwindow);
  if (BNL_em_error_handler("BOM_create_window", AH_Version, EMH_severity_error, iRetCode, true)) return iRetCode;

	/* Determine first incomming targets class can be bomrevision, or bom */
	iRetCode = BNL_tb_determine_object_class(tObject, szClass);
	if (BNL_em_error_handler("BNL_tb_determine_object_class", AH_Version, EMH_severity_error, iRetCode, true)) return iRetCode;

	if (strcmp(szClass, "PSBOMViewRevision")==0)
	{
		/* Get its bom */
		tBomViewRev = tObject;
		iRetCode = PS_ask_bom_view_of_bvr(tObject, &tBomView);
	}
	else
	{
		tBomView=tObject;
	}

  /* Set the revision rule */
  //if (strlenpsDEPdata->pszRevisionRule) > 0)
  if (psDEPdata->pszRevisionRule != NULL)
  {
    iRetCode = BNL_tb_bom_apply_config_rule(tBomwindow, psDEPdata->pszRevisionRule);
    if (BNL_em_error_handler("BNL_tb_bom_apply_config_rule", AH_Version, EMH_severity_error, iRetCode, true)) return iRetCode;
  }

	if(tBomViewRev != NULL_TAG)
	{
		/* Always one this function is compatible with previos imanversion 3.5 */
		iRetCode = PS_list_owning_revs_of_bvr(tBomViewRev, &iRevs, &ptBvrRevs);
		if (BNL_em_error_handler("PS_list_owning_revs_of_bvr", AH_Version, EMH_severity_error, iRetCode, true)) return iRetCode;

		iRetCode = BOM_set_window_top_line(tBomwindow, NULL_TAG, ptBvrRevs[0], tBomView, &tTopLine);
    if (BNL_em_error_handler("BOM_set_window_top_line", AH_Version, EMH_severity_error, iRetCode, true)) return iRetCode;

		/* Ask if bomviewrev is precise, if so set line precise */
		iRetCode = PS_ask_is_bvr_precise(tBomViewRev, &lPrecise);
    if (BNL_em_error_handler("PS_ask_is_bvr_precise", AH_Version, EMH_severity_error, iRetCode, true)) return iRetCode;

		if (lPrecise)
		{
      fprintf(ugslog, "%s: Bomline is set to precise.\n", AH_Version);
		  iRetCode = BOM_line_set_precise(tTopLine, lPrecise);
		  if (BNL_em_error_handler("BOM_line_set_precise", AH_Version, EMH_severity_error, iRetCode, true)) return iRetCode;
		}
	}
	else
	{
		iRetCode = BOM_set_window_top_line(tBomwindow, psDEPdata->tItem, NULL_TAG, tBomView, &tTopLine);
    if (BNL_em_error_handler("BOM_set_window_top_line", AH_Version, EMH_severity_error, iRetCode, true)) return iRetCode;
	}

	if (!psDEPdata->iExcludeNote)
	{
		/* Get relevant item revs from bom */
		/* We fool the BNL_tb_bom_get_item_revs by setting startlevel on 1
		   to force the top level of the bom to be added.
		   Therefore we need to increment the maxlevel too (if not -1) */
    if (psDEPdata->iBomLevel != -1) psDEPdata->iBomLevel++;
		iItemRevCount = 0;
		ptItemRevs = NULL;

    if (psDEPdata->lIgnoreUnconfigBom)
    {
		  iRetCode = BNL_tb_bom_get_item_revs(tTopLine, 1, 1, psDEPdata->iBomLevel, &iItemRevCount, &ptItemRevs);
      if (iRetCode == -1) iRetCode = 0;
    }
    else
    {
      iRetCode = BNL_tb_bom_get_item_revs(tTopLine, 0, 1, psDEPdata->iBomLevel, &iItemRevCount, &ptItemRevs);
      if (iRetCode == -1)
      {
        iRetCode = BNL_em_error_handler_s1("AH-ets-create-request", AH_Version, EMH_severity_information, BNL_display_confirmation + giErrorOffset, true, "There where unconfigured occurences.");
			  return iRetCode;
      }
    }
		if (BNL_em_error_handler("BNL_tb_bom_get_item_revs", AH_Version, EMH_severity_error, iRetCode, true)) return iRetCode;

		/* Loop through item revisions */
		psDEPdata->lIsOccOfBomView = true;
		for (i = 0; i < iItemRevCount; i++)
			process_target(psDEPdata, ptItemRevs[i]);
		psDEPdata->lIsOccOfBomView = false;
	}
	else
	{
    fprintf(ugslog, "Filter on note type [%s] with value [%s].\n", psDEPdata->pszNoteType, psDEPdata->pszNote);

		iRetCode = BNL_tb_get_occurences(tTopLine, 1, -1, &iOccs, &ptOccs);

    for(o = 0;o < iOccs; o++)
    {
      BOM_line_unpack(ptOccs[o]);
    }

    MEM_free(ptOccs);

	  if (psDEPdata->iBomLevel != -1) psDEPdata->iBomLevel++;
		iItemRevCount = 0;
		ptItemRevs = NULL;

    iRetCode = BNL_get_non_note_excluded_revs(psDEPdata->lIgnoreUnconfigBom, psDEPdata->pszNoteType, psDEPdata->pszNote, tTopLine, 1, psDEPdata->iBomLevel, &iItemRevCount, &ptItemRevs);
		if (iRetCode==-1)
    {
      iRetCode = BNL_em_error_handler_s1("AH-ets-create-request", AH_Version, EMH_severity_information, BNL_display_confirmation + giErrorOffset, true, "There where unconfigured occurences, no plotrequest created.");
			return iRetCode;
    }
		else if (BNL_em_error_handler("BNL_get_non_note_excluded_revs", AH_Version, EMH_severity_error, iRetCode, true))
    {
			return iRetCode;
    }

    /* Filter out doubles something stupid because counter did not work in
       BNL_get_non_note_excluded_revs function so filter doubles here. */
    for (d = 0; d < iItemRevCount; d++)
    {
      for(t = (d + 1); t < iItemRevCount; t++)
      {
        if (ptItemRevs[d] != NULL_TAG)
        {
          if (ptItemRevs[d] == ptItemRevs[t])
          {
            ptItemRevs[t] = NULL_TAG;
          }
        }
      }
    }
	  /* Loop through item revisions */
    psDEPdata->lIsOccOfBomView = true;
    for (i = 0; i < iItemRevCount; i++)
    {
	    if (ptItemRevs[i] != NULL_TAG) process_target(psDEPdata, ptItemRevs[i]);
    }
	  psDEPdata->lIsOccOfBomView = false;
	}

  BNL_mm_free((void*)ptItemRevs);

  iRetCode = BOM_close_window(tBomwindow);
  if (BNL_em_error_handler("BOM_close_window", AH_Version, EMH_severity_error, iRetCode, true)) return iRetCode;

  return 0;
}


static int process_object
(
  t_DEP *psDEPdata,
  tag_t tObject
)
{
  int iRetCode        = 0;
  //int iArgs           = 0;

  //char *pszProvider   = NULL;
  //char *pszTranslator = NULL;

  char szType[WSO_name_size_c + 1];
  //char szItemId[ITEM_id_size_c + 1];
  //char szItemRevId[ITEM_id_size_c + 1];

  //char **pszArgs      = NULL;

  //tag_t tRequest      = NULL_TAG;
  //tag_t tItem         = NULLTAG;


  iRetCode = WSOM_ask_object_type(tObject, szType);
  if (BNL_em_error_handler("WSOM_ask_object_type", AH_Version, EMH_severity_error, iRetCode, true)) goto function_exit;

  if ((iRetCode = BNL_create_request(tObject, NULL_TAG, szType, psDEPdata)) != 0) goto function_exit;

/*  if (psDEPdata->pszProvider != NULL)
  {
    pszProvider = psDEPdata->pszProvider;
  }
  else
  {
    if (BNL_ini_read_ini_value(psDEPdata->pszIni, DEP_INI_SEC_PROVIDERS, szType, &pszProvider) != 0)
    {
      if (BNL_ini_read_ini_value(psDEPdata->pszIni, DEP_INI_SEC_PROVIDERS, "default_provider", &pszProvider) != 0)
      {
        fprintf(ugslog, "Error: No default provider defined in ini file [%s].\n", psDEPdata->pszIni);

        iRetCode = BNL_display_confirmation + giErrorOffset;
        BNL_em_error_handler_s1("AH-dep-create-request", AH_Version, EMH_severity_information, BNL_display_confirmation + giErrorOffset, true, "Failed to find the provider.");

         goto function_exit;
      }
    }
  }

  if (psDEPdata->pszTranslator != NULL)
  {
    pszTranslator = psDEPdata->pszTranslator;
  }
  else
  {
    if (BNL_ini_read_ini_value(psDEPdata->pszIni, DEP_INI_SEC_TRANSLATORS, szType, &pszTranslator) != 0)
    {
      if (BNL_ini_read_ini_value(psDEPdata->pszIni, DEP_INI_SEC_TRANSLATORS, "default_translator", &pszTranslator) != 0)
      {
        fprintf(ugslog, "Error: No default translator defined in ini file [%s].\n", psDEPdata->pszIni);

        iRetCode = BNL_display_confirmation + giErrorOffset;
        BNL_em_error_handler_s1("AH-dep-create-request", AH_Version, EMH_severity_information, BNL_display_confirmation + giErrorOffset, true, "Failed to find the translator.");

        goto function_exit;
      }
    }
  }

  //iRetCode = ITEM_ask_item_of_rev(tObject, &tItem);
  //if (BNL_em_error_handler("ITEM_ask_item_of_rev",AH_Version,EMH_severity_error, iRetCode, true)) goto function_exit;

  //iRetCode = ITEM_ask_id(tItem, szItemId);
  //if (BNL_em_error_handler("ITEM_ask_item_id",AH_Version,EMH_severity_error, iRetCode, true)) goto function_exit;

  //iRetCode = ITEM_ask_rev_id(tObject, szItemRevId);
  //if (BNL_em_error_handler("ITEM_ask_rev_id",AH_Version,EMH_severity_error, iRetCode, true)) goto function_exit;

  // Add task puid
  if (psDEPdata->pszTaskPuid != NULL)
  {
    iArgs++;
    pszArgs = (char **)MEM_realloc(pszArgs, iArgs * sizeof(char *));
    pszArgs[iArgs - 1] = (char *) MEM_alloc((strlen(psDEPdata->pszTaskPuid) + strlen("task=") + 1) * sizeof(char));
    sprintf(pszArgs[iArgs - 1], "task=%s", psDEPdata->pszTaskPuid);
  }

  if (psDEPdata->pszLevel != NULL)
  {
    iArgs++;
    pszArgs = (char **)MEM_realloc(pszArgs, iArgs * sizeof(char *));
    pszArgs[iArgs - 1] = (char *) MEM_alloc((strlen(psDEPdata->pszLevel) + strlen("level=") + 1) * sizeof(char));
    sprintf(pszArgs[iArgs - 1], "level=%s", psDEPdata->pszLevel);
  }

  iRetCode = DISPATCHER_create_request(
    pszProvider,              // provider name
    pszTranslator,            // service name
    psDEPdata->iPriority,     // priority
    (const time_t)NULL,       // start time
    (const time_t)NULL,       // end time
    0,                        // interval
    1,                        // number of primary/secondary objects
    &tObject,                 // primary objects
    NULL,                     // secondary objects
    iArgs,                    // number of request arguments
    pszArgs,                  // request arguments
    NULL,                     // request type string
    0,                        // number of datafiles to attach
    NULL,                     // keys for data files to attach
    NULL,                     // data files to attach absolute path
    &tRequest);               // created request
  if (BNL_em_error_handler("DISPATCHER_create_request", AH_Version, EMH_severity_error, iRetCode, true)) goto function_exit;

  fprintf(ugslog, "%s: Dispatcher Request has successfully been generated.\n", AH_Version);
  psDEPdata->lTranslationRequestCreated = true;

  if (psDEPdata->pszLevel != NULL)
  {
    if ((iRetCode = BNL_tb_add_objects_to_task(psDEPdata->tRootTask, 1, &tRequest, EPM_reference_attachment)) != 0) goto function_exit;
  }
*/
function_exit:

  /*if (psDEPdata->pszProvider == NULL) free(pszProvider);
  if (psDEPdata->pszTranslator == NULL) free(pszTranslator);
  if (iArgs > 0) BNL_mm_free_list((void *)pszArgs, iArgs);*/

  return iRetCode;
}

/**
static int process_namedref
(
  t_DEP *psDEPdata,     <IO>    structure with required information
  char *pszNamedRef     <I>     current Named reference
)

  Description:

    Gather the missing data and create Dispatcher Request for the given data

  Returns:

    ITK_ok if OK

*/
static int process_namedref(t_DEP *psDEPdata, char *pszNamedRef)
{
  int iRetCode          = ITK_ok;
  //int iArgs             = 0;

  tag_t tAttrId         = NULL_TAG;
  //tag_t tRequest        = NULL_TAG;

  //char *pszProvider     = NULL;
  //char *pszTranslator   = NULL;

  //char **pszArgs        = NULL;

  char szItemRevID[ITEM_id_size_c + 1];
  char szDSname[WSO_name_size_c + 1];

  /* If the Item Revision Tag is not yet available, try to extract it from the properties of the dataset */
  if (psDEPdata->tItemRev == NULLTAG)
  {
    iRetCode = PROP_ask_property_by_name(psDEPdata->tDataset, "item_revision", &tAttrId);
    if (iRetCode == 0)
    {
      iRetCode = PROP_ask_value_tag(tAttrId, &psDEPdata->tItemRev);
      if (BNL_em_error_handler("PROP_ask_value_tag", AH_Version,EMH_severity_error, iRetCode, true)) goto function_exit;
    }
  }

  /* If no reference to the owning ItemRevision is stored in the properties of the Dataset, skip this named reference */
  if (psDEPdata->tItemRev == NULLTAG)
  {
    fprintf(ugslog, "%s: Error, Dataset has no reference to the owning Item Revision. This named reference is skipped.\n", AH_Version);
    iRetCode = -1;
    goto function_exit;
  }

  /* Get itemrev_id = part_rev */
  iRetCode = ITEM_ask_rev_id(psDEPdata->tItemRev, szItemRevID);
  if (BNL_em_error_handler("ITEM_ask_rev_id", AH_Version, EMH_severity_error, iRetCode, true)) goto function_exit;

  /* Get dataset_name = Dataset */
  iRetCode = WSOM_ask_name(psDEPdata->tDataset, szDSname);
  if (BNL_em_error_handler("WSOM_ask_name", AH_Version, EMH_severity_error, iRetCode, true)) goto function_exit;

  if ((iRetCode = BNL_create_request(psDEPdata->tDataset, psDEPdata->tItemRev, pszNamedRef, psDEPdata)) != 0) goto function_exit;

  /*
  if (psDEPdata->pszProvider != NULL)
  {
    pszProvider = psDEPdata->pszProvider;
  }
  else
  {
    if (BNL_ini_read_ini_value(psDEPdata->pszIni, DEP_INI_SEC_PROVIDERS, pszNamedRef, &pszProvider) != 0)
    {
      if (BNL_ini_read_ini_value(psDEPdata->pszIni, DEP_INI_SEC_PROVIDERS, "default_provider", &pszProvider) != 0)
      {
        fprintf(ugslog, "Error: No default provider defined in ini file [%s].\n", psDEPdata->pszIni);

        iRetCode = BNL_display_confirmation + giErrorOffset;
        BNL_em_error_handler_s1("AH-dep-create-request", AH_Version, EMH_severity_information, BNL_display_confirmation + giErrorOffset, true, "Failed to find the provider.");

         goto function_exit;
      }
    }
  }

  if (psDEPdata->pszTranslator != NULL)
  {
    pszTranslator = psDEPdata->pszTranslator;
  }
  else
  {
    if (BNL_ini_read_ini_value(psDEPdata->pszIni, DEP_INI_SEC_TRANSLATORS, pszNamedRef, &pszTranslator) != 0)
    {
      if (BNL_ini_read_ini_value(psDEPdata->pszIni, DEP_INI_SEC_TRANSLATORS, "default_translator", &pszTranslator) != 0)
      {
        fprintf(ugslog, "Error: No default translator defined in ini file [%s].\n", psDEPdata->pszIni);

        iRetCode = BNL_display_confirmation + giErrorOffset;
        BNL_em_error_handler_s1("AH-dep-create-request", AH_Version, EMH_severity_information, BNL_display_confirmation + giErrorOffset, true, "Failed to find the translator.");

        goto function_exit;
      }
    }
  }

  fprintf(ugslog, "%s: Dispacther Request will be generated:\n", AH_Version);
  //fprintf(ugslog, "\tItem: [%s]\n", szItemID);
  fprintf(ugslog, "%s: ** ItemRevision [%s], dataset [%s], priority [%d], provider [%s] and translator [%s].\n",
    AH_Version, szItemRevID, szDSname, psDEPdata->iPriority, pszProvider, pszTranslator);

  // Add task puid
  if (psDEPdata->pszTaskPuid != NULL)
  {
    iArgs++;
    pszArgs = (char **)MEM_realloc(pszArgs, iArgs * sizeof(char *));
    pszArgs[iArgs - 1] = (char *) MEM_alloc((strlen(psDEPdata->pszTaskPuid) + strlen("task=") + 1) * sizeof(char));
    sprintf(pszArgs[iArgs - 1], "task=%s", psDEPdata->pszTaskPuid);
  }

  if (psDEPdata->pszLevel != NULL)
  {
    iArgs++;
    pszArgs = (char **)MEM_realloc(pszArgs, iArgs * sizeof(char *));
    pszArgs[iArgs - 1] = (char *) MEM_alloc((strlen(psDEPdata->pszLevel) + strlen("level=") + 1) * sizeof(char));
    sprintf(pszArgs[iArgs - 1], "level=%s", psDEPdata->pszLevel);
  }

  iRetCode = DISPATCHER_create_request(
    pszProvider,              // provider name
    pszTranslator,            // service name
    psDEPdata->iPriority,     // priority
    (const time_t)NULL,       // start time
    (const time_t)NULL,       // end time
    0,                        // interval
    1,                        // number of primary/secondary objects
    &psDEPdata->tDataset,     // primary objects
    &psDEPdata->tItemRev,     // secondary objects
    iArgs,                    // number of request arguments
    pszArgs,                  // request arguments
    NULL,                     // request type string
    0,                        // number of datafiles to attach
    NULL,                     // keys for data files to attach
    NULL,                     // data files to attach absolute path
    &tRequest);               // created request
  if (BNL_em_error_handler("DISPATCHER_create_request", AH_Version, EMH_severity_error, iRetCode, true)) goto function_exit;

  fprintf(ugslog, "%s: Dispatcher Request has successfully been generated.\n", AH_Version);
  psDEPdata->lTranslationRequestCreated = true;

  if (psDEPdata->pszLevel != NULL)
  {
    if ((iRetCode = BNL_tb_add_objects_to_task(psDEPdata->tRootTask, 1, &tRequest, EPM_reference_attachment)) != 0) goto function_exit;
  }*/

function_exit:

  /*if (psDEPdata->pszProvider == NULL) free(pszProvider);
  if (psDEPdata->pszTranslator == NULL) free(pszTranslator);
  if (iArgs > 0) BNL_mm_free_list((void *)pszArgs, iArgs);*/

  return iRetCode;
}


int BNL_lookup_item_revision(tag_t tDataset, t_DEP *psDEPdata, tag_t *ptRev)
{
  int iRetCode             = 0;
  int i                    = 0;
  int j                    = 0;
  int iCount               = 0;

  char szTargetClass[IMANTYPE_name_size_c+1];

  tag_t *ptPrimaryList     = NULL;

  tag_t tAttrId            = NULL_TAG;


  *ptRev = NULL_TAG;


  fprintf(ugslog, "%s: Checking if primary ItemRevision is also target.\n", AH_Version);

  iRetCode = GRM_list_primary_objects_only(tDataset, NULL_TAG, &iCount, &ptPrimaryList);
  if (BNL_em_error_handler("GRM_list_primary_objects",AH_Version,EMH_severity_error,iRetCode, true)) goto function_exit;

  for (i = 0; i < iCount; i++)
  {
    // Check if ItemRevision
    iRetCode = BNL_tb_determine_super_class(ptPrimaryList[i], szTargetClass);
    if (BNL_em_error_handler("BNL_tb_determine_super_class", AH_Version, EMH_severity_error, iRetCode, true)) goto function_exit;

    if (strcmp(szTargetClass, "ItemRevision") == 0)
    {
      // Check if target
      for (j = 0; j < psDEPdata->iNumTargets; j++)
      {
        if (ptPrimaryList[i] == psDEPdata->ptTargets[j])
        {
          *ptRev = ptPrimaryList[i];
          fprintf(ugslog, "%s: Found!.\n", AH_Version);
          goto function_exit;
        }
      }
    }
  }

  fprintf(ugslog, "%s: No matching ItemRevision has been found, using property item_revision.\n", AH_Version);

  iRetCode = PROP_ask_property_by_name(tDataset, "item_revision", &tAttrId);
  if (iRetCode == 0)
  {
    iRetCode = PROP_ask_value_tag(tAttrId, ptRev);
    if (BNL_em_error_handler("PROP_ask_value_tag", AH_Version, EMH_severity_error, iRetCode, true))goto function_exit;
  }
  else
  {
    EMH_clear_last_error(iRetCode);
    iRetCode = 0;
  }


function_exit:

  if (iCount > 0) MEM_free(ptPrimaryList);

  return iRetCode;
}


/**
static  int get_prop
(
  tag_t   tObject,    <I> object to get the property from
  char    *szGetName, <I> name of the property
  char    *pszDate,   <I> date format if property is date
  char    **pszVal    <O> value of the property
)

  Description:

  Get the value for a given property of a given object

  Returns:

  0 if ok

*/
static int get_prop
(
  tag_t   tObject,
  char    *szGetName,
  char    *pszDate,
  char    **pszVal
)
{
  int iRetCode     = ITK_ok;
  int iValues      = 0;
  tag_t tAttrId    = NULLTAG;
  tag_t tProp      = NULL_TAG;
  date_t sDate;
  char *pszProp    = NULL;
  char *pszStart   = NULL;
  char *pszLast    = NULL;
  tag_t *ptValues  = NULL;
  logical lUseLast = false;
  logical lNonLocalized = false;
  char *pszValType  = NULL;
  PROP_value_type_t valtype;
  tag_t tDescriptor = NULL_TAG;


  // Check if it ends with #
  if (szGetName[strlen(szGetName) - 1] == '#')
  {
    fprintf(ugslog, "Retrieving non-localized value.\n");

    lNonLocalized = true;
    szGetName[strlen(szGetName) - 1] = '\0';
  }

  if (strcmp(szGetName,"puid")==0)
  {
    iRetCode = POM_tag_to_string(tObject,pszVal);
  }
  else
  {
    pszStart = szGetName;

    while ((pszProp = strchr(pszStart, '.')) != NULL)
    {
      lUseLast = false;

      *pszProp = '\0';

      if ((pszLast = strchr(pszStart, '$')) != NULL)
      {
        *pszLast = '\0';  /* strip from var */
        pszLast++;
        lUseLast = true;
      }

      fprintf(ugslog, "Retrieving property [%s]\n", pszStart);

      PROP_ask_property_by_name(tObject, pszStart, &tProp);

      if (tProp != NULL_TAG)
      {
        PROP_ask_value_tags(tProp, &iValues, &ptValues);
      }

      if (iValues > 0)
      {
        if (lUseLast)
        {
          fprintf(ugslog, "Using last element of array.\n");
          tObject = ptValues[iValues - 1];
        }
        else
        {
          fprintf(ugslog, "Using first element of array.\n");
          tObject = ptValues[0];
        }

        MEM_free(ptValues);
        ptValues = NULL;
        iValues = 0;
      }
      else
      {
        tObject = NULL_TAG;
        break;
      }

      *pszProp = '.';
      pszStart = pszProp + 1;
    }

    fprintf(ugslog, "Reading property [%s]\n", pszStart);

    if (strcmp(pszStart, "puid") == 0)
    {
      iRetCode = POM_tag_to_string(tObject,pszVal);
    }
    else
    {
      iRetCode = PROP_ask_property_by_name(tObject, pszStart, &tAttrId);
      if (iRetCode != 0)
      {
        fprintf(ugslog, "Warning, failed to read property [%s].\n", szGetName);
        EMH_clear_errors();
      }
      else
      {
        /* LB|22-11-2001 if pszDate != NULL it contains de date format of the property */
                                        if (pszDate == NULL)
      {
        iRetCode = AOM_ask_descriptor(tObject, pszStart, &tDescriptor);
        if (BNL_em_error_handler("AOM_ask_descriptor", AH_Version, EMH_severity_error, iRetCode, true)) goto function_exit;

        iRetCode = PROPDESC_ask_value_type(tDescriptor, &valtype, &pszValType);
        if (BNL_em_error_handler("PROPDESC_ask_value_type", AH_Version, EMH_severity_error, iRetCode, true)) goto function_exit;

        if (valtype == PROP_string && lNonLocalized) // we don't want to used the localized string
        {
          iRetCode = AOM_ask_value_string(tObject, pszStart, pszVal);
          if (BNL_em_error_handler("PROPDESC_ask_value_type", AH_Version, EMH_severity_error, iRetCode, true)) goto function_exit;
        }
        else
        {
          iRetCode = PROP_UIF_ask_value(tAttrId, pszVal);
          if (BNL_em_error_handler("PROP_UIF_ask_value", AH_Version, EMH_severity_error, iRetCode, true)) goto function_exit;
        }
      }
        else
        {
          iRetCode = PROP_ask_value_date(tAttrId, &sDate);
          if (BNL_em_error_handler("PROP_ask_value_date", AH_Version, EMH_severity_error, iRetCode, true)) goto function_exit;

          iRetCode = DATE_date_to_string(sDate, pszDate, pszVal);
          if (BNL_em_error_handler("DATE_date_to_string",AH_Version,EMH_severity_error,iRetCode, true)) goto function_exit;
        }
      }
    }
  }

function_exit:

  if (iValues > 0) MEM_free(ptValues);
  if (pszValType != NULL) MEM_free(pszValType);

  return iRetCode;
}

static int get_master_form_value(tag_t tItemRev, char *pszVar, char **pVar)
{
  int iRetCode        = 0;
  int iRels           = 0;

  tag_t *ptRels       = NULL;

  tag_t tProp         = NULL_TAG;
  tag_t tRel          = NULL_TAG;


  iRetCode = GRM_find_relation_type("IMAN_master_form", &tRel);
  if (BNL_em_error_handler("GRM_find_relation_type", AH_Version, EMH_severity_error, iRetCode, true)) goto function_exit;

  iRetCode = GRM_list_secondary_objects_only(tItemRev, tRel, &iRels, &ptRels);
  if (BNL_em_error_handler("GRM_list_secondary_objects_only", AH_Version, EMH_severity_error, iRetCode, true)) goto function_exit;

  if (iRels > 0) /* only one Master Form should be found */
  {
    PROP_ask_property_by_name(ptRels[0], pszVar, &tProp);

    if (tProp == NULL_TAG)
    {
      EMH_clear_errors();
      fprintf( ugslog, "%s: Could not get value [%s] from Master Form\n", AH_Version, pszVar);
    }
    else
    {
      iRetCode = PROP_UIF_ask_value(tProp, pVar);
      if (BNL_em_error_handler("PROP_UIF_ask_value", AH_Version, EMH_severity_error, iRetCode, true)) goto function_exit;

      fprintf(ugslog, "%s: read formvalue [%s]=[%s].", AH_Version, pszVar, *pVar);
    }
  }

function_exit:

  if (iRels > 0) MEM_free(ptRels);

  return iRetCode;
}

/**
static int replace_var
(
    t_DEP *psDEPdata        <I>  structure with required information to produce CEP data
    char *pszVar            <I>  string containing the attribute/property name
    char **pszValue         <OF> pointer to allocated string containing the value
)

  Description:

    Replace the variable by the property with the name in the variable
    A variable has the form <propname> where propname is the name of the property
    To get the value of the property the objects are tried bottum up:
    Girst named reference, then dataset, then item revision, then item
    except when an object is forced with ns:, ds:, rv: or it: resp.

  Returns:

    0 if found

*/
static int replace_var
(
  t_DEP *psDEPdata,
  char *pszVar,
  char **pszValue
)
{
  int iRetCode    = ITK_ok;
  int iLen        = 0;
  char *pVar      = NULL;
  char *pVal      = NULL;
  char *pRef      = NULL;
  int iCount      = 0;
  int iValLen     = 0;
  char *s         = NULL;
  char *pDate     = NULL;
  char *pTemp     = NULL;
  char szObj[256];
  tag_t tObj      = NULLTAG;
  tag_t *ptObjs   = NULL;
  int iRefCount   = 0;
  int iRefType    = 0;
  char szRefName[AE_reference_size_c];
  char szClass[TCTYPE_name_size_c + 1];
  tag_t tTask     = NULLTAG;
  tag_t tJob      = NULLTAG;
  tag_t tRoot     = NULLTAG;

  ITEM_attached_object_t *psObjs     = NULL;

  /* format for date given? */
  strcpy( szObj, pszVar);
  if ((pDate = strchr(szObj,';')) != NULL)
  {
    *pDate = '\0';  /* strip from var */
    pDate++;
    fprintf(ugslog, "%s: Got a date format [%s] for attribute [%s].\n", AH_Version, pDate, szObj);
  }

  /* object selection forced? */
  if ((pVar = strchr(szObj,':')) != NULL)
  {
    *pVar = '\0';
    pVar++;

    fprintf(ugslog, "%s: Get attribute [%s] from forced object [%s].\n", AH_Version, pVar, szObj);

    /*
    To make it possible to give a nemadereferecne and if this refrence is form and get the attribute of this
    form this code structure is changed.

    nr field will look as follow:

    nr(namedrefname)

    */

    if (_strnicmp(szObj, "nr",2) == 0)
    {
      /*get the named reference object by given nr*/
      pRef = strchr(szObj, '(');
      pRef++;

      pTemp = strchr(pRef, ')');
      *pTemp = '\0';

      /*now we have the namedref name */
      fprintf(ugslog, "%s: Getting object of namedref [%s].\n", AH_Version, pRef);

      /*if there is a namedref there should be a dataset tag.*/
      if (psDEPdata->tDataset != NULLTAG)
      {
        /*Get the named refrences from the dataset and select the one out needed*/
        /*done because the ITK documentation says to do it*/
        strcpy(szRefName, pRef);

        iRetCode = AE_ask_all_dataset_named_refs(psDEPdata->tDataset, szRefName, &iRefCount, &ptObjs);
        if (BNL_em_error_handler("AE_ask_all_dataset_named_refs", AH_Version, EMH_severity_error, iRetCode, true)) goto function_exit;

        if (iRefCount <= 0)
        {
          fprintf(ugslog, "%s: Named reference [%s] not found in dataset.\n", AH_Version, szRefName);
          iRetCode = 1;
          goto function_exit;
        }
        tObj = ptObjs[0];
        MEM_free(ptObjs);
        iRefCount = 0;

        /*the tObj can be a form first try to get the value in the form. If not found then
          try to get it from the object itself */

        iRetCode = BNL_tb_determine_super_class(tObj,szClass);
        if (BNL_em_error_handler("BNL_tb_determine_super_class", AH_Version, EMH_severity_error, iRetCode, true)) goto function_exit;

        if (strcmp(szClass,"Form")==0)
        {
          fprintf(ugslog, "%s: Asking form value [%s].\n", AH_Version, pVar);

          iRetCode = BNL_tb_form_ask_value(tObj, pVar, &pVal);
          BNL_em_error_handler("BNL_tb_form_ask_value", AH_Version ,EMH_severity_error, iRetCode, false);
          iRetCode = 0;
        }

        if (pVal == NULL)
        {
          /*then try to get the property*/
          iRetCode = get_prop(tObj, pVar, pDate, &pVal);
        }
      }
      else
      {
        fprintf(ugslog, "%s: Cant get he the forced base object of the dataset.\n", AH_Version);
        iRetCode = 1;
        goto function_exit;
      }
    }
    else if (_stricmp(szObj, "ds") == 0)
      iRetCode = get_prop(psDEPdata->tDataset, pVar, pDate, &pVal);
    else if (_stricmp(szObj, "target") == 0)
      iRetCode = get_prop(psDEPdata->tCurrentTarget, pVar, pDate, &pVal);
    else if (_stricmp(szObj, "rv") == 0)
      iRetCode = get_prop(psDEPdata->tItemRev, pVar, pDate, &pVal);
    else if (_stricmp(szObj, "it") == 0)
      iRetCode = get_prop(psDEPdata->tItem, pVar, pDate, &pVal);
    else if (_stricmp(szObj, "irm" )== 0)
      iRetCode = get_master_form_value(psDEPdata->tItemRev, pVar ,&pVal);
    else if (_stricmp(szObj, "job") == 0)
    {
      POM_string_to_tag(psDEPdata->pszTaskPuid, &tTask);
      EPM_ask_job(tTask, &tJob);
      iRetCode = get_prop(tJob, pVar, pDate, &pVal);
    }
    else if (_stricmp(szObj, "task") == 0)
    {
      POM_string_to_tag(psDEPdata->pszTaskPuid,&tTask);
      iRetCode = get_prop(tTask, pVar, pDate, &pVal);
    }
    else if (_stricmp(szObj, "roottask") == 0)
    {
      POM_string_to_tag(psDEPdata->pszTaskPuid, &tTask);
      EPM_ask_root_task(tTask, &tRoot);
      iRetCode = get_prop(tRoot, pVar, pDate, &pVal);
    }
    else if (_stricmp(szObj, "fixed") == 0)
    {
      BNL_tb_replace_all_env_for_path(pVar, &pVal);
      if (pVal == NULL)
      {
        pVal = MEM_alloc(sizeof(char) * (int)strlen(pVar) + 1);
        strcpy(pVal, pVar);
      }
    }
    else if (_stricmp(szObj, "pref") == 0)
    {
      // First check for preference %pVAR%
      BNL_tb_pref_ask_char_value(pVar, IMAN_preference_all, &pVal);

      if (pVal == NULL)
      {
        pVal = MEM_alloc(sizeof(char)*(int)strlen(pVar)+1);
        strcpy(pVal, pVar);
      }
    }
    else
    {
      fprintf( ugslog, "%s: Unknown forced object [%s].\n", AH_Version, szObj);
      iRetCode = 1;
      goto function_exit;
    }
    if (iRetCode != 0)
    {
      EMH_clear_last_error(iRetCode);
      return iRetCode;
    }
  }
  else    /* no object forced: try them all */
  {
    /* in discussion this will may be obsolete*/

    fprintf(ugslog, "%s: Find attribute [%s] from object tree: nr", AH_Version, pszVar);
    /* find the attribute: first try the named reference object */
    iRetCode = get_prop(psDEPdata->tNamedRef, pszVar, pDate, &pVal);
    if (iRetCode != 0)
    {
      /* not found? try the dataset object */
      EMH_clear_last_error( iRetCode);
      iRetCode = 0;
      fprintf(ugslog, ",ds");
      iRetCode = get_prop(psDEPdata->tDataset, pszVar, pDate, &pVal);
      if (iRetCode != 0)
      {
        /* still not found? try the item revision object */
        EMH_clear_last_error( iRetCode);
        iRetCode = 0;
        fprintf(ugslog, ",rv");
        iRetCode = get_prop(psDEPdata->tItemRev, pszVar, pDate, &pVal);
        if (iRetCode != 0)
        {
          /* still not found? try the item object */
          EMH_clear_last_error( iRetCode);
          iRetCode = 0;
          fprintf(ugslog, ",it");
          iRetCode = get_prop(psDEPdata->tItem, pszVar, pDate, &pVal);
          /* if still not found: quit */
          if (iRetCode != 0)
          {
            /* still not found? try the Item Revision Master */
            EMH_clear_last_error( iRetCode);
            iRetCode = 0;
            fprintf(ugslog, ",irm\n");
            iRetCode = get_master_form_value(psDEPdata->tItemRev, pVar ,&pVal);
          }
        }
      }
    }
    fprintf( ugslog, "\n");
  }

function_exit:

  if (iRefCount > 0) MEM_free(ptObjs);

  *pszValue = pVal;

  return 0;
}


/*
static int replace_var_list
(
  t_DEP   *psDEPdata   <I>  structure with required information to produce DEP data
  char *pszVarList     <I>  string containing the attribute/property names
  char **pszValue      <OF> pointer to allocated string containing the value
)
  More than one attribute definition can be given. Definitions are separated
  by "|" if no value found, continue with next definition
*/

static int replace_var_list
(
  t_DEP *psDEPdata,
  char *pszVarList,
  char **pszValue
)
{
  char *pszDel    = NULL;
  char *pszStart  = NULL;
  int iRetCode    = 0;


  pszStart = pszVarList;

  while ((pszDel = strchr(pszStart, '|')) != NULL)
  {
    *pszDel = '\0';
    /* get the value, if any */
    iRetCode = replace_var(psDEPdata, pszStart, pszValue);
    if (iRetCode == 0 && *pszValue != NULL)
      return iRetCode;

    *pszDel = '|';
    pszStart = pszDel + 1;
  }

  /* get the last value */
  return replace_var(psDEPdata, pszStart, pszValue);
}


int BNL_replace_enters(char *pszValue)
{
  int iRetCode   = 0;
  char *pszNode  = NULL;

  /* tc props can contain enters these are newline signs
    so far known there are no cr /13 of lf's /10 in the game*/
  pszNode = strchr(pszValue, '\n');
  while (pszNode != NULL)
  {
    *pszNode = ' ';
    pszNode = strchr(pszNode,' \n');
  }
  return iRetCode;
}


int BNL_create_request(tag_t tPrimary, tag_t tSecondary, char *pszType, t_DEP *psDEPdata)
{
  int iRetCode            = 0;
  int iArgs               = 0;
  int i                   = 0;

  char *pszProvider       = NULL_TAG;
  char *pszTranslator     = NULL_TAG;
  char *pszValue          = NULL;

  char **pszArgs          = NULL;

  tag_t tRequest          = NULL_TAG;


  if (psDEPdata->pszProvider != NULL)
  {
    pszProvider = psDEPdata->pszProvider;
  }
  else
  {
    if (BNL_ini_read_ini_value(psDEPdata->pszIni, DEP_INI_SEC_PROVIDERS, pszType, &pszProvider) != 0)
    {
      if (BNL_ini_read_ini_value(psDEPdata->pszIni, DEP_INI_SEC_PROVIDERS, "default_provider", &pszProvider) != 0)
      {
        fprintf(ugslog, "Error: No default provider defined in ini file [%s].\n", psDEPdata->pszIni);

        iRetCode = BNL_display_confirmation + giErrorOffset;
        BNL_em_error_handler_s1("AH-dep-create-request", AH_Version, EMH_severity_information, BNL_display_confirmation + giErrorOffset, true, "Failed to find the provider.");

         goto function_exit;
      }
    }
  }

  if (psDEPdata->pszTranslator != NULL)
  {
    pszTranslator = psDEPdata->pszTranslator;
  }
  else
  {
    if (BNL_ini_read_ini_value(psDEPdata->pszIni, DEP_INI_SEC_TRANSLATORS, pszType, &pszTranslator) != 0)
    {
      if (BNL_ini_read_ini_value(psDEPdata->pszIni, DEP_INI_SEC_TRANSLATORS, "default_translator", &pszTranslator) != 0)
      {
        fprintf(ugslog, "Error: No default translator defined in ini file [%s].\n", psDEPdata->pszIni);

        iRetCode = BNL_display_confirmation + giErrorOffset;
        BNL_em_error_handler_s1("AH-dep-create-request", AH_Version, EMH_severity_information, BNL_display_confirmation + giErrorOffset, true, "Failed to find the translator.");

        goto function_exit;
      }
    }
  }

  // Add task puid
  if (psDEPdata->pszTaskPuid != NULL)
  {
    iArgs++;
    pszArgs = (char **)MEM_realloc(pszArgs, iArgs * sizeof(char *));
    pszArgs[iArgs - 1] = (char *) MEM_alloc(((int)strlen(psDEPdata->pszTaskPuid) + (int)strlen("task=") + 1) * sizeof(char));
    sprintf(pszArgs[iArgs - 1], "task=%s", psDEPdata->pszTaskPuid);
  }

  if (psDEPdata->pszLevel != NULL)
  {
    iArgs++;
    pszArgs = (char **)MEM_realloc(pszArgs, iArgs * sizeof(char *));
    pszArgs[iArgs - 1] = (char *) MEM_alloc(((int)strlen(psDEPdata->pszLevel) + (int)strlen("level=") + 1) * sizeof(char));
    sprintf(pszArgs[iArgs - 1], "level=%s", psDEPdata->pszLevel);
  }

  if (psDEPdata->pszLevel != NULL && psDEPdata->pszTaskPuid != NULL)
  {
    if ((iRetCode = BNL_dep_check_for_request(psDEPdata->tRootTask, pszProvider, pszTranslator,
      psDEPdata->pszTaskPuid, psDEPdata->pszLevel, tPrimary, tSecondary, &tRequest)) != 0) goto function_exit;

    if (tRequest != NULL_TAG)
    {
      fprintf(ugslog, "%s: Existing request will be removed as reference attachment.\n", AH_Version);

      iRetCode = EPM_remove_attachments(psDEPdata->tRootTask, 1, &tRequest);
      if (BNL_em_error_handler("EPM_remove_attachments", AH_Version, EMH_severity_error, iRetCode, true)) goto function_exit;
    }
  }

  if (psDEPdata->pszIni != NULL)
  {
    // Check for extra arguments
    if (BNL_ini_read_ini_array(psDEPdata->pszIni, DEP_INI_SEC_EXTRA_ARGS, &psDEPdata->iExtraArgsCount, &psDEPdata->ppsExtraArgs) != 0)
    {
      fprintf(ugslog, "%s: No extra arguments have been defined in ini file [%s].\n", AH_Version, psDEPdata->pszIni);
    }

    for (i = 0; i < psDEPdata->iExtraArgsCount; i++)
    {
      //psDEPdata->ppsExtraArgs[i]->pszValue
      //psDEPdata->ppsExtraArgs[i]->pszKey
      iArgs++;
      pszArgs = (char **)MEM_realloc(pszArgs, iArgs * sizeof(char *));

      iRetCode = replace_var_list(psDEPdata, psDEPdata->ppsExtraArgs[i]->pszValue, &pszValue);
      if (iRetCode == 0 && pszValue != NULL)
      {
        /*check value on enters because they do corrupt the csv file replace them by spaces*/
        BNL_replace_enters(pszValue);

        pszArgs[iArgs - 1] = (char *) MEM_alloc(((int)strlen(psDEPdata->ppsExtraArgs[i]->pszKey) + (int)strlen(pszValue) + 2) * sizeof(char));
        sprintf(pszArgs[iArgs - 1], "%s=%s", psDEPdata->ppsExtraArgs[i]->pszKey, pszValue);

        MEM_free(pszValue);
        pszValue = NULL;
      }
      else
      {
        pszArgs[iArgs - 1] = (char *) MEM_alloc(((int)strlen(psDEPdata->ppsExtraArgs[i]->pszKey) + 2) * sizeof(char));
        sprintf(pszArgs[iArgs - 1], "%s=", psDEPdata->ppsExtraArgs[i]->pszKey);
      }
    }
  }

  if (tSecondary == NULL_TAG)
  {
    iRetCode = DISPATCHER_create_request(
      pszProvider,              // provider name
      pszTranslator,            // service name
      psDEPdata->iPriority,     // priority
      (const time_t)NULL,       // start time
      (const time_t)NULL,       // end time
      0,                        // interval
      1,                        // number of primary/secondary objects
      &tPrimary,                // primary objects
      NULL,                     // secondary objects
      iArgs,                    // number of request arguments
      pszArgs,                  // request arguments
      NULL,                     // request type string
      0,                        // number of datafiles to attach
      NULL,                     // keys for data files to attach
      NULL,                     // data files to attach absolute path
      &tRequest);               // created request
  }
  else
  {
    iRetCode = DISPATCHER_create_request(
      pszProvider,              // provider name
      pszTranslator,            // service name
      psDEPdata->iPriority,     // priority
      (const time_t)NULL,       // start time
      (const time_t)NULL,       // end time
      0,                        // interval
      1,                        // number of primary/secondary objects
      &tPrimary,                // primary objects
      &tSecondary,              // secondary objects
      iArgs,                    // number of request arguments
      pszArgs,                  // request arguments
      NULL,                     // request type string
      0,                        // number of datafiles to attach
      NULL,                     // keys for data files to attach
      NULL,                     // data files to attach absolute path
      &tRequest);               // created request
  }
  if (BNL_em_error_handler("DISPATCHER_create_request", AH_Version, EMH_severity_error, iRetCode, true)) goto function_exit;


  fprintf(ugslog, "%s: Dispatcher Request has successfully been generated.\n", AH_Version);
  psDEPdata->lTranslationRequestCreated = true;

  if (psDEPdata->pszLevel != NULL)
  {
    if ((iRetCode = BNL_tb_add_objects_to_task(psDEPdata->tRootTask, 1, &tRequest, EPM_reference_attachment)) != 0) goto function_exit;
  }

function_exit:

  if (psDEPdata->pszProvider == NULL) free(pszProvider);
  if (psDEPdata->pszTranslator == NULL) free(pszTranslator);
  if (iArgs > 0) BNL_mm_free_list((void *)pszArgs, iArgs);
  if (pszValue != NULL) MEM_free(pszValue);

  return iRetCode;

}
