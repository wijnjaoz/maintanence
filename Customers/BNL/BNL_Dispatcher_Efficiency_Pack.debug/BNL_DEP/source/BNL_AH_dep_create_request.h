/**
 (C) copyright by Siemens PLM Software

 Developed for IMAN
 Versions     OS                          platform
 -----------------------------------------------------------------------------------------------------------
 5.x          Microsoft Windows NT        Intel


 Description    :   Header file that belongs to the handler

 Created for    :   Siemens PLM Software

 Comments       :   Every new procedure or function starts with BNL_

 Filename       :   BNL_AH_dep_create_request.h

 Version info   :   The number consist of the iman release and then the release of the handler
                    <handlerrelease>.<pointrelease>


 History of changes
 reason /description                                    Version By      date
 ----------------------------------------------------------------------------------------------------------
 Creation                                               0.1     JM      06-09-2011

*/

#include <BNL_ini_module.h>


/* Data structure to share required data between the routines */
typedef struct s_DEP
{
  char      *pszIni;
  char      *pszTranslator;
  char      *pszProvider;
  char      **pszNamedRefs;
  char      **pszDStypes;
  char      **pszObjectsTypes;
  char      *pszNoteType;
  char      *pszNote;
  int       iPriority;
  int       iNamedRefCount;
  int       iDStypesCount;
  int       iObjectTypes;
  int       iProcessOneLine;
  int       iExtraArgsCount;
  inivaluepair_t **ppsExtraArgs;
  char      *pszListName;
  char      *pszBomview;
  char      *pszRevisionRule;
  int       iBomLevel;
  int       iExcludeNote;
  tag_t     tCurrentTarget;
  tag_t     tItem;
  tag_t     tItemRev;
  tag_t     tDataset;
  tag_t     tNamedRef;
  tag_t     tRootTask;
  logical   lRecursive;
  logical   lUseBomView;
  logical   lIgnoreUnconfigBom;
  logical   lIsOccOfBomView;
  logical   lTranslationRequestCreated;
  int       iNumTargets;
  tag_t     *ptTargets;
  char      *pszLevel;
  char      *pszTaskPuid;
} t_DEP;


extern int BNL_Register_AH_dep_create_request(void);
extern int process_target(t_DEP *psDEPdata, tag_t tObject);
extern int create_translation_requests(tag_t tJob, t_DEP *psDEPdata);

