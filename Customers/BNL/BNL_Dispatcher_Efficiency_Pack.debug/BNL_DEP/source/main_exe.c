/*=============================================================================
                Copyright (c) 2010 Siemens PLM Software
                   Unpublished - All Rights Reserved
===============================================================================
File description:

    Filename: HelloTc.c
    Module  : TRN
  Author  : S Susewitt
  Date  : Oct 14, 2010

===============================================================================*/

#include <tcinit/tcinit.h>
#include <tc/tc_startup.h>
#include <tc/emh.h>


#include <BNL_tb_module.h>
#include <BNL_fm_module.h>
#include <BNL_em_module.h>
#include <BNL_ini_module.h>
#include <BNL_errors.h>
#include <BNL_register_bnllibs.h>

#include <BNL_AH_dep_create_request.h>

static char AH_Version[] = "derek 0.1";
static char BNL_module[] = "derek 0.1";
static char DLL_module[] = "derek_exec";
/*because the length of filename is trucated we can not use dllmodule as name :-( */
char DLL_logfilename[] = "exec_log";
FILE *gpfBnlLog = NULL;

int BNL_AH_dep_create_request_tst()
{
  int iRetCode                     = ITK_ok;

  char *pszIni = "D:\\aCustomer\\FEI\\BNL_DEP_TOOL_TC101\\tool\\config\\bnl_dep.ini";
  char *pszdataset_types = "PDF";
  //char *pszobject_types = "ItemRevision";
  char *pszobject_types = "";
  char *pszProvider = "BNL";
  char *pszTranslator = "bnlreport";
  char *psznamed_references = "PDF_Reference";
  char *pszIniSection = "Request";
  char *pszTemp = NULL;

  tag_t tObject     =NULLTAG;


  t_DEP sDEPdata;

  /* Preset variables */
  sDEPdata.iBomLevel = 0;
  sDEPdata.iDStypesCount = 0;
  sDEPdata.iExcludeNote = 0;
  sDEPdata.iExtraArgsCount = 0;
  sDEPdata.iNamedRefCount= 0;
  sDEPdata.iNumTargets = 0;
  sDEPdata.iObjectTypes = 0;
  sDEPdata.iPriority = 0;
  sDEPdata.iProcessOneLine = 0;

  sDEPdata.lIgnoreUnconfigBom = false;
  sDEPdata.lIsOccOfBomView = false;
  sDEPdata.lRecursive = true; // default
  sDEPdata.lTranslationRequestCreated = false;
  sDEPdata.lUseBomView = false;

  sDEPdata.ppsExtraArgs = NULL;

  sDEPdata.pszBomview = NULL;
  sDEPdata.pszDStypes = NULL;
  sDEPdata.pszIni = NULL;
  sDEPdata.pszLevel = NULL;
  sDEPdata.pszListName = NULL;
  sDEPdata.pszNamedRefs = NULL;
  sDEPdata.pszNote = NULL;
  sDEPdata.pszNoteType = NULL;
  sDEPdata.pszObjectsTypes = NULL;
  sDEPdata.pszProvider = NULL;
  sDEPdata.pszRevisionRule = NULL;
  sDEPdata.pszTaskPuid = NULL;
  sDEPdata.pszTranslator = NULL;

  sDEPdata.ptTargets = NULL;

  sDEPdata.tDataset = NULL_TAG;
  sDEPdata.tItem = NULL_TAG;
  sDEPdata.tItemRev = NULL_TAG;
  sDEPdata.tNamedRef = NULL_TAG;
  sDEPdata.tRootTask = NULL_TAG;

  sDEPdata.pszIni = (char *)MEM_alloc(sizeof(char)*(int)strlen(pszIni)+1);
  strcpy(sDEPdata.pszIni, pszIni);
  // Read [Request] section
  BNL_ini_read_ini_value(sDEPdata.pszIni, pszIniSection, "-dataset_types", &pszTemp);
  if (pszTemp != NULL) {
  ITK_CALL(BNL_tb_parse_multiple_values(pszTemp, ';', &sDEPdata.iDStypesCount, &sDEPdata.pszDStypes));
  free(pszTemp); pszTemp = NULL;
  }
  BNL_ini_read_ini_value(sDEPdata.pszIni, pszIniSection, "-named_references", &pszTemp);
  if (pszTemp != NULL) {
  ITK_CALL(BNL_tb_parse_multiple_values(pszTemp, ';', &sDEPdata.iNamedRefCount, &sDEPdata.pszNamedRefs));
  free(pszTemp); pszTemp = NULL;
  }
  BNL_ini_read_ini_value(sDEPdata.pszIni, pszIniSection, "-object_types", &pszTemp);
  if (pszTemp != NULL) {
  ITK_CALL(BNL_tb_parse_multiple_values(pszTemp, ';', &sDEPdata.iObjectTypes, &sDEPdata.pszObjectsTypes));
  free(pszTemp); pszTemp = NULL;
  }
  BNL_ini_read_ini_value(sDEPdata.pszIni, pszIniSection, "-translator", &sDEPdata.pszTranslator);
  BNL_ini_read_ini_value(sDEPdata.pszIni, pszIniSection, "-provider", &sDEPdata.pszProvider);
  BNL_ini_read_ini_value(sDEPdata.pszIni, pszIniSection, "-priority", &pszTemp);
  if (pszTemp != NULL) {
  sDEPdata.iPriority = atoi(pszTemp);
  if (sDEPdata.iPriority <= 0 || sDEPdata.iPriority > 3)
  {
    sDEPdata.iPriority = 3;
  }
  free(pszTemp); pszTemp = NULL;
  }
  BNL_ini_read_ini_value(sDEPdata.pszIni, pszIniSection, "-recursive", &pszTemp);
  if (pszTemp != NULL) {
    if (_stricmp(pszTemp, "false") == 0)
      sDEPdata.lRecursive = false;
    else
      sDEPdata.lRecursive = true;
  free(pszTemp); pszTemp = NULL;
  }
  BNL_ini_read_ini_value(sDEPdata.pszIni, pszIniSection, "-listname", &sDEPdata.pszListName);
  BNL_ini_read_ini_value(sDEPdata.pszIni, pszIniSection, "-oneline_process", &pszTemp);
  if (pszTemp != NULL) {
    if (_stricmp(pszTemp, "true") == 0)
      sDEPdata.iProcessOneLine = 1;
    else
      sDEPdata.iProcessOneLine = 0;
  free(pszTemp); pszTemp = NULL;
  }


  BNL_ini_read_ini_value(sDEPdata.pszIni, pszIniSection, "-bomviewtype", &sDEPdata.pszBomview);
  if (sDEPdata.pszBomview != NULL) {
    sDEPdata.lUseBomView = true;
  }
  if (sDEPdata.lUseBomView)
  {
    BNL_ini_read_ini_value(sDEPdata.pszIni, pszIniSection, "-revrule", &sDEPdata.pszRevisionRule);
    BNL_ini_read_ini_value(sDEPdata.pszIni, pszIniSection, "-leveldepth", &pszTemp);
    if (pszTemp != NULL) {
      sDEPdata.iBomLevel = atoi(pszTemp);
      free(pszTemp); pszTemp = NULL;
    }
    BNL_ini_read_ini_value(sDEPdata.pszIni, pszIniSection, "-excludednote", &pszTemp);
    if (pszTemp != NULL) {
      char *pszTemp2 = NULL;
      sDEPdata.iExcludeNote = 1;

      pszTemp2 = strchr(pszTemp, ':');
      if (pszTemp2 == NULL)
      {
        fprintf(ugslog, "%s: Error the excludednote parameter value does not have the right format like notetype:notevalue.\n", AH_Version);
        goto handler_exit;
      }

      *pszTemp2 = '\0';
      BNL_tb_copy_string(pszTemp, &sDEPdata.pszNoteType);

      pszTemp2++;
      BNL_tb_copy_string(pszTemp2, &sDEPdata.pszNote);
      free(pszTemp); pszTemp = NULL;
    }
  }

  /* Initialize FOLDER module */
  iRetCode = FL_init_module();
  if (BNL_em_error_handler("FL_init_module", AH_Version, EMH_severity_error, iRetCode, true)) goto handler_exit;

  /* Initialize AE module */
  iRetCode = AE_init_module();
  if (BNL_em_error_handler("AE_init_module", AH_Version, EMH_severity_error, iRetCode, true)) goto handler_exit;

  if (0)
  {
    tag_t tJob                  = NULL_TAG;
    tag_t tRootTask             = NULL_TAG;
    char *pszTaskUid = "A1KAAAn2I660SC";

    ITK_CALL(POM_string_to_tag(pszTaskUid, &tRootTask));
    ITK_CALL(EPM_ask_job(tRootTask, &tJob));

    ITK_CALL(create_translation_requests(tJob, &sDEPdata));
  }
  else
  {
    if (0)
    {
    char *pszTaskUid = "gVOAAAXkI660SC"; // 000557/A <- PDF-dataset attached to 000558/A-GOSS
    ITK_CALL(POM_string_to_tag(pszTaskUid, &tObject));
    }
    else
    {
    ITK_CALL(ITEM_find_rev("000555","A",&tObject));
    ITK_CALL(ITEM_find_rev("000559","A",&tObject));
    }
  if (tObject==NULL_TAG)
  {
    fprintf(gpfBnlLog,"%s: tObject==NULL_TAG exit.\n", AH_Version);
    goto CLEANUP;
  }
    ITK_CALL(process_target(&sDEPdata, tObject))
  }
CLEANUP:
handler_exit:
  if (sDEPdata.pszIni != NULL) MEM_free(sDEPdata.pszIni);
  if (sDEPdata.iDStypesCount > 0) BNL_mm_free_list((void *)sDEPdata.pszDStypes, sDEPdata.iDStypesCount);
  if (sDEPdata.iNamedRefCount > 0) BNL_mm_free_list((void *)sDEPdata.pszNamedRefs, sDEPdata.iNamedRefCount);
  if (sDEPdata.iObjectTypes > 0) BNL_mm_free_list((void *)sDEPdata.pszObjectsTypes, sDEPdata.iObjectTypes);
  if (sDEPdata.pszTranslator != NULL) MEM_free(sDEPdata.pszTranslator);
  if (sDEPdata.pszProvider != NULL) MEM_free(sDEPdata.pszProvider);
  if (sDEPdata.pszListName != NULL) MEM_free(sDEPdata.pszListName);
  if (sDEPdata.pszBomview != NULL) MEM_free(sDEPdata.pszBomview);
  if (sDEPdata.pszRevisionRule != NULL) MEM_free(sDEPdata.pszRevisionRule);
  if (sDEPdata.pszNote != NULL) MEM_free(sDEPdata.pszNote);
  if (sDEPdata.pszNoteType != NULL) MEM_free(sDEPdata.pszNoteType);
  if (sDEPdata.pszLevel != NULL) MEM_free(sDEPdata.pszLevel);
  if (sDEPdata.pszTaskPuid != NULL) MEM_free(sDEPdata.pszTaskPuid);
  BNL_ini_free_ini_array(sDEPdata.iExtraArgsCount, sDEPdata.ppsExtraArgs);

  return iRetCode;
} /* End of BNL_AH_dep_create_request */


int ITK_user_main(int argc, char* argv[])
{
  int status;

  ITK_set_journalling(TRUE);
  ITK_initialize_text_services (0);

  printf("\nAttempting auto login to Teamcenter...\n");

  status = ITK_auto_login ();
  if (status != ITK_ok )
  {
    int       index = 0;
    int       n_ifails = 0;
    const int*    severities = 0;
    const int*    ifails = 0;
    const char**  texts = NULL;

    EMH_ask_errors( &n_ifails, &severities, &ifails, &texts);
    printf("%3d error(s) with ITK_auto_login\n", n_ifails);
    for( index=0; index<n_ifails; index++)
    {
      printf("\tError #%d, %s\n", ifails[index], texts[index]);
    }
    return status;
  }
  else
  {
    printf("\nLogin Successful.\n");
    printf("\nHello Teamcenter !!!\n");
  }

  /*****************************************************************
   * BNL_register_bnllibs *
   *****************************************************************/
  BNL_register_bnllibs(BNL_module, BNL_module, DLL_logfilename, &gpfBnlLog);
gpfBnlLog=stderr;
  /* Initialize the modules */
  BNL_mm_initialise(gpfBnlLog);
  BNL_em_initialise(gpfBnlLog);
  BNL_fm_initialise(gpfBnlLog);
  BNL_tb_initialise(gpfBnlLog);

  BNL_AH_dep_create_request_tst();

  ITK_exit_module(TRUE);

  return status;
}

