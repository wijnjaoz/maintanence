/**
 (C) copyright by Siemens PLM Software

 Developed for IMAN 
 Versions     OS                          platform
 -----------------------------------------------------------------------------------------------------------
 5.x          Microsoft Windows NT        Intel 
 
 
 Description    :   Header file for DEP module

 Created for    :   Siemens PLM Software

 Comments       :   Every new procedure or function starts with BNL_
                    
 Filename       :   BNL_dep_module.h

 Version info   :   The number consist of the iman release and then the release of the handler
                    <handlerrelease>.<pointrelease>

                    
 History of changes                                         
 reason /description                                    Version By      date
 ----------------------------------------------------------------------------------------------------------
 Creation                                               0.1     JM      06-09-2011

*/


int BNL_dep_initialise(FILE *pLog);

int BNL_dep_exit_module();

int BNL_dep_save_request_pm(METHOD_message_t *pmMessage, va_list args);

int BNL_dep_check_for_request(tag_t tRootTask, char *pszProviderName, char *pszServiceName, char *pszTaskPuid, char *pszLevel, tag_t tPrimary, tag_t tSecondary, tag_t *ptRequest);