/**
 (C) copyright by Siemens PLM Software


 Description  :   This dll provides the customizations for BNL Dispatcher Efficiency Pack

 Created for  :   Siemens PLM Software

 Comments     :   Every new procedure or function in this modulesstarts
                    with BNL_<module>_

 Filename     :   BNL_register_dep.h

 Version info :   release.pointrelease (versions of the modules are stored in the module itself).

 History of changes
 Reason / Description                                       Version By          Date
 ----------------------------------------------------------------------------------------------------------
 Creation                                                   0.1     JM          05-11-2011
 Updated for TC10.1                                         0.2     JM         29-08-2013
 

*/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifdef  TC10_1
#include <tc/iman.h>
#include <user_exits/user_exits.h>
#include <server_exits/user_server_exits.h>
#include <tccore/custom.h>
#else
#include <iman.h>
#include <ict_userservice.h>
#include <custom.h>
#include <user_exits.h>
#include <user_server_exits.h>
#include <iman_msg.h>
#include <method.h>
#endif

#include <BNL_fm_module.h>
#include <BNL_tb_module.h>
#include <BNL_em_module.h>
#include <BNL_register_bnllibs.h>

#include <BNL_dep_module.h>
#include <BNL_AH_dep_create_request.h>


char BNL_module[] ="BNL DEP 0.2";
char DLL_module[] ="BNL_DEP";
/*because the length of filename is trucated we can not use dllmodule as name :-( */
char DLL_logfilename[] = "BNLDEP";


DLLAPI int BNL_register_bnl_dep(int *decision, va_list args);
DLLAPI int BNL_exit_bnl_dep(int *decision, va_list args);


void welcome()
{
  if (giErrorLevel<10)
  {
    fprintf(stdout, "******************************\n");
    fprintf(stdout, "Copyright Siemens PLM Software BENELUX 2011-2014\n");
    fprintf(stdout, "Version %s registered\n", BNL_module);
    fprintf(stdout, "******************************\n");
  }

  IMAN_write_syslog("******************************\n");
  IMAN_write_syslog("Copyright Siemens PLM Software BENELUX 2011-2014\n");
  IMAN_write_syslog("Version %s registered\n", BNL_module);
  IMAN_write_syslog("******************************\n");
}


/*
DLLAPI int BNL_DEP_register_callbacks()

  Description:

  The dll entry called.

*/

DLLAPI int BNL_DEP_register_callbacks()
{
  int iRetCode        = 0;

  /*will register the modules and will open central logfile (ugslog)*/
	BNL_register_bnllibs(DLL_module, BNL_module, DLL_logfilename, &ugslog);

  welcome();

  CUSTOM_register_exit(DLL_module, "USER_init_module", (CUSTOM_EXIT_ftn_t)BNL_register_bnl_dep);
  CUSTOM_register_exit(DLL_module, "USER_exit_module", (CUSTOM_EXIT_ftn_t)BNL_exit_bnl_dep);

  return iRetCode;
}


/**
DLLAPI int BNL_register_bnl_dep(int *decision, va_list args)

  Description:

  Registers the BNL_library and open a procedure logfile.


*/
DLLAPI int BNL_register_bnl_dep(int *decision, va_list args)
{
  int iRetCode    = 0;

  METHOD_id_t method;

  *decision = ALL_CUSTOMIZATIONS;

  BNL_dep_initialise(ugslog);

  // Register the handler
  BNL_Register_AH_dep_create_request();

  iRetCode = METHOD_find_prop_method("DispatcherRequest", "currentState", PROP_set_value_string_msg, &method);
  if (BNL_em_error_handler("METHOD_find_prop_method", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

  if (method.id != 0)
  {
    iRetCode = METHOD_add_action(method, METHOD_post_action_type, BNL_dep_save_request_pm, NULL);
    if (BNL_em_error_handler("METHOD_add_action", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    fprintf(ugslog, "%s: Post action for DispatcherRequest has been reqistered.\n", BNL_module); 
  }

  fprintf(ugslog, "\n"); 

  return iRetCode;
}

/**
DLLAPI int BNL_exit_bnl_hndp(int *decision, va_list args)

  Description:

  Exit all modules and close the logfile.


*/
DLLAPI int BNL_exit_bnl_dep(int *decision, va_list args)
{
  int iRetCode    = 0;


  *decision = ALL_CUSTOMIZATIONS;

  
  BNL_dep_exit_module();

  // Will close all modules and close the logfile
  BNL_exit_bnllibs();

  return iRetCode;

}