@ECHO OFF
TITLE Import Preferences
COLOR F0

::setting required variables
set TC_USER_ID=infodba
set TC_USER_PASSWORD=infodba


::storing current directory in variable
set KIT_DIR=%~dp0
FOR %%g in ("%KIT_DIR%") do SET KIT_DIR=%%~fsg
ECHO.
ECHO Running from current directory = %KIT_DIR%


ECHO.
ECHO ****************************
ECHO Importing Preferences
preferences_manager -u=%TC_USER_ID% -p=%TC_USER_PASSWORD% -g=dba -mode=import -action=override -scope=site -file=%KIT_DIR%\kverneland.xml
::preferences_manager -u=%TC_USER_ID% -p=%TC_USER_PASSWORD% -g=dba -mode=import -action=override -scope=group -target="Configuration Management.Sonaca" -file=%KIT_DIR%\Configuration_Management_Sonaca_group_preference.xml
::preferences_manager -u=%TC_USER_ID% -p=%TC_USER_PASSWORD% -g=dba -mode=import -action=override -scope=group -target="dba" -file=%KIT_DIR%\dba_group_preference.xml
::preferences_manager -u=%TC_USER_ID% -p=%TC_USER_PASSWORD% -g=dba -mode=import -action=override -scope=group -target="Engineering.Sonaca" -file=%KIT_DIR%\Engineering_Sonaca_group_preference.xml  
::preferences_manager -u=%TC_USER_ID% -p=%TC_USER_PASSWORD% -g=dba -mode=import -action=override -scope=group -target="Manufacturing.Sonaca" -file=%KIT_DIR%\Manufacturing_Sonaca_group_preference.xml   
::preferences_manager -u=%TC_USER_ID% -p=%TC_USER_PASSWORD% -g=dba -mode=import -action=override -scope=group -target="Purchasing.Sonaca" -file=%KIT_DIR%\Purchasing_Sonaca_group_preference.xml             
ECHO ****************************                                                            
ECHO.                                                                                        
                                                                                             
PAUSE
