@ECHO OFF
TITLE Export Preferences
COLOR F0

::setting required variables
set TC_USER_ID=infodba
set TC_USER_PASSWORD=infodba


::storing current directory in variable
set KIT_DIR=%~dp0
FOR %%g in ("%KIT_DIR%") do SET KIT_DIR=%%~fsg
ECHO.
ECHO Running from current directory = %KIT_DIR%


ECHO.
ECHO ****************************
ECHO Exporting Preferences
rem @ Sonaca we do not extract all Site Preferences, instead we maintain a "modified" site preferences file
preferences_manager -u=%TC_USER_ID% -p=%TC_USER_PASSWORD% -g=dba -mode=export -scope=site -out_file=%KIT_DIR%\kverneland.xml -file=%KIT_DIR%\kverneland.prefs
::preferences_manager -u=%TC_USER_ID% -p=%TC_USER_PASSWORD% -g=dba -mode=export -scope=group -target="Configuration Management.Sonaca" -out_file=%KIT_DIR%\Configuration_Management_Sonaca_group_preference.xml
::preferences_manager -u=%TC_USER_ID% -p=%TC_USER_PASSWORD% -g=dba -mode=export -scope=group -target="dba" -out_file=%KIT_DIR%\dba_group_preference.xml
::preferences_manager -u=%TC_USER_ID% -p=%TC_USER_PASSWORD% -g=dba -mode=export -scope=group -target="Engineering.Sonaca" -out_file=%KIT_DIR%\Engineering_Sonaca_group_preference.xml  
::preferences_manager -u=%TC_USER_ID% -p=%TC_USER_PASSWORD% -g=dba -mode=export -scope=group -target="Manufacturing.Sonaca" -out_file=%KIT_DIR%\Manufacturing_Sonaca_group_preference.xml   
::preferences_manager -u=%TC_USER_ID% -p=%TC_USER_PASSWORD% -g=dba -mode=export -scope=group -target="Purchasing.Sonaca" -out_file=%KIT_DIR%\Purchasing_Sonaca_group_preference.xml             
ECHO ****************************                                                            
ECHO.                                                                                        
                                                                                             
PAUSE
