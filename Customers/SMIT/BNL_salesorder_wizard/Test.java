package com.test;

public class Test {
	
	static char[] chars = {
		'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
        '-', '*', '/', '[', ']', '{', '}', '=',
        'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
        'y', 'z', '0', '1', '2', '3', '4', '5',
        '6', '7', '8', '9', 'A', 'B', 'C', 'D',
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
        'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
        'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
        '#', '$', '%', '^', '&', '(', ')', '+',
        'U', 'V', 'W', 'X', 'Y', 'Z', '!', '@',
        '<', '>', '_', '?', '"', '.', ',', ' '
};

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String pw = "12345";
		
		test("?");
		
		test("< >?_.AB  CDEFGHIJKLMNOPQRSTUVWXYZ,.,");
	    test("< >?_.");
	    test("?_\".");
	    
	    test("12345");
	    
	    test("_$%^ jale @+-");

		
		//String pwe = SimpleEncrypt(29,pw);
		//System.out.println(pwe);

	}
	
	public static String SimpleEncrypt(int iKey, String textToEncrypt)
    {            
        StringBuilder inSb = new StringBuilder(textToEncrypt);
        StringBuilder outSb = new StringBuilder(textToEncrypt.length());

        for (int i = 0; i < textToEncrypt.length(); i++)
        {
            char c = inSb.charAt(i);
            c = (char)(c ^ (i + iKey));
            outSb.append(c);
        }
        return outSb.toString();
    }
	
	

/*
    test("This is awesome!");

    test("12345");

    test("12345 67890");

    test("abcdefghijklmnopqrstuvwxyz");

    test("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
	*/
    //test("< >?_.AB  CDEFGHIJKLMNOPQRSTUVWXYZ,.,");
    //test("< >?_.");
   // test("?_\".");

static void test(String text)
{
    int offset = 5;

    System.out.println("    input text: " + text);
    String enc = encrypt(text, offset);
    System.out.println("Encrypted text: " + enc);

    String dec = decrypt(enc, offset);
    System.out.println("Decrypted text: " + dec);
    System.out.println("--------------------------------");
}

// Caesar cipher
static String encrypt(String text, int offset)
{
    char[] plain = text.toCharArray();

    for (int i = 0; i < plain.length; i++) {
        for (int j = 0; j < chars.length; j++) {
            if (j < chars.length - offset) {
                if (plain[i] == chars[j]) {
                	//System.out.println(plain[i] + " - j + offset: " + (j + offset));
                    plain[i] = chars[j + offset];
                    break;
                }
            }
            else if (plain[i] == chars[j]) {
                plain[i] = chars[j - (chars.length - offset)];
            }
        }
    }
    return String.valueOf(plain);
}

static String decrypt(String text, int offset)
{
    char[] plain = text.toCharArray();
    
    for (int i = 0; i < plain.length; i++) {
        for (int j = 0; j < chars.length; j++) {
            if (j >= offset && plain[i] == chars[j]) {
            	plain[i] = chars[j - offset];
                break;
            }
            if (plain[i] == chars[j] && j < offset) {
            	plain[i] = chars[(chars.length - offset) + j];
                break;
            }
        }
    }
    return String.valueOf(plain);
}

}
