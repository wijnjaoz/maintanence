package com.siemens.workflow;

import com.siemens.workflow.analysis.*;
import com.siemens.workflow.model.*;
import com.siemens.workflow.view.*;

import java.io.*;
import java.util.*;
import java.util.logging.LogManager;
import java.util.logging.Logger;

class Reporter {

    private static final String LOG_CONFIG_FILE = "logging.properties";
    private static final String EXE_NAME = "WorkflowReport.jar";
    private Logger logger = null;
    private final String[] analysesToRun = new String[]{"ACL", "Forms", "LOV", "NonCOTSHandler", "Query", "RevisionRule", "Status"};
    private File input = null;
    private String commentLine = null;
    private String handlerFile = null;
    private boolean cleanup = true;
    private boolean renderAsText = false;

    public static void main(String[] args) {
        try {
            String logFile = System.getProperty("java.util.logging.config.file");
            if (logFile == null) {
                LogManager.getLogManager().readConfiguration(Reporter.class.getClassLoader().getResourceAsStream(LOG_CONFIG_FILE));
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        Reporter reporter = new Reporter(args);
        if (reporter.canReport()) {
            XMLLoader loader = new XMLLoader(reporter.getInput());
            List<WorkflowTemplate> rootTasks = loader.getRootTasks();
            String extractedOn = String.valueOf(loader.getExtractDate() + " " + loader.getExtractTime());
            String author = String.valueOf(loader.getAuthor());

            for (WorkflowTemplate rootTask : rootTasks) {
                reporter.report(rootTask, extractedOn, author);
            }
        }
    }

    private Reporter(String[] args) {
        logger = Logger.getLogger(Reporter.class.getName());
        for (String arg : args) {
            if (arg.equals("-h") || arg.equals("-help")) {
                printUsage();
                return;
            }
            else if (arg.startsWith("-comment=")) {
                commentLine = arg.substring("-comment=".length());
            }
            else if (arg.equals("-noclean")) {
                cleanup = false;
            }
            else if (arg.equals("-text")) {
                renderAsText = true;
            }
            else if (arg.startsWith("-cots_handlers=")) {
                handlerFile = arg.substring("-cots_handlers=".length());
            }
            else {
                input = new File(arg);
                if (!input.exists() || !input.canRead()) {
                    System.err.println(EXE_NAME + " : Unable to read " + arg);
                    input = null;
                }
            }
        }
        if (input == null) {
            printUsage();
        }
    }

    private void printUsage() {
        System.out.println("Usage: java -jar " + EXE_NAME + " [-text] [-comment=<comment string>] [-noclean] <template_file.xml>");
    }

    boolean canReport() {
        return input != null;
    }

    File getInput() {
        return input;
    }

    private void report(WorkflowTemplate rootTask, String extractTime, String author) {
        if (rootTask == null) {
            logger.severe("Unable to find root task");
        }
        else {
            List<WorkflowTemplate> nodes = getReportableTemplates(rootTask);
            List<String> imageFiles = new ArrayList<String>();

            System.err.flush();

            System.out.println("Nodes to report are");
            for (WorkflowTemplate node : nodes) {
                System.out.println("  " + node.getName());
            }

            // Run the analyses
            AnalysisRunner analysisRunner = new AnalysisRunner(handlerFile);
            Map<String, AnalysisResult> analysisResults = analysisRunner.runAnalyses(rootTask, analysesToRun);
            // Render the images
            ImageRenderer image = new ImageRenderer();
            for (WorkflowTemplate node : nodes) {
                String imageName = node.getImageName() + ".png";
                imageFiles.add(imageName);
                image.render(node, imageName);
            }

            // Render the Text View
            if (renderAsText) {
                TextRenderer text = new TextRenderer();
                text.render(rootTask, nodes, analysisResults, extractTime, commentLine, author);
            }

            // Render the HTML View
            HTMLRenderer html = new HTMLRenderer();
            html.render(rootTask, nodes, analysisResults, extractTime, commentLine, author);

            // Render the CSV view
            CSVDependencyRenderer csv = new CSVDependencyRenderer();
            csv.render(analysisResults, rootTask.getFileName() + "_dependencies.csv");

            // Render the CSV Summary
            CSVSummaryRenderer csvSummary = new CSVSummaryRenderer();
            csvSummary.render(rootTask, rootTask.getName() + "_summary.csv");

            // Save the icons we used
            IconManager iconManager = new IconManager();
            Set<String> usedIconKeys = getIconKeys(rootTask);
            usedIconKeys.add("up");  // Don't forget the up arrow for the html
            for (String iconKey : usedIconKeys) {
                iconManager.saveIcon(iconKey);
                imageFiles.add(iconKey + ".png");
            }

            Packager packer = new Packager();
            packer.packageAsMHT(rootTask.getFileName() + ".mht",
                    rootTask.getFileName() + ".html",
                    imageFiles);

            if (cleanup) {
                // Clean up the temp files
                for (String fileName : imageFiles) {
                    File imageFile = new File(fileName);
                    //noinspection ResultOfMethodCallIgnored
                    imageFile.delete();
                }
                File htmlFile = new File(rootTask.getFileName() + ".html");
                //noinspection ResultOfMethodCallIgnored
                htmlFile.delete();
            }
        }
    }

    private Set<String> getIconKeys(WorkflowTemplate rootTask) {
        Set<String> keys = new HashSet<String>();
        Stack<WorkflowTemplate> stack = new Stack<WorkflowTemplate>();
        stack.push(rootTask);
        while (!stack.isEmpty()) {
            WorkflowTemplate node = stack.pop();
            keys.add(node.getIconKey());
            stack.addAll(node.getSubTemplates());
        }
        return keys;
    }


    private List<WorkflowTemplate> getReportableTemplates(WorkflowTemplate rootTask) {
        // Work out which nodes to report on and gather the external dependencies
        List<WorkflowTemplate> nodes = new ArrayList<WorkflowTemplate>();
        Stack<WorkflowTemplate> stack = new Stack<WorkflowTemplate>();
        stack.push(rootTask);
        while (!stack.isEmpty()) {
            WorkflowTemplate node = stack.pop();
            if (node.getSubTemplates().size() > 0) {
                nodes.add(node);
                for (int idx = node.getSubTemplates().size() - 1; idx >= 0; --idx) {
                    stack.push(node.getSubTemplates().get(idx));
                }
            }
        }
        return nodes;
    }

}
