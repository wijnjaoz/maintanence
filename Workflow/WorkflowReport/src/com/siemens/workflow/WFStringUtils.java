package com.siemens.workflow;

public class WFStringUtils {
    public static String[] refIDsToStrings(String refsStr) {
        if (refsStr == null || refsStr.length() == 0)
            return new String[0];
        return refsStr.substring(1).replace('#', ' ').split("\\s+");
    }

    public static int hexToDec(String location) {
        return Integer.parseInt(location, 16);
    }

    private static boolean isErrorList(String label) {
        //noinspection SimplifiableIfStatement,SimplifiableIfStatement
        if (label == null) return false;
        return label.matches("^\\d+(,\\d+)*$");
    }

    public static String getLabelFromValue(String label) {
        if (label == null) return null;
        else if ("true".equals(label)) return "T";
        else if ("false".equals(label)) return "F";
        else if ("ANY".equals(label)) return null;
        else if (WFStringUtils.isErrorList(label)) return null;
        else {
            int idx = label.indexOf(',');
            if (idx != -1) {
                return label.substring(0, idx) + "...";
            }
        }
        return label;
    }

    public static String spanList(String value) {
        if (value == null) return null;
        if (value.length() == 0) return value;
        if (value.indexOf(',') == -1) return value;
        StringBuilder buf = new StringBuilder(value.length());
        String[] split = value.split(",");
        for (int i = 0; i < split.length - 1; i++) {
            String val = split[i];
            buf.append("<span>");
            buf.append(val);
            buf.append(",</span>");
            if (i > 0 && i % 4 == 0) {
                buf.append("<br/>");
            }
        }
        buf.append("<span>");
        buf.append(split[split.length - 1]);
        buf.append("</span>");
        return buf.toString();
    }

    public static String safeCSV(String value) {
        if (value == null || value.length() == 0) return "";

        return value.replaceAll("\"", "\"\"");
    }

    public static String safeHTML(String templatePath) {
        int pathLength = templatePath.length();
        StringBuilder sb = new StringBuilder(pathLength);
        for (int idx = 0; idx < pathLength; idx++) {
            char ch = templatePath.charAt(idx);
            switch(ch) {
                case '"' :
                    sb.append("&quot;");
                    break;
                case '\'' :
                    sb.append("&apos;");
                    break;
                case '<' :
                    sb.append("&lt;");
                    break;
                case '>' :
                    sb.append("&gt;");
                    break;
                case '&' :
                    sb.append("&amp;");
                    break;
                default:
                    sb.append(ch);
            }
        }
        return sb.toString();
    }
}
