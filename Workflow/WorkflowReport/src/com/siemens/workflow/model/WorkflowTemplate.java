package com.siemens.workflow.model;

import com.siemens.workflow.WFStringUtils;
import com.siemens.workflow.view.NodeMetric;

import java.util.ArrayList;
import java.util.List;

public class WorkflowTemplate extends WorkflowNode {
    private String name;
    private String[] dependencyTaskTemplateRefIDs;
    @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
    private final List<WorkflowTemplate> dependencyTaskTemplates = new ArrayList<WorkflowTemplate>();
    private String parentTaskTemplateRefID;
    private WorkflowTemplate parentTaskTemplate;
    private String iconKey;
    private boolean waitForUndecidedReviewers = false;
    private String templateClassification;
    private boolean showInProcessStage;
    private boolean asynchronous;
    private String objectType;
    private String[] subTemplateRefIDs;
    private final List<WorkflowTemplate> subTemplates = new ArrayList<WorkflowTemplate>();
    private String signoffQuorum;
    private int xloc;
    private int yloc;
    private int startxloc;
    private int startyloc;
    private int finishxloc;
    private int finishyloc;
    private String[] actionIDs;
    private final List<WorkflowAction> actions = new ArrayList<WorkflowAction>();
    private String taskDescription;
    private final List<Reference> references = new ArrayList<Reference>();
    private final List<AssociatedDataSet> dataSets = new ArrayList<AssociatedDataSet>();
    private final List<WorkflowSignoffProfile> signoffProfiles = new ArrayList<WorkflowSignoffProfile>();
    private List<NodeMetric> nodeMetrics;

    public WorkflowTemplate(String id) {
        super(id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getDependencyTaskTemplateRefIDs() {
        return dependencyTaskTemplateRefIDs;
    }

    public void setDependencyTaskTemplateRefIDs(String[] dependencyTaskTemplateRefIDs) {
        this.dependencyTaskTemplateRefIDs = dependencyTaskTemplateRefIDs;
    }

    public String getIconKey() {
        return iconKey;
    }

    public void setIconKey(String iconKey) {
        this.iconKey = iconKey;
    }

    public String getTemplateClassification() {
        return templateClassification;
    }

    public void setTemplateClassification(String templateClassification) {
        this.templateClassification = templateClassification;
    }

    public boolean isShowInProcessStage() {
        return showInProcessStage;
    }

    public void setShowInProcessStage(boolean showInProcessStage) {
        this.showInProcessStage = showInProcessStage;
    }

    public boolean isAsynchronous() {
        return asynchronous;
    }

    public void setAsynchronous(boolean asynchronous) {
        this.asynchronous = asynchronous;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public String[] getSubTemplateRefIDs() {
        return subTemplateRefIDs;
    }

    public void setSubTemplateRefIDs(String[] subTemplateRefIDs) {
        this.subTemplateRefIDs = subTemplateRefIDs;
    }

    public String getSignoffQuorum() {
        return signoffQuorum;
    }

    public void setSignoffQuorum(String signoffQuorum) {
        this.signoffQuorum = signoffQuorum;
    }

    public int getXloc() {
        return xloc;
    }

    public int getYloc() {
        return yloc;
    }

    public int getStartxloc() {
        return startxloc;
    }

    public int getStartyloc() {
        return startyloc;
    }

    public int getFinishxloc() {
        return finishxloc;
    }

    public int getFinishyloc() {
        return finishyloc;
    }

    public void setLocation(String location) {
        if (location == null) return;
        if (location.length() == 0) return;
        String locations[] = location.split(",");
        if (locations.length != 6) {
            return;
        }
        xloc = WFStringUtils.hexToDec(locations[0]);
        yloc = WFStringUtils.hexToDec(locations[1]);
        startxloc = WFStringUtils.hexToDec(locations[2]);
        startyloc = WFStringUtils.hexToDec(locations[3]);
        finishxloc = WFStringUtils.hexToDec(locations[4]);
        finishyloc = WFStringUtils.hexToDec(locations[5]);
    }

    public String[] getActionIDs() {
        return actionIDs;
    }

    public void setActionIDs(String[] actionIDs) {
        this.actionIDs = actionIDs;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public List<Reference> getReferences() {
        return references;
    }

    public void addReference(String value, String title, String dataRef) {
        references.add(new Reference(value, title, dataRef));
    }

    public String getParentTaskTemplateRefID() {
        return parentTaskTemplateRefID;
    }

    public void setParentTaskTemplateRefID(String parentTaskTemplateRefID) {
        this.parentTaskTemplateRefID = parentTaskTemplateRefID;
    }

    WorkflowTemplate getParentTaskTemplate() {
        return parentTaskTemplate;
    }

    public void setParentTaskTemplate(WorkflowTemplate parentTaskTemplate) {
        this.parentTaskTemplate = parentTaskTemplate;
    }

    public void addDependencyTaskTemplate(WorkflowTemplate template) {
        dependencyTaskTemplates.add(template);
    }

    public List<AssociatedDataSet> getDataSets() {
        return dataSets;
    }

    public void addDataSet(AssociatedDataSet dataSet) {
        dataSets.add(dataSet);
    }

    public String getConditionValue(String name) {
        final WFUtil wfUtil = WFUtil.getInstance();
        return wfUtil.getConditionValue(this, name);
    }

    public String getFileName() {
        return name.replaceAll("[^a-zA-Z0-9_+-]", "_");
    }

    public String getImageName() {
        List<WorkflowTemplate> path = new ArrayList<WorkflowTemplate>();
        WorkflowTemplate pathnode = this;
        while (pathnode.getParentTaskTemplate() != null) {
            path.add(pathnode);
            pathnode = pathnode.getParentTaskTemplate();
        }
        path.add(pathnode);
        StringBuilder pathStr = new StringBuilder();
        for (int idx = path.size() - 1; idx > 0; --idx) {
            pathStr.append(path.get(idx).getFileName());
            pathStr.append("__");
        }
        pathStr.append(path.get(0).getFileName());
        return pathStr.toString();
    }

    public boolean isSubTask() {
        return parentTaskTemplateRefID != null && !"".equals(parentTaskTemplateRefID);
    }

    public void addSubTemplate(WorkflowTemplate template) {
        subTemplates.add(template);
    }

    public void addAction(WorkflowAction action) {
        actions.add(action);
    }

    public List<WorkflowTemplate> getSubTemplates() {
        return subTemplates;
    }

    public List<WorkflowAction> getActions() {
        return actions;
    }

    public void addSignoffProfile(WorkflowSignoffProfile signoffProfile) {
        signoffProfiles.add(signoffProfile);
    }

    public List<WorkflowSignoffProfile> getSignoffProfiles() {
        return signoffProfiles;
    }

    public boolean isWaitForUndecidedReviewers() {
        return waitForUndecidedReviewers;
    }

    public void setWaitForUndecidedReviewers(boolean waitForUndecidedReviewers) {
        this.waitForUndecidedReviewers = waitForUndecidedReviewers;
    }

    public String getTemplatePath() {
        List<WorkflowTemplate> path = new ArrayList<WorkflowTemplate>();
        WorkflowTemplate pathnode = this;
        while (pathnode.getParentTaskTemplate() != null) {
            path.add(pathnode);
            pathnode = pathnode.getParentTaskTemplate();
        }
        path.add(pathnode);
        StringBuilder pathStr = new StringBuilder();
        for (int idx = path.size() - 1; idx > 0; --idx) {
            pathStr.append(path.get(idx).getName());
            pathStr.append(" / ");
        }
        pathStr.append(path.get(0).getName());
        return pathStr.toString();
    }

    public void setNodeMetrics(List<NodeMetric> nodeMetrics) {
        this.nodeMetrics = nodeMetrics;
    }

    public List<NodeMetric> getNodeMetrics() {
        return nodeMetrics;
    }

    public int getMaxDepth() {
        return recurseMaxDepth(this, 1, 1);
    }

    private int recurseMaxDepth(WorkflowTemplate node, int depth, int maxdepth) {
        if (node.subTemplates.size() == 0) {
            return Math.max(depth, maxdepth);
        }
        for (WorkflowTemplate subTemplate : node.subTemplates) {
            maxdepth = Math.max(maxdepth, recurseMaxDepth(subTemplate, depth + 1, maxdepth));
        }
        return maxdepth;
    }

    public boolean setsACL() {
        WFUtil utils = WFUtil.getInstance();
        for (WorkflowAction action : actions) {
            for (WorkflowHandler workflowHandler : action.getActionHandlers()) {
                if (utils.usesACL(workflowHandler)) {
                    return true;
                }
            }

        }
        return false;
    }

    public boolean hasHandlers() {
        for (WorkflowAction action : actions) {
            if (action.hasHandlers()) {
                return true;
            }
        }
        return false;
    }
}

