package com.siemens.workflow.model;

import java.util.ArrayList;
import java.util.List;

// WorkflowBusinessRule
public class WorkflowBusinessRule extends WorkflowNode {
    private String[] ruleHandlerRefIDs;
    private String quorum;
    private final List<WorkflowHandler> ruleHandlers = new ArrayList<WorkflowHandler>();

    public WorkflowBusinessRule(String id) {
        super(id);
    }

    public String[] getRuleHandlerRefIDs() {
        return ruleHandlerRefIDs;
    }

    public void setRuleHandlerRefIDs(String[] ruleHandlerRefIDs) {
        this.ruleHandlerRefIDs = ruleHandlerRefIDs;
    }

    public String getQuorum() {
        return quorum;
    }

    public void setQuorum(String quorum) {
        this.quorum = quorum;
    }

    public void addRuleHandler(WorkflowHandler handler) {
        ruleHandlers.add(handler);
    }

    public List<WorkflowHandler> getRuleHandlers() {
        return ruleHandlers;
    }
}
