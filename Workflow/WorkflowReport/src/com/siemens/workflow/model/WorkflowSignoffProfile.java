package com.siemens.workflow.model;

public class WorkflowSignoffProfile extends WorkflowNode {
    private String numberOfSignoffs;
    private String groupRefID;
    private String roleRefID;
    private boolean allowSubgroups;
    private String signoffQuorum;
    private WorkflowOrganisation organisation;
    private WorkflowRole role;

    public WorkflowSignoffProfile(String id) {
        super(id);
    }

    public String getNumberOfSignoffs() {
        return numberOfSignoffs;
    }

    public void setNumberOfSignoffs(String numberOfSignoffs) {
        this.numberOfSignoffs = numberOfSignoffs;
    }

    public String getGroupRefID() {
        return groupRefID;
    }

    public void setGroupRefID(String groupRefID) {
        if (groupRefID != null && groupRefID.startsWith("#"))
            this.groupRefID = groupRefID.substring(1);
        else
            this.groupRefID = groupRefID;
    }

    public String getRoleRefID() {
        return roleRefID;
    }

    public void setRoleRefID(String roleRefID) {
        if (roleRefID != null && roleRefID.startsWith("#"))
            this.roleRefID = roleRefID.substring(1);
        else
            this.roleRefID = roleRefID;
    }

    public boolean isAllowSubgroups() {
        return allowSubgroups;
    }

    public void setAllowSubgroups(boolean allowSubgroups) {
        this.allowSubgroups = allowSubgroups;
    }

    public String getSignoffQuorum() {
        return signoffQuorum;
    }

    public void setSignoffQuorum(String signoffQuorum) {
        this.signoffQuorum = signoffQuorum;
    }

    public WorkflowOrganisation getOrganisation() {
        return organisation;
    }

    public void setOrganisation(WorkflowOrganisation organisation) {
        this.organisation = organisation;
    }

    public WorkflowRole getRole() {
        return role;
    }

    public void setRole(WorkflowRole role) {
        this.role = role;
    }
}
