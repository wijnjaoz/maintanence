package com.siemens.workflow.model;

import java.util.List;

class UnconfiguredVersion extends WFUtil {

    @Override
    public boolean isCOTS(WorkflowHandler handler) {
        throw new IllegalStateException("WFUtil is not configured.  Check author attribute of PLMXML tag is supported.");
    }

    @Override
    public boolean usesACL(WorkflowHandler handler) {
        throw new IllegalStateException("WFUtil is not configured.  Check author attribute of PLMXML tag is supported.");
    }

    @Override
    public List<String> getACLS(WorkflowHandler handler) {
        throw new IllegalStateException("WFUtil is not configured.  Check author attribute of PLMXML tag is supported.");
    }

    @Override
    public boolean usesStatus(WorkflowHandler handler) {
        throw new IllegalStateException("WFUtil is not configured.  Check author attribute of PLMXML tag is supported.");
    }

    @Override
    public List<String> getStatuses(WorkflowHandler handler) {
        throw new IllegalStateException("WFUtil is not configured.  Check author attribute of PLMXML tag is supported.");
    }

    @Override
    public boolean usesRevisionRule(WorkflowHandler handler) {
        throw new IllegalStateException("WFUtil is not configured.  Check author attribute of PLMXML tag is supported.");
    }

    @Override
    public List<String> getRevisionRules(WorkflowHandler handler) {
        throw new IllegalStateException("WFUtil is not configured.  Check author attribute of PLMXML tag is supported.");
    }

    @Override
    public boolean usesLOV(WorkflowHandler handler) {
        throw new IllegalStateException("WFUtil is not configured.  Check author attribute of PLMXML tag is supported.");
    }

    @Override
    public List<String> getLOVs(WorkflowHandler handler) {
        throw new IllegalStateException("WFUtil is not configured.  Check author attribute of PLMXML tag is supported.");
    }

    @Override
    public boolean usesQuery(WorkflowHandler handler) {
        throw new IllegalStateException("WFUtil is not configured.  Check author attribute of PLMXML tag is supported.");
    }

    @Override
    public List<String> getQueries(WorkflowHandler handler) {
        throw new IllegalStateException("WFUtil is not configured.  Check author attribute of PLMXML tag is supported.");
    }

    @Override
    public boolean usesForms(WorkflowHandler handler) {
        throw new IllegalStateException("WFUtil is not configured.  Check author attribute of PLMXML tag is supported.");
    }

    @Override
    public List<String> getForms(WorkflowHandler handler) {
        throw new IllegalStateException("WFUtil is not configured.  Check author attribute of PLMXML tag is supported.");
    }

    @Override
    public String getConditionValue(WorkflowTemplate task, String targetTask) {
        throw new IllegalStateException("WFUtil is not configured.  Check author attribute of PLMXML tag is supported.");
    }
}
