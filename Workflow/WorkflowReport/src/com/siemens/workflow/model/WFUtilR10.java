package com.siemens.workflow.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class WFUtilR10 extends WFUtil {
    WFUtilR10() {
        Collections.addAll(handlers, new String[]{});
        handlers.add("AI-export-AH");
        handlers.add("AI-process-import");
        handlers.add("AI-process-export");
        handlers.add("APPR-update-from-targets");
        handlers.add("AR-mark-archive");
        handlers.add("ASBUILT-release-asbuilt-structure");
        handlers.add("ASMAINTAINED-release-asmaintained-structure");
        handlers.add("BC-perform-export");
        handlers.add("CAE-batch-meshing-handler");
        handlers.add("CAE-simulation-process-launch-handler");
        handlers.add("CONFMGMT-cut-back-effectivity");
        handlers.add("CONTMGMTS1000D-increment");
        handlers.add("CONTMGMTS1000D-setQAStatus");
        handlers.add("CPD-update-item-realization");
        handlers.add("CPD-where-used-item-revision");
        handlers.add("CSI-propagate-folder-contents");
        handlers.add("DOCMGT-render-document-revision");
        handlers.add("DPV-export-device-to-ai");
        handlers.add("DPV-export-plant-to-ai");
        handlers.add("DPV-export-routine-to-ai");
        handlers.add("EPM-adhoc-signoffs");
        handlers.add("EPM-apply-digital-signature");
        handlers.add("EPM-assign-responsible-party-dynamic-participant");
        handlers.add("EPM-assign-signoff-dynamic-participant");
        handlers.add("EPM-assign-team-selector");
        handlers.add("EPM-attach-related-objects");
        handlers.add("EPM-auto-assign");
        handlers.add("EPM-auto-assign-rest");
        handlers.add("EPM-auto-check-in-out");
        handlers.add("EPM-change-all-started-to-pending");
        handlers.add("EPM-change-group-owner");
        handlers.add("EPM-change-ownership");
        handlers.add("EPM-change-target-group");
        handlers.add("EPM-change-target-group-owner");
        handlers.add("EPM-check-signoff-comments");
        handlers.add("EPM-create-form");
        handlers.add("EPM-create-relation");
        handlers.add("EPM-create-status");
        handlers.add("EPM-create-sub-process");
        handlers.add("EPM-debug");
        handlers.add("EPM-demote");
        handlers.add("EPM-demote-on-reject");
        handlers.add("EPM-display-form");
        handlers.add("EPM-execute-follow-up");
        handlers.add("EPM-fill-in-reviewers");
        handlers.add("EPM-inherit");
        handlers.add("EPM-invoke-system-action");
        handlers.add("EPM-late-notification");
        handlers.add("EPM-notify");
        handlers.add("EPM-notify-report");
        handlers.add("EPM-notify-signoffs");
        handlers.add("EPM-remove-objects");
        handlers.add("EPM-request-PKI-authentication");
        handlers.add("EPM-require-authentication");
        handlers.add("EPM-run-external-command");
        handlers.add("EPM-set-condition");
        handlers.add("EPM-set-duration");
        handlers.add("ERP-set-form-value-AH");
        handlers.add("EPM-set-job-protection");
        handlers.add("EPM-set-parent-result");
        handlers.add("EPM-set-property");
        handlers.add("EPM-set-rule-based-protection");
        handlers.add("EPM-set-status");
        handlers.add("EPM-set-task-result-to-property");
        handlers.add("EPM-suspend-on-reject");
        handlers.add("EPM-system");
        handlers.add("EPM-trigger-action");
        handlers.add("EPM-trigger-action-on-related-process-task");
        handlers.add("ERP-att-logfile-as-dataset-RH");
        handlers.add("ERP-attach-targets-AH");
        handlers.add("ERP-delete-log-dataset-AH");
        handlers.add("ERP-download-AH");
        handlers.add("ERP-post-upload-AH");
        handlers.add("ERP-set-pathnames-in-logds-AH");
        handlers.add("ERP-transform-AI-contents-AH");
        handlers.add("GMIMAN-invoke-subscription-event-on-item");
        handlers.add("ISSUEMGT-check-review-decision");
        handlers.add("ISSUEMGT-update-issue-status");
        handlers.add("ME-stamp-ids-AH");
        handlers.add("MES-Update3DPDFReports");
        handlers.add("OBJIO-release-and-replicate");
        handlers.add("OBJIO-send-target-objects");
        handlers.add("PIE-export-to-plmxmlfile");
        handlers.add("PS-attach-assembly-components");
        handlers.add("PS-make-mature-design-primary");
        handlers.add("PUBR-publish-target-objects");
        handlers.add("PUBR-unpublish-target-objects");
        handlers.add("RDV-delete-ugcgm-markup");
        handlers.add("RDV-generate-image");
        handlers.add("RDV-generate-ugcgm-drawing");
        handlers.add("RDV-tessellation-handler");
        handlers.add("RM-attach-SM-tracelink-requirement");
        handlers.add("RM-attach-tracelink-requirement");
        handlers.add("SAP-set-valid-data-AH");
        handlers.add("SAP-upload-AH");
        handlers.add("SCHMGT-approve-timesheetentries");
        handlers.add("SCHMGT-reject-timesheetentries");
        handlers.add("SCHMGT-revise-timesheetentries");
        handlers.add("SERVICEFORECASTING-approve-ma-extension");
        handlers.add("SERVICEPROCESSING-approve-service-structure");
        handlers.add("SMP-auto-relocate-file");
        handlers.add("TCRS-auto-approve-first-step");
        handlers.add("TCRS-create-form");
        handlers.add("TCRS-Create-Print-Requests");
        handlers.add("TCRS-create-snapshot");
        handlers.add("TCRS-Create-Translation-Request");
        handlers.add("TCRS-delete-dataset");
        handlers.add("TCRS-delete-log-datasets");
        handlers.add("TCRS-export-signoff-data");
        handlers.add("TCRS-export-to-tcxmlfile");
        handlers.add("TCRS-IRM-cleanfields");
        handlers.add("TCRS-purge-dataset");
        handlers.add("TCRS-release-previous-itemrevs");
        handlers.add("TCRS-remove-targets-with-status");
        handlers.add("TCRS-set-bom-precise");
        handlers.add("TCRS-setstatus-EO-folder");
        handlers.add("TCRS-store-review-data");
        handlers.add("TCRS-trigger-approve-first-step");
        handlers.add("TSTK-CreateTranslationRequest");
        handlers.add("VAL-approve-result-overrides");
        handlers.add("VAL-reject-result-overrides");
        handlers.add("VAL-set-condition-by-check-validation-result");
        handlers.add("VAL-set-condition-result-overrides");
        handlers.add("ASBUILT-validate-for-checkedout-physicalpartrevision");
        handlers.add("ASBUILT-validate-for-physicalpartrevision");
        handlers.add("ASBUILT-validate-missing-structure");
        handlers.add("ASMAINTAINED-validate-for-checkedout-physicalpartrevision");
        handlers.add("ASMAINTAINED-validate-for-latest-asmphysicalpartrevision");
        handlers.add("ASMAINTAINED-validate-for-unserviceable-physicalpartrevision");
        handlers.add("ASMAINTAINED-validate-missing-asmaintained-structure");
        handlers.add("AUTOSCHEDULING-person-reassign-validate");
        handlers.add("EPM-assert-signoffs-target-read-access");
        handlers.add("EPM-assert-targets-checked-in");
        handlers.add("EPM-check-action-performer-role");
        handlers.add("EPM-check-condition");
        handlers.add("EPM-check-item-status");
        handlers.add("EPM-check-object-properties");
        handlers.add("EPM-check-related-objects");
        handlers.add("EPM-check-responsible-party");
        handlers.add("EPM-check-signoff");
        handlers.add("EPM-check-status-progression");
        handlers.add("EPM-check-target-attachments");
        handlers.add("EPM-check-target-object");
        handlers.add("EPM-debug-rule");
        handlers.add("EPM-disallow-adding-targets");
        handlers.add("EPM-disallow-removing-targets");
        handlers.add("EPM-hold");
        handlers.add("EPM-invoke-system-rule");
        handlers.add("EPM-validate-target-objects");
        handlers.add("ERP-check-effective-date-RH");
        handlers.add("ERP-check-target-status-RH");
        handlers.add("ERP-validate-data-RH");
        handlers.add("ICS-assert-target-classified");
        handlers.add("MESINTEG_ValidateReleaseAndExport");
        handlers.add("MFG-invoke-customized-validations");
        handlers.add("MROCORE-validate-for-class");
        handlers.add("PS-check-assembly-status-progression");
        handlers.add("PS-check-occ-notes");
        handlers.add("SAP-check-forms-attached-RH");
        handlers.add("SAP-check-forms-to-download-RH");
        handlers.add("TCRS-check-approver");
        handlers.add("TCRS-check-bom-precise");
        handlers.add("TCRS-check-bomchild-statuslist");
        handlers.add("TCRS-check-comps-against-pattern");
        handlers.add("TCRS-check-datasets");
        handlers.add("TCRS-check-itemrev-status");
        handlers.add("TCRS-check-jobowner");
        handlers.add("TCRS-check-prev-itemrev-status");
        handlers.add("TCRS-check-signoff");
        handlers.add("TCRS-check-status");
        handlers.add("TCRS-has-target-drawing");
        handlers.add("VAL-check-validation-result");
        handlers.add("VAL-check-validation-result-with-rules");
    }

    @Override
    public List<String> getACLS(WorkflowHandler handler) {
        final List<String> aclList = new ArrayList<String>();
        for (final NamedArg namedArg : handler.getArguments()) {
            final String arg = namedArg.getValue();
            if (arg.startsWith("-acl=")) {
                aclList.add(arg.substring(5));
            }
        }
        return aclList;
    }

    @Override
    public boolean usesStatus(final WorkflowHandler handler) {
        for (final NamedArg namedArg : handler.getArguments()) {
            final String arg = namedArg.getValue();
            if (arg.startsWith("-status=") ||
                    arg.startsWith("-new_status=") ||
                    arg.startsWith("-release=") ||
                    arg.startsWith("-allowed_status=") ||
                    arg.startsWith("-disallowed_status=") ||
                    arg.startsWith("-rev_status=") ||
                    arg.startsWith("-stateus_name=") ||
                    arg.startsWith("-statelist=") ||
                    arg.startsWith("-check_component_status=") ||
                    arg.startsWith("-change_status_on_go=") ||
                    arg.startsWith("-change_status_on_nogo=") ||
                    arg.startsWith("-change_status_on_undecided=") ||
                    arg.startsWith("-status_to_keep=")
                    ) {
                return true;
            }
        }
        return false;
    }

    @Override
    public List<String> getStatuses(final WorkflowHandler handler) {
        final List<String> statusList = new ArrayList<String>();
        for (final NamedArg namedArg : handler.getArguments()) {
            final String arg = namedArg.getValue();
            if (arg.startsWith("-status=") ||
                    arg.startsWith("-new_status=") ||
                    arg.startsWith("-release=") ||
                    arg.startsWith("-allowed_status=") ||
                    arg.startsWith("-disallowed_status=") ||
                    arg.startsWith("-rev_status=") ||
                    arg.startsWith("-stateus_name=") ||
                    arg.startsWith("-statelist=") ||
                    arg.startsWith("-check_component_status=") ||
                    arg.startsWith("-change_status_on_go=") ||
                    arg.startsWith("-change_status_on_nogo=") ||
                    arg.startsWith("-change_status_on_undecided=") ||
                    arg.startsWith("-status_to_keep=")
                    ) {
                int idx = arg.indexOf('=');
                String values;
                if (idx == -1) {
                    values = arg;
                }
                else {
                    values = arg.substring(idx + 1);
                }
                for (final String value : values.split(",")) {
                    if (isReportedStatus(value)) {
                        statusList.add(value);
                    }
                }
            }
        }
        return statusList;
    }

    @Override
    public boolean usesRevisionRule(WorkflowHandler handler) {
        for (final NamedArg namedArg : handler.getArguments()) {
            final String arg = namedArg.getValue();
            if (arg != null) {
                if (arg.startsWith("-rev_rule=") ||
                        arg.startsWith("-revrule=") ||
                        arg.startsWith("-revision_rule=") ||
                        arg.startsWith("-revisionrule=") ||
                        arg.startsWith("-RevisionRule=")
                        ) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public List<String> getRevisionRules(WorkflowHandler handler) {
        final List<String> revRuleList = new ArrayList<String>();
        for (final NamedArg namedArg : handler.getArguments()) {
            String arg = namedArg.getValue();
            if (arg != null) {
                if (arg.startsWith("-rev_rule=") ||
                        arg.startsWith("-revrule=") ||
                        arg.startsWith("-revision_rule=") ||
                        arg.startsWith("-revisionrule=") ||
                        arg.startsWith("-RevisionRule=")
                        ) {
                    int idx = arg.indexOf('=');
                    String revRule = arg.substring(idx + 1);
                    revRuleList.add(revRule);
                }
            }
        }
        return revRuleList;
    }

    @Override
    public boolean usesQuery(final WorkflowHandler handler) {
        if (handler.getName().equals("EPM-set-condition")) {
            for (final NamedArg namedArg : handler.getArguments()) {
                final String arg = namedArg.getValue();
                if (arg != null && arg.startsWith("-query=")) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public List<String> getQueries(final WorkflowHandler handler) {
        final List<String> queryNames = new ArrayList<String>();
        if (handler.getName().equals("EPM-set-condition")) {
            for (final NamedArg namedArg : handler.getArguments()) {
                final String arg = namedArg.getValue();
                if (arg != null && arg.startsWith("-query=")) {
                    String query = arg.substring(7);
                    queryNames.add(query);
                }
            }
        }
        return queryNames;
    }

    @Override
    public boolean usesForms(WorkflowHandler handler) {
        final String handlerName = handler.getName();
        return handlerName.equals("EPM-create-form") ||
                handlerName.equals("EPM-display-form") ||
                handlerName.equals("TCRS-create-form");
    }

    @Override
    public List<String> getForms(WorkflowHandler handler) {
        final String handlerName = handler.getName();
        final List<String> formNames = new ArrayList<String>();
        for (final NamedArg namedArg : handler.getArguments()) {
            String arg = namedArg.getValue();
            if ((handlerName.equals("EPM-create-form") || handlerName.equals("EPM-display-form")) &&
                    arg != null && arg.startsWith("-type=")) {
                final String form = arg.substring(6);
                formNames.add(form);
            }
            else if (handlerName.equals("TCRS-create-form") && arg != null && arg.startsWith("-form_type=")) {
                final String form = arg.substring(11);
                formNames.add(form);
            }
        }
        return formNames;
    }

    @Override
    public String getConditionValue(final WorkflowTemplate task, final String targetTask) {
        for (WorkflowAction action : task.getActions()) {
            for (WorkflowBusinessRule workflowBusinessRule : action.getRules()) {
                for (WorkflowHandler handler : workflowBusinessRule.getRuleHandlers()) {
                    if (handler.getName().equals("EPM-check-condition")) {
                        String source_task = null;
                        String decision = null;
                        for (NamedArg namedArg : handler.getArguments()) {
                            String argValue = namedArg.getValue();
                            if (argValue.startsWith("-source_task=")) {
                                source_task = argValue.substring(13);
                            }
                            else if (argValue.startsWith("-decision=")) {
                                decision = argValue.substring(10);
                            }
                        }
                        if (targetTask.equals(source_task)) {
                            return decision;
                        }
                    }
                }
            }
        }
        return null;

    }

}