package com.siemens.workflow.model;

import com.siemens.workflow.WFStringUtils;

import javax.xml.stream.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class XMLLoader {
    private static final String WORKFLOW_TEMPLATE_TAG = "WorkflowTemplate";
    private static final String WORKFLOW_ACTION_TAG = "WorkflowAction";
    private static final String WORKFLOW_HANDLER_TAG = "WorkflowHandler";
    private static final String WORKFLOW_BUSINESS_RULE_TAG = "WorkflowBusinessRule";
    private static final String WORKFLOW_BUSINESS_RULE_HANDLER_TAG = "WorkflowBusinessRuleHandler";
    private static final String WORKFLOW_SIGNOFF_PROFILE_TAG = "WorkflowSignoffProfile";
    private static final String ORGANISATION_TAG = "Organisation";
    private static final String ROLE_TAG = "Role";
    private static final String ACCESS_INTENT_TAG = "AccessIntent";
    private static final String SITE_TAG = "Site";
    private static final String ARGUMENTS_TAG = "Arguments";
    private final Map<String, WorkflowNode> nodeMap = new HashMap<String, WorkflowNode>();
    private Logger logger = null;
    private List<WorkflowTemplate> rootTasks = null;
    private final File source;
    private String extractDate;
    private String extractTime;
    private String author;

    public XMLLoader(File source) {
        this.source = source;
        logger = Logger.getLogger(XMLLoader.class.getName());
    }

    public List<WorkflowTemplate> getRootTasks() {
        if (rootTasks == null) {
            rootTasks = new ArrayList<WorkflowTemplate>();
            parseInput();
            assembleNodes();
        }
        return rootTasks;
    }

    public String getExtractDate() {
        return extractDate;
    }

    public String getExtractTime() {
        return extractTime;
    }

    public String getAuthor() {
        return author;
    }

    private void assembleNodes() {
        // Start with the root task
        Stack<WorkflowTemplate> templates = new Stack<WorkflowTemplate>();
        templates.addAll(rootTasks);
        while (!templates.isEmpty()) {
            WorkflowTemplate template = templates.pop();

            // Turn the dependency task RefIDs into proper object relationships
            if (template.getDependencyTaskTemplateRefIDs() != null) {
                for (String templateID : template.getDependencyTaskTemplateRefIDs()) {
                    template.addDependencyTaskTemplate((WorkflowTemplate) nodeMap.get(templateID));
                }
            }

            // Turn the parent RefID into a proper object relationship
            if (template.getParentTaskTemplateRefID() != null)
                template.setParentTaskTemplate((WorkflowTemplate) nodeMap.get(template.getParentTaskTemplateRefID()));

            // Turn the subtemplate RefIDs into proper object relationships
            if (template.getSubTemplateRefIDs() != null) {
                for (String templateID : template.getSubTemplateRefIDs()) {
                    template.addSubTemplate((WorkflowTemplate) nodeMap.get(templateID));
                }
            }

            // Process the signoff profiles and associated organisations
            for (AssociatedDataSet dataSet : template.getDataSets()) {
                if ("EPM_signoff_profile".equals(dataSet.getRole())) {
                    WorkflowSignoffProfile signoffProfile = (WorkflowSignoffProfile) nodeMap.get(dataSet.getDataSetRef());
                    template.addSignoffProfile(signoffProfile);
                    // Process the groups and roles in the profile
                    if (signoffProfile.getGroupRefID() != null) {
                        WorkflowOrganisation org = (WorkflowOrganisation) nodeMap.get(signoffProfile.getGroupRefID());
                        signoffProfile.setOrganisation(org);
                        // Process the org roles
                        if (org.getRoles().size() == 0) {
                            for (String roleRef : org.getListOfRoleRefs()) {
                                WorkflowRole role = (WorkflowRole) nodeMap.get(roleRef);
                                org.addRole(role);
                            }
                        }
                    }
                    if (signoffProfile.getRoleRefID() != null) {
                        WorkflowRole role = (WorkflowRole) nodeMap.get(signoffProfile.getRoleRefID());
                        signoffProfile.setRole(role);
                    }
                }
            }


            // Turn the reference RefIDs into proper object relationships
            for (Reference reference : template.getReferences()) {
                if (reference.getDataRefID() != null) {
                    reference.setReference((WorkflowTemplate) nodeMap.get(reference.getDataRefID()));
                }
            }

            // Turn the action and rule handlers into proper object relationships
            if (template.getActionIDs() != null) {
                for (String actionID : template.getActionIDs()) {
                    WorkflowAction action = (WorkflowAction) nodeMap.get(actionID);
                    template.addAction(action);

                    // Fix the references in the action
                    if (action.getActionHandlerRefIDs() != null) {
                        for (String handlerID : action.getActionHandlerRefIDs()) {
                            action.addActionHandler((WorkflowHandler) nodeMap.get(handlerID));
                        }
                    }
                    if (action.getRuleRefIDS() != null) {
                        for (String ruleID : action.getRuleRefIDS()) {
                            WorkflowBusinessRule rule = (WorkflowBusinessRule) nodeMap.get(ruleID);
                            action.addRule(rule);
                            if (rule.getRuleHandlerRefIDs() != null) {
                                for (String handlerID : rule.getRuleHandlerRefIDs()) {
                                    rule.addRuleHandler((WorkflowHandler) nodeMap.get(handlerID));
                                }
                            }
                        }
                    }

                }
            }

            templates.addAll(template.getSubTemplates());
        }
    }


    private void parseInput() {
        FileInputStream stream = null;
        try {
            int seqId = 0;
            // Parse the source XML file
            XMLInputFactory factory = XMLInputFactory.newFactory();
            stream = new FileInputStream(source);
            XMLStreamReader reader = factory.createXMLStreamReader(stream);
            // Report the contents
            while (reader.hasNext()) {
                seqId++;
                int event = reader.next();
                switch (event) {
                    case XMLStreamConstants.START_ELEMENT:
                        String elementType = reader.getLocalName();
                        if (elementType.equals("PLMXML")) {
                            extractDate = reader.getAttributeValue(null, "date");
                            extractTime = reader.getAttributeValue(null, "time");
                            author = reader.getAttributeValue(null, "author");
                            logger.info("Extracted at " + extractDate + " " + extractTime);
                            WFUtil.configureVersion(author);
                        }
                        else //noinspection StatementWithEmptyBody
                            if (elementType.equals("Header")) {
                            }
                            else if (elementType.equals(WORKFLOW_TEMPLATE_TAG)) {
                                WorkflowTemplate template = getWorkflowTemplate(reader);
                                template.setSeqId(seqId);
                                nodeMap.put(template.getId(), template);
                                logger.fine("Got WorkflowTemplate : " + template.getId() + " : " + template.getName());
                                if (template.getParentTaskTemplateRefID() == null) {
                                    rootTasks.add(template);
                                }
                            }
                            else if (elementType.equals(WORKFLOW_ACTION_TAG)) {
                                WorkflowAction action = getWorkflowAction(reader);
                                action.setSeqId(seqId);
                                nodeMap.put(action.getId(), action);
                                logger.fine("Got WorkflowAction : " + action.getId());
                            }
                            else if (elementType.equals(WORKFLOW_HANDLER_TAG)) {
                                WorkflowHandler handler = getWorkflowHandler(reader);
                                handler.setSeqId(seqId);
                                nodeMap.put(handler.getId(), handler);
                                logger.fine("Got WorkflowHandler : " + handler.getId() + " : " + handler.getName());
                            }
                            else if (elementType.equals(WORKFLOW_BUSINESS_RULE_TAG)) {
                                WorkflowBusinessRule rule = getWorkflowBusinessRule(reader);
                                rule.setSeqId(seqId);
                                nodeMap.put(rule.getId(), rule);
                                logger.fine("Got WorkflowBusinessRule : " + rule.getId());
                            }
                            else if (elementType.equals(WORKFLOW_BUSINESS_RULE_HANDLER_TAG)) {
                                WorkflowHandler handler = getWorkflowBusinessRuleHandler(reader);
                                handler.setSeqId(seqId);
                                nodeMap.put(handler.getId(), handler);
                                logger.fine("Got WorkflowBusinessRuleHandler : " + handler.getId());
                            }
                            else if (elementType.equals(WORKFLOW_SIGNOFF_PROFILE_TAG)) {
                                WorkflowSignoffProfile profile = getWorkflowSignoffProfile(reader);
                                profile.setSeqId(seqId);
                                nodeMap.put(profile.getId(), profile);
                                logger.fine("Got SignoffProfile : " + profile.getId());
                            }
                            else if (elementType.equals(ORGANISATION_TAG)) {
                                WorkflowOrganisation org = getWorklowOrganisation(reader);
                                org.setSeqId(seqId);
                                nodeMap.put(org.getId(), org);
                                logger.fine("Got Organisation : " + org.getId());
                            }
                            else if (elementType.equals(ROLE_TAG)) {
                                WorkflowRole role = getWorkflowRole(reader);
                                role.setSeqId(seqId);
                                nodeMap.put(role.getId(), role);
                                logger.fine("Got Role : " + role.getId());
                            }
                            else if (elementType.equals(ACCESS_INTENT_TAG)) {
                                skipToEndOfTag(reader, ACCESS_INTENT_TAG);
                            }
                            else if (elementType.equals(SITE_TAG)) {
                                skipToEndOfTag(reader, SITE_TAG);
                            }
                            else {
                                logger.warning("Skipping Unknown element " + elementType + " at line " + reader.getLocation().getLineNumber());
//                            if (elementType.equals("ApplicationRef")) {
//                                logger.warning(reader.getAttributeValue(null, "version"));
//                            }
                                skipToEndOfTag(reader, elementType);
                            }
                        break;
                }
            }
        }
        catch (XMLStreamException e) {
            final Location errorLoc = e.getLocation();
            String errorMsg = "Parse error at line " + errorLoc.getLineNumber();
            logger.log(Level.SEVERE, errorMsg, e);
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        finally {
            if (stream != null)
                try {
                    stream.close();
                }
                catch (IOException ignored) {
                }
        }
        logger.info("Read " + nodeMap.size() + " nodes");
    }

    private WorkflowTemplate getWorkflowTemplate(XMLStreamReader reader) {
        Map<String, String> attrs = getAttrMap(reader);
        WorkflowTemplate template = new WorkflowTemplate(attrs.get("id"));
        template.setName(attrs.get("name"));
        template.setDependencyTaskTemplateRefIDs(WFStringUtils.refIDsToStrings(attrs.get("dependencyTaskTemplateRefs")));
        template.setIconKey(attrs.get("iconKey"));
        template.setTemplateClassification(attrs.get("templateClassification"));
        template.setShowInProcessStage(Boolean.valueOf(attrs.get("showInProcessStage")));
        template.setAsynchronous(Boolean.valueOf(attrs.get("asynchronous")));
        if (attrs.containsKey("waitForUndecidedReviewers")) {
            template.setWaitForUndecidedReviewers(Boolean.valueOf(attrs.get("waitForUndecidedReviewers")));
        }
        template.setObjectType(attrs.get("objectType"));
        String parentTaskTemplateRef = attrs.get("parentTaskTemplateRef");
        if (parentTaskTemplateRef != null) {
            parentTaskTemplateRef = parentTaskTemplateRef.substring(1);
        }
        template.setParentTaskTemplateRefID(parentTaskTemplateRef);
        template.setSubTemplateRefIDs(WFStringUtils.refIDsToStrings(attrs.get("subTemplateRefs")));
        template.setSignoffQuorum(attrs.get("signoffQuorum"));
        template.setLocation(attrs.get("location"));
        template.setActionIDs(deDuplicate("Template with id=" + template.getId(), WFStringUtils.refIDsToStrings(attrs.get("actions"))));

        //Parse the WorkflowTemplate children (ApplicationRef, AssociatedDataSet, UserData, TaskDescription)
        try {
            while (reader.hasNext()) {
                int event = reader.nextTag();
                if (event == XMLStreamConstants.START_ELEMENT) {
                    String elementType = reader.getLocalName();
                    if (elementType.equals("AssociatedDataSet")) {
                        AssociatedDataSet dataSet = new AssociatedDataSet(reader.getAttributeValue(null, "id"));
                        dataSet.setRole(reader.getAttributeValue(null, "role"));
                        dataSet.setDataSetRef(reader.getAttributeValue(null, "dataSetRef").substring(1));
                        template.addDataSet(dataSet);
                    }
                    else if (elementType.equals("UserValue")) {
                        template.addReference(
                                reader.getAttributeValue(null, "value"),
                                reader.getAttributeValue(null, "title"),
                                reader.getAttributeValue(null, "dataRef").substring(1)
                        );
                    }
                    else if (elementType.equals("Item")) {
                        template.setTaskDescription(reader.getAttributeValue(null, "value"));
                    }
                }
                else if (event == XMLStreamConstants.END_ELEMENT) {
                    String elementType = reader.getLocalName();
                    if (elementType.equals("WorkflowTemplate")) {
                        break;
                    }
                }
            }
        }
        catch (XMLStreamException e) {
            logger.log(Level.WARNING, "Reading template children", e);
        }
        // Keep reading tokens until we hit the end WorkflowTemplate
        skipToEndOfTag(reader, WORKFLOW_TEMPLATE_TAG);

        return template;
    }

    private WorkflowAction getWorkflowAction(XMLStreamReader reader) {
        Map<String, String> attrs = getAttrMap(reader);
        WorkflowAction action = new WorkflowAction(attrs.get("id"));

        action.setActionType(attrs.get("actionType"));
        action.setParentRef(attrs.get("parentRef"));
        action.setActionHandlerRefIDs(WFStringUtils.refIDsToStrings(attrs.get("actionHandlerRefs")));
        action.setRuleRefIDS(WFStringUtils.refIDsToStrings(attrs.get("ruleRefs")));

        // Skip the ApplicationRef child
        skipToEndOfTag(reader, WORKFLOW_ACTION_TAG);
        return action;
    }

    private WorkflowHandler getWorkflowHandler(XMLStreamReader reader) {
        Map<String, String> attrs = getAttrMap(reader);
        WorkflowHandler handler = new WorkflowHandler(attrs.get("id"));
        handler.setName(attrs.get("name"));
        getHandlerArguments(reader, handler, WORKFLOW_HANDLER_TAG);
        skipToEndOfTag(reader, WORKFLOW_HANDLER_TAG);
        return handler;
    }

    private WorkflowHandler getWorkflowBusinessRuleHandler(XMLStreamReader reader) {
        Map<String, String> attrs = getAttrMap(reader);
        WorkflowHandler handler = new WorkflowHandler(attrs.get("id"));
        handler.setName(attrs.get("name"));
        getHandlerArguments(reader, handler, WORKFLOW_BUSINESS_RULE_HANDLER_TAG);
        skipToEndOfTag(reader, WORKFLOW_BUSINESS_RULE_HANDLER_TAG);
        return handler;
    }

    private WorkflowBusinessRule getWorkflowBusinessRule(XMLStreamReader reader) {
        Map<String, String> attrs = getAttrMap(reader);
        WorkflowBusinessRule rule = new WorkflowBusinessRule(attrs.get("id"));
        rule.setRuleHandlerRefIDs(WFStringUtils.refIDsToStrings(attrs.get("ruleHandlerRefs")));
        rule.setQuorum(attrs.get("ruleQuorum"));
        skipToEndOfTag(reader, WORKFLOW_BUSINESS_RULE_TAG);
        return rule;
    }

    private WorkflowSignoffProfile getWorkflowSignoffProfile(XMLStreamReader reader) {
        Map<String, String> attrs = getAttrMap(reader);
        WorkflowSignoffProfile profile = new WorkflowSignoffProfile(attrs.get("id"));
        profile.setNumberOfSignoffs(attrs.get("numberOfSignoffs"));
        profile.setGroupRefID(attrs.get("groupRef"));
        profile.setRoleRefID(attrs.get("roleRef"));
        profile.setAllowSubgroups(Boolean.valueOf(attrs.get("allowSubgroups")));
        profile.setSignoffQuorum(attrs.get("signoffQuorum"));
        skipToEndOfTag(reader, WORKFLOW_SIGNOFF_PROFILE_TAG);
        return profile;
    }

    private WorkflowOrganisation getWorklowOrganisation(XMLStreamReader reader) {
        Map<String, String> attrs = getAttrMap(reader);
        WorkflowOrganisation org = new WorkflowOrganisation(attrs.get("id"));
        org.setName(attrs.get("name"));

        while (skipToStartTag(reader, "UserValue", ORGANISATION_TAG)) {
            String title = reader.getAttributeValue(null, "title");
            if (title.equals("list_of_role")) {
                String dataRef = reader.getAttributeValue(null, "dataRef");
                org.addRoleRef(dataRef.substring(1));
            }
            else if (title.equals("privilege")) {
                org.setPrivilege(reader.getAttributeValue(null, "value"));
            }
        }

        skipToEndOfTag(reader, ORGANISATION_TAG);
        return org;
    }

    private WorkflowRole getWorkflowRole(XMLStreamReader reader) {
        Map<String, String> attrs = getAttrMap(reader);
        WorkflowRole role = new WorkflowRole(attrs.get("id"));
        role.setName(attrs.get("name"));
        skipToEndOfTag(reader, ROLE_TAG);
        return role;
    }

    private void getHandlerArguments(XMLStreamReader reader, WorkflowHandler handler, String endTag) {
        if (skipToStartTag(reader, ARGUMENTS_TAG, endTag)) {
            try {
                while (reader.hasNext()) {
                    int event = reader.nextTag();
                    if (event == XMLStreamConstants.START_ELEMENT) {
                        String elementType = reader.getLocalName();
                        if (elementType.equals("UserValue")) {
                            String title = reader.getAttributeValue(null, "title");
                            String value = reader.getAttributeValue(null, "value");
                            handler.addArgument(title, value);
                        }
                    }
                    if (event == XMLStreamConstants.END_ELEMENT) {
                        String elementType = reader.getLocalName();
                        if (elementType.equals(ARGUMENTS_TAG))
                            break;
                    }
                }
            }
            catch (XMLStreamException e) {
                logger.log(Level.WARNING, "Reading Args", e);
            }
        }
    }

    private Map<String, String> getAttrMap(XMLStreamReader reader) {
        Map<String, String> attrs = new HashMap<String, String>();
        final int attrCount = reader.getAttributeCount();
        for (int idx = 0; idx < attrCount; idx++) {
            attrs.put(reader.getAttributeLocalName(idx), reader.getAttributeValue(idx));
        }
        return attrs;
    }

    private boolean skipToStartTag(XMLStreamReader reader, String tag, String stopTag) {
        try {
            while (reader.hasNext()) {
                int event = reader.next();
                if (event == XMLStreamConstants.START_ELEMENT) {
                    String elementType = reader.getLocalName();
                    if (elementType.equals(tag)) {
                        return true;
                    }
                }
                else if (event == XMLStreamConstants.END_ELEMENT) {
                    String elementType = reader.getLocalName();
                    if (elementType.equals(stopTag)) {
                        return false;
                    }
                }
            }
        }
        catch (XMLStreamException e) {
            logger.log(Level.WARNING, "skipToStartTag(" + tag + "," + stopTag + ")", e);
        }
        return false;
    }

    private void skipToEndOfTag(XMLStreamReader reader, String tagName) {
        try {
            int event = reader.getEventType();
            if (event == XMLStreamConstants.END_ELEMENT) {
                String currentTag = reader.getLocalName();
                if (currentTag.equals(tagName)) {
                    return;
                }
            }
            while (reader.hasNext()) {
                event = reader.next();
                if (event == XMLStreamConstants.END_ELEMENT) {
                    String elementType = reader.getLocalName();
                    if (elementType.equals(tagName)) {
                        break;
                    }
                }
            }
        }
        catch (XMLStreamException e) {
            logger.log(Level.WARNING, "skipToEndOfTag(" + tagName + ")", e);
        }
    }

    private String[] deDuplicate(String context, String[] source) {
        if (source == null) return null;
        if (source.length == 0) return source;
        List<String> list = new ArrayList<String>();
        for (String ref : source) {
            if (list.contains(ref))
                logger.warning(context + " " + ref + " is duplicated");
            else
                list.add(ref);
        }
        return list.toArray(new String[list.size()]);
    }


}
