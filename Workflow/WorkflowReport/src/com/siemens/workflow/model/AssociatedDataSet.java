package com.siemens.workflow.model;

class AssociatedDataSet extends WorkflowNode {
    private String role;
    private String dataSetRef;

    AssociatedDataSet(String id) {
        super(id);
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDataSetRef() {
        return dataSetRef;
    }

    public void setDataSetRef(String dataSetRef) {
        this.dataSetRef = dataSetRef;
    }
}
