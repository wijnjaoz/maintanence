package com.siemens.workflow.model;

import java.util.ArrayList;
import java.util.List;

// WorkflowHandler or WorkflowBusinessRuleHandler
public class WorkflowHandler extends WorkflowNode {
    private String name;
    private final List<NamedArg> arguments = new ArrayList<NamedArg>();

    public WorkflowHandler(String id) {
        super(id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<NamedArg> getArguments() {
        return arguments;
    }

    public void addArgument(String name, String value) {
        NamedArg arg = new NamedArg(name, value);
        arguments.add(arg);
    }

}
