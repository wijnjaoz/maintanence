package com.siemens.workflow.model;

public class NamedArg {
    private final String name;
    private final String value;

    public NamedArg(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }
}
