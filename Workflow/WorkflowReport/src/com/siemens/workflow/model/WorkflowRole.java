package com.siemens.workflow.model;

public class WorkflowRole extends WorkflowNode {
    private String name;

    public WorkflowRole(String id) {
        super(id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
