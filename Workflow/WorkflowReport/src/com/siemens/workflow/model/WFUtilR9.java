package com.siemens.workflow.model;

import java.util.ArrayList;
import java.util.List;

class WFUtilR9 extends WFUtil {

    WFUtilR9() {
        // Action Handlers
        handlers.add("add-status");
        handlers.add("adhoc-signoffs");
        handlers.add("AI-process-export");
        handlers.add("AI-process-import");
        handlers.add("APPR-update-from-targets");
        handlers.add("approve-service-structure");
        handlers.add("auto-assign");
        handlers.add("auto-assign-rest");
        handlers.add("auto-relocate-file");
        handlers.add("CAE-batch-meshing-handler");
        handlers.add("CAE-simulation-process-launch-handler");
        handlers.add("change-all-started-to-pending");
        handlers.add("CONFMGMT-cut-back-effectivity");
        handlers.add("CR-assign-team-selector");
        handlers.add("CR-change-group-owner");
        handlers.add("CR-change-target-group");
        handlers.add("CR-change-target-group-owner");
        handlers.add("CR-fill-in-reviewers");
        handlers.add("CR-notify");
        handlers.add("create-status");
        handlers.add("debug");
        handlers.add("demote");
        handlers.add("demote-on-reject");
        handlers.add("DOCMGT-render-document-revision");
        handlers.add("DPV-export-device-to-ai");
        handlers.add("DPV-export-plant-to-ai");
        handlers.add("DPV-export-routine-to-ai");
        handlers.add("ECM-add-affected-irs-as-target");
        handlers.add("ECM-add-new-status-for-aff-revs");
        handlers.add("ECM-attach-components-to-change");
        handlers.add("ECM-copy-end-item-effectivity");
        handlers.add("ECM-create-base-revrule-form");
        handlers.add("ECM-notify-competing-changes");
        handlers.add("ECM-set-base-revrule");
        handlers.add("ECM-start-new-sub-processes");
        handlers.add("EPM-attach-assembly-components");
        handlers.add("EPM-attach-item-revision-targets");
        handlers.add("EPM-attach-related-objects");
        handlers.add("EPM-auto-check-in-out");
        handlers.add("EPM-change-ownership");
        handlers.add("EPM-check-signoff-comments");
        handlers.add("EPM-create-form");
        handlers.add("EPM-create-relation");
        handlers.add("EPM-create-sub-process");
        handlers.add("EPM-delete-ugcgm-markup");
        handlers.add("EPM-display-form");
        handlers.add("EPM-export-AI-AH");
        handlers.add("EPM-export-to-plmxmlfile");
        handlers.add("EPM-generate-image");
        handlers.add("EPM-generate-ugcgm-drawing");
        handlers.add("EPM-make-mature-design-primary");
        handlers.add("EPM-mark-archive");
        handlers.add("EPM-perform-offline-export");
        handlers.add("EPM-publish-target-objects");
        handlers.add("EPM-remove-objects");
        handlers.add("EPM-run-external-command");
        handlers.add("EPM-send-target-objects");
        handlers.add("EPM-send-to-oramfg");
        handlers.add("EPM-set-condition-by-check-validation-result");
        handlers.add("EPM-set-form-value-AH");
        handlers.add("EPM-set-job-protection");
        handlers.add("EPM-set-property");
        handlers.add("EPM-set-rule-based-protection");
        handlers.add("EPM-set-task-result-to-property");
        handlers.add("EPM-tessellation-handler");
        handlers.add("EPM-unpublish-target-objects");
        handlers.add("ERP-att-logfile-as-dataset-RH");
        handlers.add("ERP-attach-targets-AH");
        handlers.add("ERP-delete-log-dataset-AH");
        handlers.add("ERP-download-AH");
        handlers.add("ERP-post-upload-AH");
        handlers.add("ERP-set-pathnames-in-logds-AH");
        handlers.add("ERP-transform-AI-contents-AH");
        handlers.add("execute-follow-up");
        handlers.add("inherit");
        handlers.add("invoke-system-action");
        handlers.add("ISSUEMGT-check-review-decision");
        handlers.add("ISSUEMGT-update-issue-status");
        handlers.add("late-notification");
        handlers.add("OBJIO-release-and-replicate");
        handlers.add("notify");
        handlers.add("notify-signoffs");
        handlers.add("release-asbuilt-structure");
        handlers.add("release-asmaintained-structure");
        handlers.add("require-authentication");
        handlers.add("RM-attach-SM-tracelink-requirement");
        handlers.add("RM-attach-tracelink-requirement");
        handlers.add("SAP-set-valid-date-AH");
        handlers.add("SAP-upload-AH");
        handlers.add("schmgt-approve-timesheetentries");
        handlers.add("schmgt-revise-timesheetentries");
        handlers.add("set-condition");
        handlers.add("set-duration");
        handlers.add("set-parent-result");
        handlers.add("set-status");
        handlers.add("suspend-on-reject");
        handlers.add("system");
        handlers.add("TCX-auto-approve-first-step");
        handlers.add("TCX-create-form");
        handlers.add("TCX-Create-Print-Requests");
        handlers.add("TCX-create-snapshot");
        handlers.add("TCX-Create-Translation-Request");
        handlers.add("TCX-delete-dataset");
        handlers.add("TCX-delete-log-datasets");
        handlers.add("TCX-export-signoff-data");
        handlers.add("TCX-IRM-cleanfields");
        handlers.add("TCX-purge-dataset");
        handlers.add("TCX-release-previous-itemrevs");
        handlers.add("TCX-remove-targets-with-status");
        handlers.add("TCX-set-bom-precise");
        handlers.add("TCX-setstatus-EO-folder");
        handlers.add("TCX-store-cr-data");
        handlers.add("TCX-trigger-approve-first-step");
        handlers.add("trigger-action");
        handlers.add("trigger-action-on-related-process-task");
        handlers.add("TSTK-CreateTranslationRequest");
        handlers.add("VAL-approve-result-overrides");
        handlers.add("VAL-reject-result-overrides");
        handlers.add("VAL-set-condition-result-overrides");

        // Rule Handlers
        handlers.add("assert-signoffs-target-read-access");
        handlers.add("check-condition");
        handlers.add("check-process-completion");
        handlers.add("check-responsible-party");
        handlers.add("check-signoff");
        handlers.add("CR-assert-targets-checked-in");
        handlers.add("CR-check-item-status");
        handlers.add("debug-rule");
        handlers.add("disallow-adding-targets");
        handlers.add("disallow-removing-targets");
        handlers.add("ECM-check-target-is-ec");
        handlers.add("ECM-is-all-affected-irs-released");
        handlers.add("ECM-is-all-affected-irs-target");
        handlers.add("ECM-is-valid-ec-process");
        handlers.add("EPM-assert-target-classified");
        handlers.add("EPM-check-action-performer-role");
        handlers.add("EPM-check-assembly-status-progression");
        handlers.add("EPM-check-object-properties");
        handlers.add("EPM-check-occ-notes");
        handlers.add("EPM-check-oramfg-transfer-status");
        handlers.add("EPM-check-related-objects");
        handlers.add("EPM-check-status-progression");
        handlers.add("EPM-check-target-attachments");
        handlers.add("EPM-check-target-object");
        handlers.add("EPM-check-validation-result");
        handlers.add("EPM-check-validation-result-with-rules");
        handlers.add("EPM-hold");
        handlers.add("EPM-validate-target-objects");
        handlers.add("ERP-check-effective-date-RH");
        handlers.add("ERP-check-target-status-RH");
        handlers.add("ERP-validate-data-RH");
        handlers.add("invoke-system-rule");
        handlers.add("SAP-check-forms-attached-RH");
        handlers.add("SAP-check-forms-to-download-RH");
        handlers.add("TCX-check-approver");
        handlers.add("TCX-check-bom-precise");
        handlers.add("TCX-check-bomchild-statuslist");
        handlers.add("TCX-check-comps-against-pattern");
        handlers.add("TCX-check-datasets");
        handlers.add("TCX-check-itemrev-status");
        handlers.add("TCX-check-jobowner");
        handlers.add("TCX-check-prev-itemrev-status");
        handlers.add("TCX-check-signoff");
        handlers.add("TCX-check-status");
        handlers.add("TCX-has-target-drawing");
        handlers.add("validate-for-checkedout-asmaintained-physicalpartrevision");
        handlers.add("validate-for-checkedout-physicalpartrevision");
        handlers.add("validate-for-class");
        handlers.add("validate-for-latest-asmphysicalpartrevision");
        handlers.add("validate-for-physicalpartrevision");
        handlers.add("validate-for-unserviceable-physicalpartrevision");
        handlers.add("validate-missing-asmaintained-structure");
        handlers.add("validate-missing-structure");
    }

    @Override
    public List<String> getACLS(final WorkflowHandler handler) {
        final List<String> aclList = new ArrayList<String>();
        for (final NamedArg namedArg : handler.getArguments()) {
            aclList.add(namedArg.getValue());
        }
        return aclList;
    }

    @Override
    public boolean usesStatus(final WorkflowHandler handler) {
        if (handler.getName().equals("create-status")) return true;
        for (final NamedArg namedArg : handler.getArguments()) {
            final String arg = namedArg.getValue();
            if (arg.startsWith("-status=") || arg.startsWith("-status_allow=") || arg.startsWith("-status_disallow=")) {
                return true;
            }
        }
        return false;
    }

    @Override
    public List<String> getStatuses(final WorkflowHandler handler) {
        final List<String> statusList = new ArrayList<String>();
        if (handler.getName().equals("create-status")) {
            for (final NamedArg namedArg : handler.getArguments()) {
                statusList.add(namedArg.getValue());
            }
        }
        else {
            for (final NamedArg namedArg : handler.getArguments()) {
                final String arg = namedArg.getValue();
                if (arg.startsWith("-status=") || arg.startsWith("-status_allow=") || arg.startsWith("-status_disallow=")) {
                    int idx = arg.indexOf('=');
                    String values;
                    if (idx == -1) {
                        values = arg;
                    }
                    else {
                        values = arg.substring(idx + 1);
                    }
                    for (final String value : values.split(",")) {
                        if (isReportedStatus(value)) {
                            statusList.add(value);
                        }
                    }
                }
            }
        }
        return statusList;
    }

    @Override
    public boolean usesRevisionRule(final WorkflowHandler handler) {
        for (final NamedArg namedArg : handler.getArguments()) {
            final String arg = namedArg.getValue();
            if (arg != null) {
                if (arg.startsWith("-rev_rule=") || arg.startsWith("-revrule=") || arg.startsWith("-revisionrule=")) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public List<String> getRevisionRules(final WorkflowHandler handler) {
        final List<String> revRuleList = new ArrayList<String>();
        for (final NamedArg namedArg : handler.getArguments()) {
            String arg = namedArg.getValue();
            if (arg != null) {
                if (arg.startsWith("-rev_rule=") || arg.startsWith("-revrule=") || arg.startsWith("-revisionrule=")) {
                    int idx = arg.indexOf('=');
                    String revRule = arg.substring(idx + 1);
                    revRuleList.add(revRule);
                }
            }
        }
        return revRuleList;
    }

    @Override
    public boolean usesQuery(final WorkflowHandler handler) {
        if (handler.getName().equals("set-condition")) {
            for (final NamedArg namedArg : handler.getArguments()) {
                final String arg = namedArg.getValue();
                if (arg != null && arg.startsWith("$Query=")) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public List<String> getQueries(final WorkflowHandler handler) {
        final List<String> queryNames = new ArrayList<String>();
        if (handler.getName().equals("set-condition")) {
            for (final NamedArg namedArg : handler.getArguments()) {
                final String arg = namedArg.getValue();
                if (arg != null && arg.startsWith("$Query=")) {
                    String query = arg.substring(7);
                    queryNames.add(query);
                }
            }
        }
        return queryNames;
    }

    @Override
    public boolean usesForms(WorkflowHandler handler) {
        final String handlerName = handler.getName();
        return handlerName.equals("EPM-create-form") ||
                handlerName.equals("EPM-display-form");
    }

    @Override
    public List<String> getForms(WorkflowHandler handler) {
        final String handlerName = handler.getName();
        final List<String> formNames = new ArrayList<String>();
        for (final NamedArg namedArg : handler.getArguments()) {
            String arg = namedArg.getValue();
            if ((handlerName.equals("EPM-create-form") || handlerName.equals("EPM-display-form")) &&
                    arg != null && arg.startsWith("-type=")) {
                final String form = arg.substring(6);
                formNames.add(form);
            }
        }
        return formNames;
    }

    @Override
    public String getConditionValue(final WorkflowTemplate task, final String targetTask) {
        for (WorkflowAction action : task.getActions()) {
            for (WorkflowBusinessRule workflowBusinessRule : action.getRules()) {
                for (WorkflowHandler handler : workflowBusinessRule.getRuleHandlers()) {
                    if (handler.getName().equals("check-condition")) {
                        for (NamedArg namedArg : handler.getArguments()) {
                            String argValue = namedArg.getValue();
                            if (argValue.startsWith(targetTask + "=")) {
                                return argValue.substring(targetTask.length() + 1);
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

}
