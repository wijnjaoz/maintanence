package com.siemens.workflow.model;

import java.util.ArrayList;
import java.util.List;

// WorkflowAction
public class WorkflowAction extends WorkflowNode {
    private String actionType;
    private String parentRef;
    private String[] actionHandlerRefIDs;
    private final List<WorkflowHandler> actionHandlers = new ArrayList<WorkflowHandler>();
    private String[] ruleRefIDS;
    private final List<WorkflowBusinessRule> rules = new ArrayList<WorkflowBusinessRule>();

    public WorkflowAction(String id) {
        super(id);
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getParentRef() {
        return parentRef;
    }

    public void setParentRef(String parentRef) {
        this.parentRef = parentRef;
    }

    public String[] getActionHandlerRefIDs() {
        return actionHandlerRefIDs;
    }

    public void setActionHandlerRefIDs(String[] actionHandlerRefIDs) {
        this.actionHandlerRefIDs = actionHandlerRefIDs;
    }

    public String[] getRuleRefIDS() {
        return ruleRefIDS;
    }

    public void setRuleRefIDS(String[] ruleRefIDS) {
        this.ruleRefIDS = ruleRefIDS;
    }

    public void addActionHandler(WorkflowHandler handler) {
        actionHandlers.add(handler);
    }

    public void addRule(WorkflowBusinessRule workflowBusinessRule) {
        rules.add(workflowBusinessRule);
    }

    public List<WorkflowBusinessRule> getRules() {
        return rules;
    }

    public List<WorkflowHandler> getActionHandlers() {
        return actionHandlers;
    }

    public String getAactionTypeStr() {
        if (actionType == null || actionType.length() == 0) return "";
        else if (actionType.equals("1")) return "Assign";
        else if (actionType.equals("2")) return "Start";
        else if (actionType.equals("100")) return "Perform";
        else if (actionType.equals("4")) return "Complete";
        else if (actionType.equals("5")) return "Skip";
        else if (actionType.equals("6")) return "Suspend";
        else if (actionType.equals("7")) return "Resume";
        else if (actionType.equals("8")) return "Undo";
        else if (actionType.equals("9")) return "Abort";
        return "";
    }

    public boolean hasHandlers() {
        if (getActionHandlers().size() > 0) {
            return true;
        }
        for (WorkflowBusinessRule rule : getRules()) {
            if (rule.getRuleHandlers().size() > 0) {
                return true;
            }
        }
        return false;
    }
}
