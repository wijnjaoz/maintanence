package com.siemens.workflow.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class WFUtil {
    private static WFUtil INSTANCE = new UnconfiguredVersion();
    final Set<String> handlers = new HashSet<String>();

    WFUtil() {
    }

    public static void configureVersion(final String author) {
        if (author.startsWith("Teamcenter V8000")) {
            INSTANCE = new WFUtilR9();
        }
        else if (author.startsWith("Teamcenter V9000")) {
            INSTANCE = new WFUtilR9();
        }
        else if (author.startsWith("Teamcenter V10000")) {
            INSTANCE = new WFUtilR10();
        }
		else if (author.startsWith("Teamcenter V11000")) {
            INSTANCE = new WFUtilR11();
        }
		else if (author.startsWith("Teamcenter V12000")) {
            INSTANCE = new WFUtilR12();
        }
    }

    public static WFUtil getInstance() {
        return INSTANCE;
    }

    public boolean isCOTS(final WorkflowHandler handler) {
        return handlers.contains(handler.getName());
    }

    public boolean usesACL(final WorkflowHandler handler) {
        return handler.getName() != null && handler.getName().equals("EPM-set-rule-based-protection");
    }

    public abstract List<String> getACLS(final WorkflowHandler handler);
    public abstract boolean usesStatus(final WorkflowHandler handler);
    public abstract List<String> getStatuses(final WorkflowHandler handler);
    public abstract boolean usesRevisionRule(final WorkflowHandler handler);
    public abstract List<String> getRevisionRules(final WorkflowHandler handler);

    public boolean usesLOV(final WorkflowHandler handler) {
        for (final NamedArg namedArg : handler.getArguments()) {
            final String arg = namedArg.getValue();
            if (arg != null) {
                if (arg.startsWith("-lov=")) {
                    return true;
                }
            }
        }
        return false;
    }

    public List<String> getLOVs(final WorkflowHandler handler) {
        final List<String> lovNames = new ArrayList<String>();
        for (final NamedArg namedArg : handler.getArguments()) {
            final String arg = namedArg.getValue();
            if (arg != null) {
                if (arg.startsWith("-lov=")) {
                    String lov = arg.substring(5);
                    lovNames.add(lov);
                }
            }
        }
        return lovNames;
    }
    public abstract boolean usesQuery(final WorkflowHandler handler);
    public abstract List<String> getQueries(final WorkflowHandler handler);
    public abstract boolean usesForms(final WorkflowHandler handler);
    public abstract List<String> getForms(final WorkflowHandler handler);
    public abstract String getConditionValue(final WorkflowTemplate task, final String targetTask);

    boolean isReportedStatus(final String statusName) {
        return !("$NONE".equalsIgnoreCase(statusName) ||
                "NONE".equalsIgnoreCase(statusName) ||
                "ALL".equalsIgnoreCase(statusName) ||
                "ANY".equalsIgnoreCase(statusName) ||
                "IN_PROCESS".equalsIgnoreCase(statusName) ||
                "NULL".equalsIgnoreCase(statusName) ||
                "*".equals(statusName));
    }
}
