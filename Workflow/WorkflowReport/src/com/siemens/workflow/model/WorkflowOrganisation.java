package com.siemens.workflow.model;

import java.util.ArrayList;
import java.util.List;

public class WorkflowOrganisation extends WorkflowNode {
    private String name;
    private final List<String> listOfRoleRefs = new ArrayList<String>();
    private final List<WorkflowRole> roles = new ArrayList<WorkflowRole>();
    private String privilege;

    public WorkflowOrganisation(String id) {
        super(id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrivilege() {
        return privilege;
    }

    public void setPrivilege(String privilege) {
        this.privilege = privilege;
    }

    public List<String> getListOfRoleRefs() {
        return listOfRoleRefs;
    }

    public void addRoleRef(String ref) {
        listOfRoleRefs.add(ref);
    }

    public void addRole(WorkflowRole role) {
        roles.add(role);
    }

    public List<WorkflowRole> getRoles() {
        return roles;
    }
}
