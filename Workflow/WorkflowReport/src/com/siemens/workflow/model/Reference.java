package com.siemens.workflow.model;

public class Reference {
    private final String value;
    private final String title;
    private final String dataRefID;
    private WorkflowTemplate reference;

    public Reference(String value, String title, String dataRefID) {
        this.value = value;
        this.title = title;
        this.dataRefID = dataRefID;
    }

    public String getValue() {
        return value;
    }

    public String getTitle() {
        return title;
    }

    public String getDataRefID() {
        return dataRefID;
    }

    public WorkflowTemplate getReference() {
        return reference;
    }

    public void setReference(WorkflowTemplate reference) {
        this.reference = reference;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Reference)) return false;

        Reference reference = (Reference) o;

        return !(dataRefID != null ? !dataRefID.equals(reference.dataRefID) : reference.dataRefID != null);

    }

    @Override
    public int hashCode() {
        return dataRefID != null ? dataRefID.hashCode() : 0;
    }
}
