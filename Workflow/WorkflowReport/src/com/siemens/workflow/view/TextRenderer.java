package com.siemens.workflow.view;

import com.siemens.workflow.analysis.AnalysisResult;
import com.siemens.workflow.model.*;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.*;

public class TextRenderer {


    public void render(WorkflowTemplate rootTask, List<WorkflowTemplate> nodesToRender, Map<String,AnalysisResult> analysisResults, String extractTime, String commentLine, String author) {
        PrintStream ps;
        try {
            ps = new PrintStream(rootTask.getFileName() + ".txt");
            // Write the header
            ps.println(rootTask.getName());

            if (commentLine != null && commentLine.length() > 0) ps.println(commentLine);
//            ps.println("Extracted " + extractTime + " from " + author);

            // Write the summary table
            ps.println();
            ps.println("SUMMARY");
            ps.println("=======");
            ps.println();
            reportSummary(ps, rootTask);

            // Write the node content
            ps.println();
            ps.println("DETAILS");
            ps.println("=======");
            ps.println();
            reportTemplate(ps, rootTask);

            // Close the stream
            if (ps != System.out)
                ps.close();

        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    private void reportSummary(PrintStream ps, WorkflowTemplate rootTask) {
        Stack<WorkflowTemplate> nodeStack = new Stack<WorkflowTemplate>();
        Stack<Integer> levelStack = new Stack<Integer>();

        nodeStack.push(rootTask);
        levelStack.push(0);

        while (!nodeStack.isEmpty()) {
            WorkflowTemplate node = nodeStack.pop();
            int level = levelStack.pop();

            // Indent based on level
            StringBuilder lineBuilder = new StringBuilder();
            for (int idx = 0; idx < level; idx++) {
                lineBuilder.append("    ");
            }
            lineBuilder.append(node.getName());

            // If there is a description then write it on a single line.
            // Embedded new lines in the description are turned into #
            if (node.getTaskDescription() != null) {
                lineBuilder.append(" : ");
                String[] lines = node.getTaskDescription().split("\n");
                if (lines.length > 0) {
                    for (int i = 0; i < lines.length - 1; i++) {
                        lineBuilder.append(lines[i]);
                        lineBuilder.append('#');
                    }
                    lineBuilder.append(lines[lines.length - 1]);
                }
            }
            ps.println(lineBuilder);

            // Recurse down the children
            List<WorkflowTemplate> children = node.getSubTemplates();
            for (int idx = children.size() - 1; idx >= 0; --idx) {
                nodeStack.push(children.get(idx));
                levelStack.push(level + 1);
            }
        }
    }

    private void reportTemplate(PrintStream ps, WorkflowTemplate node) {
        String pathStr = node.getTemplatePath();
        ps.println(String.format("Path : %s", pathStr));
        ps.println(String.format("Type : %s", node.getObjectType()));

        // The task description indented
        ps.println("Description : ");
        if (node.getTaskDescription() != null) {
            for (String line : node.getTaskDescription().split("\n")) {
                ps.println("    " + line);
            }
        }

        // Is it asynchronous?
        ps.println("Asynchronous : " + String.valueOf(node.isAsynchronous()));
        
        // The task actions
        for (WorkflowSignoffProfile signoffProfile : node.getSignoffProfiles()) {
            ps.println("Signoff Profile");
            ps.println("  Wait for Undecided Reviewers : " + (node.isWaitForUndecidedReviewers() ? "Y" : "N"));
            reportSignoffProfile(ps, signoffProfile);
        }

        if (node.hasHandlers()) {
            ps.println("Actions");

            for (WorkflowAction action : reorderActions(node.getActions())) {
                if (action.hasHandlers()) {
                    for (WorkflowBusinessRule rule : action.getRules()) {
                        for (WorkflowHandler workflowHandler : rule.getRuleHandlers()) {
                            ps.println(String.format("%-10.10s : Rule(Quorum=%-2s) : %s",
                                    action.getAactionTypeStr(),
                                    rule.getQuorum(),
                                    getHandlerString(workflowHandler)));
                        }
                    }
                    if (action.getActionHandlers().size() > 0) {
                        for (WorkflowHandler workflowHandler : action.getActionHandlers()) {
                            ps.println(String.format("%-10.10s : Action          : %s",
                                    action.getAactionTypeStr(),
                                    getHandlerString(workflowHandler)));
                        }
                    }
                }
            }
        }

        ps.println("============");
        ps.println();

        // Recurse down the children
        for (WorkflowTemplate subTask : node.getSubTemplates()) {
            reportTemplate(ps, subTask);
        }

    }

    private String getHandlerString(WorkflowHandler workflowHandler) {
        StringBuilder builder = new StringBuilder();
        builder.append(workflowHandler.getName());
        for (NamedArg namedArg : workflowHandler.getArguments()) {
            builder.append(" ");
            builder.append(namedArg.getValue());
        }
        return builder.toString();
    }

    private void reportSignoffProfile(PrintStream ps, WorkflowSignoffProfile profile) {
        ps.println(String.format("  Signoff Quorum : %s", profile.getSignoffQuorum()));
        ps.println(String.format("  Allow Sub Groups: %s", (profile.isAllowSubgroups() ? " Y " : " N ")));
        if (profile.getGroupRefID() != null) {
            WorkflowOrganisation org = profile.getOrganisation();
            String orgName = org.getName();
            String roleName = "*";
            if (profile.getRole() != null) {
                WorkflowRole role = profile.getRole();
                roleName = role.getName();
            }
            ps.println("  " + orgName + "/" + roleName + "/" + profile.getNumberOfSignoffs());
        }
        else if (profile.getRole() != null) {
            WorkflowRole role = profile.getRole();
            ps.println("  */" + role.getName() + "/" + profile.getNumberOfSignoffs());
        }
    }

    private List<WorkflowAction> reorderActions(List<WorkflowAction> actions) {
        List<WorkflowAction> orderedActions = new ArrayList<WorkflowAction>();
        for (String type : new String[]{"1", "2", "100", "4", "5", "6", "7", "9", "8"}) {
            for (WorkflowAction action : actions) {
                if (action.getActionType().equals(type)) {
                    orderedActions.add(action);
                }
            }
        }
        return orderedActions;
    }

}
