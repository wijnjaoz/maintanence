package com.siemens.workflow.view;

import com.siemens.workflow.WFStringUtils;
import com.siemens.workflow.analysis.AnalysisResult;
import com.siemens.workflow.model.*;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.*;

public class HTMLRenderer {


    public void render(WorkflowTemplate rootTask, List<WorkflowTemplate> nodesToRender, Map<String,AnalysisResult> analysisResults, String extractTime, String commentLine, String author) {
        // Calculate the width of the handler arg names
        int argNameWIdth = calculateArgumentNameWidth(rootTask);

        PrintStream ps;
        try {
            ps = new PrintStream(rootTask.getFileName() + ".html");
            // Write the header
            ps.println("<!DOCTYPE html>");
            ps.println("<html>");
            ps.println("<head>");
            ps.println("<title>" + rootTask.getName() + "</title>");

            ps.println("<style>");
            ps.println("table, tr, td { border: black 1px solid; border-collapse: collapse;}");
            ps.println("table.tasktable {min-width: 800px}");
            ps.println("body { font-family: sans-serif; font-size: 10pt;}");
            ps.println("tr.tasktitle { background-color: rgb(200,200,200);}");
            ps.println(".spacer { width: 50px;}");
            ps.println("td.actiontype { min-width: 80px; text-align: right; vertical-align: top;}");
            ps.println("td.actionarg { vertical-align: top;}");
            ps.println("td.argname { vertical-align: top; width:" + (argNameWIdth * 3 / 5) + "em;}");
            ps.println("td.argvalue { vertical-align: top;}");
            ps.println("td.tablecol1 { vertical-align: top; width: 100px;}");
            ps.println("td.level { min-width: 60px; vertical-align: top;}");
            ps.println("td.up { width: 16px;}");
            ps.println("img { border: none;}");
            ps.println("a { color: WindowText; text-decoration: none;}");
            ps.println("</style>");
            ps.println("</head>");
            ps.println("<body>");

            ps.println("<h1>" + rootTask.getName() + "</h1>");
            if (commentLine != null && commentLine.length() > 0) ps.println("<h3>" + commentLine + "</h3>");
            ps.println("<div>Extracted " + extractTime + " from " + author + "</div>");

            // Write the image maps
            for (WorkflowTemplate node : nodesToRender) {
                List<NodeMetric> metrics = node.getNodeMetrics();
                ps.println("<map name=\"" + node.getId() + "\">");
                for (NodeMetric metric : metrics) {
                    String title = metric.getTitle();
                    if (!title.equals("Start") && !title.equals("Finish")) {
                        ps.println("  <area href=\"#" + metric.getNode().getId() + (metric.getNode().getSubTemplates().size() > 0 ? "_top" : "") +
                                "\" alt=\"" + WFStringUtils.safeHTML(metric.getNode().getTemplatePath()) +
                                "\" shape=\"rect\" coords=\"" +
                                metric.getLeft() + "," +
                                metric.getTop() + "," +
                                metric.getRight() + "," +
                                metric.getBottom() +
                                "\">");
                    }
                }
                ps.println("</map>");
            }

            // Write the summary table
            reportSummary(ps, rootTask);

            // Write the node content
            for (WorkflowTemplate node : nodesToRender) {
                reportTemplate(ps, node, true);
            }

            ps.println("<h2>External Dependencies</h2>");

            for (String key : new TreeSet<String>(analysisResults.keySet())) {
                AnalysisResult result = analysisResults.get(key);
                ps.println("<h3>" + result.getTitle() + "</h3>");
                ps.println("<table>");
                reportAnalysisResult(ps, result);
                ps.println("</table>");
            }

            // Write the trailer
            ps.println("</body>");
            ps.println("</html>");


            // Close the stream
            if (ps != System.out)
                ps.close();

        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    private int calculateArgumentNameWidth(WorkflowTemplate rootTask) {
        int argWidth = 0;
        Stack<WorkflowTemplate> nodeStack = new Stack<WorkflowTemplate>();
        nodeStack.push(rootTask);

        while (!nodeStack.empty()) {
            WorkflowTemplate node = nodeStack.pop();

            for (WorkflowAction action : node.getActions()) {
                for (WorkflowBusinessRule rule : action.getRules()) {
                    for (WorkflowHandler handler : rule.getRuleHandlers()) {
                        for (NamedArg arg : handler.getArguments()) {
                            int nameLen = arg.getValue().indexOf('=');
                            if (nameLen > argWidth) argWidth = nameLen;
                        }
                    }
                }
                for (WorkflowHandler handler : action.getActionHandlers()) {
                    for (NamedArg arg : handler.getArguments()) {
                        int nameLen = arg.getValue().indexOf('=');
                        if (nameLen > argWidth) argWidth = nameLen;
                    }
                }
            }

            if (node.getSubTemplates().size() > 0)
                nodeStack.addAll(node.getSubTemplates());
        }

        return argWidth;
    }

    private void reportSummary(PrintStream ps, WorkflowTemplate rootTask) {

        int nlevels = rootTask.getMaxDepth();
        // Create a table with nlevels + 1 cols
        ps.printf("<h2 class=\"tasktitle\">Task Summary</h2>\n");
        ps.println("<table>");
        ps.println("<tr class=\"tasktitle\">");
        for (int idx = 0; idx < nlevels; idx++) {
            ps.printf("<td class=\"level\">Level %d</td>", idx);
        }
        ps.println("<td>Description</td>");
        ps.println("</tr>");

        Stack<WorkflowTemplate> nodeStack = new Stack<WorkflowTemplate>();
        Stack<Integer> levelStack = new Stack<Integer>();

        nodeStack.push(rootTask);
        levelStack.push(0);

        while (!nodeStack.isEmpty()) {
            WorkflowTemplate node = nodeStack.pop();
            int level = levelStack.pop();
            List<WorkflowTemplate> children = node.getSubTemplates();

            ps.println("<tr>");
            if (level > 0)
                ps.printf("<td colspan=%d>&nbsp;</td>", level);
            ps.printf("<td class=\"level\" colspan=%d><a href=\"#%s%s\"><img src=\"%s.png\">%s</a></td>",
                    nlevels - level, node.getId(), children.size() > 0 ? "_top" : "", node.getIconKey(), node.getName());

            ps.print("<td>");
            if (node.getTaskDescription() != null) {
                for (String line : node.getTaskDescription().split("\n")) {
                    ps.println(line + "<br>");
                }
            }
            else {
                ps.print("&nbsp;");
            }
            ps.print("</td>");
            ps.println("</tr>");

            for (int idx = children.size() - 1; idx >= 0; --idx) {
                nodeStack.push(children.get(idx));
                levelStack.push(level + 1);
            }
        }

        ps.println("</table>");
    }

    private void reportTemplate(PrintStream ps, WorkflowTemplate node, boolean isTopLevel) {

        String pathStr = node.getTemplatePath();

        if (isTopLevel) {
            ps.println("<h2 class=\"tasktitle\">" + node.getName() + "</h2>");
            ps.println("<a name=\"" + node.getId() + "_top\"></a>");
        }
        else
            ps.println("<a name=\"" + node.getId() + "\"></a>");

        ps.println("<table class=\"tasktable\">");
        // task name
        ps.println("<tr class=\"tasktitle\"><td class=\"tablecol1\">");
        if (!isTopLevel) ps.print("Sub");
        ps.println("Task Name</td>");
        if (node.getSubTemplates().size() > 0) {
            ps.println("<td><a href=\"#" + node.getId() + "_top\">" + node.getName() + "</a></td>");
        }
        else {
            ps.println("<td>" + node.getName() + "</td>");
        }
        ps.println("<td>" + node.getObjectType() + "</td>");
        if (node.getParentTaskTemplateRefID() != null) {
            ps.println("<td class=\"up\"><a href=\"#" + node.getParentTaskTemplateRefID() + "_top\"><img src=\"up.png\"></a></td>");
        }
        ps.println("</tr>");

        if (isTopLevel) {
            // The task image
            ps.println("<tr><td colspan=3><img src=\"" + node.getImageName() + ".png\" usemap=\"#" + node.getId() + "\"></td></tr>");
        }

        // The task path

        ps.print("<tr><td class=\"tablecol1\">Path</td><td colspan=2>");
        ps.println(pathStr + "</td></tr>");

        // The task description
        ps.println("<tr><td class=\"tablecol1\">Description</td><td colspan=2>");
        if (node.getTaskDescription() != null) {
            for (String line : node.getTaskDescription().split("\n")) {
                ps.println(line + "<br>");
            }
        }
        else {
            ps.print("&nbsp;");
        }
        ps.println("</td></tr>");

        // Is it asynchronous?
        ps.println("<tr><td class=\"tablecol1\">Asynchronous</td><td colspan=2>" +
                String.valueOf(node.isAsynchronous()) + "</td></tr>");
        

        // The task actions
        for (WorkflowSignoffProfile signoffProfile : node.getSignoffProfiles()) {
            ps.println("<tr><td class=\"tablecol1\">Signoff Profile</td><td>Wait for Undecided Reviewers</td><td>" +
                        (node.isWaitForUndecidedReviewers() ? "Y" : "N") +
                        "</td></tr>");
            reportSignoffProfile(ps, signoffProfile);
        }

        if (node.hasHandlers()) {
            ps.println("<tr><td colspan=3>Actions</td></tr>");

            for (WorkflowAction action : reorderActions(node.getActions())) {
                if (action.hasHandlers()) {
                    ps.println("<tr><td colspan=3>" + action.getAactionTypeStr() + "</td></tr>");
                    for (WorkflowBusinessRule rule : action.getRules()) {
                        ps.println("<tr><td class=\"actiontype tablecol1\">Rule</td><td colspan=2>");
                        ps.println("<table style=\"width:100%\">");
                        ps.println("<tr><td colspan=3>Quorum = " + rule.getQuorum() + "</td></tr>");
                        reportHandlers(ps, rule.getRuleHandlers());
                        ps.println("</table></td></tr>");
                    }
                    if (action.getActionHandlers().size() > 0) {
                        ps.println("<tr><td class=\"actiontype tablecol1\">Action</td><td colspan=2>");
                        ps.println("<table style=\"width:100%\">");
                        reportHandlers(ps, action.getActionHandlers());
                        ps.println("</table></td></tr>");
                    }
                }
            }
        }
        ps.println("</table>");
        ps.println("<br>");

        // Iterate over the direct sub tasks (should be on the diagram)
        if (isTopLevel) {
            for (WorkflowTemplate subTask : node.getSubTemplates()) {
                reportTemplate(ps, subTask, false);
            }
        }

    }

    private void reportSignoffProfile(PrintStream ps, WorkflowSignoffProfile profile) {
        ps.println("<tr><td>&nbsp;</td><td>Signoff Quorum : " + profile.getSignoffQuorum() +
                "</td><td>Allow Sub Groups : " + (profile.isAllowSubgroups() ? "Y" : "N") +
                "</td></tr>");
        if (profile.getGroupRefID() != null) {
            WorkflowOrganisation org = profile.getOrganisation();
            String orgName = org.getName();
            String roleName = "*";
            if (profile.getRole() != null) {
                WorkflowRole role = profile.getRole();
                roleName = role.getName();
            }
            ps.println("<tr><td>&nbsp;</td><td colspan=2>" + orgName + "/" + roleName + "/" + profile.getNumberOfSignoffs() + "</td></tr>");
        }
        else if (profile.getRole() != null) {
            WorkflowRole role = profile.getRole();
            ps.println("<tr><td>&nbsp;</td><td colspan=2>*/" + role.getName() + "/" + profile.getNumberOfSignoffs() + "</td></tr>");
        }
    }

    private List<WorkflowAction> reorderActions(List<WorkflowAction> actions) {
        List<WorkflowAction> orderedActions = new ArrayList<WorkflowAction>();
        for (String type : new String[]{"1", "2", "100", "4", "5", "6", "7", "9", "8"}) {
            for (WorkflowAction action : actions) {
                if (action.getActionType().equals(type)) {
                    orderedActions.add(action);
                }
            }
        }
        return orderedActions;
    }

    private void reportHandlers(PrintStream ps, List<WorkflowHandler> handlers) {
        if (handlers == null || handlers.size() == 0)
            return;
        for (WorkflowHandler handler : handlers) {
            ps.println("<tr><td colspan=3>" + handler.getName() + "</td></tr>");
            for (NamedArg namedArg : handler.getArguments()) {
                String rawValue = namedArg.getValue();
                int idx = rawValue.indexOf('=');
                String name;
                String value;
                if (idx != -1) {
                    name = rawValue.substring(0, idx);
                    value = rawValue.substring(idx + 1);
                    ps.println("<tr><td class=\"spacer\"><div class=\"spacer\">&nbsp;</div></td><td class=\"argname\">" + name + "</td><td class=\"argvalue\">" + WFStringUtils.spanList(value) + "</td></tr>");
                }
                else {
                    name = rawValue;
                    ps.println("<tr><td class=\"spacer\"><div class=\"spacer\">&nbsp;</div></td><td class=\"actionarg\" colspan=2>" + WFStringUtils.spanList(name) + "</td></tr>");
                }
            }
        }
    }

    private void reportAnalysisResult(PrintStream ps, AnalysisResult analysisResult) {
        Map<String, List<WorkflowTemplate>> results = analysisResult.getResults();
        for (String key : results.keySet()) {
            List<WorkflowTemplate> whereUsed = results.get(key);
            ps.println("<tr><td rowspan=" + whereUsed.size() + ">" + key +
                    "</td><td><a href=\"#" + whereUsed.get(0).getId() + "\">" + whereUsed.get(0).getTemplatePath() + "</a></td></tr>");
            for (int idx = 1; idx < whereUsed.size(); idx++) {
                ps.println("<tr><td><a href=\"#" + whereUsed.get(idx).getId() + "\">" + whereUsed.get(idx).getTemplatePath() + "</a></td></tr>");
            }
        }
    }

}
