package com.siemens.workflow.view;

import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.util.List;

public class Packager {
    private static final String CRLF = "\015\012";
    private static final int LINE_LENGTH = 76;


    public void packageAsMHT(String outputFile, String htmlFile, List<String> imageFiles) {
        // Create the output stream
        PrintStream ps = null;
        try {
            ps = new PrintStream(outputFile);
            // Make the MIME boundary
            int randomInt = (int) (Math.random() * Integer.MAX_VALUE);
            String boundary = String.format("%x", randomInt);

            // Write the MIME Header
            ps.print("MIME-Version: 1.0");
            ps.print(CRLF);
            ps.print("Content-Type: multipart/related; boundary=\"");
            ps.print(boundary);
            ps.print("\"; type=\"text/html\"");
            ps.print(CRLF);
            ps.print(CRLF);

            // Write the HTML part
            ps.print("--");
            ps.print(boundary);
            ps.print(CRLF);
            ps.print("Content-Type: text/html");
            ps.print(CRLF);
            ps.print("Content-Transfer-Encoding: quoted-printable");
            ps.print(CRLF);
            ps.print("Content-Location: ");
            ps.print(htmlFile);
            ps.print(CRLF);
            ps.print(CRLF);

            streamTextFile(htmlFile, ps);

            // Loop over the images
            for (String imageFile : imageFiles) {
                ps.print("--");
                ps.print(boundary);
                ps.print(CRLF);
                ps.print("Content-Location: ");
                ps.print(imageFile);
                ps.print(CRLF);
                ps.print("Content-Type: image/png");
                ps.print(CRLF);
                ps.print("Content-Transfer-Encoding: base64");
                ps.print(CRLF);
                ps.print(CRLF);

                streamImageInBase64(imageFile, ps);
            }

            ps.print("--");
            ps.print(boundary);
            ps.print("--");
            ps.print(CRLF);
            ps.print(CRLF);

            ps.close();
            ps = null;
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        finally {
            // Close the output stream
            if (ps != null)
                ps.close();
        }
    }

    private void streamImageInBase64(String source, PrintStream ps) {
        File image = new File(source);
        int fileSize = (int) image.length();

        byte buffer[] = new byte[fileSize];
        int remaining = fileSize;

        try {
            FileInputStream fis = new FileInputStream(image);
            int nread = fis.read(buffer);
            while (nread != -1 && remaining > 0) {
                remaining -= nread;
                if (remaining > 0) {
                    nread = fis.read(buffer, fileSize - remaining, remaining);
                }
            }
            fis.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        String imageB64 = DatatypeConverter.printBase64Binary(buffer);
        int numLines = imageB64.length() / LINE_LENGTH;
        int remainder = imageB64.length() - (numLines * LINE_LENGTH);

        for (int idx = 0; idx < numLines; idx++) {
            ps.append(imageB64, idx * LINE_LENGTH, idx * LINE_LENGTH + LINE_LENGTH);
            ps.print(CRLF);
        }
        if (remainder > 0) {
            ps.append(imageB64, numLines * LINE_LENGTH, imageB64.length());
            ps.print(CRLF);
        }
    }

    private void streamTextFile(String htmlFile, PrintStream ps) {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(htmlFile));

            String line = br.readLine();
            StringBuilder sb = new StringBuilder(80);
            while (line != null) {
                int lineLen = line.length();
                for (int idx = 0; idx < lineLen; idx++) {
                    char ch = line.charAt(idx);

                    if (ch == '=')
                        sb.append("=3D");
                    else
                        sb.append(ch);
                    if (sb.length() >= LINE_LENGTH) {
                        ps.print(sb.toString());
                        ps.print("=");
                        ps.print(CRLF);
                        sb.setLength(0);
                    }
                }
                if (sb.length() > 0) {
                    ps.print(sb.toString());
                    ps.print(CRLF);
                    sb.setLength(0);
                }
                line = br.readLine();
            }

            br.close();
            br = null;
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if (br != null)
                try {
                    br.close();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }


}
