package com.siemens.workflow.view;

import com.siemens.workflow.WFStringUtils;
import com.siemens.workflow.model.Reference;
import com.siemens.workflow.model.WorkflowTemplate;

import javax.imageio.ImageIO;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ImageRenderer {
    private static final int HEADER_HEIGHT = 16;
    private static final int BORDER_WIDTH = 4;
    private static final double EDGE_OFFSET = 3.0;
    private static final double MINANGLE = (5 * Math.PI / 180.0);
    private static final Color NODE_BACKGROUND = Color.decode("#d4d0c8");
    private static final Color HEADER_COLOUR = Color.decode("#8c8c8c");
    private static final Color EDGE_COLOUR = Color.decode("#7f7f7f");
    private static final Color DASHED_EDGE_COLOUR = Color.decode("#d00000");
    private static final Color CANVAS_BACKGROUND = Color.decode("#ffffff");
    private static final Color ASYNC_COLOUR = Color.decode("#a000a0");
    private final int NODE_MAX_X = 500;
    private final int NODE_MAX_Y = 66;
    private static final BasicStroke normalStroke = new BasicStroke(2);
    private static final BasicStroke dashedStroke = new BasicStroke(2, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER, 10f, new float[]{10.0f}, 0.0f);
    private static final double EPS = 0.001;
    private final IconManager iconManager = new IconManager();

    public ImageRenderer() {
    }

    public void render(WorkflowTemplate task, String imageName) {
        Map<String, NodeMetric> nodeMetrics = new HashMap<String, NodeMetric>();
        Map<String, BufferedImage> nodeImages = new HashMap<String, BufferedImage>();
        Set<Edge> edges = new HashSet<Edge>();

        // Iterate over the nodes generating images and node metrics for each one
        // First the start and finish nodes
        // Paint the nodes
        String startName = "start";
        String finishName = "finish";
        if (task.isSubTask()) {
            startName = "subtaskstart";
            finishName = "subtaskfinish";
        }
        BufferedImage startImage = getNodeImage("Start", new String[]{startName}, task.setsACL(), task.isAsynchronous());
        NodeMetric startMetric = new NodeMetric("Start", task.getStartxloc(), task.getStartyloc(),
                task.getStartxloc() + startImage.getWidth(), task.getStartyloc() + startImage.getHeight());
        nodeImages.put("start", startImage);
        nodeMetrics.put("start", startMetric);
        nodeMetrics.put(task.getId(), startMetric);

        BufferedImage finishImage = getNodeImage("Finish", new String[]{finishName}, false, false);
        NodeMetric finishMetric = new NodeMetric("Finish", task.getFinishxloc(), task.getFinishyloc(),
                task.getFinishxloc() + finishImage.getWidth(), task.getFinishyloc() + finishImage.getHeight());
        nodeImages.put("finish", finishImage);
        nodeMetrics.put("finish", finishMetric);

        for (WorkflowTemplate subTask : task.getSubTemplates()) {
            String icons[];

            if (subTask.getSubTemplateRefIDs().length > 0) {
                icons = new String[]{"subtasks", subTask.getIconKey()};
            }
            else {
                icons = new String[]{subTask.getIconKey()};
            }
            BufferedImage nodeImage = getNodeImage(subTask.getName(), icons, subTask.setsACL(), subTask.isAsynchronous());
            NodeMetric metric = new NodeMetric(subTask.getName(), subTask.getXloc(), subTask.getYloc(),
                    subTask.getXloc() + nodeImage.getWidth(), subTask.getYloc() + nodeImage.getHeight());
            nodeImages.put(subTask.getId(), nodeImage);
            metric.setNode(subTask);
            nodeMetrics.put(subTask.getId(), metric);
        }

        // Build an image that is big enough to contain all the nodes (plus a bit)
        int maxX = 0;
        int maxY = 0;
        for (NodeMetric nodeMetric : nodeMetrics.values()) {
            maxX = Math.max(maxX, nodeMetric.getRight());
            maxY = Math.max(maxY, nodeMetric.getBottom());
        }

        maxX += 20;
        maxY += 20;

        BufferedImage image = new BufferedImage(maxX, maxY, BufferedImage.TYPE_INT_RGB);
        Graphics2D imageGraphics = image.createGraphics();
        imageGraphics.setBackground(CANVAS_BACKGROUND);
        imageGraphics.clearRect(0, 0, maxX, maxY);
        imageGraphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        // Place all of the node sub images onto the main image
        for (String nodeId : nodeMetrics.keySet()) {
            NodeMetric metric = nodeMetrics.get(nodeId);
            imageGraphics.drawImage(nodeImages.get(nodeId), null, metric.getLeft(), metric.getTop());
        }

        // Render the edges
        // Add the complete edges to the root finish node
        for (Reference reference : task.getReferences()) {
            if (reference.getTitle().equals("completeDependencyTaskRef")) {
                edges.add(
                        new Edge(nodeMetrics.get(reference.getDataRefID()), finishMetric, Edge.NORMAL, null)
                );
            }
        }

        for (WorkflowTemplate subTask : task.getSubTemplates()) {
            if (task.getObjectType().equals("EPMRouteTaskTemplate")) {
                edges.add(
                        new Edge(startMetric, nodeMetrics.get(subTask.getId()), Edge.NORMAL, null)
                );
            }
            if (subTask.getReferences().size() == 0) {
                edges.add(
                        new Edge(nodeMetrics.get(subTask.getParentTaskTemplateRefID()), nodeMetrics.get(subTask.getId()), Edge.NORMAL, null)
                );
            }
            else {
                boolean hadStartingReference = false;
                for (Reference reference : subTask.getReferences()) {
                    if (reference.getTitle().equals("startDependencyTaskRef") ||
                            reference.getTitle().equals("failDependencyTaskRef") ||
                            reference.getTitle().equals("refailDependencyTaskRef")
                            )
                        hadStartingReference = true;
                    if (reference.getTitle().equals("parentDependencyTaskRef")) {
                        edges.add(
                                new Edge(nodeMetrics.get(reference.getDataRefID()), nodeMetrics.get(subTask.getId()), Edge.NORMAL, null)
                        );
                    }
                    else if (reference.getTitle().equals("failDependencyTaskRef") || reference.getTitle().equals("refailDependencyTaskRef")) {
                        WorkflowTemplate targetTask = reference.getReference();
                        String label = subTask.getConditionValue(targetTask.getName());
                        String labelText = WFStringUtils.getLabelFromValue(label);
                        edges.add(
                                new Edge(nodeMetrics.get(reference.getDataRefID()), nodeMetrics.get(subTask.getId()), Edge.FAIL, labelText)
                        );
                    }
                    else if (reference.getTitle().equals("startDependencyTaskRef") || reference.getTitle().equals("restartDependencyTaskRef")) {
                        WorkflowTemplate targetTask = reference.getReference();
                        String label = subTask.getConditionValue(targetTask.getName());
                        String labelText = WFStringUtils.getLabelFromValue(label);
                        edges.add(
                                new Edge(nodeMetrics.get(reference.getDataRefID()), nodeMetrics.get(subTask.getId()), Edge.NORMAL, labelText)
                        );
                    }
                }
                if (!hadStartingReference && task.getParentTaskTemplateRefID() == null) {
                    edges.add(
                            new Edge(nodeMetrics.get(subTask.getParentTaskTemplateRefID()), nodeMetrics.get(subTask.getId()), Edge.NORMAL, null)
                    );
                }
            }
        }

        for (Edge edge : edges) {
            paintEdge(imageGraphics, edge);
        }

        // Write the image
        try {
            ImageIO.write(image, "png", new File(imageName));
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        // Store the node metrics on the node
        List<NodeMetric> metrics = new ArrayList<NodeMetric>();
        metrics.addAll(nodeMetrics.values());
        task.setNodeMetrics(metrics);
    }

    private BufferedImage getNodeImage(String title, String[] nodeIcons, boolean setsACL, boolean isAsync) {
        BufferedImage nodeImage = new BufferedImage(NODE_MAX_X, NODE_MAX_Y, BufferedImage.TYPE_INT_RGB);
        Graphics2D nodeGraphics = nodeImage.createGraphics();

        // Work out the size of the title
        Font font = new Font(Font.SANS_SERIF, Font.PLAIN, 9);
        nodeGraphics.setFont(font);
        FontMetrics metrics = nodeGraphics.getFontMetrics();
        Rectangle2D bounds = metrics.getStringBounds(title, nodeGraphics);

        int nodeWidth = Math.max(42, (int) bounds.getWidth() + BORDER_WIDTH * 2);

        nodeGraphics.setBackground(NODE_BACKGROUND);
        nodeGraphics.clearRect(0, 0, nodeWidth, NODE_MAX_Y);
        nodeGraphics.setColor(Color.BLACK);
        int textpos = (int) ((nodeWidth - bounds.getWidth()) / 2);
        nodeGraphics.drawString(title, textpos, NODE_MAX_Y - BORDER_WIDTH);
        nodeGraphics.setColor(HEADER_COLOUR);
        nodeGraphics.fillRect(0, 0, nodeWidth, HEADER_HEIGHT);

        if (isAsync) {
            nodeGraphics.setColor(ASYNC_COLOUR);
            nodeGraphics.setStroke(dashedStroke);
            nodeGraphics.drawRect(1, 1, nodeWidth - 2, NODE_MAX_Y - 2);
            nodeGraphics.setStroke(normalStroke);
        }

        BufferedImage icons[] = new BufferedImage[nodeIcons.length];
        int totalIconWidth = 0;
        if (nodeIcons.length > 0) {
            for (int idx = 0; idx < nodeIcons.length; idx++) {
                icons[idx] = iconManager.getIcon(nodeIcons[idx]);
                totalIconWidth += icons[idx].getWidth();
            }
        }
        if (nodeIcons.length > 0) {
            int iconStart = (nodeWidth - totalIconWidth) / 2;
            for (BufferedImage icon : icons) {
                nodeGraphics.drawImage(icon, null, iconStart, (66 - icon.getHeight()) / 2);
                iconStart += icon.getWidth();
            }
        }
        if (setsACL) {
            nodeGraphics.drawImage(iconManager.getIcon("setsACL"), null, 0, 0);
        }

        return nodeImage.getSubimage(0, 0, nodeWidth, NODE_MAX_Y);
    }

    private void paintEdge(Graphics2D imageGraphics, Edge edge) {
        int fromX = edge.getFromX();
        int toX = edge.getToX();
        int fromY = edge.getFromY();
        int toY = edge.getToY();

        int edgeLen = (int) Math.sqrt((toX - fromX) * (toX - fromX) + (toY - fromY) * (toY - fromY));
        double deltaX = (toX - fromX);
        double deltaY = (toY - fromY);

        // Offset the edge by a few pixels to the right when viewed in the from-to direction
        int offsetX = (int) Math.rint(-EDGE_OFFSET * deltaY / edgeLen);
        int offsetY = (int) Math.rint(-EDGE_OFFSET * deltaX / edgeLen);

        fromX += offsetX;
        fromY += offsetY;

        // Compute intersections of the edge with the nodes
        double tmin = Double.MAX_VALUE;
        double tmax = 0.0;
        if (Math.abs(deltaY) >= EPS) {  // Line is not horizontal, so check top and bottom edges.
            double t = (edge.from.top - fromY) / deltaY;
            if (t > 0.0 && t < tmin) {
                int xpos = (int) (fromX + t * deltaX);
                if (xpos >= edge.from.left && xpos <= edge.from.right) {
                    tmin = t;
                }
            }
            t = (edge.from.bottom - fromY) / deltaY;
            if (t > 0.0 && t < tmin) {
                int xpos = (int) (fromX + t * deltaX);
                if (xpos >= edge.from.left && xpos <= edge.from.right) {
                    tmin = t;
                }
            }

            t = (edge.to.top - fromY) / deltaY;
            if (t <= 1.0 && t > tmax) {
                int xpos = (int) (fromX + t * deltaX);
                if (xpos >= edge.to.left && xpos <= edge.to.right) {
                    tmax = t;
                }
            }
            t = (edge.to.bottom - fromY) / deltaY;
            if (t <= 1.0 && t > tmax) {
                int xpos = (int) (fromX + t * deltaX);
                if (xpos >= edge.to.left && xpos <= edge.to.right) {
                    tmax = t;
                }
            }
        }

        if (Math.abs(deltaX) >= EPS) {  // Line is not vertical, check left and right
            double t = (edge.from.left - fromX) / deltaX;
            if (t > 0.0 && t < tmin) {
                int ypos = (int) (fromY + t * deltaY);
                if (ypos >= edge.from.top && ypos <= edge.from.bottom) {
                    tmin = t;
                }
            }
            t = (edge.from.right - fromX) / deltaX;
            if (t > 0.0 && t < tmin) {
                int ypos = (int) (fromY + t * deltaY);
                if (ypos >= edge.from.top && ypos <= edge.from.bottom) {
                    tmin = t;
                }
            }

            t = (edge.to.left - fromX) / deltaX;
            if (t <= 1.0 && t > tmax) {
                int ypos = (int) (fromY + t * deltaY);
                if (ypos >= edge.to.top && ypos <= edge.to.bottom) {
                    tmax = t;
                }
            }
            t = (edge.to.right - fromX) / deltaX;
            if (t <= 1.0 && t > tmax) {
                int ypos = (int) (fromY + t * deltaY);
                if (ypos >= edge.to.top && ypos <= edge.to.bottom) {
                    tmax = t;
                }
            }
        }

        // Offset a couple of pixels and get the endpoints
        tmin += 2.0 / edgeLen;
        tmax -= 2.0 / edgeLen;
        int startX = (int) (fromX + tmin * deltaX);
        int startY = (int) (fromY + tmin * deltaY);
        int endX = (int) (fromX + tmax * deltaX);
        int endY = (int) (fromY + tmax * deltaY);

        //Square up the almost vertical / horizontal lines
        double angle = Math.atan2(endY - startY, endX - startX);
        if (Math.abs(Math.abs(angle) - Math.PI / 2.0) < MINANGLE) { // Vertical
            int mid = (startX + endX) / 2;
            startX = endX = mid;
            angle = Math.atan2(endY - startY, endX - startX);
        }
        else if (Math.abs(angle) < MINANGLE || Math.abs(Math.abs(angle) - Math.PI) < MINANGLE) { // Horizontal
            int mid = (startY + endY) / 2;
            startY = endY = mid;
            angle = Math.atan2(endY - startY, endX - startX);
        }

        // Draw the line
        imageGraphics.setColor(EDGE_COLOUR);
        if (edge.style == Edge.NORMAL)
            imageGraphics.setStroke(normalStroke);
        else if (edge.style == Edge.FAIL) {
            imageGraphics.setColor(DASHED_EDGE_COLOUR);
            imageGraphics.setStroke(dashedStroke);
        }
        else
            imageGraphics.setStroke(normalStroke);
        imageGraphics.drawLine(startX, startY, endX, endY);

        //Put an arrow on the end point
        Polygon arrowPolygon = new Polygon();
        arrowPolygon.addPoint(0, 0);
        arrowPolygon.addPoint(-8, 4);
        arrowPolygon.addPoint(-8, -4);
        AffineTransform tx = new AffineTransform();
        tx.translate(endX, endY);
        tx.rotate(angle);
        Shape arrow = tx.createTransformedShape(arrowPolygon);
        imageGraphics.fill(arrow);

        // Put the label on
        if (edge.label != null) {
            FontMetrics metrics = imageGraphics.getFontMetrics();
            Rectangle2D bounds = metrics.getStringBounds(edge.label, imageGraphics);
            int labelX = (int) ((startX + endX - bounds.getWidth()) / 2);
            int labelY = (int) ((startY + endY + bounds.getHeight()) / 2) - 2; //TODO this should be based on the metrics of the label.
            //TODO use proper rounding
            imageGraphics.setStroke(normalStroke);
            imageGraphics.setColor(Color.WHITE);
            imageGraphics.fillRoundRect(labelX - 2, (int) (labelY - bounds.getHeight() + 2),
                    (int) bounds.getWidth() + 4, (int) bounds.getHeight() + 2,
                    (int) bounds.getHeight(), (int) bounds.getHeight());
            imageGraphics.setColor(EDGE_COLOUR);
            imageGraphics.drawRoundRect(labelX - 2, (int) (labelY - bounds.getHeight() + 2),
                    (int) bounds.getWidth() + 4, (int) bounds.getHeight() + 2,
                    (int) bounds.getHeight(), (int) bounds.getHeight());
            imageGraphics.drawString(edge.label, labelX, labelY);
        }
    }

}
