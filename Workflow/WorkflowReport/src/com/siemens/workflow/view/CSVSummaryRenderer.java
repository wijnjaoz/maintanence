package com.siemens.workflow.view;

import com.siemens.workflow.WFStringUtils;
import com.siemens.workflow.model.WorkflowTemplate;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.List;
import java.util.Stack;

public class CSVSummaryRenderer {
    public void render(WorkflowTemplate rootTask, String fileName) {
        PrintStream ps = null;
        try {
            ps = new PrintStream(fileName);
            int nlevels = rootTask.getMaxDepth();

            for (int idx = 0; idx < nlevels; idx++) {
                ps.printf("\"Level %d\",", idx);
            }
            ps.println("\"Description\"");

            Stack<WorkflowTemplate> nodeStack = new Stack<WorkflowTemplate>();
            Stack<Integer> levelStack = new Stack<Integer>();

            nodeStack.push(rootTask);
            levelStack.push(0);

            while (!nodeStack.isEmpty()) {
                WorkflowTemplate node = nodeStack.pop();
                int level = levelStack.pop();
                List<WorkflowTemplate> children = node.getSubTemplates();

                for (int idx = 0; idx < level; idx++) ps.print("\"\",");
                ps.printf("\"%s\",", WFStringUtils.safeCSV(node.getName()));
                for (int idx = level + 1; idx < nlevels; idx++) ps.print("\"\",");
                ps.printf("\"%s\"\n", WFStringUtils.safeCSV(node.getTaskDescription()));

                for (int idx = children.size() - 1; idx >= 0; --idx) {
                    nodeStack.push(children.get(idx));
                    levelStack.push(level + 1);
                }
            }

            ps.close();
            ps = null;
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        finally {
            if (ps != null)
                ps.close();
        }

    }
}
