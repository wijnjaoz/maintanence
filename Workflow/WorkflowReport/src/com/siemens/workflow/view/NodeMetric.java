package com.siemens.workflow.view;

import com.siemens.workflow.model.WorkflowTemplate;

public class NodeMetric {
    private final String title;
    final int top;
    final int bottom;
    final int left;
    final int right;
    private WorkflowTemplate node;

    public NodeMetric(String title, int left, int top, int right, int bottom) {
        this.title = title;
        this.top = top;
        this.bottom = bottom;
        this.left = left;
        this.right = right;
    }

    public int getCentreX() {
        return (left + right) / 2;
    }

    public int getCentreY() {
        return (top + bottom) / 2;
    }

    public String getTitle() {
        return title;
    }

    public int getTop() {
        return top;
    }

    public int getBottom() {
        return bottom;
    }

    public int getLeft() {
        return left;
    }

    public int getRight() {
        return right;
    }

    public void setNode(WorkflowTemplate node) {
        this.node = node;
    }

    public WorkflowTemplate getNode() {
        return node;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NodeMetric that = (NodeMetric) o;

        return !(title != null ? !title.equals(that.title) : that.title != null);

    }

    @Override
    public int hashCode() {
        return title != null ? title.hashCode() : 0;
    }
}
