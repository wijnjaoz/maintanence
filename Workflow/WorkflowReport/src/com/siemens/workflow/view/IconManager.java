package com.siemens.workflow.view;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class IconManager {
    private final Map<String, String> iconKeyToFile = new HashMap<String, String>();
    private final Map<String, BufferedImage> iconKeyToImage = new HashMap<String, BufferedImage>();

    public IconManager() {
        iconKeyToFile.put("up", "up.png");
        iconKeyToFile.put("setsACL", "changeaccess_16.png");
        iconKeyToFile.put("process", "process_16.png");
        iconKeyToFile.put("start", "startnode_16.png");
        iconKeyToFile.put("finish", "completenode_16.png");
        iconKeyToFile.put("subtaskstart", "subtaskstartnode_24.png");
        iconKeyToFile.put("subtaskfinish", "subtaskcompletenode_24.png");
        iconKeyToFile.put("subtasks", "add_16.png");

        iconKeyToFile.put("addStatusTask", "addstatustasknode_16.png");
        iconKeyToFile.put("doTask", "dotasknode_16.png");
        iconKeyToFile.put("conditionTask", "conditiontask_16.png");
        iconKeyToFile.put("performSignoffTask", "performsignofftask_16.png");
        iconKeyToFile.put("reviewTask", "reviewtask_16.png");
        iconKeyToFile.put("reviewProcess", "reviewtask_16.png");
        iconKeyToFile.put("selectSignoffTask", "selectsignofftask_16.png");
        iconKeyToFile.put("orTask", "ortasknode_16.png");
        iconKeyToFile.put("task", "tasknode_16.png");
        iconKeyToFile.put("validateTask", "validateTask_16.png");
        iconKeyToFile.put("checkListTask", "checkListTask_16.png");
        iconKeyToFile.put("impactAnalysisTask", "impactAnalysisTask_16.png");
        iconKeyToFile.put("prepareecoTask", "prepareecoTask_16.png");

        iconKeyToFile.put("routeTask", "routetasknode_16.png");
        iconKeyToFile.put("syncTask", "synctasknode_16.png");
        iconKeyToFile.put("acknowledgeTask", "acknowledgetasknode_16.png");
        iconKeyToFile.put("notifyTask", "notifytasknode_16.png");
    }

    public BufferedImage getIcon(String nodeIcon) {
        if (!iconKeyToImage.containsKey(nodeIcon)) {
            try {
                URL resource = getClass().getResource("/nodeicons/" + iconKeyToFile.get(nodeIcon));
                BufferedImage image = ImageIO.read(resource);
                iconKeyToImage.put(nodeIcon, image);
            }
            catch (IOException e) {
                System.err.println("Looking for icon name " + nodeIcon);
                e.printStackTrace();
            }
        }
        return iconKeyToImage.get(nodeIcon);
    }

    public void saveIcon(String iconKey) {
        if (iconKeyToFile.containsKey(iconKey)) {
            InputStream is = getClass().getResourceAsStream("/nodeicons/" + iconKeyToFile.get(iconKey));
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(iconKey + ".png");
                byte buffer[] = new byte[1024];
                int nread = is.read(buffer);
                while (nread != -1) {
                    fos.write(buffer, 0, nread);
                    nread = is.read(buffer);
                }
                fos.close();
                is.close();
                is = null;
                fos = null;
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                if (is != null)
                    try {
                        is.close();
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                if (fos != null)
                    try {
                        fos.close();
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
            }
        }
    }
}
