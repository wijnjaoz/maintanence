package com.siemens.workflow.view;

class Edge {
    public static final int NORMAL = 1;
    public static final int FAIL = 2;

    final NodeMetric from;
    final NodeMetric to;
    final int style;
    final String label;

    public Edge(NodeMetric from, NodeMetric to, int style, String label) {
        this.from = from;
        this.to = to;
        this.style = style;
        this.label = label;
    }

    public int getFromX() {
        return from.getCentreX();
    }

    public int getToX() {
        return to.getCentreX();
    }

    public int getFromY() {
        return from.getCentreY();
    }

    public int getToY() {
        return to.getCentreY();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Edge edge = (Edge) o;

        return from.equals(edge.from) && to.equals(edge.to);

    }

    @Override
    public int hashCode() {
        int result = from.hashCode();
        result = 31 * result + to.hashCode();
        return result;
    }
}
