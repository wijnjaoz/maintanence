package com.siemens.workflow.view;

import com.siemens.workflow.analysis.AnalysisResult;
import com.siemens.workflow.model.WorkflowTemplate;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.List;
import java.util.Map;

public class CSVDependencyRenderer {
    public void render(Map<String,AnalysisResult> analysisResultMap, String fileName) {
        PrintStream ps = null;
        try {
            ps = new PrintStream(fileName);

            for (String analysisType : analysisResultMap.keySet()) {
                AnalysisResult analysisResult = analysisResultMap.get(analysisType);
                Map<String, List<WorkflowTemplate>> results = analysisResult.getResults();
                for (String key : results.keySet()) {
                    List<WorkflowTemplate> nodes = results.get(key);
                    for (WorkflowTemplate node : nodes) {
                        ps.print('"');
                        ps.print(analysisType);
                        ps.print("\",\"");
                        ps.print(key);
                        ps.print("\",\"");
                        ps.print(node.getTemplatePath());
                        ps.println('"');
                    }
                }
            }

            ps.close();
            ps = null;
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        finally {
            if (ps != null)
                ps.close();
        }
    }
}
