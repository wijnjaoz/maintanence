package com.siemens.workflow.analysis;

import com.siemens.workflow.model.*;

import java.util.*;

@SuppressWarnings("UnusedDeclaration")
public class QueryAnalyser extends Analyser {
    @Override
    public void execute(WorkflowTemplate root, String handlerFile) {
        final WFUtil wfUtil = WFUtil.getInstance();
        final Stack<WorkflowTemplate> stack = new Stack<WorkflowTemplate>();
        stack.push(root);
        while (!stack.isEmpty()) {
            final WorkflowTemplate node = stack.pop();
            final List<WorkflowHandler> handlers = getWorkflowHandlers(node);

            for (final WorkflowHandler handler : handlers) {
                if (wfUtil.usesQuery(handler)) {
                    for (final String queryName : wfUtil.getQueries(handler)) {
                        addToResultMap(queryName, node);
                    }
                }
            }

            if (node.getSubTemplates().size() > 0) {
                for (int idx = node.getSubTemplates().size() - 1; idx >= 0; --idx) {
                    stack.push(node.getSubTemplates().get(idx));
                }
            }
        }

        results = new AnalysisResult("QueryAnalyser", "Queries", resultMap);
    }
}
