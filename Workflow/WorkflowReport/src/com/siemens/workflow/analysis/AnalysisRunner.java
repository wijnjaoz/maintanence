package com.siemens.workflow.analysis;

import com.siemens.workflow.model.WorkflowTemplate;

import java.util.HashMap;
import java.util.Map;

public class AnalysisRunner {
    private final String handlerFile;

    public AnalysisRunner(String handlerFile) {
        this.handlerFile = handlerFile;
    }

    public Map<String, AnalysisResult> runAnalyses(WorkflowTemplate rootTask, String[] analyses) {
        Map<String, AnalysisResult> results = new HashMap<String, AnalysisResult>();

        for (String analysis : analyses) {
            try {
                Class analyserClass = Class.forName("com.siemens.workflow.analysis." + analysis + "Analyser");
                Analyser analyser = (Analyser) analyserClass.newInstance();
                analyser.execute(rootTask, handlerFile);
                results.put(analysis, analyser.getResults());
            }
            catch (InstantiationException e) {
                e.printStackTrace();
            }
            catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return results;
    }
}
