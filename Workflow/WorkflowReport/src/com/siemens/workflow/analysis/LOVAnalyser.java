package com.siemens.workflow.analysis;

import com.siemens.workflow.model.*;

import java.util.*;

@SuppressWarnings("UnusedDeclaration")
public class LOVAnalyser extends Analyser {

    @Override
    public void execute(WorkflowTemplate root, String handlerFile) {
        final WFUtil wfUtil = WFUtil.getInstance();
        final Stack<WorkflowTemplate> stack = new Stack<WorkflowTemplate>();
        stack.push(root);
        while (!stack.isEmpty()) {
            final WorkflowTemplate node = stack.pop();
            final List<WorkflowHandler> handlers = getWorkflowHandlers(node);

            for (final WorkflowHandler handler : handlers) {
                if (wfUtil.usesLOV(handler)) {
                    for (final String lovName : wfUtil.getLOVs(handler)) {
                        addToResultMap(lovName, node);
                    }
                }
            }

            if (node.getSubTemplates().size() > 0) {
                for (int idx = node.getSubTemplates().size() - 1; idx >= 0; --idx) {
                    stack.push(node.getSubTemplates().get(idx));
                }
            }
        }

        results = new AnalysisResult("LOVAnalyser", "LOVs", resultMap);
    }
}
