package com.siemens.workflow.analysis;

import com.siemens.workflow.model.WorkflowAction;
import com.siemens.workflow.model.WorkflowBusinessRule;
import com.siemens.workflow.model.WorkflowHandler;
import com.siemens.workflow.model.WorkflowTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

abstract class Analyser {

    final Map<String,List<WorkflowTemplate>> resultMap = new HashMap<String, List<WorkflowTemplate>>();
    AnalysisResult results;

    public abstract void execute(WorkflowTemplate root, String handlerFile);

    public AnalysisResult getResults() {
        return results;
    }

    List<WorkflowHandler> getWorkflowHandlers(WorkflowTemplate node) {
        final List<WorkflowHandler> handlers = new ArrayList<WorkflowHandler>();
        for (final WorkflowAction action : node.getActions()) {
            for (final WorkflowBusinessRule rule : action.getRules()) {
                handlers.addAll(rule.getRuleHandlers());
            }
            if (action.getActionHandlers().size() > 0) {
                handlers.addAll(action.getActionHandlers());
            }
        }
        return handlers;
    }

    void addToResultMap(String value, WorkflowTemplate node) {
        if (!resultMap.containsKey(value)) {
            ArrayList<WorkflowTemplate> templates = new ArrayList<WorkflowTemplate>();
            templates.add(node);
            resultMap.put(value, templates);
        }
        else {
            List<WorkflowTemplate> templates = resultMap.get(value);
            if (!templates.contains(node)) {
                templates.add(node);
            }
        }
    }
}
