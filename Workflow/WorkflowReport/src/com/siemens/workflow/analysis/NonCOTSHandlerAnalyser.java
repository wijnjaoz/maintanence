package com.siemens.workflow.analysis;

import com.siemens.workflow.model.*;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

@SuppressWarnings("UnusedDeclaration")
public class NonCOTSHandlerAnalyser extends Analyser {
    @Override
    public void execute(WorkflowTemplate root, String handlerFile) {
        final WFUtil wfUtil = WFUtil.getInstance();
        final Set<String> includeAsCOTS = new HashSet<String>();
        if (handlerFile != null) {
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new FileReader(handlerFile));
                String line = reader.readLine();
                while(line != null) {
                    includeAsCOTS.add(line);
                    line = reader.readLine();
                }
            }
            catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                if (reader != null) try {
                    reader.close();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        final Stack<WorkflowTemplate> stack = new Stack<WorkflowTemplate>();
        stack.push(root);
        while (!stack.isEmpty()) {
            final WorkflowTemplate node = stack.pop();
            final List<WorkflowHandler> handlers = getWorkflowHandlers(node);

            for (final WorkflowHandler handler : handlers) {
                String handlerName = handler.getName();
                if (handlerFile != null) {
                    if (!includeAsCOTS.contains(handlerName)) {
                        addToResultMap(handlerName, node);
                    }
                }
                else if (!wfUtil.isCOTS(handler)) {
                    addToResultMap(handlerName, node);
                }
            }

            if (node.getSubTemplates().size() > 0) {
                for (int idx = node.getSubTemplates().size() - 1; idx >= 0; --idx) {
                    stack.push(node.getSubTemplates().get(idx));
                }
            }
        }

        results = new AnalysisResult("NonCOTSHandlerAnalyser", "Non-COTS Handlers", resultMap);
    }
}
