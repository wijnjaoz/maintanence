package com.siemens.workflow.analysis;

import com.siemens.workflow.model.WorkflowTemplate;

import java.util.List;
import java.util.Map;

public class AnalysisResult {
    private final String analysisClass;
    private final String title;
    private final Map<String, List<WorkflowTemplate>> results;

    public AnalysisResult(String analysisClass, String title, Map<String, List<WorkflowTemplate>> results) {
        this.analysisClass = analysisClass;
        this.title = title;
        this.results = results;
    }

    public String getAnalysisClass() {
        return analysisClass;
    }

    public String getTitle() {
        return title;
    }

    public Map<String, List<WorkflowTemplate>> getResults() {
        return results;
    }
}
