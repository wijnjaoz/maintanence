PREREQUISITES
=============
You must have jdk1.6.0_18 or higher to build, and jre1.6.0_18 or higher to run.


BUILDING
========

1. Extract the archive into a directory on your image.
2. Start a Teamcenter shell.
3. cd to the directory you extracted into.
4. Execute antset.bat
5. Execute ant

You should now have a WorkflowReport.jar in the dist directory.


RUNNING
=======

1. Have a workflow export plmxml file.
   This file may contain multiple process templates.
   You will get one set of output for each process template.
2. Execute java -jar WorkflowReport.jar <name of xml file>

You should now have 3 files for each process template in the current directory;

<normalised name of file>.mht  -  The main html report.
        Note that there are lots of hyperlinks in the report, but they don't look like links.
        This is so it can be printed neatly.  Move your mouse around on the images, task names, and paths and you should
        see your cursor change.
        Tasks that have an EPM-set-rule-based-protection handler show a key symbol on the top left of the task.
        Tasks that are marked to run asynchronously have a dashed outline.

<normalised name of file>-dependencies.csv  -  The workflow dependencies report as a CSV.

<normalised name of file>-summary.csv  -  The tasks and descriptions in a CSV ready for cut and paste.



Command line options are as follows.
    -h or -help                 : Prints a usage message.
    -comment="<message>"        : Adds <message> into the header of the file below the title.
                                : Note that the message goes into the html unescaped, so if you want you can include markup
                                : such as <i>italic</i>, or even links.  Don't get too crazy or you may break the html.
    -noclean                    : Retains the temporary html and image files.
    -cots_handlers=handler_file : Uses a list of handlers (one per line) from the supplied file name.
    -text                       : Writes a textual version of the template for use with diff.

FUTURE WORK
===========
???
