@echo off
rem set ANT_HOME=C:\apps\apache-ant-1.7.0
rem set ANT_HOME=%TC_ROOT%\bmide\client\plugins\org.apache.ant_1.7.1.v20100518-1145
rem
rem
rem  Get ANT_HOME from BMIDE environment ...
for /D %%D in (%TC_ROOT%\bmide\client\plugins\org.apache.ant_*) do set ANT_HOME=%%D
if "%ANT_HOME%"=="" (
echo.
echo ERROR:  ANT_HOME not set.
echo Please ensure you have Teamcenter BMIDE installed and TC_ROOT set correctly.
echo.
echo TC_ROOT=%TC_ROOT%
exit/b
)

rem Get JDK_HOME and JAVA_HOME from BMIDE startup script ...
for /f "usebackq tokens=2 delims==" %%c in (`findstr set.JDK %TC_ROOT%\bmide\client\bmide.bat`) do set JDK_HOME=%%c
if "%JDK_HOME%"=="" (
echo.
echo ERROR:  JDK_HOME not set.
echo Please set this to the installation path for a Java SDK [not JRE].
exit/b
)

rem Find the version of the RAC...
for /f "usebackq tokens=2,3,4 delims=_." %%B in (`dir /b %TC_ROOT%\portal\plugins\configuration_*`) do set RAC_VERSION=%%B.%%C.%%D

set JAVA_HOME=%JDK_HOME%
PATH=%JAVA_HOME%\bin;%PATH%;%ANT_HOME%\bin

set ANT_HOME
set JAVA_HOME
set RAC_VERSION
