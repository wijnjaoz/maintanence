rem @echo off
rem Copyright by Siemens PLM Software 2013
rem
rem Start script for the BnlSe2Pdf
rem
rem J. Mansvelders   Created                       01-04-2014
rem
rem
rem ********************************************************************

setlocal

rem ********************************************************************
rem Ghostscript bin directory
rem ********************************************************************
set GS_HOME=C:\Program Files\gs\gs9.10\bin

rem ********************************************************************
rem INPUT PARAMETERS
rem ********************************************************************
set INPUTFILE=D:\Testing\BNL_DEP_SeRepro_1_4_0_0_TC10.1\resources\Module\Translators\bnlse\ST6_Draft.dft
set OUTPUTDIR=D:\Testing\BNL_DEP_SeRepro_1_4_0_0_TC10.1\resources\Module\Translators\bnlse
set PRINTFILE=printfile
set SYNCFILE=sync.txt

rem ********************************************************************
rem TRANSLATOR DIRECTORY
rem ********************************************************************
SET TRANSLATE_DIR=%~dp0

rem ********************************************************************
rem RUN SE PRINT
rem ********************************************************************
rem                    Available command line options:
rem                       Required:
rem                        -logfile=<logfilename>
rem                        -filename=<part-name>
rem                       Optional:
rem                        -queue=<printer-or-plottername>
rem                        -printfile=<outputfile>
rem                        -pdf=true | false
rem                        -color=true | false
rem                        -orientation=landscape | potrait
rem                        -numofcopies=
rem                        -macro=<path\macro.exe>
rem                        -syncfile=<path\sync.txt>
rem                        -layerson=layer 1;layer 2;�;layer n
rem                        -layersoff=layer 1;layer 2;�;layer n
rem                        -ignorelayererrors=Y | N

set SE_COMMAND=%TRANSLATE_DIR%\BNL_SE_print.exe "-logfile=%CD%\selog.log" "-filename=%INPUTFILE%" "-queue=Solid Edge Velocity PS Printer 2.0" "-printfile=%OUTPUTDIR%\%PRINTFILE%" "-syncfile=%SYNCFILE%" "-pdf=N" "-color=true"

echo Running [%SE_COMMAND%]
%SE_COMMAND%
echo xxxxxx

FOR /F "TOKENS=*" %%G IN ('dir /b "%OUTPUTDIR%\*.pri"') DO CALL :CreatePDF %%G
echo %ERRORLEVEL%
pause
set EXITVALUE=%ERRORLEVEL%

goto :End

:CreatePDF
echo CreatePDF
set PSFILE=%PRINTFILE%
set PDF_COMMAND="%GS_HOME%\gswin64c.exe" -dNOPAUSE -dBATCH -sDEVICE#pdfwrite -sOutputFile="%OUTPUTDIR%\%PSFILE%.pdf" -f "%OUTPUTDIR%\%PSFILE%.pri"

echo Running [%PDF_COMMAND%]
%PDF_COMMAND%
echo.

goto :EOF
  
:End
endlocal

@echo on
rem EXIT %EXITVALUE%