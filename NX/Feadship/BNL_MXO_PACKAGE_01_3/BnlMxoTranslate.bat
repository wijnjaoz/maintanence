rem @echo off
rem Copyright by Siemens PLM Software Benelux 2009
rem
rem Start script for the UPM
rem
rem J. Mansvelders   Creation              02-Nov-2009
rem J. Mansvelders   Updated               04-Nov-2009
rem J. Mansvelders   Updated               05-Nov-2009
rem J. Mansvelders   Updated for dxf       08-Dec-2009
rem
rem
rem ********************************************************************

rem %1 Path and name of XML file to be translated
rem %2 output directory (Optional)

rem Set translator settings
set XALAN_PATH=%~dp0\Xalan-C_1_10_0-win32-msvc_60\bin
set XERCES_PATH=%~dp0\xerces-c-windows_2000-msvc_60\bin
set XSL_FILE=%~dp0\xsl\alma_html_br_generic.xsl
set XSL_FILE_DXF=%~dp0\xsl\dxf_stylesheet.xsl

set PATH=%XALAN_PATH%;%XERCES_PATH%;%PATH%
set TRANSLATOR=XalanTransform.exe

rem Logging
set LOG_FILE=%TEMP%\translate.log

set FILEIN=%1
set OUTPUT_DIR=%2
set OUTPUT_DIR_DXF=%2

rem Generate the output file
set FILEOUT=%FILEIN:.xml=.ini%

rem Generate the output dxf file
set FILEOUT_DXF=%FILEIN:.xml=.dxf%

rem Replace xml by ini in the output file
if NOT %OUTPUT_DIR%.==. set OUTPUT_DIR=%OUTPUT_DIR:\xml\=\ini\%

rem Replace xml by dxf in the output file
if NOT %OUTPUT_DIR_DXF%.==. set OUTPUT_DIR_DXF=%OUTPUT_DIR_DXF:\xml\=\dxf\%

echo Settings: > "%LOG_FILE%" 2>>&1
echo XALAN_PATH=%XALAN_PATH% >> "%LOG_FILE%" 2>>&1
echo XERCES_PATH=%XERCES_PATH% >> "%LOG_FILE%" 2>>&1
echo TRANSLATOR=%TRANSLATOR% >> "%LOG_FILE%" 2>>&1
echo XSL_FILE=%XSL_FILE% >> "%LOG_FILE%" 2>>&1
echo XSL_FILE_DXF=%XSL_FILE_DXF% >> "%LOG_FILE%" 2>>&1
echo FILEIN=%FILEIN% >> "%LOG_FILE%" 2>>&1
echo FILEOUT=%FILEOUT% >> "%LOG_FILE%" 2>>&1
echo FILEOUT_DXF=%FILEOUT_DXF% >> "%LOG_FILE%" 2>>&1
echo OUTPUT_DIR=%OUTPUT_DIR% >> "%LOG_FILE%" 2>>&1
echo OUTPUT_DIR_DXF=%OUTPUT_DIR_DXF% >> "%LOG_FILE%" 2>>&1

rem perform the transformation
@echo %time% >> %LOG_FILE%

%TRANSLATOR% %FILEIN% %XSL_FILE% %FILEOUT% >> "%LOG_FILE%" 2>>&1

rem Check for errors
if %ERRORLEVEL% EQU 0 (
  if NOT %OUTPUT_DIR%.==. (
  
    xcopy /Y /I /S /E /Q %FILEOUT% %OUTPUT_DIR%\
    echo Deleting original file >> "%LOG_FILE%" 2>>&1
    del /F %FILEOUT%
  )
) else (
  rem An error occured
  echo Error occured >> "%LOG_FILE%" 2>>&1
  exit -1
)

%TRANSLATOR% %FILEIN% %XSL_FILE_DXF% %FILEOUT_DXF% >> "%LOG_FILE%" 2>>&1

rem Check for errors
if %ERRORLEVEL% EQU 0 (
  if NOT %OUTPUT_DIR_DXF%.==. (
  
    xcopy /Y /I /S /E /Q %FILEOUT_DXF% %OUTPUT_DIR_DXF%\
    echo Deleting original file >> "%LOG_FILE%" 2>>&1
    del /F %FILEOUT_DXF%
  )
) else (
  rem An error occured
  echo Error occured >> "%LOG_FILE%" 2>>&1
  exit -1
)

@echo %time% >> %LOG_FILE%
