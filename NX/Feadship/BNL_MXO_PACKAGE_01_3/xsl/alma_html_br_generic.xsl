<?xml version="1.0" encoding="ISO-8859-1"?>


<!--

	20-Feb-2008		H.Petzold		Added <Value name="yyyy-mm-ddThh:mm:ss"> for CreationDate and DueDate

	01-Jul-2008		H.Petzold		Deactivated <xsl:output .../>
									Added <br/> to t_lf
									
	09-Feb-2009		H.Petzold		Activated <xsl:output .../>
									remove <br/> to t_lf									
-->


<xsl:transform version="1.0"
  xmlns:nxsdci="http://www.ugs.com/NXShipDesign/CuttingInterface"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="text" encoding="ISO-8859-1" media-type="text/plain" omit-xml-declaration="yes"/>


  <xsl:template match="/nxsdci:NXShipDesignCuttingInterface">
    <xsl:text>/MO</xsl:text> <xsl:call-template name="t_lf"/> 
    

    
    

    

   

    
    <xsl:apply-templates select="Part"/> 
    
    <xsl:text>/END</xsl:text> <xsl:call-template name="t_lf"/>
  </xsl:template>
 

 
  <xsl:template match="Part">

    
    <xsl:text>*PN</xsl:text> <xsl:call-template name="t_lf"/>
    <xsl:call-template name="t_mustPropsValsResolve"> <!-- Identifier/@Name -->
      <xsl:with-param name="p_avl" select="'SA_COMPONENT_PROJECT'"/>
      <xsl:with-param name="p_vvl" select="'Amount'"/>        
    </xsl:call-template> 
    <xsl:call-template name="t_lf"/>

    <xsl:text>*MN</xsl:text> <xsl:call-template name="t_lf"/>   
    <xsl:call-template name="t_mustPropsValsResolve"> <!-- Identifier/@Name -->
      <xsl:with-param name="p_avl" select="'SA_COMPONENT_SECTIONNO'"/>   
      <xsl:with-param name="p_vvl" select="'Amount'"/>        
    </xsl:call-template>
    <xsl:call-template name="t_mustPropsValsResolve"> <!-- Identifier/@Name -->
      <xsl:with-param name="p_avl" select="'SA_COMPONENT_PARTID'"/>   
      <xsl:with-param name="p_vvl" select="'Amount'"/>        
    </xsl:call-template>    
    <xsl:call-template name="t_lf"/>

    <xsl:text>*DC</xsl:text> <xsl:call-template name="t_lf"/>      
    <xsl:call-template name="t_mustPropsValsResolve"> <!-- CreationDate/@yyyy-mm-ddThh:mm:ss @Month @Day @Year-->
      <xsl:with-param name="p_avl" select="'XMLCreationDate'"/>   
      <xsl:with-param name="p_vvl" select="'Name'"/>   
    </xsl:call-template>    
    <xsl:call-template name="t_lf"/>

    <xsl:text>*UN</xsl:text> <xsl:call-template name="t_lf"/>   
    <xsl:call-template name="t_mustPropsValsResolve"> <!-- Unit/@Name -->
      <xsl:with-param name="p_avl" select="'Unit'"/>   
      <xsl:with-param name="p_vvl" select="'Name'"/>   
    </xsl:call-template>
    <xsl:text>0</xsl:text>
    <xsl:call-template name="t_lf"/>

    <xsl:text>*MU</xsl:text> <xsl:call-template name="t_lf"/>   
    <xsl:call-template name="t_mustPropsValsResolve"> <!-- LotSize/@Amount -->
      <xsl:with-param name="p_avl" select="'SA_COMPONENT_QUANTITY'"/>   
      <xsl:with-param name="p_vvl" select="'Amount'"/>   
    </xsl:call-template>
    <xsl:call-template name="t_lf"/>
    
    <xsl:text>*RN</xsl:text> <xsl:call-template name="t_lf"/>   
    <xsl:call-template name="t_mustPropsValsResolve"> <!-- ModelRevision/@Name -->
      <xsl:with-param name="p_avl" select="'DB_PART_REV______'"/>   
      <xsl:with-param name="p_vvl" select="'Amount'"/>   
    </xsl:call-template>
    <xsl:text>0</xsl:text>
    <xsl:call-template name="t_lf"/>

    <xsl:text>/PA</xsl:text> <xsl:call-template name="t_lf"/>


    <xsl:text>*ID</xsl:text> <xsl:call-template name="t_lf"/>   
    <xsl:call-template name="t_mustPropsValsResolve"> <!-- Identifier/@Name -->
      <xsl:with-param name="p_avl" select="'SA_COMPONENT_PROJECT'"/>
      <xsl:with-param name="p_vvl" select="'Amount'"/>
    </xsl:call-template>
    <xsl:call-template name="t_mustPropsValsResolve"> <!-- Identifier/@Name -->
      <xsl:with-param name="p_avl" select="'SA_COMPONENT_SECTIONNO'"/>
      <xsl:with-param name="p_vvl" select="'Amount'"/>
    </xsl:call-template>
    <xsl:call-template name="t_mustPropsValsResolve"> <!-- Identifier/@Name -->
      <xsl:with-param name="p_avl" select="'SA_COMPONENT_PARTID'"/>   
      <xsl:with-param name="p_vvl" select="'Amount'"/>        
    </xsl:call-template>    
    <xsl:call-template name="t_mustPropsValsResolve"> <!-- Identifier/@Name -->
      <xsl:with-param name="p_avl" select="'SA_COMPONENT_SIZE'"/>   
      <xsl:with-param name="p_vvl" select="'Amount'"/>        
    </xsl:call-template>
    <xsl:call-template name="t_lf"/>
    
    <xsl:text>*RE</xsl:text> <xsl:call-template name="t_lf"/>   
    <xsl:call-template name="t_mustPropsValsResolve"> <!-- Reference/@Name -->
      <xsl:with-param name="p_avl" select="'SA_COMPONENT_NAME'"/>
      <xsl:with-param name="p_vvl" select="'Amount'"/>   
    </xsl:call-template>
    <xsl:call-template name="t_mustPropsValsResolve"> <!-- Reference/@Name -->
      <xsl:with-param name="p_avl" select="'SA_COMPONENT_ASSY'"/>   
      <xsl:with-param name="p_vvl" select="'Amount'"/>   
    </xsl:call-template>    
    <xsl:call-template name="t_lf"/>
    
    <xsl:text>*RN</xsl:text> <xsl:call-template name="t_lf"/>   
    <xsl:call-template name="t_mustPropsValsResolve"> <!-- PartRevision/@Name -->
      <xsl:with-param name="p_avl" select="'DB_PART_REV_____'"/>   
      <xsl:with-param name="p_vvl" select="'Amount'"/>   
    </xsl:call-template>
    <xsl:text>0</xsl:text>
    <xsl:call-template name="t_lf"/>
    
    <xsl:text>*TH</xsl:text> <xsl:call-template name="t_lf"/>   
    <xsl:call-template name="t_mustPropsValsResolve"> <!-- Thickness/@Amount -->
      <xsl:with-param name="p_avl" select="'SA_COMPONENT_SIZE'"/>   
      <xsl:with-param name="p_vvl" select="'Amount'"/>   
    </xsl:call-template>
    <xsl:call-template name="t_lf"/>
    
    <xsl:text>*GR</xsl:text> <xsl:call-template name="t_lf"/>   
    <xsl:call-template name="t_mustPropsValsResolve"> <!-- Reference/@Name -->
      <xsl:with-param name="p_avl" select="'SA_COMPONENT_MATERIAL'"/>   
      <xsl:with-param name="p_vvl" select="'Amount'"/>   
    </xsl:call-template>
    <xsl:call-template name="t_lf"/>
    
    <xsl:text>*MA</xsl:text> <xsl:call-template name="t_lf"/>   
    <xsl:text>material</xsl:text>
    <xsl:call-template name="t_lf"/>

    <xsl:text>*QU</xsl:text> <xsl:call-template name="t_lf"/>   
    <xsl:call-template name="t_mustPropsValsResolve"> <!-- Quantity/@Amount -->
      <xsl:with-param name="p_avl" select="'SA_COMPONENT_QUANTITY'"/>   
      <xsl:with-param name="p_vvl" select="'Amount'"/>   
    </xsl:call-template>
    <xsl:call-template name="t_lf"/>
     
    <xsl:text>*TO</xsl:text> <xsl:call-template name="t_lf"/>   
    <xsl:call-template name="t_mustPropsValsResolve"> <!-- Tolerance/@Amount -->
      <xsl:with-param name="p_avl" select="'Tolerance'"/>   
      <xsl:with-param name="p_vvl" select="'Amount'"/>   
    </xsl:call-template>
    <xsl:text>0.2</xsl:text>
    <xsl:call-template name="t_lf"/>
        
    <xsl:for-each select="Property[@name='UserProperty']"> <!-- UserProperty/@Name  @Index  @Value  --> 
      <xsl:if test="position()=1"> <xsl:text>*PR</xsl:text> <xsl:call-template name="t_lf"/> </xsl:if>      
      <xsl:call-template name="t_propValsResolve">
        <xsl:with-param name="p_nvl" select="'UserProperty'"/> 
        <xsl:with-param name="p_vvl" select="'Name'"/>
        <xsl:with-param name="p_lvl" select="self::node()"/> 
      </xsl:call-template>     
      <xsl:call-template name="t_propValsResolve">
        <xsl:with-param name="p_nvl" select="'UserProperty'"/> 
        <xsl:with-param name="p_vvl" select="'Index'"/>
        <xsl:with-param name="p_lvl" select="self::node()"/> 
      </xsl:call-template>
      <xsl:call-template name="t_propValsResolve">
        <xsl:with-param name="p_nvl" select="'UserProperty'"/> 
        <xsl:with-param name="p_vvl" select="'Value'"/>
        <xsl:with-param name="p_lvl" select="self::node()"/> 
      </xsl:call-template>
      <xsl:call-template name="t_lf"/>   
    </xsl:for-each>

    <xsl:text>*GE</xsl:text> <xsl:call-template name="t_lf"/>
    
    <xsl:apply-templates select="Loop"/>
    <xsl:apply-templates select="Marking"/>    
    <xsl:apply-templates select="Label"/>    
   
  </xsl:template>
    
 
 
  <xsl:template match="Part/Loop">
    <xsl:apply-templates select="Line|Arc"/>    
  </xsl:template>
  
  
  
  <xsl:template match="Part/Loop/Line">
    <xsl:text>0</xsl:text> <xsl:call-template name="t_bl"/> <!-- Type : 0 for line -->
    <xsl:call-template name="t_mustPropsValsResolve"> <!-- Operation/@Name -->
      <xsl:with-param name="p_avl" select="'Operation'"/>   
      <xsl:with-param name="p_vvl" select="'Name'"/>   
    </xsl:call-template>         
    <xsl:call-template name="t_startPoint"/> 
    <xsl:call-template name="t_endPoint"/>  
    <xsl:call-template name="t_mustPropsValsResolve"> <!-- KerfCompensation/@Name -->
      <xsl:with-param name="p_avl" select="'KerfCompensation'"/>   
      <xsl:with-param name="p_vvl" select="'Name'"/>   
    </xsl:call-template>    
    <xsl:call-template name="t_mustPropsValsResolve"> <!-- Tool/@Name @StartValue @EndValue -->
      <xsl:with-param name="p_avl" select="'Tool'"/>   
      <xsl:with-param name="p_vvl" select="'Name StartValue EndValue'"/>   
    </xsl:call-template>        
    <xsl:call-template name="t_canPropsValsResolve"> <!-- UserProperty/@Index -->
      <xsl:with-param name="p_avl" select="'UserProperty'"/>   
      <xsl:with-param name="p_vvl" select="'Index'"/>   
    </xsl:call-template>           
    <xsl:call-template name="t_lf"/>    
  </xsl:template>
  
  
    
  <xsl:template match="Part/Loop/Arc">
    <xsl:text>1</xsl:text> <xsl:call-template name="t_bl"/> <!-- Type : 1 for arc -->       
    <xsl:call-template name="t_mustPropsValsResolve"> <!-- Operation/@Name -->
      <xsl:with-param name="p_avl" select="'Operation'"/>   
      <xsl:with-param name="p_vvl" select="'Name'"/>   
    </xsl:call-template>     
    <xsl:call-template name="t_startPoint"/> 
    <xsl:call-template name="t_endPoint"/>  
    <xsl:call-template name="t_centerPoint"/>  
    <xsl:value-of select="Clockwise/text()"/> <xsl:call-template name="t_bl"/>
    <xsl:call-template name="t_mustPropsValsResolve"> <!-- KerfCompensation/@Name -->
      <xsl:with-param name="p_avl" select="'KerfCompensation'"/>   
      <xsl:with-param name="p_vvl" select="'Name'"/>   
    </xsl:call-template>    
    <xsl:call-template name="t_mustPropsValsResolve"> <!-- Tool/@Name @StartValue @EndValue -->
      <xsl:with-param name="p_avl" select="'Tool'"/>   
      <xsl:with-param name="p_vvl" select="'Name StartValue EndValue'"/>   
    </xsl:call-template>        
    <xsl:call-template name="t_canPropsValsResolve"> <!-- UserProperty/@Index -->
      <xsl:with-param name="p_avl" select="'UserProperty'"/>   
      <xsl:with-param name="p_vvl" select="'Index'"/>   
    </xsl:call-template>       
    <xsl:call-template name="t_lf"/>    
  </xsl:template>
  
   
 
  <xsl:template match="Part/Label">
    <xsl:text>9</xsl:text> <xsl:call-template name="t_bl"/> <!-- Type : 9 for text -->      
    <xsl:text>4</xsl:text> <xsl:call-template name="t_bl"/> <!-- Machining : 1 for marking -->      
    <xsl:call-template name="t_startPoint"/> 
    <xsl:call-template name="t_endPoint"/>    
    <xsl:call-template name="t_mustPropsValsResolve"> <!-- Font/@Name -->
      <xsl:with-param name="p_avl" select="'Font'"/>   
      <xsl:with-param name="p_vvl" select="'Name'"/>   
    </xsl:call-template>
    <xsl:value-of select="FontSizeScale/Y/text()"/> <xsl:call-template name="t_bl"/>
    <xsl:value-of select="FontSizeScale/X/text()"/> <xsl:call-template name="t_bl"/> 
    <xsl:text>-1</xsl:text> <xsl:call-template name="t_bl"/> <!-- Kerf : 0 for no -->
    <xsl:text>32</xsl:text> <xsl:call-template name="t_bl"/> <!-- Tool class : 31 for Marking -->
    <xsl:text>0</xsl:text> <xsl:call-template name="t_bl"/> <!-- first value of tool : 0 -->
    <xsl:text>0</xsl:text> <xsl:call-template name="t_bl"/> <!-- second value of tool : 0 -->
    <xsl:call-template name="t_canPropsValsResolve"> <!-- UserProperty/@Index -->
      <xsl:with-param name="p_avl" select="'UserProperty'"/>   
      <xsl:with-param name="p_vvl" select="'Index'"/>   
    </xsl:call-template>       
    <xsl:call-template name="t_lf"/>
    <xsl:value-of select="Text/text()"/>
    <xsl:call-template name="t_lf"/>           
  </xsl:template>
  
  
  
  <xsl:template match="Part/Marking">
    <xsl:apply-templates select="Line|Arc"/>    
  </xsl:template>
  
  
  
  <xsl:template match="Part/Marking/Line">
    <xsl:text>0</xsl:text> <xsl:call-template name="t_bl"/> <!-- Type : 0 for line -->
    <xsl:text>1</xsl:text> <xsl:call-template name="t_bl"/> <!-- Machining : 1 for marking -->      
    <xsl:call-template name="t_startPoint"/> 
    <xsl:call-template name="t_endPoint"/>  
    <xsl:call-template name="t_mustPropsValsResolve"> <!-- KerfCompensation/@Name -->
      <xsl:with-param name="p_avl" select="'KerfCompensation'"/>   
      <xsl:with-param name="p_vvl" select="'Name'"/>   
    </xsl:call-template> 
    <xsl:call-template name="t_mustPropsValsResolve"> <!-- Tool/@Name @StartValue @EndValue -->
      <xsl:with-param name="p_avl" select="'Tool'"/>   
      <xsl:with-param name="p_vvl" select="'Name StartValue EndValue'"/>   
    </xsl:call-template>        
          
    <xsl:call-template name="t_lf"/>    
  </xsl:template>
  
  
  
  <xsl:template match="Part/Marking/Arc">
    <xsl:text>0</xsl:text> <xsl:call-template name="t_bl"/> <!-- Type : 0 for line -->
    <xsl:text>1</xsl:text> <xsl:call-template name="t_bl"/> <!-- Machining : 1 for marking -->      
    <xsl:call-template name="t_startPoint"/> 
    <xsl:call-template name="t_endPoint"/>  
    <xsl:call-template name="t_centerPoint"/>  
    <xsl:value-of select="Clockwise/text()"/> <xsl:call-template name="t_bl"/>
    <xsl:text>0</xsl:text> <xsl:call-template name="t_bl"/> <!-- KerfCompensation : 0 for no -->
    <xsl:call-template name="t_mustPropsValsResolve"> <!-- Tool/@Name @StartValue @EndValue -->
      <xsl:with-param name="p_avl" select="'Tool'"/>   
      <xsl:with-param name="p_vvl" select="'Name StartValue EndValue'"/>   
    </xsl:call-template>        
    <xsl:call-template name="t_canPropsValsResolve"> <!-- UserProperty/@Index -->
      <xsl:with-param name="p_avl" select="'UserProperty'"/>   
      <xsl:with-param name="p_vvl" select="'Index'"/>   
    </xsl:call-template>           
    <xsl:call-template name="t_lf"/>    
  </xsl:template>
  
  
  
  
  
  
  <!-- 
    Gibt von allen <Property> Elementen mit gleichem gegebenem Wert in @name 
    resp. @type den Inhalt aller <Value> Elemente mit gegebenen Werten in 
    @name aus.
    
    Sucht die <Property> Elemente ausgehend vom aktuellen Kontext aufsteigend 
    bis zum Wurzelknoten nur dann, wenn sie selbst schon im aktuellen Kontext
    vorhanden sind.
  -->
  <xsl:template name="t_canPropsValsResolve">
    <xsl:param name="p_avl"/> <!-- @name-Wert des <Property> Elementes -->
    <xsl:param name="p_vvl"/> <!-- Leerzeichen-separierte Liste von @name-Werten der <Value> Elemente -->
    <xsl:if test="count(Property[@name=$p_avl])&gt;0">
      <xsl:call-template name="t_mustPropsValsResolve"> 
        <xsl:with-param name="p_avl" select="$p_avl"/>   
        <xsl:with-param name="p_vvl" select="$p_vvl"/>   
      </xsl:call-template> 
    </xsl:if>   
  </xsl:template>
  
  
  
  <!-- 
    Gibt von allen <Property> Elementen mit gleichem gegebenem Wert in @name 
    resp. @type den Inhalt aller <Value> Elemente mit gegebenen Werten in 
    @name aus.
    
    Sucht die <Property> Elemente ausgehend vom aktuellen Kontext aufsteigend 
    bis zum Wurzelknoten auch dann, wenn sie nicht im aktuellen Kontext 
    vorhanden sind.
  -->
  <xsl:template name="t_mustPropsValsResolve">
    <xsl:param name="p_avl"/> <!-- @name-Wert des <Property> Elementes -->
    <xsl:param name="p_vvl"/> <!-- Leerzeichen-separierte Liste von @name-Werten der <Value> Elemente -->
    <xsl:variable name="v_sep" select="' '"/>
    
    <xsl:choose>
      <xsl:when test="contains($p_vvl,$v_sep)">
        
        <xsl:call-template name="t_propsValsResolve">
          <xsl:with-param name="p_att" select="'name'"/>
          <xsl:with-param name="p_avl" select="$p_avl"/>
          <xsl:with-param name="p_nvl" select="$p_avl"/>
          <xsl:with-param name="p_vvl" select="substring-before($p_vvl,$v_sep)"/>
          <xsl:with-param name="p_lvl" select="self::node()"/>   
        </xsl:call-template>                                     
                       
        <xsl:call-template name="t_mustPropsValsResolve">
          <xsl:with-param name="p_att" select="'name'"/>
          <xsl:with-param name="p_avl" select="$p_avl"/>
          <xsl:with-param name="p_nvl" select="$p_avl"/>
          <xsl:with-param name="p_vvl" select="substring-after($p_vvl,$v_sep)"/>
          <xsl:with-param name="p_lvl" select="self::node()"/>   
        </xsl:call-template>    
        
      </xsl:when>
      <xsl:otherwise>

        <xsl:call-template name="t_propsValsResolve">
          <xsl:with-param name="p_att" select="'name'"/>
          <xsl:with-param name="p_avl" select="$p_avl"/>
          <xsl:with-param name="p_nvl" select="$p_avl"/>
          <xsl:with-param name="p_vvl" select="$p_vvl"/>
          <xsl:with-param name="p_lvl" select="self::node()"/>   
        </xsl:call-template>                                     
                
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>



  <!-- 
    Gibt von allen <Property> Elementen mit gleichem gegebenem Wert in @name 
    resp. @type den Inhalt aller <Value> Elemente mit gegebenem Wert in @name 
    aus.
    
    Sucht die <Property> Elemente ausgehend vom gegebenen Kontext aufsteigend 
    bis zum Wurzelknoten.
  -->
  <xsl:template name="t_propsValsResolve">
    <xsl:param name="p_att"/> <!-- Name des <Property> Element Attributes ('name' or 'type') -->
    <xsl:param name="p_avl"/> <!-- @name-Wert or @type-Wert des <Property> Elementes -->
    <xsl:param name="p_nvl"/> <!-- @name-Wert des <Property> Elementes -->
    <xsl:param name="p_vvl"/> <!-- @name-Wert des auszugebenden <Value> Elementes -->
    <xsl:param name="p_lvl"/> <!-- Kontext -->   
    <xsl:variable name="v_prp" select="$p_lvl/Property[@name                  =$p_nvl]
                                                      [@*[local-name()=$p_att]=$p_avl]"/>    
    <xsl:choose>   
      
      <!-- Kontext enth�lt gesuchte <Property> Elemente -->
      <xsl:when test="count($v_prp) &gt; 0">
        <xsl:for-each select="$v_prp">  
          
          <xsl:call-template name="t_propValsResolve">
            <xsl:with-param name="p_nvl" select="$p_nvl"/>
            <xsl:with-param name="p_vvl" select="$p_vvl"/>
            <xsl:with-param name="p_lvl" select="$p_lvl"/>
          </xsl:call-template>
                    
        </xsl:for-each>               
      </xsl:when> 
      
      <!-- Kontext enth�lt keine gesuchte <Property> Elemente -->
      <xsl:otherwise>
        <xsl:if test="count($p_lvl) &gt; 0">
          
          <xsl:call-template name="t_propsValsResolve">     
            <xsl:with-param name="p_att" select="$p_att"/>   
            <xsl:with-param name="p_avl" select="$p_avl"/>  
            <xsl:with-param name="p_nvl" select="$p_nvl"/>   
            <xsl:with-param name="p_vvl" select="$p_vvl"/>   
            <xsl:with-param name="p_lvl" select="$p_lvl/parent::node()"/>   
          </xsl:call-template>   
          
        </xsl:if>   
      </xsl:otherwise> 
    </xsl:choose>

  </xsl:template>   
   
  
  
  <!-- 
    Gibt von einem <Property> Element mit gegebenem Wert in @name den Inhalt 
    aller <Value> Elemente mit gegebenem Wert in @name aus.
    
    Sucht die <Property> Elemente ausgehend vom gegebenen Kontext aufsteigend 
    bis zum Wurzelknoten.
    
    Setzt man statt 'Name', 'Day', 'Month' oder 'Year' einen anderen Wert ein,
    z.B. 'dbg_Name', unterbbleibt die Abbildung des Wertes auf einen anderen.
  -->
  <xsl:template name="t_propValsResolve">
    <xsl:param name="p_nvl"/> <!-- @name-Wert des <Property> Elementes -->
    <xsl:param name="p_vvl"/> <!-- @name-Wert des auszugebenden <Value> Elementes -->
    <xsl:param name="p_lvl"/> <!-- Kontext -->   
    <xsl:variable name="v_val" select="Value[@name=$p_vvl]"/>
    
    <xsl:choose>   
   
      <!-- Gegebenes <Property> Element enth�lt gesuchte <Value> Elemente -->
      <xsl:when test="count($v_val)&gt;0"> 
        <xsl:for-each select="$v_val">      
                   
          <xsl:choose>
            <!-- -->
            <xsl:when test="$p_vvl='Name'">
            
              <!-- [Un]mittelbare Ausgabe des gefundenen <Value> Elementes -->
              <xsl:call-template name="t_mapPropValContent">
                <xsl:with-param name="p_nvl" select="$p_nvl"/>
                <xsl:with-param name="p_vtx" select="text()"/>
              </xsl:call-template>                                     
              <xsl:call-template name="t_bl"/>
            
            </xsl:when>
            <xsl:when test="$p_vvl='yyyy-mm-ddThh:mm:ss'">
            
              <!-- Formatierte Ausgabe des gefundenen <Value> Elementes -->
              <xsl:value-of select="substring(normalize-space(text()),6,2)"/>
              <xsl:text>/</xsl:text>   
              <xsl:value-of select="substring(normalize-space(text()),9,2)"/>
              <xsl:text>/</xsl:text>   
              <xsl:value-of select="substring(normalize-space(text()),3,2)"/>
   
            </xsl:when>    
            <xsl:when test="$p_vvl='Day' or $p_vvl='Month'">
            
              <!-- Formatierte Ausgabe des gefundenen <Value> Elementes -->
              <xsl:if test="string-length(normalize-space(text()))=1">
                <xsl:text>0</xsl:text>
              </xsl:if>    
              <xsl:value-of select="normalize-space(text())"/>     
              <xsl:text>/</xsl:text>   
            
            </xsl:when>    
            <xsl:when test="$p_vvl='Year'">
            
              <!-- Formatierte Ausgabe des gefundenen <Value> Elementes -->
              <xsl:value-of select="substring(normalize-space(text()),3,2)"/>
            
            </xsl:when>    
            <xsl:otherwise>
            
              <!-- Unmittelbare Ausgabe des gefundenen <Value> Elementes -->
              <xsl:value-of select="text()"/> 
              <xsl:call-template name="t_bl"/>
            
            </xsl:otherwise>
          </xsl:choose>
               
        </xsl:for-each>        
        
        <!-- 
          Spezialfall f�r <Property name="UserProperty"> 
                                              <Value name="Value"> ...
          
          Obwohl gegebenes <Property> Element bereits gesuchte <Value> 
          Elemente enth�lt, suche weiter nach oben nach allen weiteren 
          <Property name="UserProperty"> <Value name="Value"> Elementen 
        -->
        <xsl:if test="$p_nvl='UserProperty' and $p_vvl='Value'">
            <xsl:if test="count($p_lvl/..) &gt; 0">
            
            <xsl:call-template name="t_propsValsResolve">     
              <xsl:with-param name="p_att" select="'type'"/>  
              <xsl:with-param name="p_avl" select="@type"/>  
              <xsl:with-param name="p_nvl" select="$p_nvl"/>   
              <xsl:with-param name="p_vvl" select="$p_vvl"/>   
              <xsl:with-param name="p_lvl" select="$p_lvl/../parent::node()"/>   
            </xsl:call-template>   
            
          </xsl:if>   
        </xsl:if>
      </xsl:when>
      
      <!-- Gegebenes <Property> Element enth�lt keine gesuchten <Value> Elemente -->
      <xsl:otherwise> 
        <xsl:if test="count($p_lvl) &gt; 0">
          
          <xsl:call-template name="t_propsValsResolve">     
            <xsl:with-param name="p_att" select="'type'"/>  
            <xsl:with-param name="p_avl" select="@type"/>  
            <xsl:with-param name="p_nvl" select="$p_nvl"/>   
            <xsl:with-param name="p_vvl" select="$p_vvl"/>   
            <xsl:with-param name="p_lvl" select="$p_lvl/parent::node()"/>   
          </xsl:call-template>   
          
        </xsl:if>   
      </xsl:otherwise>  
    </xsl:choose>  
    
  </xsl:template>
  
   
   
  <!-- Gibt Text oder Abbildung des Textes aus --> 
  <xsl:template name="t_mapPropValContent">
    <xsl:param name="p_nvl"/> <!-- Wert des <Property> Element Attributes name -->
    <xsl:param name="p_vtx"/> <!-- Text des auszugebenden <Value> Elementes-->
        
    <xsl:choose>
            
      <xsl:when test="normalize-space($p_nvl)='Action'">
        <xsl:choose>
          <xsl:when test="normalize-space($p_vtx)='delete'"> <xsl:text>0</xsl:text> </xsl:when>
          <xsl:when test="normalize-space($p_vtx)='add'">    <xsl:text>1</xsl:text> </xsl:when>
          <xsl:when test="normalize-space($p_vtx)='modify'"> <xsl:text>2</xsl:text> </xsl:when>
          <xsl:otherwise>
            <xsl:text>*** ALMA error: Action is neither "delete", "add" nor "modify" but "</xsl:text>
            <xsl:value-of select="$p_vtx"/>
            <xsl:text>" ***</xsl:text>
          </xsl:otherwise>    
        </xsl:choose>  
      </xsl:when>
      
      <xsl:when test="normalize-space($p_nvl)='Font'">
        <xsl:choose>
          <xsl:when test="normalize-space($p_vtx)='TEXT1'">  <xsl:text>0</xsl:text> </xsl:when>
          <xsl:when test="normalize-space($p_vtx)='TEXT21'"> <xsl:text>1</xsl:text> </xsl:when>
          <xsl:when test="normalize-space($p_vtx)='TEXT22'"> <xsl:text>2</xsl:text> </xsl:when>
          <xsl:otherwise>
            <xsl:text>*** ALMA error: Font is neither "TEXT1", "TEXT21" nor "TEXT22" but "</xsl:text>
            <xsl:value-of select="$p_vtx"/>
            <xsl:text>" ***</xsl:text>
          </xsl:otherwise>    
        </xsl:choose>  
      </xsl:when>
      
      <xsl:when test="normalize-space($p_nvl)='KerfCompensation'">
        <xsl:choose>
          <xsl:when test="normalize-space($p_vtx)='no'">            <xsl:text>-1</xsl:text> </xsl:when>
          <xsl:when test="normalize-space($p_vtx)='marking'">            <xsl:text>0</xsl:text> </xsl:when>
          <xsl:when test="normalize-space($p_vtx)='lefthandside'">  <xsl:text>1</xsl:text> </xsl:when>
          <xsl:when test="normalize-space($p_vtx)='righthandside'"> <xsl:text>2</xsl:text> </xsl:when>
          <xsl:otherwise>
            <xsl:text>*** ALMA error: KerfCompensation is neither "no", "lefthandside" nor "righthandside" but "</xsl:text>
            <xsl:value-of select="$p_vtx"/>
            <xsl:text>" ***</xsl:text>
          </xsl:otherwise>    
        </xsl:choose>  
      </xsl:when>
      
      <xsl:when test="normalize-space($p_nvl)='NibblingTool'">
        <xsl:choose>
          <xsl:when test="normalize-space($p_vtx)='no'"> <xsl:text>0</xsl:text> </xsl:when>
           <xsl:otherwise>
             <xsl:text>*** ALMA error: NibblingTool is not "no", but "</xsl:text>
            <xsl:value-of select="$p_vtx"/>
            <xsl:text>" ***</xsl:text>
          </xsl:otherwise>    
        </xsl:choose>  
      </xsl:when>
      
      <xsl:when test="normalize-space($p_nvl)='Operation'">
        <xsl:choose>
          <xsl:when test="normalize-space($p_vtx)='cut'">   <xsl:text>0</xsl:text> </xsl:when>
          <xsl:when test="normalize-space($p_vtx)='mark'">  <xsl:text>1</xsl:text> </xsl:when>
          <xsl:when test="normalize-space($p_vtx)='punch'"> <xsl:text>2</xsl:text> </xsl:when>
          <xsl:otherwise>
            <xsl:text>*** ALMA error: Operation is neither "cut", "mark" nor "punch" but "</xsl:text>
            <xsl:value-of select="$p_vtx"/>
            <xsl:text>" ***</xsl:text>
          </xsl:otherwise>    
        </xsl:choose>  
      </xsl:when>
            
      <xsl:when test="normalize-space($p_nvl)='Tool'">
        <xsl:choose>
          <xsl:when test="normalize-space($p_vtx)='unspecified'">     <xsl:text>0</xsl:text> </xsl:when>
          <xsl:when test="normalize-space($p_vtx)='Torch'">     <xsl:text>1</xsl:text> </xsl:when>
          <xsl:when test="normalize-space($p_vtx)='Bevel'">     <xsl:text>29</xsl:text> </xsl:when>
          <xsl:when test="normalize-space($p_vtx)='Marking'">   <xsl:text>0</xsl:text> </xsl:when>
          <xsl:when test="normalize-space($p_vtx)='round'">     <xsl:text>51</xsl:text> </xsl:when>
          <xsl:when test="normalize-space($p_vtx)='rectangle'"> <xsl:text>52</xsl:text> </xsl:when>
          <xsl:when test="normalize-space($p_vtx)='oblong'">    <xsl:text>53</xsl:text> </xsl:when>
          <xsl:when test="normalize-space($p_vtx)='special'">   <xsl:text>58</xsl:text> </xsl:when>
          <xsl:otherwise>
            <xsl:text>*** ALMA error: Tool is neither "Torch", "Bevel", "Marking", "round", "rectangle", "oblong" nor "special" but "</xsl:text>
            <xsl:value-of select="$p_vtx"/>
            <xsl:text>" ***</xsl:text>
          </xsl:otherwise>    
        </xsl:choose>  
      </xsl:when>
      
      <xsl:when test="normalize-space($p_nvl)='Unit'">
        <xsl:choose>
          <xsl:when test="normalize-space($p_vtx)='mm'"> <xsl:text>0</xsl:text> </xsl:when>
          <xsl:when test="normalize-space($p_vtx)='in'"> <xsl:text>1</xsl:text> </xsl:when>
          <xsl:otherwise>
            <xsl:text>*** ALMA error: Unit is neither "mm" nor "in" but "</xsl:text>
            <xsl:value-of select="$p_vtx"/>
            <xsl:text>" ***</xsl:text>
          </xsl:otherwise>    
        </xsl:choose>  
      </xsl:when>

      <xsl:otherwise>
        <xsl:value-of select="$p_vtx"/>
      </xsl:otherwise>
    </xsl:choose>  
    
  </xsl:template>                                     
  
  
  
  <xsl:template name="t_lf">
    <xsl:text>&#10;</xsl:text>
  </xsl:template>

  <xsl:template name="t_bl">
    <xsl:text>&#32;</xsl:text>
  </xsl:template>
  
  
  
  <xsl:template name="t_date">
    <xsl:if test="string-length(normalize-space(Value[@name='Month']/text()))=1">
      <xsl:text>0</xsl:text>
    </xsl:if>    
    <xsl:value-of select="normalize-space(Value[@name='Month']/text())"/>     
    <xsl:text>/</xsl:text>   
    <xsl:if test="string-length(normalize-space(Value[@name='Day']/text()))=1">
      <xsl:text>0</xsl:text>
    </xsl:if>       
    <xsl:value-of select="normalize-space(Value[@name='Day']/text())"/>     
    <xsl:text>/</xsl:text> 
    <xsl:value-of select="substring(normalize-space(Value[@name='Year']/text()),3,2)"/>      
   </xsl:template>  
    
  <xsl:template name="t_startPoint">
    <xsl:value-of select="StartPoint/X/text()"/> <xsl:call-template name="t_bl"/>         
    <xsl:value-of select="StartPoint/Y/text()"/> <xsl:call-template name="t_bl"/>    
  </xsl:template>
  
  <xsl:template name="t_endPoint">
    <xsl:value-of select="EndPoint/X/text()"/> <xsl:call-template name="t_bl"/>         
    <xsl:value-of select="EndPoint/Y/text()"/> <xsl:call-template name="t_bl"/>    
  </xsl:template>
  
  <xsl:template name="t_centerPoint">
    <xsl:value-of select="CenterPoint/X/text()"/> <xsl:call-template name="t_bl"/>         
    <xsl:value-of select="CenterPoint/Y/text()"/> <xsl:call-template name="t_bl"/>    
  </xsl:template>
  
  
  
</xsl:transform>    
    
    
    