<?xml version="1.0" encoding="ISO-8859-1"?>

<!--

Date               Author               Changes
08 Dec 2009        J. Mansvelders       Created
14 Dec 2009        J. Mansvelders       Updated placement of text

-->

<xsl:transform version="1.0"
  xmlns:nxsdci="http://www.ugs.com/NXShipDesign/CuttingInterface"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:exsl="http://exslt.org/common"
  xmlns:math="http://exslt.org/math"
  extension-element-prefixes="exsl math ">

  <xsl:output method="text" encoding="ISO-8859-1" media-type="text/plain" omit-xml-declaration="yes"/>


  <!-- Global definitions -->

  <!-- Cutting lines/arcs -->
  <xsl:param name="cutLayer">1</xsl:param>
  <xsl:param name="cutColor">1</xsl:param>

  <!-- Marking lines/arcs -->
  <xsl:param name="markingLayer">2</xsl:param>
  <xsl:param name="markingColor">2</xsl:param>

  <!-- Labels -->
  <xsl:param name="labelLayer">40</xsl:param>
  <xsl:param name="labelColor">5</xsl:param>

  <xsl:param name="labelHeight">30</xsl:param>
  <xsl:param name="labelWidthScale">0.5</xsl:param>

  <!-- Properties -->
  <xsl:param name="thicknessLayer">61</xsl:param>
  <xsl:param name="thicknessColor">1</xsl:param>
  <xsl:param name="thicknessPrefix">[T]</xsl:param>

  <xsl:param name="materialLayer">62</xsl:param>
  <xsl:param name="materialColor">2</xsl:param>
  <xsl:param name="materialPrefix">[M]</xsl:param>

  <xsl:param name="quantityLayer">63</xsl:param>
  <xsl:param name="quantityColor">3</xsl:param>
  <xsl:param name="quantityPrefix">[Q]</xsl:param>
  
  <xsl:param name="propHeight">30</xsl:param>
  <xsl:param name="propWidthScale">0.5</xsl:param>

  <!-- User Properties -->
  <xsl:param name="userpropLayer">51</xsl:param>
  <xsl:param name="userpropColor">4</xsl:param>
  
  <xsl:param name="userpropHeight">30</xsl:param>
  <xsl:param name="userpropWidthScale">0.5</xsl:param>


  <xsl:template match="/nxsdci:NXShipDesignCuttingInterface">
    <xsl:text>  0&#10;</xsl:text>
    <xsl:text>SECTION&#10;</xsl:text>
    <xsl:text>  2&#10;</xsl:text>

    <xsl:text>ENTITIES&#10;</xsl:text>
    <xsl:text>  0&#10;</xsl:text>

    <xsl:apply-templates select="Part"/>

    <xsl:text>ENDSEC&#10;</xsl:text>
    <xsl:text>  0&#10;</xsl:text>
    <xsl:text>EOF&#10;</xsl:text>
  </xsl:template>


  <xsl:template match="Part">

    <xsl:apply-templates select="Loop"/>

    <xsl:apply-templates select="Marking"/>

    <xsl:apply-templates select="Label"/>

    <!-- Process properties Size, Quantity and Material-->
    <xsl:call-template name="processProperty">
      <xsl:with-param name="property" select="'SA_COMPONENT_SIZE'"/>
      <xsl:with-param name="layer" select="$thicknessLayer"/>
      <xsl:with-param name="color" select="$thicknessColor"/>
      <xsl:with-param name="prefix" select="$thicknessPrefix"/>
    </xsl:call-template>

    <xsl:call-template name="processProperty">
      <xsl:with-param name="property" select="'SA_COMPONENT_QUANTITY'"/>
      <xsl:with-param name="layer" select="$quantityLayer"/>
      <xsl:with-param name="color" select="$quantityColor"/>
      <xsl:with-param name="prefix" select="$quantityPrefix"/>
    </xsl:call-template>

    <xsl:call-template name="processProperty">
      <xsl:with-param name="property" select="'SA_COMPONENT_MATERIAL'"/>
      <xsl:with-param name="layer" select="$materialLayer"/>
      <xsl:with-param name="color" select="$materialColor"/>
      <xsl:with-param name="prefix" select="$materialPrefix"/>
    </xsl:call-template>

    <!-- Process user properties -->
    <xsl:for-each select="Property[@name='UserProperty']">
      <xsl:call-template name="processUserProperty">
      <xsl:with-param name="layer" select="$userpropLayer"/>
      <xsl:with-param name="color" select="$userpropLayer"/>
    </xsl:call-template>
    </xsl:for-each>

  </xsl:template>


  <xsl:template match="Part/Loop">
    <xsl:apply-templates select="Line|Arc"/>
  </xsl:template>

  <xsl:template match="Part/Loop/Line">
    <xsl:call-template name="processLine">
      <xsl:with-param name="layer" select="$cutLayer"/>
      <xsl:with-param name="color" select="$cutColor"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="Part/Loop/Arc">
    <xsl:call-template name="processArc">
      <xsl:with-param name="layer" select="$cutLayer"/>
      <xsl:with-param name="color" select="$cutColor"/>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="Part/Label">
    <xsl:variable name="text">
      <xsl:value-of select="Text/text()"/>
    </xsl:variable>

    <xsl:variable name="x_start">
      <xsl:value-of select="StartPoint/X/text()"/>
    </xsl:variable>

    <xsl:variable name="y_start">
      <xsl:value-of select="StartPoint/Y/text()"/>
    </xsl:variable>
    
    <xsl:variable name="x_end">
      <xsl:value-of select="EndPoint/X/text()"/>
    </xsl:variable>
    
    <xsl:variable name="y_end">
      <xsl:value-of select="EndPoint/Y/text()"/>
    </xsl:variable>
    
    <!-- calculate angle -->
    <xsl:variable name="dx">
      <xsl:value-of select="$x_end - $x_start"/>
    </xsl:variable>
    
    <xsl:variable name="dy">
      <xsl:value-of select="$y_end - $y_start"/>
    </xsl:variable>
    
    <xsl:variable name="angle">
      <xsl:value-of select="math:atan(math:abs($dy div $dx)) * 57.2957795"/>
    </xsl:variable>
    
    <xsl:variable name="text_angle">
      <xsl:choose>
        <xsl:when test="$dx &gt; 0 and $dy &gt; 0">
          <!-- 0-90 degrees -->
          <xsl:value-of select='$angle'/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:choose>
            <!-- 90-180 degrees -->
            <xsl:when test="$dx &lt; 0 and $dy &gt; 0">
              <xsl:value-of select="180 - $angle"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:choose>
                <!-- 180-270 degrees -->
                <xsl:when test="$dx &lt; 0 and $dy &lt; 0">
                  <xsl:value-of select="180 + $angle"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:choose>
                    <!-- 270-360 degrees -->
                    <xsl:when test="$dx &gt; 0 and $dy &lt; 0">
                      <xsl:value-of select="360 - $angle"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:choose>
                        <xsl:when test="$dx=0 and $dy &gt; 0">
                          <xsl:value-of select="90"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:choose>
                            <xsl:when test="$dx=0 and $dy &lt; 0">
                              <xsl:value-of select="270"/>
                            </xsl:when>
                            <xsl:otherwise>
                              <xsl:choose>
                                <xsl:when test="$dy=0 and $dx &gt; 0">
                                  <xsl:value-of select="0"/>
                                </xsl:when>
                                <xsl:otherwise>
                                  <xsl:value-of select="180"/>
                                </xsl:otherwise>
                              </xsl:choose>
                            </xsl:otherwise>
                          </xsl:choose>
                        </xsl:otherwise>
                      </xsl:choose>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:call-template name="processText">
      <xsl:with-param name="text" select="$text"/>
      <xsl:with-param name="x_coord" select="$x_start"/>
      <xsl:with-param name="y_coord" select="$y_start"/>
      <xsl:with-param name="z_coord" select="'0'"/>
      <xsl:with-param name="angle" select="$text_angle"/>
      <xsl:with-param name="layer" select="$labelLayer"/>
      <xsl:with-param name="color" select="$labelColor"/>
      <xsl:with-param name="height" select="$labelHeight"/>
      <xsl:with-param name="widthscale" select="$labelWidthScale"/>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="Part/Marking">
    <xsl:apply-templates select="Line|Arc"/>
  </xsl:template>

  <xsl:template match="Part/Marking/Line">
    <xsl:call-template name="processLine">
      <xsl:with-param name="layer" select="$markingLayer"/>
      <xsl:with-param name="color" select="$markingColor"/>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="Part/Marking/Arc">
    <xsl:call-template name="processArc">
      <xsl:with-param name="layer" select="$markingLayer"/>
      <xsl:with-param name="color" select="$markingColor"/>
    </xsl:call-template>
  </xsl:template>


  <xsl:template name="processArcAngle">
    <xsl:param name="clockwise"/>
    <xsl:param name="angle"/>
    <xsl:param name="dx"/>
    <xsl:param name="dy"/>

    <xsl:choose>
      <xsl:when test="$clockwise=1">
        <xsl:text> 50&#10;</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text> 51&#10;</xsl:text>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:choose>
      <xsl:when test="$dx &gt; 0 and $dy &gt; 0">
        <!-- 0-90 degrees -->
        <xsl:value-of select='round(1000*$angle) div 1000'/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <!-- 90-180 degrees -->
          <xsl:when test="$dx &lt; 0 and $dy &gt; 0">
            <xsl:value-of select="180 - round(1000*$angle) div 1000"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:choose>
              <!-- 180-270 degrees -->
              <xsl:when test="$dx &lt; 0 and $dy &lt; 0">
                <xsl:value-of select="180 + round(1000*$angle) div 1000"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:choose>
                  <!-- 270-360 degrees -->
                  <xsl:when test="$dx &gt; 0 and $dy &lt; 0">
                    <xsl:value-of select="360 - round(1000*$angle) div 1000"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:choose>
                      <xsl:when test="$dx=0 and $dy &gt; 0">
                        <xsl:value-of select="90"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:choose>
                          <xsl:when test="$dx=0 and $dy &lt; 0">
                            <xsl:value-of select="270"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:choose>
                              <xsl:when test="$dy=0 and $dx &gt; 0">
                                <xsl:value-of select="0"/>
                              </xsl:when>
                              <xsl:otherwise>
                                <xsl:value-of select="180"/>
                              </xsl:otherwise>
                            </xsl:choose>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:text>&#10;</xsl:text>
  </xsl:template>


  <xsl:template name="processText">
    <xsl:param name="text"/>
    <xsl:param name="x_coord"/>
    <xsl:param name="y_coord"/>
    <xsl:param name="z_coord"/>
    <xsl:param name="angle"/>
    <xsl:param name="layer"/>
    <xsl:param name="color"/>
    <xsl:param name="height"/>
    <xsl:param name="widthscale"/>

    <xsl:text>TEXT&#10;</xsl:text>
    <!-- Layer -->
    <xsl:text>  8&#10;</xsl:text>
    <xsl:value-of select="$layer"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text> 62&#10;</xsl:text>
    <!-- color -->
    <xsl:value-of select="$color"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text> 10&#10;</xsl:text>
    <xsl:value-of select="$x_coord"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text> 20&#10;</xsl:text>
    <xsl:value-of select="$y_coord"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text> 30&#10;</xsl:text>
    <xsl:value-of select="$z_coord"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text> 40&#10;</xsl:text>
    <xsl:value-of select="$height"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text> 41&#10;</xsl:text>
    <xsl:value-of select="$widthscale"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>  1&#10;</xsl:text>
    <xsl:value-of select="$text"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text> 50&#10;</xsl:text>
    <xsl:value-of select="$angle"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>  0&#10;</xsl:text>
  </xsl:template>


  <xsl:template name="processProperty">
    <xsl:param name="property"/>
    <xsl:param name="layer"/>
    <xsl:param name="color"/>
    <xsl:param name="prefix"/>

    <xsl:variable name="text">
      <xsl:value-of select="$prefix"/>
      <xsl:call-template name="t_mustPropsValsResolve">
        <xsl:with-param name="p_avl" select="$property"/>
        <xsl:with-param name="p_vvl" select="'Amount'"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:call-template name="processText">
      <xsl:with-param name="text" select="$text"/>
      <xsl:with-param name="x_coord" select="'0'"/>
      <xsl:with-param name="y_coord" select="'0'"/>
      <xsl:with-param name="z_coord" select="'0'"/>
      <xsl:with-param name="angle" select="'0'"/>
      <xsl:with-param name="layer" select="$layer"/>
      <xsl:with-param name="color" select="$color"/>
      <xsl:with-param name="height" select="$propHeight"/>
      <xsl:with-param name="widthscale" select="$propWidthScale"/>
    </xsl:call-template>
  </xsl:template>


  <xsl:template name="processUserProperty">
    <xsl:param name="layer"/>
    <xsl:param name="color"/>

    <xsl:variable name="text">
      <xsl:call-template name="t_propValsResolve">
        <xsl:with-param name="p_nvl" select="'UserProperty'"/>
        <xsl:with-param name="p_vvl" select="'Name'"/>
        <xsl:with-param name="p_lvl" select="self::node()"/>
      </xsl:call-template>
      <xsl:call-template name="t_propValsResolve">
        <xsl:with-param name="p_nvl" select="'UserProperty'"/>
        <xsl:with-param name="p_vvl" select="'Index'"/>
        <xsl:with-param name="p_lvl" select="self::node()"/>
      </xsl:call-template>
      <xsl:call-template name="t_propValsResolve">
        <xsl:with-param name="p_nvl" select="'UserProperty'"/>
        <xsl:with-param name="p_vvl" select="'Value'"/>
        <xsl:with-param name="p_lvl" select="self::node()"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:call-template name="processText">
      <xsl:with-param name="text" select="$text"/>
      <xsl:with-param name="x_coord" select="'0'"/>
      <xsl:with-param name="y_coord" select="'0'"/>
      <xsl:with-param name="z_coord" select="'0'"/>
      <xsl:with-param name="angle" select="'0'"/>
      <xsl:with-param name="layer" select="$layer"/>
      <xsl:with-param name="color" select="$color"/>
      <xsl:with-param name="height" select="$userpropHeight"/>
      <xsl:with-param name="widthscale" select="$userpropWidthScale"/>
    </xsl:call-template>
  </xsl:template>


  <xsl:template name="processLine">
    <xsl:param name="layer"/>
    <xsl:param name="color"/>

    <xsl:variable name="x_start">
      <xsl:value-of select="StartPoint/X/text()"/>
    </xsl:variable>

    <xsl:variable name="x_end">
      <xsl:value-of select="EndPoint/X/text()"/>
    </xsl:variable>

    <xsl:variable name="y_start">
      <xsl:value-of select="StartPoint/Y/text()"/>
    </xsl:variable>

    <xsl:variable name="y_end">
      <xsl:value-of select="EndPoint/Y/text()"/>
    </xsl:variable>

    <xsl:text>LINE&#10;</xsl:text>
    <xsl:text>  8&#10;</xsl:text>
    <xsl:value-of select="$layer"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text> 62&#10;</xsl:text>
    <xsl:value-of select="$color"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text> 10&#10;</xsl:text>
    <xsl:value-of select='round(1000*$x_start) div 1000'/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text> 20&#10;</xsl:text>
    <xsl:value-of select='round(1000*$y_start) div 1000'/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text> 11&#10;</xsl:text>
    <xsl:value-of select='round(1000*$x_end) div 1000'/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text> 21&#10;</xsl:text>
    <xsl:value-of select='round(1000*$y_end) div 1000'/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>  0&#10;</xsl:text>

  </xsl:template>
  
  <xsl:template name="processArc">
    <xsl:param name="layer"/>
    <xsl:param name="color"/>

    <xsl:variable name="clockwise">
      <xsl:value-of select="Clockwise/text()"/>
    </xsl:variable>
        
    <xsl:variable name="clockwise_1">
      <xsl:choose>
        <xsl:when test="$clockwise=1">
          <xsl:value-of select="'0'"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'1'"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="center_x">
      <xsl:value-of select="CenterPoint/X/text()"/>
    </xsl:variable>

    <xsl:variable name="center_y">
      <xsl:value-of select="CenterPoint/Y/text()"/>
    </xsl:variable>

    <xsl:variable name="start_x">
      <xsl:value-of select="StartPoint/X/text()"/>
    </xsl:variable>

    <xsl:variable name="start_y">
      <xsl:value-of select="StartPoint/Y/text()"/>
    </xsl:variable>

    <xsl:variable name="end_x">
      <xsl:value-of select="EndPoint/X/text()"/>
    </xsl:variable>

    <xsl:variable name="end_y">
      <xsl:value-of select="EndPoint/Y/text()"/>
    </xsl:variable>

    <xsl:variable name="dx_start">
      <xsl:value-of select="$start_x - $center_x"/>
    </xsl:variable>

    <xsl:variable name="dy_start">
      <xsl:value-of select="$start_y - $center_y"/>
    </xsl:variable>

    <xsl:variable name="dx_end">
      <xsl:value-of select="$end_x - $center_x"/>
    </xsl:variable>

    <xsl:variable name="dy_end">
      <xsl:value-of select="$end_y - $center_y"/>
    </xsl:variable>

    <xsl:variable name="radius">
      <xsl:value-of select="math:sqrt(math:power($dy_start, 2) + math:power($dx_start, 2))"/>
    </xsl:variable>

    <xsl:variable name="angle_1">
      <xsl:value-of select="math:atan(math:abs($dy_start div $dx_start)) * 57.2957795"/>
    </xsl:variable>

    <xsl:variable name="angle_2">
      <xsl:value-of select="math:atan(math:abs($dy_end div $dx_end)) * 57.2957795"/>
    </xsl:variable>

    <xsl:text>ARC&#10;</xsl:text>
    <xsl:text>  8&#10;</xsl:text>
    <xsl:value-of select="$layer"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text> 62&#10;</xsl:text>
    <xsl:value-of select="$color"/>
    <xsl:text>&#10;</xsl:text>

    <xsl:text> 10&#10;</xsl:text>
    <xsl:value-of select='round(1000*$center_x) div 1000'/>
    <xsl:text>&#10;</xsl:text>

    <xsl:text> 20&#10;</xsl:text>
    <xsl:value-of select='round(1000*$center_y) div 1000'/>
    <xsl:text>&#10;</xsl:text>

    <xsl:text> 30&#10;</xsl:text>
    <xsl:text>0&#10;</xsl:text>

    <xsl:text> 40&#10;</xsl:text>
    <xsl:value-of select='round(1000*$radius) div 1000'/>
    <xsl:text>&#10;</xsl:text>

    <xsl:call-template name="processArcAngle">
      <xsl:with-param name="clockwise" select="$clockwise"/>
      <xsl:with-param name="angle" select="$angle_2"/>
      <xsl:with-param name="dx" select="$dx_end"/>
      <xsl:with-param name="dy" select="$dy_end"/>
    </xsl:call-template>

    <xsl:call-template name="processArcAngle">
      <xsl:with-param name="clockwise" select="$clockwise_1"/>
      <xsl:with-param name="angle" select="$angle_1"/>
      <xsl:with-param name="dx" select="$dx_start"/>
      <xsl:with-param name="dy" select="$dy_start"/>
    </xsl:call-template>
    
    <xsl:text>  0&#10;</xsl:text>

  </xsl:template>



<!--  ** The definitions below are taken from alma_stylesheet.xsl. **   -->

  <xsl:template name="t_bl">
    <xsl:text>&#32;</xsl:text>
  </xsl:template>
  

  <!--
    Gibt von allen <Property> Elementen mit gleichem gegebenem Wert in @name
    resp. @type den Inhalt aller <Value> Elemente mit gegebenen Werten in
    @name aus.

    Sucht die <Property> Elemente ausgehend vom aktuellen Kontext aufsteigend
    bis zum Wurzelknoten auch dann, wenn sie nicht im aktuellen Kontext
    vorhanden sind.
  -->
  <xsl:template name="t_mustPropsValsResolve">
    <xsl:param name="p_avl"/> <!-- @name-Wert des <Property> Elementes -->
    <xsl:param name="p_vvl"/> <!-- Leerzeichen-separierte Liste von @name-Werten der <Value> Elemente -->
    <xsl:variable name="v_sep" select="' '"/>

    <xsl:choose>
      <xsl:when test="contains($p_vvl,$v_sep)">

        <xsl:call-template name="t_propsValsResolve">
          <xsl:with-param name="p_att" select="'name'"/>
          <xsl:with-param name="p_avl" select="$p_avl"/>
          <xsl:with-param name="p_nvl" select="$p_avl"/>
          <xsl:with-param name="p_vvl" select="substring-before($p_vvl,$v_sep)"/>
          <xsl:with-param name="p_lvl" select="self::node()"/>
        </xsl:call-template>

        <xsl:call-template name="t_mustPropsValsResolve">
          <xsl:with-param name="p_att" select="'name'"/>
          <xsl:with-param name="p_avl" select="$p_avl"/>
          <xsl:with-param name="p_nvl" select="$p_avl"/>
          <xsl:with-param name="p_vvl" select="substring-after($p_vvl,$v_sep)"/>
          <xsl:with-param name="p_lvl" select="self::node()"/>
        </xsl:call-template>

      </xsl:when>
      <xsl:otherwise>

        <xsl:call-template name="t_propsValsResolve">
          <xsl:with-param name="p_att" select="'name'"/>
          <xsl:with-param name="p_avl" select="$p_avl"/>
          <xsl:with-param name="p_nvl" select="$p_avl"/>
          <xsl:with-param name="p_vvl" select="$p_vvl"/>
          <xsl:with-param name="p_lvl" select="self::node()"/>
        </xsl:call-template>

      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>



  <!--
    Gibt von allen <Property> Elementen mit gleichem gegebenem Wert in @name
    resp. @type den Inhalt aller <Value> Elemente mit gegebenem Wert in @name
    aus.

    Sucht die <Property> Elemente ausgehend vom gegebenen Kontext aufsteigend
    bis zum Wurzelknoten.
  -->
  <xsl:template name="t_propsValsResolve">
    <xsl:param name="p_att"/> <!-- Name des <Property> Element Attributes ('name' or 'type') -->
    <xsl:param name="p_avl"/> <!-- @name-Wert or @type-Wert des <Property> Elementes -->
    <xsl:param name="p_nvl"/> <!-- @name-Wert des <Property> Elementes -->
    <xsl:param name="p_vvl"/> <!-- @name-Wert des auszugebenden <Value> Elementes -->
    <xsl:param name="p_lvl"/> <!-- Kontext -->
    <xsl:variable name="v_prp" select="$p_lvl/Property[@name                  =$p_nvl]
                                                      [@*[local-name()=$p_att]=$p_avl]"/>
    <xsl:choose>

      <!-- Kontext enth�lt gesuchte <Property> Elemente -->
      <xsl:when test="count($v_prp) &gt; 0">
        <xsl:for-each select="$v_prp">

          <xsl:call-template name="t_propValsResolve">
            <xsl:with-param name="p_nvl" select="$p_nvl"/>
            <xsl:with-param name="p_vvl" select="$p_vvl"/>
            <xsl:with-param name="p_lvl" select="$p_lvl"/>
          </xsl:call-template>

        </xsl:for-each>
      </xsl:when>

      <!-- Kontext enth�lt keine gesuchte <Property> Elemente -->
      <xsl:otherwise>
        <xsl:if test="count($p_lvl) &gt; 0">

          <xsl:call-template name="t_propsValsResolve">
            <xsl:with-param name="p_att" select="$p_att"/>
            <xsl:with-param name="p_avl" select="$p_avl"/>
            <xsl:with-param name="p_nvl" select="$p_nvl"/>
            <xsl:with-param name="p_vvl" select="$p_vvl"/>
            <xsl:with-param name="p_lvl" select="$p_lvl/parent::node()"/>
          </xsl:call-template>

        </xsl:if>
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>



  <!--
    Gibt von einem <Property> Element mit gegebenem Wert in @name den Inhalt
    aller <Value> Elemente mit gegebenem Wert in @name aus.

    Sucht die <Property> Elemente ausgehend vom gegebenen Kontext aufsteigend
    bis zum Wurzelknoten.

    Setzt man statt 'Name', 'Day', 'Month' oder 'Year' einen anderen Wert ein,
    z.B. 'dbg_Name', unterbbleibt die Abbildung des Wertes auf einen anderen.
  -->
  <xsl:template name="t_propValsResolve">
    <xsl:param name="p_nvl"/> <!-- @name-Wert des <Property> Elementes -->
    <xsl:param name="p_vvl"/> <!-- @name-Wert des auszugebenden <Value> Elementes -->
    <xsl:param name="p_lvl"/> <!-- Kontext -->
    <xsl:variable name="v_val" select="Value[@name=$p_vvl]"/>

    <xsl:choose>

      <!-- Gegebenes <Property> Element enth�lt gesuchte <Value> Elemente -->
      <xsl:when test="count($v_val)&gt;0">
        <xsl:for-each select="$v_val">

          <xsl:choose>
            <!-- -->
            <xsl:when test="$p_vvl='Name'">

              <!-- [Un]mittelbare Ausgabe des gefundenen <Value> Elementes -->
              <xsl:call-template name="t_mapPropValContent">
                <xsl:with-param name="p_nvl" select="$p_nvl"/>
                <xsl:with-param name="p_vtx" select="text()"/>
              </xsl:call-template>
              <xsl:call-template name="t_bl"/>

            </xsl:when>
            <xsl:when test="$p_vvl='yyyy-mm-ddThh:mm:ss'">

              <!-- Formatierte Ausgabe des gefundenen <Value> Elementes -->
              <xsl:value-of select="substring(normalize-space(text()),6,2)"/>
              <xsl:text>/</xsl:text>
              <xsl:value-of select="substring(normalize-space(text()),9,2)"/>
              <xsl:text>/</xsl:text>
              <xsl:value-of select="substring(normalize-space(text()),3,2)"/>

            </xsl:when>
            <xsl:when test="$p_vvl='Day' or $p_vvl='Month'">

              <!-- Formatierte Ausgabe des gefundenen <Value> Elementes -->
              <xsl:if test="string-length(normalize-space(text()))=1">
                <xsl:text>0</xsl:text>
              </xsl:if>
              <xsl:value-of select="normalize-space(text())"/>
              <xsl:text>/</xsl:text>

            </xsl:when>
            <xsl:when test="$p_vvl='Year'">

              <!-- Formatierte Ausgabe des gefundenen <Value> Elementes -->
              <xsl:value-of select="substring(normalize-space(text()),3,2)"/>

            </xsl:when>
            <xsl:otherwise>

              <!-- Unmittelbare Ausgabe des gefundenen <Value> Elementes -->
              <xsl:value-of select="text()"/>
              <xsl:call-template name="t_bl"/>

            </xsl:otherwise>
          </xsl:choose>

        </xsl:for-each>

        <!--
          Spezialfall f�r <Property name="UserProperty">
                                              <Value name="Value"> ...

          Obwohl gegebenes <Property> Element bereits gesuchte <Value>
          Elemente enth�lt, suche weiter nach oben nach allen weiteren
          <Property name="UserProperty"> <Value name="Value"> Elementen
        -->
        <xsl:if test="$p_nvl='UserProperty' and $p_vvl='Value'">
            <xsl:if test="count($p_lvl/..) &gt; 0">

            <xsl:call-template name="t_propsValsResolve">
              <xsl:with-param name="p_att" select="'type'"/>
              <xsl:with-param name="p_avl" select="@type"/>
              <xsl:with-param name="p_nvl" select="$p_nvl"/>
              <xsl:with-param name="p_vvl" select="$p_vvl"/>
              <xsl:with-param name="p_lvl" select="$p_lvl/../parent::node()"/>
            </xsl:call-template>

          </xsl:if>
        </xsl:if>
      </xsl:when>

      <!-- Gegebenes <Property> Element enth�lt keine gesuchten <Value> Elemente -->
      <xsl:otherwise>
        <xsl:if test="count($p_lvl) &gt; 0">

          <xsl:call-template name="t_propsValsResolve">
            <xsl:with-param name="p_att" select="'type'"/>
            <xsl:with-param name="p_avl" select="@type"/>
            <xsl:with-param name="p_nvl" select="$p_nvl"/>
            <xsl:with-param name="p_vvl" select="$p_vvl"/>
            <xsl:with-param name="p_lvl" select="$p_lvl/parent::node()"/>
          </xsl:call-template>

        </xsl:if>
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>



  <!-- Gibt Text oder Abbildung des Textes aus -->
  <xsl:template name="t_mapPropValContent">
    <xsl:param name="p_nvl"/> <!-- Wert des <Property> Element Attributes name -->
    <xsl:param name="p_vtx"/> <!-- Text des auszugebenden <Value> Elementes-->

    <xsl:choose>

      <xsl:when test="normalize-space($p_nvl)='Action'">
        <xsl:choose>
          <xsl:when test="normalize-space($p_vtx)='delete'"> <xsl:text>0</xsl:text> </xsl:when>
          <xsl:when test="normalize-space($p_vtx)='add'">    <xsl:text>1</xsl:text> </xsl:when>
          <xsl:when test="normalize-space($p_vtx)='modify'"> <xsl:text>2</xsl:text> </xsl:when>
          <xsl:otherwise>
            <xsl:text>*** ALMA error: Action is neither "delete", "add" nor "modify" but "</xsl:text>
            <xsl:value-of select="$p_vtx"/>
            <xsl:text>" ***</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>

      <xsl:when test="normalize-space($p_nvl)='Font'">
        <xsl:choose>
          <xsl:when test="normalize-space($p_vtx)='TEXT1'">  <xsl:text>0</xsl:text> </xsl:when>
          <xsl:when test="normalize-space($p_vtx)='TEXT21'"> <xsl:text>1</xsl:text> </xsl:when>
          <xsl:when test="normalize-space($p_vtx)='TEXT22'"> <xsl:text>2</xsl:text> </xsl:when>
          <xsl:otherwise>
            <xsl:text>*** ALMA error: Font is neither "TEXT1", "TEXT21" nor "TEXT22" but "</xsl:text>
            <xsl:value-of select="$p_vtx"/>
            <xsl:text>" ***</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>

      <xsl:when test="normalize-space($p_nvl)='KerfCompensation'">
        <xsl:choose>
          <xsl:when test="normalize-space($p_vtx)='no'">            <xsl:text>-1</xsl:text> </xsl:when>
          <xsl:when test="normalize-space($p_vtx)='marking'">            <xsl:text>0</xsl:text> </xsl:when>
          <xsl:when test="normalize-space($p_vtx)='lefthandside'">  <xsl:text>1</xsl:text> </xsl:when>
          <xsl:when test="normalize-space($p_vtx)='righthandside'"> <xsl:text>2</xsl:text> </xsl:when>
          <xsl:otherwise>
            <xsl:text>*** ALMA error: KerfCompensation is neither "no", "lefthandside" nor "righthandside" but "</xsl:text>
            <xsl:value-of select="$p_vtx"/>
            <xsl:text>" ***</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>

      <xsl:when test="normalize-space($p_nvl)='NibblingTool'">
        <xsl:choose>
          <xsl:when test="normalize-space($p_vtx)='no'"> <xsl:text>0</xsl:text> </xsl:when>
           <xsl:otherwise>
             <xsl:text>*** ALMA error: NibblingTool is not "no", but "</xsl:text>
            <xsl:value-of select="$p_vtx"/>
            <xsl:text>" ***</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>

      <xsl:when test="normalize-space($p_nvl)='Operation'">
        <xsl:choose>
          <xsl:when test="normalize-space($p_vtx)='cut'">   <xsl:text>0</xsl:text> </xsl:when>
          <xsl:when test="normalize-space($p_vtx)='mark'">  <xsl:text>1</xsl:text> </xsl:when>
          <xsl:when test="normalize-space($p_vtx)='punch'"> <xsl:text>2</xsl:text> </xsl:when>
          <xsl:otherwise>
            <xsl:text>*** ALMA error: Operation is neither "cut", "mark" nor "punch" but "</xsl:text>
            <xsl:value-of select="$p_vtx"/>
            <xsl:text>" ***</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>

      <xsl:when test="normalize-space($p_nvl)='Tool'">
        <xsl:choose>
          <xsl:when test="normalize-space($p_vtx)='unspecified'">     <xsl:text>0</xsl:text> </xsl:when>
          <xsl:when test="normalize-space($p_vtx)='Torch'">     <xsl:text>1</xsl:text> </xsl:when>
          <xsl:when test="normalize-space($p_vtx)='Bevel'">     <xsl:text>29</xsl:text> </xsl:when>
          <xsl:when test="normalize-space($p_vtx)='Marking'">   <xsl:text>0</xsl:text> </xsl:when>
          <xsl:when test="normalize-space($p_vtx)='round'">     <xsl:text>51</xsl:text> </xsl:when>
          <xsl:when test="normalize-space($p_vtx)='rectangle'"> <xsl:text>52</xsl:text> </xsl:when>
          <xsl:when test="normalize-space($p_vtx)='oblong'">    <xsl:text>53</xsl:text> </xsl:when>
          <xsl:when test="normalize-space($p_vtx)='special'">   <xsl:text>58</xsl:text> </xsl:when>
          <xsl:otherwise>
            <xsl:text>*** ALMA error: Tool is neither "Torch", "Bevel", "Marking", "round", "rectangle", "oblong" nor "special" but "</xsl:text>
            <xsl:value-of select="$p_vtx"/>
            <xsl:text>" ***</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>

      <xsl:when test="normalize-space($p_nvl)='Unit'">
        <xsl:choose>
          <xsl:when test="normalize-space($p_vtx)='mm'"> <xsl:text>0</xsl:text> </xsl:when>
          <xsl:when test="normalize-space($p_vtx)='in'"> <xsl:text>1</xsl:text> </xsl:when>
          <xsl:otherwise>
            <xsl:text>*** ALMA error: Unit is neither "mm" nor "in" but "</xsl:text>
            <xsl:value-of select="$p_vtx"/>
            <xsl:text>" ***</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>

      <xsl:otherwise>
        <xsl:value-of select="$p_vtx"/>
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>



</xsl:transform>


