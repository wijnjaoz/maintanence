/*HEAD ModlFeatureJA_SimplExBuilder CXX MODL */
/*==================================================================================================

                    Copyright (c) 2007 UGS Corp.
                   Unpublished - All rights reserved

====================================================================================================
File description:

====================================================================================================
   Date           Name                    Description of Change
Vinit Shukla                           Create Parasolid Swept Body   
$HISTORY$
==================================================================================================*/

#include <unidefs.h>
//#include <schema/SC_loop.hxx>
#include <schema/SC_section.hxx>
#include <modl.h>
#include <px.h>

#include <exp.h>
#include <units_interface.h>
#include <ugc_error_codes.h>
using namespace UGS;

/*  Create the swept Parasolid body for simple extrude
*/       
static int CreateParasolidSweptBody(SC_section *section,
                     double distance,
                     VEC3_t *direction, // unit vector
                     PK_BODY_t *body)
{
    int error = CM_NO_ERRORS;
    /* If section is NULL, we cant extrude, return*/
    if(section == NULL)
        return CM_CONT_INVALID_SECTION;

    int numSectionCurves = 0;
    tag_t * sectionCurves = NULL;
    /* Get the output curves of the section*/
    section->ask_output_entities(&numSectionCurves, &sectionCurves);

    /* Now we need to create parasolid curve from UG curve.*/
    PK_CURVE_t *parasolidCurves = NULL;
    parasolidCurves = (PK_CURVE_t *)SM_alloc(numSectionCurves * sizeof(PK_CURVE_t));
    PK_INTERVAL_t *curveIntervals = NULL;
    curveIntervals = (PK_INTERVAL_t *)SM_alloc(numSectionCurves * sizeof(PK_INTERVAL_t));

    for(int i = 0; i < numSectionCurves; i++)
    {
        PK_CURVE_t parasolidCurve = PK_ENTITY_null;
        /* Create parasolid curve*/
        FTN(pxcutc)(&sectionCurves[i], &parasolidCurve, &error);
        parasolidCurves[i] = parasolidCurve;

        /* Also find out the interval of each curve, we need it call to PK_CURVE_make_wire_body_2*/
        PK_INTERVAL_t curveInterval = {0.0,0.0};
        PK_CURVE_ask_interval(parasolidCurve, &curveInterval);
        curveIntervals[i].value[0] = curveInterval.value[0];
        curveIntervals[i].value[1] = curveInterval.value[1];
    }

    SM_free(sectionCurves);
    sectionCurves = NULL;
    /*  (3) Make a wire body that represents the profile to be swept. 

    We shall later call PK_BODY_sweep() to create the sheet body for this feature
    */
    PK_CURVE_make_wire_body_o_t wireOptions;
    PK_CURVE_make_wire_body_o_m ( wireOptions );

    /* PK functions always do an ERROR_raise() ( equivalent to a C longjmp() )
    Hence we always ERROR PROTECT calls to PK functions that can return an
    error
    */
    //PK_BODY_t body = PK_ENTITY_null;
    int n_new_edges = 0;  //number of new edges
    PK_EDGE_t *new_edges = NULL; //new edges
    int *edge_index = NULL;   //pos in original array

    ERROR_PROTECT   
    {
        PK_CURVE_make_wire_body_2 ( numSectionCurves, parasolidCurves, curveIntervals,
        &wireOptions, body, &n_new_edges, &new_edges, &edge_index);
    }
    ERROR_RECOVER
    {
        SM_free(edge_index);
        edge_index = NULL;

        SM_free(new_edges);
        new_edges = NULL;
        /* we have to offset Parasolid error codes from PK functions by ERROR_KI_base */
        int error = ERROR_ask_failure_code()-ERROR_KI_base;
        if (error == CM_PROFILE_STATUS_MULTI_LOOPS ||
            error == CM_PROFILE_STATUS_INTERSECT   ||
            error == CM_NON_MANIFOLD )
        {
            ERROR_acknowledge_handled();
        }
        else
        {
            ERROR_reraise();
        }
    }
    ERROR_END

    SM_free(edge_index);
    edge_index = NULL;
    SM_free(new_edges);
    new_edges = NULL;

    if (error != CM_NO_ERRORS)
    {        
        return error;
    }
    /*  (1) Get the conversion factor to convert from UG to Parasolid units UG.

    Units of Parasolid entities are in metres. Therefore if we want 
    say a block of 1" height, we have to convert 1" to metres before we
    call the Parasolid function to create the Parasolid entity.
    */
    double ps_to_ug = 0.0, ug_to_ps = 0;
    PART_ask_ps_scale_factors ( OM_ask_object_tag(section), &ps_to_ug, &ug_to_ps);

    /* (2) Instantiate the vector along which these curve will be swept.
    */
    PK_VECTOR_t path;
    path.coord[0] = VEC3_X(direction) * distance * ug_to_ps;
    path.coord[1] = VEC3_Y(direction) * distance * ug_to_ps;
    path.coord[2] = VEC3_Z(direction) * distance * ug_to_ps;

    /*  (5) Sweep the wire body along the vector to produce the body for this feature
    */
    PK_EDGE_t *base_edges = NULL;
    int n_lateral_faces = 0;
    PK_FACE_t *lateral_faces   = NULL;
    ERROR_PROTECT
    {        
        PK_local_check_t results;
        PK_BODY_sweep (*body, path, PK_LOGICAL_false, &n_lateral_faces, &lateral_faces, &base_edges, &results);
    }
    ERROR_RECOVER
    {
        SM_free(lateral_faces); 
        lateral_faces = NULL;            

        SM_free(base_edges);
        base_edges = NULL;
        error = ERROR_ask_failure_code();
        if (error == CM_CREATE_SWEEP_ERROR ||
            error == CM_ERR_STATE_RTOFSX   ||
            error == CM_NON_MANIFOLD )
        {
            ERROR_acknowledge_handled();
        }
        else
        {
            ERROR_reraise();
        }
    }
    ERROR_END

        SM_free(lateral_faces); 
    lateral_faces = NULL;            

    SM_free(base_edges);
    base_edges = NULL;
    return error;
}
