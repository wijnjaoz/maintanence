//HEAD CodeGenerator CXX CODE_GENERATOR
/*==================================================================================================

                        Copyright (c) 2007 UGS Corp.
                     Unpublished - All rights reserved

====================================================================================================
File description:


====================================================================================================
   Date      Name                    Description of Change
09-Feb-2007  Vincent Lucas           Initial creation
09-Feb-2007  Vincent Lucas           Fix SUN compile errors
21-Feb-2007  Vincent Lucas           Add special function capability
                                     Allow xml files to be located in multiple directories and use a path to search for them
22-Feb-2007  Vincent Lucas           Add Reusable Block as block type
23-Feb-2007  Vincent Lucas           Improved input error processing
16-Mar-2007  Vincent Lucas           For SetList, add API List Element Type as code generator input
02-Apr-2007  Vincent Lucas           Improve code gemeration for Type and Enumeration
30-Apr-2007  Vincent Lucas           Skip invalid directories in the xmlpath
02-May-2007  Vincent Lucas           Permit nested JA namespaces
02-May-2007  Vincent Lucas           Print JA defaults
04-Jun-2007  Vincent Lucas           Code generation of Expression Dimensionality property
11-Jun-2007  Vincent Lucas           Selection block name changed
27-Jun-2007  Vincent Lucas           Fix problem that IP delta not applied to group
25-Jul-2007  Vincent Lucas           Port to linux
26-Jul-2007  Vincent Lucas           Add code generation for state manager
30-Jul-2007  Vincent Lucas           Separate default values for inch and metric parts
31-Aug-2007  Vincent Lucas           Use camel case in JA function names
13-Aug-2008  Vincent Lucas           Make ostreamstream<XMLCh> work on Linux
01-Apr-2010  Santosh Naidu           6302819: Release method causing heap corrution
$HISTORY$
==================================================================================================*/

// NOTES: This library is designed to be independent of other NX libraries to the maximum extent
// possible, so that the library could also be used in a stand-alone executable.
//    - std::runtime_error is used instead of ERROR_raise to keep this library independent, and
//    also ERROR_raise is too slow because it gets a stack trace.  I don't want to get a stack trace.

#include <unidefs.h>
#include <CodeGenerator.hxx>
#include <CodeGenerator_FileOps.hxx>
#define USE_XERCES_STRINGHELPER_X_MACRO 1
#include <XercesC_2_6_StringHelper.hxx>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/regx/RegularExpression.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <string>
#include <functional>
#include <algorithm>
#include <sstream>
#include <vector>
#include <map>
#include <list>
#include <fstream>
#include <stdexcept>
#include <time.h>

using namespace xercesc_2_6;
using namespace UGS::XercesC_2_6::StringHelper;

static bool inDEBUG = false;

namespace UGS {
    namespace CodeGenerator {

        //using UGS::XercesC_2_6::StringHelper::Replace;
        void FormatString( stringl& newText, const stringl& format );
        stringl ConvertToLowerCaseWithUnderscores(const stringl& s);
        stringl ConvertToUpperCaseWithUnderscores(const stringl& s);
        void ReleseString(XMLCh* xmlText);
        namespace Path {
            std::string Join( const std::string& x, const std::string& y );
            std::vector<std::string> SplitExt( const std::string& name );
            std::string BaseName( const std::string& name );
            std::vector<std::string> SplitPath( const std::string& path );
        }
        std::string SearchForFileUsingPath( const std::string& fileName, const std::string& path );
        DOMNodeList* GetElementsByTagName(DOMNode* node, const XMLCh* tagName);
        stringl ExtractText(DOMNode* node);
        /*inline void Replace(stringl& s, const wchar_t* pattern, const wchar_t* substitution)
        {
            UGS::XercesC_2_6::StringHelper::Replace(s, pattern, substitution);
        }
        inline void Replace(stringl& s, XMLCh pattern, XMLCh substitution)
        {
            UGS::XercesC_2_6::StringHelper::Replace(s, pattern, substitution);
        }
        inline void Replace(stringl& s, const XMLCh* pattern, const XMLCh* substitution)
        {
            UGS::XercesC_2_6::StringHelper::Replace(s, pattern, substitution);
        }*/
        void Replace1(DOMDocument* doc, DOMNode *node, const XMLCh* tagName, const XMLCh* newTextIn );
        void ReplaceNumeric(DOMDocument* doc, DOMElement* node, 
            const XMLCh* tagName, int newNumber);
        void Unquote(stringl& text);
        void RemoveIndent(stringl& text);
        void AddIndent(stringl& text, const XMLCh* indent);
        stringl ExtractText(DOMNode* node);
        stringl GetBodyIndent(const DOMNode* propertyNode);
        void AddAttribute( DOMDocument* ownerDocument, DOMNamedNodeMap* attributes, const XMLCh* name, const XMLCh* value );
        void AddAttribute( DOMNamedNodeMap* attributes, const XMLCh* name, const XMLCh* value );
        DOMDocumentFragment* NewDocumentFragment(DOMDocument* doc, const DOMNodeList* nodeList);
        void EvaluateDoc( DOMNode* doc, DOMNamedNodeMap* attributes );
        DOMDocument* ImportXMLFile( const char* xml_file, const char* import_path, DOMNamedNodeMap* attributes );
        stringl MakeFQName( const stringl& x, const stringl& y, const stringl& joiner );
        stringl MakeFQName( const stringl& x, const stringl& y, XMLCh joiner );
        stringl GetYear();
    }
}

namespace UGS {
    namespace CodeGenerator {
        template <class T, class X>
        class DOMIterator
        {
        public:
            explicit DOMIterator(T& container) : m_container(&container), m_index(0) {}
            bool hasNext() const { return m_index < m_container->getLength(); }
            DOMIterator& operator++() { ++m_index; return *this; }
            DOMIterator operator++(int) { return ++DOMIterator(*this); }
            X operator*() { return m_container->item(m_index); }
            DOMIterator(const DOMIterator& d) : m_container(d.m_container), m_index(d.m_index) {}
        private:
            T* m_container;
            XMLSize_t m_index;
            DOMIterator(); // for qaz
        };

        typedef DOMIterator<DOMNamedNodeMap, DOMNode*> DOMNamedNodeMapIterator;
        typedef DOMIterator<DOMNodeList, DOMNode*> DOMNodeListIterator;

        
        void ReleseString(XMLCh* xmlText) 
        {
            /* 01-Apr-2010  Santosh Naidu           6302819 
               Appears that freeware, Xerces, is causing issue when string is released. 
               For now I am allowing the object to leak untill we move to latest version 
               of xerces. I know this is not the best solution but considering time/effort
               required to correctly fix this I think it is better to allow developer/PD 
               to generate internal code for now.
               
               Note that this library (or file) is only for internal 
               use and not shipped to the customer.               
             */
            //XMLString::release(&xmlText);

            return;
        }

        stringl ConvertToLowerCaseWithUnderscores(const stringl& s)
        {
            static RegularExpression regex(X("([a-z0-9])([A-Z])"));
            XMLCh *result = regex.replace(s.c_str(), X("$1_$2"));
            XMLString::lowerCase(result);
            stringl retV(result);
            ReleseString(result);
            return retV;
        }
        stringl ConvertToUpperCaseWithUnderscores(const stringl& s)
        {
            static RegularExpression regex(X("([a-z0-9])([A-Z])"));
            XMLCh *result = regex.replace(s.c_str(), X("$1_$2"));
            XMLString::upperCase(result);
            stringl retV(result);
            ReleseString(result);
            return retV;
        }


        namespace Path
        {
            const char DIRSEP = 
#ifdef WNT
                    '\\';
#else
                    '/';
#endif
            const char PATHSEP = 
#ifdef WNT
                    ';';
#else
                    ':';
#endif
            std::string Join( const std::string& x, const std::string& y )
            {
                return x == "" ? y : (x[x.length()-1] == DIRSEP ? x + y : x + DIRSEP + y);
            }
            std::vector<std::string> SplitPath( const std::string& path )
            {
                std::vector<std::string> result;
                Split(path, PATHSEP, result);
                return result;
            }
            std::vector<std::string> SplitExt( const std::string& name )
            {
                std::vector<std::string> result(2);
                size_t idx = name.find_last_of('.');
                if ( idx != std::string::npos )
                {
                    result[0] = name.substr(0, idx);
                    result[1] = name.substr(idx+1);
                }
                else
                {
                    result[0] = name;
                    result[1] = "";
                }
                return result;
            }
            std::string BaseName( const std::string& name )
            {
                size_t idx = name.rfind(DIRSEP);
                if ( idx != std::string::npos )
                    return name.substr(idx+1);
                return name;
            }
        }
    }
}

namespace UGS {
namespace CodeGenerator
{
    DOMNodeList* GetElementsByTagName(DOMNode* node, const XMLCh* tagName)
    {
        if ( dynamic_cast<DOMElement*>(node) )
            return dynamic_cast<DOMElement*>(node)->getElementsByTagName(tagName);
        else if ( dynamic_cast<DOMDocument*>(node) )
            return dynamic_cast<DOMDocument*>(node)->getElementsByTagName(tagName);

        throw std::runtime_error("Unexpected type in GetElementsByTagName");

    }
    void FormatString( stringl& newText, const stringl& format )
    {
        if ( format == X("lower") )
            MakeLowerCase(newText);
        else if ( format == X("upper") )
            MakeUpperCase(newText);
        else if ( format == X("lowerWithUnderscores") )
            newText = ConvertToLowerCaseWithUnderscores(newText);
        else if ( format == X("upperWithUnderscores") )
            newText = ConvertToUpperCaseWithUnderscores(newText);
        else if ( format == X("lowerWithSpaces") )
        {
            newText = ConvertToLowerCaseWithUnderscores(newText);
            Replace(newText, XMLCh('_'), XMLCh(' '));
        }
        else if ( format == X("replaceDotsWithUnderscores") )
            Replace(newText, XMLCh('.'), XMLCh('_'));
        else if ( format == X("replaceScopeWithUnderscore") )
            Replace(newText, X("::"), X("_"));
        else if ( format == X("replaceSpaceWithUnderscore") )
            Replace(newText, XMLCh(' '), XMLCh('_'));
        else if ( format == X("removeEverythingAfterDot") )
        {
            size_t pos = newText.find_first_of('.');
            if ( pos != stringl::npos )
                newText.erase(pos);
        }
        else if ( format == X("constType") )
        {
            if ( EndsWith(newText, X("_p_t")) )
                newText.replace(newText.size()-4, 5, X("_pc_t"));
            if ( EndsWith(newText, X("*")) )
                newText.insert(0, X("const "));
        }
        else
            throw std::runtime_error((std::string("Unrecognized format ") + XMLString::transcode(format.c_str())).c_str());
    }
    void Replace1(DOMDocument* doc, DOMNode *node, const XMLCh* tagName, const XMLCh* newTextIn )
    {
        DOMNodeList* elements = GetElementsByTagName(node, tagName);
        while ( elements->getLength() > 0 )
//        for ( DOMNodeListIterator iter(*elements); iter.hasNext(); ++iter )
        {
            DOMElement *anElement = dynamic_cast<DOMElement*>(elements->item(0));
//            DOMElement *anElement = dynamic_cast<DOMElement*>(*iter);
            stringl newText(newTextIn);
            if ( anElement->getAttributeNode(X("format")) )
            {
                stringl format = anElement->getAttribute(X("format"));
                FormatString( newText, format );
            }
            if ( anElement->getAttributeNode(X("truncate")) )
            {
                int maxlen = ToInt(anElement->getAttribute(X("truncate")));
                if ( newText.size() > maxlen )
                    newText.erase(maxlen);
            }
            stringl docText;
            if ( inDEBUG )
                docText = ExtractText(doc);
            DOMText* newTextElement = doc->createTextNode(newText.c_str());
            anElement->getParentNode()->replaceChild(newTextElement, anElement);
            if ( inDEBUG )
                docText = ExtractText(doc);
        }
    }

    void ReplaceNumeric(DOMDocument* doc, DOMElement* node, 
        const XMLCh* tagName, int newNumber)
    {
         DOMNodeList *elements = node->getElementsByTagName(tagName);
         while ( elements->getLength() > 0 )
         //for ( DOMNodeListIterator iter(*elements); iter.hasNext(); ++iter )
         {
             DOMElement *anElement = dynamic_cast<DOMElement*>(elements->item(0));
             int x = newNumber;
             if ( anElement->getAttributeNode(X("multiplier")) )
                x = x * ToInt(anElement->getAttribute(X("multiplier")));
             if ( anElement->getAttributeNode(X("offset")) )
                x = x + ToInt(anElement->getAttribute(X("offset")));
             ostringstreamXMLCh sout;
             sout << x;
             DOMText* newTextElement = doc->createTextNode(sout.str().c_str());
             anElement->getParentNode()->replaceChild(newTextElement, anElement);
         }
    }
    void Unquote(stringl& text)
    {
        Replace(text, X("&lt;"), X("<"));
        Replace(text, X("&gt;"), X(">"));
        Replace(text, X("&quot;"), X("\""));
        Replace(text, X("&amp;"), X("&")); // Must be last to avoid amgiguous cases
    }

    void RemoveIndent(stringl& text)
    {
        if ( text[0] == '\n' )
        {
            size_t idx = 1;
            while ( ::iswspace(text[idx]) )
                ++idx;
            stringl indent = text.substr(0, idx);
            Replace( text, indent.c_str(), X("\n") );
        }
    }

    void AddIndent(stringl& text, const XMLCh* indent)
    {
        Replace(text, X("\n"), (stringl(X("\n")) + indent).c_str());
        if ( text[0] != '\n' )
            text.insert(0, indent);
    }

    stringl ExtractText(DOMNode* node)
    {
        DOMDocument *owner = node->getOwnerDocument();
        if ( !owner )
            owner = dynamic_cast<DOMDocument*>(node);
        DOMImplementation *impl = owner->getImplementation();
        DOMWriter* writer = impl->createDOMWriter();
        XMLCh* text = writer->writeToString(*node);
        RegularExpression regex(X("<!---[^>]*>"));
        text = regex.replace(text, X(""));
        stringl textl(text);
        ReleseString(text);
        size_t itmp = textl.find('>') + 1;
        textl = textl.substr( itmp, textl.rfind('<') - itmp );
        Unquote(textl);
        return textl;
    }

    stringl GetBodyIndent(const DOMNode* propertyNode)
    {
        const DOMNode* bodyNode = propertyNode->getParentNode();
        while ( stringl(bodyNode->getNodeName()) != X("body") )
            bodyNode = bodyNode->getParentNode();
        stringl text = bodyNode->getFirstChild()->getNodeValue(); // FIXME: Correct?
        if ( text[0] == '\n' )
        {
            return text.substr(1, text.find_first_not_of(' ', 1)-1 );
        }
        return stringl(X(""));
    }

    DOMDocumentFragment* NewDocumentFragment(DOMDocument* doc, const DOMNodeList* nodeList)
    {
        DOMDocumentFragment *docFrag = doc->createDocumentFragment();
        for ( size_t idx = 0; idx < nodeList->getLength(); ++idx )
        {
            if ( doc == nodeList->item(idx)->getOwnerDocument() )
                docFrag->appendChild(nodeList->item(idx)->cloneNode(true));
            else
                docFrag->appendChild( doc->importNode(nodeList->item(idx), true) );
        }
        return docFrag;
    }

    void EvaluateDoc( DOMNode* doc, DOMNamedNodeMap* attributes )
    {
        DOMDocument* ownerDoc = doc->getOwnerDocument();
        if ( !ownerDoc )
            ownerDoc = &dynamic_cast<DOMDocument&>(*doc);

        stringl undefinedAttrs = X("^$");
        if ( attributes && attributes->getNamedItem(X("__UNDEF")) )
            undefinedAttrs = dynamic_cast<DOMAttr*>(attributes->getNamedItem(X("__UNDEF")))->getValue();
        RegularExpression undefinedAttrsRegex(undefinedAttrs.c_str());
        DOMNodeList *ifNodes = GetElementsByTagName(doc, X("if"));
        size_t curIfNode = 0;
        while ( ifNodes->getLength() > curIfNode )
        {
            //if ( !ifNodes || ifNodes->getLength() == 0 )
            //    break;
            DOMElement *ifNode = static_cast<DOMElement*>(ifNodes->item(curIfNode));
            stringl ifType = ifNode->getAttributes()->item(0)->getNodeName();
            if ( ifType != X("true") && ifType != X("false") && 
                ifType != X("def") && ifType != X("ndef") )
                throw std::runtime_error("Attribute for if node must be true, false, def, or ndef");
            stringl ifValue = ifNode->getAttributes()->item(0)->getNodeValue();
            if ( undefinedAttrsRegex.matches( ifValue.c_str() ) )
            {
                ++curIfNode;
                continue;
            }
            bool passes = false;
            if ( ifType == X("true") || ifType == X("false") )
                passes = ( ifType == attributes->getNamedItem(ifValue.c_str())->getNodeValue() );
            else
            {
                if ( attributes != 0 && attributes->getNamedItem(ifValue.c_str()) != 0 )
                    passes = true;
                if ( ifType == X("ndef") )
                    passes = !passes;
            }

            if ( passes )
            {
                DOMDocumentFragment *frag = NewDocumentFragment( ownerDoc, ifNode->getChildNodes() );
                ifNode->getParentNode()->replaceChild( frag, ifNode );
            }
            else
                ifNode->getParentNode()->removeChild(ifNode);
            
        } // end for(;;)

        if ( attributes )
        {
            for ( size_t attrIdx = 0; attrIdx < attributes->getLength(); ++attrIdx )
            {
                DOMAttr* attr = static_cast<DOMAttr*>(attributes->item(attrIdx) );
                if ( stringl(attr->getName()) != X("file") && !undefinedAttrsRegex.matches( attr->getName() ) )
                    Replace1( ownerDoc, doc, attr->getName(), attr->getValue() );
            }
        }
    }

    std::string SearchForFileUsingPath( const std::string& fileName, const std::string& path )
    {
        std::vector<std::string> dirs = Path::SplitPath( path );
        for ( size_t kk=0; kk<dirs.size(); ++kk )
        {
            std::string file = Path::Join( dirs[kk], fileName );
            if ( FileOps::FileExists(file.c_str() ) )
                return file;
        }
        return "";
    }

    DOMDocument* ImportXMLFile( const char* xml_file, const char* import_path, DOMNamedNodeMap* attributes )
    {
        static XercesDOMParser parser;
        parser.parse(xml_file);
        DOMDocument *doc = parser.getDocument();
        DOMNodeList *parameterNodes = doc->getElementsByTagName(X("parameters"));
        if ( parameterNodes->getLength() > 1 )
            throw std::runtime_error((std::string("Exception in ") + xml_file + "\nMore than one parameters node").c_str());
        if ( parameterNodes->getLength() == 1 )
        {
            for ( DOMNodeListIterator paramNodeIter(*parameterNodes->item(0)->getChildNodes()); 
                paramNodeIter.hasNext(); ++paramNodeIter )
            {
                DOMElement* paramNode = static_cast<DOMElement*>(*paramNodeIter);
                if ( paramNode->getNodeType() == DOMNode::TEXT_NODE )
                    continue;
                if ( stringl(paramNode->getNodeName()) != X("param") )
                    throw std::runtime_error("Only param nodes are permitted inside a parameters node");
                if ( paramNode->getAttributeNode( X("name") ) == 0 )
                    throw std::runtime_error("Param node must have a name");
                stringl paramName = paramNode->getAttribute( X("name") );
                if ( paramNode->getAttributeNode(X("default")) != 0 )
                {
                    if ( attributes->getNamedItem(paramName.c_str()) == 0 )
                    {
                        DOMDocument *attrDoc = (attributes->getLength() > 0) ? attributes->item(0)->getOwnerDocument() : doc;
                       DOMAttr *attr = attrDoc->createAttribute(paramName.c_str());
                       stringl newValue = paramNode->getAttribute(X("default"));
                       if ( newValue[0] == '@' )
                           newValue = attributes->getNamedItem( newValue.c_str() + 1 )->getNodeValue();
                       attr->setValue( newValue.c_str() );
                       attributes->setNamedItem(attr);
                    }
                }
                else
                {
                    if ( attributes->getNamedItem(paramName.c_str()) == 0 &&
                         stringl(paramNode->getAttribute(X("optional"))) != X("true") ) 
                         throw std::runtime_error((std::string("Missing parameter\n   ") + 
                         XMLString::transcode(paramName.c_str()) + " is a required parameter to " + xml_file + 
                            " but it has not been specified").c_str());
                }
                       
                if ( paramNode->getAttributeNode(X("format")) != 0 )
                {
                    RegularExpression regex( paramNode->getAttribute(X("format")) );
                    if ( !regex.matches( attributes->getNamedItem(paramName.c_str())->getNodeValue() ) )
                        throw std::runtime_error((std::string("Parameter ") + XMLString::transcode(paramName.c_str()) + " does not have the required format\n"
                                       "Parameter value is " + XMLString::transcode(attributes->getNamedItem(paramName.c_str())->getNodeValue()) + "\n"
                                       "The parameter is required to match the regular expression \"" + XMLString::transcode(paramNode->getAttribute(X("format"))) + "\"").c_str());
                }
    
            }
        }

        DOMNodeList *aImportNodeList = doc->getElementsByTagName(X("import"));
        while ( aImportNodeList->getLength() > 0 )
//        for ( DOMNodeListIterator iter = DOMNodeListIterator(*doc->getElementsByTagName(X("import")));
//            iter.hasNext(); ++iter )
        {
            DOMElement *aImport = dynamic_cast<DOMElement*>(aImportNodeList->item(0));
            for ( size_t attrIdx = 0; attrIdx < attributes->getLength(); ++attrIdx )
            {
                DOMAttr* attr = static_cast<DOMAttr*>(attributes->item(attrIdx));
                if ( stringl(attr->getName()) != X("file") )
                    AddAttribute(doc, aImport->getAttributes(), attr->getName(), attr->getValue());
                    //aImport->getAttributes()->setNamedItem(attributes->item(attrIdx));
            }

            DOMDocument* subdoc = NULL;
            
            //try
            //{
                std::string importFileFullName = SearchForFileUsingPath( Transcode(aImport->getAttribute(X("file"))), import_path );
                if ( importFileFullName == "" )
                    throw std::runtime_error( (std::string(xml_file) + " attempted to import file named " 
                        + Transcode(aImport->getAttribute(X("file"))) + " but the file was not found").c_str() );
                subdoc = 
                    ImportXMLFile(  importFileFullName.c_str(), 
                                    import_path, aImport->getAttributes() );
            //}
            //catch (...)
            //{
            //}
            for ( DOMNodeListIterator iter = DOMNodeListIterator(*doc->getElementsByTagName(X("construct")));
                iter.hasNext(); ++iter )
            {
                DOMElement *aConstructs = dynamic_cast<DOMElement*>(*iter);
                for ( DOMNodeListIterator iter2 = DOMNodeListIterator(*subdoc->getElementsByTagName(X("construct")));
                    iter2.hasNext();  )
                {
                    DOMElement *bConstructs = dynamic_cast<DOMElement*>(*iter2);
                    if ( stringl(aConstructs->getAttribute(X("name"))) == bConstructs->getAttribute(X("name")) )
                    {
                        DOMNodeList* supers = aConstructs->getElementsByTagName(X("SUPER"));
                        if ( supers->getLength() > 1 )
                            throw std::runtime_error("There can only be one SUPER tag");
                        if ( supers->getLength() > 0 )
                            supers->item(0)->getParentNode()->replaceChild( 
                                NewDocumentFragment( doc, bConstructs->getChildNodes() ), supers->item(0) );
                        bConstructs->getParentNode()->removeChild( bConstructs );
                    }
                    else
                        ++iter2;
                }
            }

            DOMDocumentFragment *importDocFrag = 
                NewDocumentFragment( doc, subdoc->getDocumentElement()->getChildNodes() );
            aImport->getParentNode()->replaceChild( importDocFrag, aImport );
        } // for each aImport

        EvaluateDoc( doc, attributes );

        return doc;
    }

    class PropertyType
    {
    private:
        std::string m_xml_file, m_import_path;
        stringl m_name;
        DOMDocument *m_doc;
        DOMNamedNodeMap *m_attributes;
    public:
        PropertyType( const char* xml_file, const char* import_path, DOMNamedNodeMap* attributes ) :
          m_xml_file(xml_file), m_import_path(import_path), m_name(), m_doc(NULL), m_attributes(attributes)
        {
            std::string name = Path::SplitExt( Path::BaseName(m_xml_file) )[0];
            m_name = Transcode(name.c_str());
        }

        const stringl& Name() const { return m_name; }
        DOMDocument* Doc() const { return m_doc; }

        DOMElement* GetConstruct(const stringl& name)
        {
            if ( m_doc == NULL )
                m_doc = ImportXMLFile( m_xml_file.c_str(), m_import_path.c_str(), m_attributes );
            for ( DOMNodeListIterator iter = DOMNodeListIterator(*m_doc->getElementsByTagName(X("construct")) );
                iter.hasNext(); ++iter )
            {
                DOMElement *construct = dynamic_cast<DOMElement*>(*iter);
                if ( stringl(construct->getAttribute(X("name"))) == name )
                    return construct;
            }
            return NULL;
        }
    private:
        PropertyType(); // for qaz
    };

}
}

using std::string;

namespace UGS {
namespace CodeGenerator
{
    struct RunnerImplInput
    {
        RunnerImplInput() : builderLibrary(), uiclassLibrary(), license(), 
           componentNS(), component(), componentSuperclass(), uicompNS(),
           uicompBasename(), persistentDataJASuperclassNS(), builderSuperclass(),
           persistentDataJAClass(), persistentDataRecordClass(), persistentDataJASuperclass(),
           omComponentNS(), omComponentBasename(), topUIComponent(), nxVersion(), uicompStateCode(),
           properties() {}
        ~RunnerImplInput();
        stringl builderLibrary;
        stringl uiclassLibrary;
        stringl license;
        stringl componentNS;
        stringl component;
        stringl componentSuperclass;
        stringl uicompNS;
        stringl uicompBasename;
        stringl persistentDataJASuperclassNS;
        stringl builderSuperclass;
        stringl persistentDataJAClass;
        stringl persistentDataRecordClass;
        stringl persistentDataJASuperclass;
        stringl omComponentNS;
        stringl omComponentBasename;
        stringl topUIComponent;
        stringl nxVersion;
        stringl uicompStateCode;

        struct Property
        {
            stringl type;
            std::vector<stringl> typeParameters;
            stringl name;
            stringl defaultValue;
            stringl defaultValueInchesPart;
            typedef std::map< stringl, SpecialFunction > SpecialFunctionMap;
            SpecialFunctionMap specialFunctions;
        };
        typedef std::vector<Property> Properties;
        Properties properties;
    private:
        RunnerImplInput(const RunnerImplInput&);
        RunnerImplInput& operator=(const RunnerImplInput&);
    };

    RunnerImplInput::~RunnerImplInput()
    {
        for ( Properties::iterator iter = properties.begin(); iter != properties.end(); ++iter )
        {
            for ( Property::SpecialFunctionMap::iterator iter2 = iter->specialFunctions.begin(); iter2 != iter->specialFunctions.end(); ++iter2 )
            {
                iter2->second.FreeData(&iter2->second);
                iter2->second.data = NULL;
            }
        }
    }

    class RunnerImpl : private RunnerImplInput
    {
    public:
        RunnerImpl( const Input& input, const char* root_dir_path_ );
        void Run();
        void GenerateOutput(const char* out_dir) const;
        const std::vector<string>& GetWarnings() const { return m_warnings; }
        typedef Runner::InputException InputException;

    private:
        void AddWarning(const string&);
        void ValidateInput();
        template <class Iter>
        DOMNamedNodeMap* NewAttributeNodeMap(Iter begin, Iter end);
        DOMDocument *m_doc;
        string root_dir_path;
        std::vector<string> m_warnings;
        RunnerImpl(); // for qaz
    };  //lint !e1509

    inline const char* find(const std::map<string, string>& m, const char* s)
    {
        std::map<string, string>::const_iterator iter = m.find(s);
        if ( iter != m.end() )
            return iter->second.c_str();
        return "";
    }

    RunnerImpl::RunnerImpl( const Input& input, const char* root_dir_path_ ) :
        RunnerImplInput(), m_doc(NULL), root_dir_path(root_dir_path_), m_warnings()
    {
        static bool firstTime = true;
        if ( firstTime )
        {
            XMLPlatformUtils::Initialize();
            firstTime = false;
        }
        const char* lib = find( input.dataMap, "builderLib");
        if ( strncmp( lib, "lib", 3 ) != 0 )
            throw InputException("builderLib", "The builder library must begin with 'lib'");
        builderLibrary = Transcode(lib + 3);
        lib = find( input.dataMap, "uiclassLib");
        if ( strncmp( lib, "lib", 3 ) != 0 )
            throw InputException("uiclassLib", "The UI component library must begin with 'lib'");
        uiclassLibrary = Transcode(lib+3);
        license = Transcode(find( input.dataMap, "license"));
        componentNS = Transcode(find( input.dataMap, "componentNS"));
        component = Transcode(find( input.dataMap, "component"));
        componentSuperclass = Transcode(find( input.dataMap, "componentSuperclass"));
        string uicomp = find( input.dataMap, "uicomp");
        size_t idx = uicomp.rfind("::");
        if ( idx == string::npos )
        {
            uicompNS = X("");
            uicompBasename = Transcode(uicomp.c_str());
        }
        else
        {
            uicompNS = Transcode(uicomp.substr(0, idx).c_str());
            uicompBasename = Transcode(uicomp.substr(idx+2).c_str());
        }
        persistentDataJASuperclassNS = Transcode(find( input.dataMap, "persistentDataJASuperclassNS"));
        builderSuperclass = Transcode(find( input.dataMap, "builderSuperclass"));
        persistentDataJAClass = Transcode(find( input.dataMap, "persistentDataJAClass"));
        persistentDataRecordClass = Transcode(find( input.dataMap, "persistentDataRecordClass"));
        persistentDataJASuperclass = Transcode(find( input.dataMap, "persistentDataJASuperclass"));
        string omComponent = find( input.dataMap, "omComponent");
        idx = omComponent.rfind("::");
        if ( idx == string::npos )
        {
            omComponentNS = X("");
            omComponentBasename = Transcode(omComponent.c_str());
        }
        else
        {
            omComponentNS = Transcode(omComponent.substr(0, idx).c_str());
            omComponentBasename = Transcode(omComponent.substr(idx+2).c_str());
        }
        topUIComponent = Transcode(find( input.dataMap, "uicomp"));
        properties.resize(input.properties.size());
        RunnerImplInput::Properties::iterator iter2 = properties.begin();
        for ( Input::Properties::const_iterator iter = input.properties.begin(); iter != input.properties.end(); 
            ++iter, ++iter2 )
        {
            iter2->name = Transcode(iter->name.c_str());
            iter2->type = Transcode(iter->type.c_str());
            iter2->defaultValue = Transcode(iter->defaultValue.c_str());
            iter2->defaultValueInchesPart = Transcode(iter->defaultValueInchesPart.c_str());
            iter2->typeParameters.resize(iter->typeParameters.size());
            for ( size_t ii=0; ii<iter->typeParameters.size(); ++ii )
            {
                iter2->typeParameters[ii] = Transcode(iter->typeParameters[ii].c_str());
            }
            for ( Input::Property::SpecialFunctionMap::const_iterator iter3 = iter->specialFunctions.begin();
                iter3 != iter->specialFunctions.end(); ++iter3 )
            {
                iter2->specialFunctions[Transcode(iter3->first)] = iter3->second;
            }
        }
        nxVersion = Transcode(find( input.dataMap, "nxVersion"));
        uicompStateCode = Transcode(find( input.dataMap, "uicompStateCode"));
    }

    Runner::Runner( const Input& input, const char* root_dir_path_ ) : m_impl(NULL)
    {
        m_impl = new RunnerImpl(input, root_dir_path_);
    }

    void Runner::Run()
    {
        m_impl->Run();
    }

    void Runner::GenerateOutput(const char* out_dir) const
    {
        m_impl->GenerateOutput(out_dir);
    }

    const std::vector<string>& Runner::GetWarnings() const
    {
        return m_impl->GetWarnings();
    }

    Runner::~Runner()
    {
        delete m_impl;
    }

    stringl MakeFQName( const stringl& x, const stringl& y, const stringl& joiner )
    {
        if ( x == X("") )
            return y;
        return x + joiner + y;
    }
    stringl MakeFQName( const stringl& x, const stringl& y, XMLCh joiner )
    {
        if ( x == X("") )
            return y;
        return x + joiner + y;
    }

    void AddAttribute( DOMDocument* ownerDocument, DOMNamedNodeMap* attributes, const XMLCh* name, const XMLCh* value )
    {
        DOMAttr *attr = ownerDocument->createAttribute(name);
        attr->setValue(value);
        attributes->setNamedItem(attr);
    }
    void AddAttribute( DOMNamedNodeMap* attributes, const XMLCh* name, const XMLCh* value )
    {
        if ( attributes->getLength() == 0 )
            throw std::runtime_error("attributes.length must be > 0 for this function");
        AddAttribute( attributes->item(0)->getOwnerDocument(), attributes, name, value );
    }

    stringl GetYear()
    {
        time_t t = time(NULL);
        ostringstreamXMLCh sout;
        sout << (localtime( &t )->tm_year + 1900);
        return sout.str();
    }

    void RunnerImpl::AddWarning(const string& warning)
    {
        for ( size_t ii=0; ii<m_warnings.size(); ++ii )
        {
            if ( m_warnings[ii] == warning )
                return;
        }
        m_warnings.push_back(warning);
    }

    void RunnerImpl::ValidateInput()
    {
        std::ostringstream sout;
        if ( !RegularExpression(X("^[a-z_]+$")).matches(builderLibrary.c_str()) )
            throw InputException("builderLib", "The name of the library for the builder must be an identifier that start with 'lib'");
        if ( !RegularExpression(X("^[a-z_]+$")).matches(uiclassLibrary.c_str()) )
            throw InputException("uiclassLib", "The name of the UI library must be an identifier that start with 'lib'");
        if ( !RegularExpression(X("^[A-Z][a-zA-Z0-9]*$")).matches(component.c_str()) )
            throw InputException("component", "The name of the builder must be an identifier that starts with an upper case letter and contains only letters and numbers");
        if ( license != X("") && license != X("???") && 
                !RegularExpression(X("^[A-Za-z0-9_]+(\\s*\\|\\s*[A-Za-z0-9_]+)*$")).matches(license.c_str()) )
            throw InputException("license", "The license does not have the expected format");
        for ( size_t ii=0; ii<properties.size(); ++ii )
        {
            if ( !RegularExpression(X("^[a-z][a-zA-Z0-9]*$")).matches(properties[ii].name.c_str()) )
            {
                sout << "The API Name must be an identifier that starts with a lower case letter";
                throw InputException(ii, sout.str().c_str());
            }
            if ( !RegularExpression(X("^[A-Z]\\w*$")).matches(properties[ii].type.c_str()) )
            {
                sout << "The block type must be an identifier that starts with a lower case letter.\n"
                    << "The block type is \"" << Transcode(properties[ii].type) << "\"";
                throw InputException(ii, sout.str().c_str());
            }
            size_t numTypeParametersReqd = 0;
            if ( properties[ii].type == X("Enumeration") || 
                 properties[ii].type == X("SetList") ||
                 properties[ii].type == X("SetListWithDims") ||
                 properties[ii].type == X("Expression") ||
                 RegularExpression(X("^SelectObject")).matches(properties[ii].type.c_str()) ||
                 RegularExpression(X("^Selection")).matches(properties[ii].type.c_str()) )
            {
                numTypeParametersReqd = 1;
            }
            if ( numTypeParametersReqd != properties[ii].typeParameters.size() )
            {
                sout << "The type " << Transcode(properties[ii].type.c_str()) << 
                    " requires " << numTypeParametersReqd << " parameters";
                throw InputException(ii, sout.str().c_str());
            }
            if ( properties[ii].typeParameters.size() == 1 )
            {
                const stringl& typeParameter = properties[ii].typeParameters[0];
                if ( properties[ii].type == X("Expression") )
                {
                    ;
                }
                else if ( properties[ii].type == X("Enumeration") )
                {
                    if ( !RegularExpression(X("^[A-Z][a-zA-Z0-9]*$")).matches(typeParameter.c_str()) )
                    {
                        sout << "The API Enumeration Type Name must be an identifier that starts with an upper case letter and contains only letters and numbers"
                            << "The API Enumeration Type Name is \"" << Transcode(typeParameter) << "\"";
                        throw InputException(ii, sout.str().c_str());
                    }
                    if ( ToUpperCase(typeParameter.substr(0,1)) == ToUpperCase(properties[ii].name.substr(0,1)) &&
                        typeParameter.substr(1) == properties[ii].name.substr(1) )
                    {
                        sout << "The enumeration type must have a different name than the enumeration property\n"
                            << "The API Enumeration Type Name is \"" << Transcode(typeParameter) << "\"";
                        throw InputException(ii, sout.str().c_str());
                    }
                }
                else  // a SelectObject or SetList or SetListWithDims block
                {
                    const char *propertyName = ((properties[ii].type == X("SetList") || properties[ii].type == X("SetListWithDims")) ? "API List Element Type" : "API Selection Type");
                    if ( !RegularExpression(X("^[A-Z][a-zA-Z0-9]*(\\.[A-Z][a-zA-Z0-9]*)*$")).matches(typeParameter.c_str()) )
                    {
                        sout << "The " << propertyName << " must be an identifier that starts with an upper case letter and contains only letters and numbers\n"
                            << "The " << propertyName << " is \"" << Transcode(typeParameter) << "\"";
                        throw InputException(ii, sout.str().c_str());
                    }
                    if ( RegularExpression(X("^nxopen\\.")).matches(ToLowerCase(typeParameter).c_str()) )
                    {
                        sout << "The " << propertyName << " should not start with 'NXOpen.'.\n"
                            << "Leave out the NXOpen when giving the name of the journamation class.\n"
                            << "The " << propertyName << " is \"" << Transcode(typeParameter) << "\"";;
                        throw InputException(ii, sout.str().c_str());
                    }
                }
            }
        } // end for all properties
        // TODO: In UI, make sure result class is "" iff codetype == None
        if ( persistentDataRecordClass != X("") )
        {
            if ( !RegularExpression(X("^[A-Z][a-zA-Z0-9]*$")).matches(persistentDataJAClass.c_str()) )
            {
                if ( RegularExpression(X("[:.]")).matches(persistentDataJAClass.c_str()) )
                    throw InputException("persistentDataJAClass", "The journamation class name should not include the namespace.  Put the namespace in the input field for the namespace");
                else
                    throw InputException("persistentDataJAClass", "The name of the journamation class for the result of the commit must be an identifier that starts with an upper case letter and contains only letters and numbers");
            }
            if ( !RegularExpression(X("^[A-Z][a-zA-Z0-9]*$")).matches(persistentDataJASuperclass.c_str()) )
            {
                if ( RegularExpression(X("[:.]")).matches(persistentDataJASuperclass.c_str()) )
                    throw InputException("persistentDataJASuperclass", "The journamation superclass name should not include the namespace.  Put the namespace in the input field for the namespace");
                else
                    throw InputException("persistentDataJASuperclass", "The name of the journamation superclass for the result of the commit must be an identifier that starts with an upper case letter and contains only letters and numbers");
            }
            // TODO: What if the NS for the superclass isn't the NS for the class
            if ( persistentDataJASuperclassNS != X("") )
            {
                if ( !RegularExpression(X("^[A-Z][a-zA-Z0-9.]*$")).matches(persistentDataJASuperclassNS.c_str()) )
                    throw InputException("persistentDataJASuperclassNS", "The name of the journamation superclass namespace for the result of the commit must be an identifier that starts with an upper case letter and contains only letters and numbers");
                if ( RegularExpression(X("^NXOPEN")).matches(ToUpperCase(persistentDataJASuperclassNS).c_str()) )
                    throw InputException("persistentDataJASuperclassNS", "Do not include NXOpen when specifying the JA superclass namespace.  Leave out the NXOpen.");
            }
            if ( !RegularExpression(X("^[A-Za-z]\\w*(::[A-Za-z]\\w*)*$")).matches(persistentDataRecordClass.c_str()) )
                throw InputException("persistentDataRecordClass", "The name of the OM class for the result of the commit must be an identifier that starts with a capital letter");
        }
        else
        {
            if ( persistentDataJAClass != X("") || persistentDataJASuperclass != X("") ||
                persistentDataJASuperclassNS != X("") )
                throw InputException("persistentDataRecordClass", "If you have a result JA class, or result JA superclass, you must have a result OM class");
        }
        if ( componentNS != X("") && 
            !RegularExpression(X("^[A-Z][a-zA-Z0-9.]*$")).matches(componentNS.c_str()) )
            throw InputException("componentNS", "The name of the namespace must be an identifier that starts with an upper case letter and contains only letters and numbers");
        if ( componentSuperclass == X("FeatureBuilder") && 
            (componentNS != X("Features") && !RegularExpression(X("^Features\\.")).matches(componentNS.c_str()) ))
            throw InputException("componentNS", "The JA namespace for a feature must be Features or must be a namespace inside the Features namespace");
        if ( ToLowerCase(componentNS) == X("nxopen") )
            throw InputException("componentNS", "NXOpen is not a valid namespace.  Leave the namespace field blank if you want to use the top-level automation namespace");
        // TODO: What if componentSuperclass is in a diff namespace than component
        if ( !RegularExpression(X("^[A-Z][a-zA-Z0-9]*$")).matches(componentSuperclass.c_str()) &&
             componentSuperclass != X("GeometricUtilities.IComponentBuilder") )
            throw InputException("componentSuperclass", "The name of the superclass must be an identifier that starts with an upper case letter and contains only letters and numbers");
        if ( (omComponentNS != X("") && !RegularExpression(X("^[A-Za-z]\\w*(::[A-Za-z]\\w*)*$")).matches(omComponentNS.c_str())) ||
            !RegularExpression(X("^[A-Za-z]\\w*$")).matches(omComponentBasename.c_str()) )
            throw InputException("omComponent", "The name of the OM builder must be an identifier that starts with a letter");
        if ( !RegularExpression(X("^[A-Za-z]\\w*(::[A-Za-z]\\w*)*$")).matches(builderSuperclass.c_str()) )
            throw InputException("builderSuperclass", "The name of the OM builder superclass must be an identifier that starts with a letter");
        if ( !RegularExpression(X("^[A-Za-z]\\w*$")).matches(uicompBasename.c_str()) ||
             ( uicompNS != X("") && !RegularExpression(X("^[A-Za-z]\\w*(::[A-Za-z]\\w*)*$")).matches(uicompNS.c_str()) ))
            throw InputException("uicomp", "The name of the UI component class must be an identifier that starts with a letter");
    }

    namespace {
        struct MyPair { stringl first; stringl second; };
    }


    void RunnerImpl::Run( )
    {
        ValidateInput();
        stringl feature;
        string template_file_name;
        if ( componentSuperclass == X("FeatureBuilder") )
        {
            if ( !EndsWith(component, X("Builder")) )
                throw InputException("component", "component must end with 'Builder'");
            feature = component.substr( 0, component.size() - 7 );
            template_file_name = "feature_builder_template.xml";
        }
        else if ( componentSuperclass == X("GeometricUtilities.IComponentBuilder") )
        {
            componentSuperclass = X("Component");
            builderSuperclass = X("JAX_BASE_BUILDER");
            template_file_name = "component_builder_template.xml";
            topUIComponent = X("");
        }
        else
        {
            template_file_name = persistentDataJAClass == X("") ? "nothing_builder_template.xml" : "nonfeature_builder_template.xml";
        }
        stringl fqComponent = MakeFQName(componentNS, component, '.');
        stringl fqPersistentDataJAClass = MakeFQName(componentNS, persistentDataJAClass, '.');
        stringl componentNSReplaceDotWithUnderscore = componentNS;
        Replace( componentNSReplaceDotWithUnderscore, XMLCh('.'), XMLCh('_') );
        stringl componentJAFileName = MakeFQName(componentNSReplaceDotWithUnderscore, component, '_');
        stringl componentJAHeaderFileName = stringl(X("ja_")) + MakeFQName( 
            ConvertToLowerCaseWithUnderscores(componentNSReplaceDotWithUnderscore),
            ConvertToLowerCaseWithUnderscores(component), '_' );
        stringl componentJAXFileName = stringl(X("jax_")) + MakeFQName( 
            ConvertToLowerCaseWithUnderscores(componentNSReplaceDotWithUnderscore),
            ConvertToLowerCaseWithUnderscores(component), '_' );
        stringl persistentDataJAClassFileName = MakeFQName( componentNSReplaceDotWithUnderscore, persistentDataJAClass, '_' );

        stringl fqPersistentDataJASuperclass = MakeFQName( persistentDataJASuperclassNS, persistentDataJASuperclass, '.');

        stringl omComponent = MakeFQName( omComponentNS, omComponentBasename, X("::") );
        stringl uicompName = MakeFQName( uicompNS, uicompBasename, X("::") );

        stringl jaNSDecl;
        stringl jaEndNSDecl;
        if ( componentNS != X("") )
        {
            std::list<stringl> nsTree;
            Split( componentNS, XMLCh('.'), nsTree );
            stringl indent;
            for ( std::list<stringl>::const_iterator iter = nsTree.begin();
                iter != nsTree.end(); ++iter )
            {
                jaNSDecl += indent + X("namespace ") + *iter + X("\n") + indent + X("{\n");
                jaEndNSDecl = indent + X("}\n") + jaEndNSDecl;
                indent += X("    ");
            }
        }

        if ( license == X("") )
            license = X("no_license");
        else
            license = stringl(X("license(")) + license + X(")");

        string template_file = SearchForFileUsingPath( template_file_name, root_dir_path );
        if ( template_file == "" )
            throw std::runtime_error( (template_file_name + " does not exist").c_str() );

        MyPair stdAttrs[] = {
            { X("Feature"), feature },
            { X("feature"), ConvertToLowerCaseWithUnderscores(feature)},
            { X("FEATURE"), ConvertToUpperCaseWithUnderscores(feature)},
            { X("BUILDERLIB"), ToUpperCase(builderLibrary)},
            { X("UICLASSLIB"), ToUpperCase(uiclassLibrary)},
            { X("Component"), component},
            { X("component"), ConvertToLowerCaseWithUnderscores(component)},
            { X("COMPONENT"), ConvertToUpperCaseWithUnderscores(component)},
            { X("FQComponent"), fqComponent },
            { X("ComponentJAFileName"), componentJAFileName},
            { X("ComponentJAHeaderFileName"), componentJAHeaderFileName},
            { X("ComponentJAXFileName"), componentJAXFileName},
            { X("ComponentSuperclass"), componentSuperclass},
            { X("TopUIComponent"), topUIComponent},
            { X("BuilderSuperclass"), builderSuperclass},
            { X("PersistentDataJAClass"), persistentDataJAClass},
            { X("FQPersistentDataJAClass"), fqPersistentDataJAClass},
            { X("PersistentDataJAClassFileName"), persistentDataJAClassFileName},
            { X("PersistentDataRecordClass"), persistentDataRecordClass},
            { X("PersistentDataJASuperclass"), persistentDataJASuperclass},
            { X("FQPersistentDataJASuperclass"), fqPersistentDataJASuperclass},
            { X("OMComponent"), omComponent},
            { X("OMComponentBasename"), omComponentBasename},
            { X("om_component"), ConvertToLowerCaseWithUnderscores(omComponent)},
            { X("OM_COMPONENT"), ConvertToUpperCaseWithUnderscores(omComponent)},
            { X("UIComp"), uicompName},
            { X("UICompBasename"), uicompBasename},
            { X("ComponentNSDecl"), jaNSDecl},
            { X("ComponentEndNSDecl"), jaEndNSDecl},
            { X("year"), GetYear() },
            { X("NXVersion"), nxVersion},
            { X("license"), license},
            { X("UICompStateCode"), uicompStateCode },
            { X("__UNDEF"), X("^(propname|PropName|prop_name|propnumber|default_value|default_value_inch_part|arg\\d+)$")}
        };
        DOMNamedNodeMap* standardAttributes = NewAttributeNodeMap( stdAttrs, stdAttrs + sizeof(stdAttrs)/sizeof(MyPair) );
        if ( componentNS != X("") )
            AddAttribute( standardAttributes, X("ComponentNamespace"), componentNS.c_str() );
        if ( omComponentNS != X("") )
           AddAttribute( standardAttributes, X("OMComponentNamespace"), omComponentNS.c_str() );
        if ( uicompNS != X("") )
           AddAttribute( standardAttributes, X("UICompNamespace"), uicompNS.c_str());

        std::map<stringl, PropertyType*> templates;
        std::vector<string> root_dirs = Path::SplitPath( root_dir_path );
        for ( size_t kk=0; kk<root_dirs.size(); ++kk )
        {
            string template_dir = root_dirs[kk]; //Path::Join( root_dirs[kk], "Properties" );
            FileOps::DirHandle* dirHandle = FileOps::Opendir(template_dir.c_str(), "*.xml");
            if ( dirHandle == NULL )
                continue;
            for(;;)
            {
                string x = FileOps::GetNextFile(dirHandle);
                if ( x == "" )
                    break;
                string xName = Path::SplitExt( x )[0];
                if ( EndsWith(xName, "_template") || templates[ Transcode(xName) ] != NULL )
                    continue;
                //if ( Path::SplitExt(x)[1] != "xml" )
                //    continue;
                PropertyType* p = new PropertyType( Path::Join( template_dir, x ).c_str(), root_dir_path.c_str(), standardAttributes );
                templates[ p->Name() ] = p;
            }
            FileOps::Closedir( dirHandle );
        }

        // FIXME: Handle user_template_dir

        DOMDocument* doc = ImportXMLFile( template_file.c_str(), root_dir_path.c_str(), standardAttributes );

        DOMNodeList *propertiesList = doc->getElementsByTagName(X("properties"));
        while ( propertiesList->getLength() > 0 )
        //for ( DOMNodeListIterator iter = DOMNodeListIterator(*doc->getElementsByTagName(X("properties")));
        //    iter.hasNext(); ++iter )
        {
            DOMElement* aPropertySection = dynamic_cast<DOMElement*>(propertiesList->item(0)/* *iter */);
            stringl templateName = aPropertySection->getAttribute(X("template"));

            std::list<stringl> texts;
            for ( size_t propnumber = 0; propnumber < properties.size(); ++propnumber )
            {
                DOMAttr *specialFunctionNode = aPropertySection->getAttributeNode(X("special_function"));
                if ( specialFunctionNode != NULL )
                {
                    Property::SpecialFunctionMap::iterator it = properties[propnumber].specialFunctions.find(specialFunctionNode->getValue());
                    if ( it != properties[propnumber].specialFunctions.end() )
                    {
                        stringl propertyText = Transcode(it->second.Execute(&it->second, Transcode(aPropertySection->getAttribute(X("special_function_parameter"))).c_str()));
                        RemoveIndent(propertyText);
                        AddIndent(propertyText, aPropertySection->getAttribute(X("indent")));
                        texts.push_back(propertyText);
                    }
                    continue;
                }
                const stringl& proptype = properties[propnumber].type;
                const stringl& propname = properties[propnumber].name;
                const std::vector<stringl>& propargs = properties[propnumber].typeParameters;
                //stringl prop_name = ConvertToLowerCaseWithUnderscores(propname);
                stringl PropName = ToUpperCase(propname.substr(0,1)) + propname.substr(1);
                stringl prop_name = PropName;
                /*if not templates.has_key(proptype) and os.path.exists(os.path.join(work_dir, proptype + '.cdl')):
                    retcode = subprocess.call(['python', sys.argv[0], os.path.join(work_dir, proptype + '.cdl')])
                    if retcode != 0:
                        raise Exception("Processing " + proptype + ".cdl failed with error code " + str(retcode))
                    p = PropertyType(os.path.join(work_dir, proptype + '.xml'), template_dir, standardAttributes)
                    templates[p.Name()] = p */
                if ( templates.find( proptype ) != templates.end() )
                {
                    PropertyType* p = templates[proptype];
                    DOMElement *xmlFragment = p->GetConstruct(templateName);
                    if (xmlFragment)
                    {
                        DOMElement* xmlFragmentClone = &dynamic_cast<DOMElement&>(*xmlFragment->cloneNode(true));
                        MyPair propertyAttrArray[] = { 
                            {X("propname"), propname}, {X("PropName"), PropName}, {X("prop_name"), prop_name} };
                        DOMNamedNodeMap* propertyAttrs = NewAttributeNodeMap( propertyAttrArray, propertyAttrArray+3 );
                        for ( size_t ii=0; ii<propargs.size(); ++ii )
                        {
                            ostringstreamXMLCh sout;
                            sout << X("arg") << (ii+1);
                            AddAttribute( propertyAttrs, sout.str().c_str(), propargs[ii].c_str() );
                        }
                        if ( properties[propnumber].defaultValue != X("") )
                            AddAttribute( propertyAttrs, X("default_value"), properties[propnumber].defaultValue.c_str() );
                        if ( properties[propnumber].defaultValueInchesPart != X("") )
                            AddAttribute( propertyAttrs, X("default_value_inch_part"), properties[propnumber].defaultValueInchesPart.c_str() );
                        EvaluateDoc( xmlFragmentClone, propertyAttrs );
                        ReplaceNumeric(p->Doc(), xmlFragmentClone, X("propnumber"),  propnumber);
                        stringl propertyText = ExtractText(xmlFragmentClone);
                        RemoveIndent(propertyText);
                        AddIndent(propertyText, aPropertySection->getAttribute(X("indent")));
                        texts.push_back(propertyText);
                    }
                    else
                    {
                        if ( aPropertySection->getAttributeNode(X("optional")) == NULL || 
                             stringl(aPropertySection->getAttribute(X("optional"))) != X("true") )
                             throw std::runtime_error( (string("Construct ") + Transcode(templateName) + " not found for type " + Transcode(proptype)).c_str() );
                    }
                }
                else
                {
                    AddWarning( 
                        string("Skipping ") + Transcode(proptype) + ".  Unrecognized type.\n" 
                               "   Contact the maintainers of the " +  Transcode(proptype) + " block and tell them to add a code generator xml file for this block and/or\n"
                               "   make sure the AskBlockName is returning the correct name for this block.\n");
                }
            } // for all properties

            texts.erase(
                std::remove_if( texts.begin(), texts.end(), std::bind2nd( std::equal_to<stringl>(), stringl() ) ),
                texts.end()
                );

            stringl propertyText;
            Join( texts.begin(), texts.end(), stringl(aPropertySection->getAttribute(X("join"))), propertyText );
            if ( aPropertySection->getAttributeNode(X("remove_duplicates")) )
            {
                texts.clear();
                Split( propertyText, XMLCh('\n'), texts );
                std::transform( texts.begin(), texts.end(), texts.begin(), RemoveEndSpaces<XMLCh> );
                texts.sort();
                texts.unique();
                propertyText = X("");
                Join( texts.begin(), texts.end(), stringl(X("\n")), propertyText );
            }
            if ( propertyText == X("") && aPropertySection->getAttributeNode(X("if_empty_use")) )
                propertyText = aPropertySection->getAttribute(X("if_empty_use"));
            if ( propertyText != X("") && propertyText.find('\n') != stringl::npos )
                AddIndent(propertyText, GetBodyIndent(aPropertySection).c_str());
            DOMText* propTextNode = doc->createTextNode(propertyText.c_str());
            aPropertySection->getParentNode()->replaceChild(propTextNode, aPropertySection);
        }

        ReplaceNumeric( doc, doc->getDocumentElement(), X("propnumber"), properties.size() );

        this->m_doc = doc;
           
    }

    void RunnerImpl::GenerateOutput(const char* out_dir) const
    {
        for ( DOMNodeListIterator aFileTemplateIter = DOMNodeListIterator(*m_doc->getElementsByTagName(X("fileTemplate")));
            aFileTemplateIter.hasNext(); ++aFileTemplateIter )
        {
            DOMElement *aFileTemplate = static_cast<DOMElement*>(*aFileTemplateIter);
            DOMElement *filenameNode = static_cast<DOMElement*>(aFileTemplate->getElementsByTagName(X("filename"))->item(0));
            DOMElement *bodyNode = static_cast<DOMElement*>(aFileTemplate->getElementsByTagName(X("body"))->item(0));
            string fileName = Path::Join( out_dir, Transcode(ExtractText(filenameNode)));
            string fileNameOut = fileName;
            //if ( FileOps::FileExists(fileName.c_str()) )
            //    fileNameOut += "_tmp";
            std::ofstream fout( fileNameOut.c_str() );
            if ( !fout )
                throw std::runtime_error( (string("Error opening file ") + fileNameOut + " for write").c_str() );
            stringl fileText = ExtractText(bodyNode);
            RemoveIndent(fileText);
            if ( fileText != X("") && fileText[0] == '\n' )
                fileText.erase(0,1);
            fout << Transcode(fileText) << std::endl;
            if ( !fout )
                throw std::runtime_error((string("Error writing to file ") + fileNameOut).c_str());
            fout.close();
        }
            /*
            if fileName != fileNameOut:
                if filecmp.cmp(fileName, fileNameOut, False):
                    os.remove(fileNameOut)
                else:
                    os.remove(fileName)
                    os.rename(fileNameOut, fileName)
                    */
    }

    // Iter must be a forward iterator over std::pair<stringl, stringl>
    template <class Iter>
    DOMNamedNodeMap* RunnerImpl::NewAttributeNodeMap(Iter begin, Iter end)
    {
        DOMImplementation* domImpl = DOMImplementationRegistry::getDOMImplementation(NULL);
        DOMDocument* dummydoc = domImpl->createDocument(NULL, X("xx"), NULL);
        for ( Iter iter = begin; iter != end; ++iter )
        {
            AddAttribute( dummydoc, dummydoc->getDocumentElement()->getAttributes(), iter->first.c_str(), iter->second.c_str());
        }
        return dummydoc->getDocumentElement()->getAttributes();
    }

    ErrorData::ErrorData() : type(TypeUnknown), message(), inputName(), blockNumber(-1)
    {
    }
    void ErrorData::Clear()
    {
        type = TypeUnknown;
        message = inputName = "";
        blockNumber = -1;
    }


}
}

static UGS::CodeGenerator::ErrorData *lastErrorData = NULL;


extern bool CODE_GENERATOR_run( const UGS::CodeGenerator::Input *input, const char* rootDir, const char* outDir )
{
    if ( !lastErrorData )
        lastErrorData = new UGS::CodeGenerator::ErrorData();
    else
        lastErrorData->Clear();
    try
    {
        //const UGS::CodeGenerator::Input *input = static_cast<const UGS::CodeGenerator::Input*>(inputs);
        UGS::CodeGenerator::Runner runner( *input, rootDir );
        runner.Run();
        runner.GenerateOutput( outDir );
        if ( runner.GetWarnings().size() > 0 )
        {
            lastErrorData->type = UGS::CodeGenerator::ErrorData::TypeWarning;
            for ( size_t ii=0; ii<runner.GetWarnings().size(); ++ii )
                lastErrorData->message += runner.GetWarnings()[ii];
        }
        return runner.GetWarnings().size() == 0;
    }
    catch( const XMLException& ex )
    {
        lastErrorData->type = UGS::CodeGenerator::ErrorData::TypeXml;
        const XMLCh* message = ex.getMessage();
        lastErrorData->message = Transcode(message);
    }
    catch ( const DOMException &ex )
    {
        lastErrorData->type = UGS::CodeGenerator::ErrorData::TypeDom;
        const XMLCh* message = ex.getMessage();
        lastErrorData->message = Transcode(message);
    }
    catch ( const UGS::CodeGenerator::RunnerImpl::InputException& ex )
    {
        lastErrorData->type = UGS::CodeGenerator::ErrorData::TypeInput;
        lastErrorData->message = ex.message;
        lastErrorData->inputName = ex.input;
        lastErrorData->blockNumber = ex.blockNumber;
    }
    catch ( const std::exception &ex )
    {
        lastErrorData->type = UGS::CodeGenerator::ErrorData::TypeError;
        const char* message = ex.what();
        lastErrorData->message = message;
    }

    return false;
}
extern const UGS::CodeGenerator::ErrorData* CODE_GENERATOR_get_error_data( )
{
    return lastErrorData;
}

// Format a string according to one of the formats available for code generator xml
// tags.  The return value is a pointer to static memory, and will become invalid
// the next time this function is called.  Therefore, the result should be immediately 
// copied by the caller.
extern const char* CODE_GENERATOR_format_string( const char* str, const char* format )
{
    stringl formatL = Transcode( format );
    stringl resultL = Transcode( str );
    UGS::CodeGenerator::FormatString( resultL, formatL );
    static std::string result;
    result = Transcode( resultL );
    return result.c_str();
}
