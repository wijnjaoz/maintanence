@echo off
REM ===========================================================================
REM runDispatcherClient.bat
REM
REM Description:
REM
REM    This file will start the Dispatcher Client for Teamcenter application
REM    which connects to Dispatcher Server to translate datasets.
REM
REM ===========================================================================
set ARG1=%1
set TC_ROOT=D:\PLM\TC12
set TC_DATA=D:\PLM\PLMShare\tcdata12

call "D:\PLM\TC12\install\tem_init.bat"
set JAVA_HOME=%TC_JRE_HOME%
IF NOT DEFINED JAVA_HOME GOTO JAVA_HOME_NOT_DEFINED

for /f "tokens=*" %%a in ('cd') do set CURRDIR=%%a

cd /d %~dp0

set CLASSPATH=

REM
REM Set the Dispatcher Client related environment variables
REM
call setDispatcherClientEnv

set CLASSPATH=%CLASSPATH%;%FMS_HOME%\jar\fccclient.jar

set JAVAARG=%JAVAARG% -Djava.class.path="%CLASSPATH%"
set JAVAARG=%JAVAARG% -Dtc.bin=%TC_ROOT%\bin\tcserver.exe
set JAVAARG=%JAVAARG% -Dtc.env="%HOME%\bin\tsenv.bat"
set JAVA_LIB_PATH=%FMS_HOME%\lib

IF /I "%TC_USE_KEYMANAGER%"=="FALSE" (
goto continue_cmd
)
IF NOT DEFINED KEY_MANAGER_SATELLITE_HOME goto KEY_MANAGER_SATELLITE_HOME_NOT_DEFINED
IF NOT DEFINED TC_KEY_MANAGER_PIPE goto TC_KEY_MANAGER_PIPE_NOT_DEFINED
set JAVA_LIB_PATH=%JAVA_LIB_PATH%;%KEY_MANAGER_SATELLITE_HOME%\modules\Tctp


:continue_cmd
set JAVAARG=%JAVAARG% -Djava.library.path="%JAVA_LIB_PATH%"
set JAVAARG=%JAVAARG% -Xdebug -Xrunjdwp:transport=dt_socket,address=8998,server=y
if "%ARG1%" == "-getenv" goto end
if "%ARG1%" == "-translate" goto translate
if "%ARG1%" == "-history" goto history

rem DC_COMMAND required for internal use.
set DC_COMMAND=start "Dispatcher Client" "%JAVA_HOME%\bin\java" %JAVAARG% com.teamcenter.ets.ServiceMode %*
goto start

:translate
set DC_COMMAND="%JAVA_HOME%\bin\java" %JAVAARG% com.teamcenter.ets.translatormode.TranslatorMode %*
goto start

:history
set DC_COMMAND="%JAVA_HOME%\bin\java" %JAVAARG% com.teamcenter.ets.util.history.ParseHistoryLog %*

:start
%DC_COMMAND%
goto end

:JAVA_HOME_NOT_DEFINED
echo JAVA_HOME is not defined. Set your JAVA_HOME variable.
goto end

:KEY_MANAGER_SATELLITE_HOME_NOT_DEFINED
echo KEY_MANAGER_SATELLITE_HOME is not defined. Set your KEY_MANAGER_SATELLITE_HOME variable.
goto end

:TC_KEY_MANAGER_PIPE_NOT_DEFINED
echo TC_KEY_MANAGER_PIPE is not defined. Set your TC_KEY_MANAGER_PIPE variable.
goto end

:end
cd /d %CURRDIR%
set CLASSPATH=
set DC_COMMAND=
