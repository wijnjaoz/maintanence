@echo off
rem
rem This script covers all steps to import preferences into Teamcenter
rem - Generic site preferences
rem - Site specific site preferences
rem - group specific preferences
rem - I-deas site preferences
rem 
rem Filename : PMB_Import_Preferences.bat
rem
rem History: 
rem =============================================================================================================
rem Rev  Date           Author              Description
rem =============================================================================================================
rem 001  16-Jul-2010    Bart Latten       Initial creation
rem =============================================================================================================

rem NOTE: Set password here or conceal it by using an environment variable.
if not defined TC_USER_PASSWD set TC_USER_PASSWD="your password here"

rem Verify password was set.
if not %TC_USER_PASSWD%=="your password here" goto start
echo ERROR: You must first edit this script to insert the
echo        password for infodba or set TC_USER_PASSWD.
echo        (e.g. set TC_USER_PASSWD=password)
goto :eof

rem Start running the commands.
:start

REM Import General Site Preferences
%TC_ROOT%\bin\preferences_manager -u=infodba -p=%TC_USER_PASSWD% -g=dba -mode=import -scope=SITE -file=PMB_General_Site_Preferences.xml -action=OVERRIDE

REM Import Group (dba and Engineering) preferences
REM %TC_ROOT%\bin\preferences_manager -u=infodba -p=%TC_USER_PASSWD% -g=dba -mode=import -scope=GROUP -target=dba -file=PMB_Group_dba_preferences.xml -action=OVERRIDE
REM %TC_ROOT%\bin\preferences_manager -u=infodba -p=%TC_USER_PASSWD% -g=dba -mode=import -scope=GROUP -target=Engineering -file=PMB_Group_Engineering_preferences.xml -action=OVERRIDE

REM I-deas site preferences
REM %TC_ROOT%\bin\preferences_manager -u=infodba -p=%TC_USER_PASSWD% -g=dba -mode=import -scope=SITE -file=PMB_I-deas_Site_Preferences.xml -action=OVERRIDE

REM pause
