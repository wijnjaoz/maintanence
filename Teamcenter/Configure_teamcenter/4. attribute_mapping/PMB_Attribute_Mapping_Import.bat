@echo off
rem
rem This script covers all steps to extend the TcEng datamodel:
rem - Configure Attribute Mapping for NX I-deas
rem
rem Filename : KVG_Import_Attribute_Mapping.txt
rem
rem History: 
rem =============================================================================================================
rem Rev  Date           Author              Description
rem =============================================================================================================
rem 001  18-Feb-2009    Jouke Viersen       Initial creation
rem =============================================================================================================

rem NOTE: Set password here or conceal it by using an environment variable.
if not defined TC_USER_PASSWD set TC_USER_PASSWD="your password here"

rem Verify password was set.
if not %TC_USER_PASSWD%=="your password here" goto start
echo.
echo ERROR: You must first edit this script to insert the
echo        password for infodba or set TC_USER_PASSWD.
echo        (e.g. set TC_USER_PASSWD=password)
goto :eof

rem Start running the commands.
:start

REM Export existing attribute mapping
%TC_ROOT%\bin\export_attr_mappings -u=infodba -p=%TC_USER_PASSWD% -g=dba -file=PMB_Attribute_Mapping_old.txt

REM Import attribute mapping
%TC_ROOT%\bin\import_attr_mappings -u=infodba -p=%TC_USER_PASSWD% -g=dba -file=PMB_Attribute_Mapping.txt

REM export new attribute mapping
%TC_ROOT%\bin\export_attr_mappings -u=infodba -p=%TC_USER_PASSWD% -g=dba -file=PMB_Attribute_Mapping_new.txt

REM pause
