@echo off
rem
rem This script covers all steps to create the base organization model
rem - Groups
rem - Roles
rem - Roles in Groups
rem - Mandatory users
rem
rem Filename : KVG_Create_Organization.bat
rem
rem History: 
rem =============================================================================================================
rem Rev  Date           Author              Description
rem =============================================================================================================
rem 001  20-Jul-2010    Bart Latten       Initial creation
rem =============================================================================================================

rem NOTE: Set password here or conceal it by using an environment variable.
if not defined TC_USER_PASSWD set TC_USER_PASSWD="your password here"

rem Verify password was set.
if not %TC_USER_PASSWD%=="your password here" goto start
echo ERROR: You must first edit this script to insert the
echo        password for infodba or set TC_USER_PASSWD.
echo        (e.g. set TC_USER_PASSWD=password)
goto :eof

rem Start running the commands.
:start

REM Create groups
REM make_user -u=infodba -p=%TC_USER_PASSWD% -g=dba -group="MT"

REM Create roles and assign to groups
REM make_user -u=infodba -p=%TC_USER_PASSWD% -g=dba -group="Engineering" -role="Approver"
REM make_user -u=infodba -p=%TC_USER_PASSWD% -g=dba -group="Engineering" -role="Project Administrator"
REM make_user -u=infodba -p=%TC_USER_PASSWD% -g=dba -group="Engineering" -role="Project Team Administrator"
REM make_user -u=infodba -p=%TC_USER_PASSWD% -g=dba -group="Engineering" -role="Viewer"
REM make_user -u=infodba -p=%TC_USER_PASSWD% -g=dba -group="MT" -role="Approver"
REM make_user -u=infodba -p=%TC_USER_PASSWD% -g=dba -group="MT" -role="MT"

REM Create mandatory users
make_user.exe -u=%DBAUSER% -p=%DBAUSER% -g=dba -file=users.txt

REM Assign Role(s) for mandatory users
REM make_user -u=infodba -p=%TC_USER_PASSWD% -g=dba -user="ielock" -group="Standardization" -role="Designer"

REM pause
