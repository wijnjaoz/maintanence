@echo off
rem
rem This script covers all steps to extend the TcEng datamodel:
rem - Create XML REndering StyleSheets
rem - Configure Stylesheets (preferences)
rem
rem Filename : PMB_Create_XMLRenderingStyleSheets.txt
rem
rem History: 
rem =============================================================================================================
rem Rev  Date           Author              Description
rem =============================================================================================================
rem 001  16-Jul-2010    Bart Latten       Initial creation
rem =============================================================================================================

rem NOTE: Set password here or conceal it by using an environment variable.
if not defined TC_USER_PASSWD set TC_USER_PASSWD="your password here"

rem Verify password was set.
if not %TC_USER_PASSWD%=="your password here" goto start
echo ERROR: You must first edit this script to insert the
echo        password for infodba or set TC_USER_PASSWD.
echo        (e.g. set TC_USER_PASSWD=password)
goto :eof

rem Start running the commands.
:start

REM Creation of XMLStyleSheet Datasets and import Named Referenced XML files
%TC_ROOT%\bin\import_file -u=infodba -p=%TC_USER_PASSWD% -g=dba -type=XMLRenderingStylesheet -d="pmbPart Revision Master" -ref=XMLRendering -f=pmbPartRevisionMaster.xml -de=r
%TC_ROOT%\bin\import_file -u=infodba -p=%TC_USER_PASSWD% -g=dba -type=XMLRenderingStylesheet -d="pmbStandardPart Revision Master" -ref=XMLRendering -f=pmbStandardPartRevisionMaster.xml -de=r
%TC_ROOT%\bin\import_file -u=infodba -p=%TC_USER_PASSWD% -g=dba -type=XMLRenderingStylesheet -d="pmbVendorComp Revision Master" -ref=XMLRendering -f=pmbVendorCompRevisionMaster.xml -de=r
%TC_ROOT%\bin\import_file -u=infodba -p=%TC_USER_PASSWD% -g=dba -type=XMLRenderingStylesheet -d="pmbDrawing Revision Master" -ref=XMLRendering -f=pmbDrawingRevisionMaster.xml -de=r
%TC_ROOT%\bin\import_file -u=infodba -p=%TC_USER_PASSWD% -g=dba -type=XMLRenderingStylesheet -d="pmbStandAloneDrw Revision Master" -ref=XMLRendering -f=pmbStandAloneDrwRevisionMaster.xml -de=r


REM Import Preferences for XMLStyleSheet Rendering
%TC_ROOT%\bin\preferences_manager -u=infodba -p=%TC_USER_PASSWD% -g=dba -mode=import -scope=SITE -file=pmb_XMLRenderingStyleSheets_Site_Preferences.xml -action=OVERRIDE

REM pause
