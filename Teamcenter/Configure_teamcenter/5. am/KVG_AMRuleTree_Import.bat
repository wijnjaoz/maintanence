@echo off
rem
rem This script imports the KVG Access Manager Rule Tree
rem
rem Filename : KVG_AMRuleTree_Import.bat
rem
rem History: 
rem =============================================================================================================
rem Rev  Date           Author              Description
rem =============================================================================================================
rem 001  20-Feb-2009    Jouke Viersen       Initial creation
rem =============================================================================================================

rem NOTE: Set password here or conceal it by using an environment variable.
if not defined TC_USER_PASSWD set TC_USER_PASSWD="your password here"

rem Verify password was set.
if not %TC_USER_PASSWD%=="your password here" goto start
echo ERROR: You must first edit this script to insert the
echo        password for infodba or set TC_USER_PASSWD.
echo        (e.g. set TC_USER_PASSWD=password)
goto :eof

rem Start running the commands.
:start


REM Import the AM Rule Tree
%TC_BIN%\am_install_tree -u=infodba -p=%TC_USER_PASSWD% -g=dba -path=.\KVG_AMRuleTree.txt -mode=replace_all



REM pause
